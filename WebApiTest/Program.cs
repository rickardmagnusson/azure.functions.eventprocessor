﻿using Simple.OData.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace WebApiTest
{
    class Program
    {
        static void Main(string[] args)
        {
            var baseUri = "https://crmeikaftv8.api.crm4.dynamics.com/api/data/v8.2/";
            var settings = new ODataClientSettings(new Uri(baseUri));
            var token = Crm.Services.Extensions.TokenHelper.GetToken();

            settings.BeforeRequest = (message) =>
            {
                message.Headers.TryAddWithoutValidation("Authorization", "bearer " + token);
            };

            var client = new ODataClient(new ODataClientSettings(new Uri(baseUri))
            {
                IgnoreResourceNotFoundException = false,
                OnTrace = (x, y) => Console.WriteLine(string.Format(x, y)),
                Credentials = settings.Credentials
            });

            Console.WriteLine($"Connection: {(client.Equals(null) == false)}");

            var annotations = new ODataFeedAnnotations();

            Task.Run(async () =>
            {
                // var x = new ODataFeedAnnotations();
                var contact = await client
                .For("Contacts")
                .Filter("length(FirstName)+eq+4")
                .FindEntriesAsync();

            });

            Console.ReadLine();

        }

        public class Contact
        {

        }
    }
}
