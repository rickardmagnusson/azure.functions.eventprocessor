﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common;
using Common.Adapters;
using Common.Entities;
using Common.Enums;
using Common.Helpers;
using Common.Logging;
using Common.RequestBuilders;
using Microsoft.Xrm.Sdk;
using Azure.Queues.Process.ProcessEntityHelper;
using System.Text;
using Azure.Logging;

namespace Azure.Queues.Process.ProcessHelpers
{
    public class ProcessHelper
    {
        //private static readonly MsmqLogger Log = MsmqLogger.CreateLogger("process-helper");
        private static SingletonLogger Log = SingletonLogger.Instance();

        public static bool ValidateCustomerEssentials(KundeRegisterEvent evt, ProcessedCustomerResponse response, MsmqLogger Log)
        {
            if (!string.IsNullOrEmpty(evt.CustomerSourceId) && !string.IsNullOrEmpty(evt.DistributorBankNumber) &&
                !string.IsNullOrEmpty(evt.CustomerNumber)) return true;

            LogHelper.Msg($"{nameof(ValidateCustomerEssentials)} failed. The event is missing either sourceid, customer number or bank number.", response, LogLevel.Error, Log, true);
            return false;
        }
        public static bool ValidateBank(Guid bankId, string bankNumber, ProcessedCustomerResponse response, MsmqLogger Log)
        {
            if (bankId != Guid.Empty) return true;

            var errorMsg = $"The BankNumber '{bankNumber}' is not valid (not found). The change is ignored.";
            Log.Warn(errorMsg);
            response.ErrorMessage = errorMsg;
            response.Success = false;
            return false;
        }
        public static bool ValidateTimestamp(KundeRegisterEvent evt, CrmCustomer crmCustomerCard, CustomerExtra cust, MsmqLogger Log)
        {
            if (crmCustomerCard != null)
            {
                //cust.CustomerId = crmCustomerCard.Id;
                Log.Debug($"crm date: {crmCustomerCard.CapEventdate} __ vs __ evt date: {evt.CustomerEventSent}");

                if (crmCustomerCard.CapEventdate > evt.CustomerEventSent)
                {
                    var errorMsg = $"A newer event has already been run. Skipping this change. Last datestamp on this customer: {crmCustomerCard.CapEventdate}";
                    Log.Warn(errorMsg);
                    return false;
                }
            }

            return true;
        }

        public static bool HandleDeleteAndUnshareRelatedRecords(KundeRegisterEvent evt, CrmCustomer crmCustomerCard, bool productCompanyCustomerCard, ProcessedCustomerResponse response, QueryCrm query)
        {
            var source = evt.Source.ToString().ToLower();
            var isPersonCompanyCard = productCompanyCustomerCard && evt.Source == Source.NicePerson;

            var subjectId = query.GetSubjectIdByTitle(Helper.MapSubject(source));
            var vendorBankId = query.GetBankIdByBankNo(Helper.GetVendorBankNumber(source));

            if (!IncidentManager.DeleteRelatedIncidents(crmCustomerCard.Id, subjectId, vendorBankId, isPersonCompanyCard, evt.DistributorBankNumber, response, query)) return false;
            
            if (!productCompanyCustomerCard)
            {
                if (!OpportunityManager.HandleUnsharingOfRelatedSalesOpportunities(crmCustomerCard.Id, subjectId, vendorBankId, evt.DistributorBankNumber, response, query)) return false;
            }

            return true;
        }

        public static CrmCustomer GetCustomerCard(KundeHendelse evt, CustomerExtra cust, ProcessedCustomerResponse response, QueryExpressionHelper query)
        {
            var crmCustomersBySourceId = query.GetDetailedCrmCustomerBySourceId(evt.FagsystemKundenummer, cust, evt.FagSystem, response);
            var crmCustomersByCustomerNumber = query.GetDetailedCrmCustomerByCustomerNumber(evt.KundeId, cust, evt.FagSystem, response);

            var customers = new List<CrmCustomer>();
            customers.AddRange(crmCustomersBySourceId);
            customers.AddRange(crmCustomersByCustomerNumber);
            if (customers.Count == 0) return new CrmCustomer();

            customers = SortCustomerListByPriority(customers);

            cust.CustomerId = customers.First().Id;
            return customers.First();
        }
        public static CrmCustomer GetCustomerCard(SakHendelse evt, CustomerExtra cust, ProcessedCustomerResponse response, QueryExpressionHelper query)
        {
            if(string.IsNullOrEmpty(evt.Interessent.OffentligId)) return new CrmCustomer();

            var crmCustomersBySourceId = query.GetDetailedCrmCustomerBySourceId(evt.FagsystemKundenummer, cust, evt.FagSystem, response);
            var crmCustomersByCustomerNumber = query.GetDetailedCrmCustomerByCustomerNumber(evt.Interessent.OffentligId, cust, evt.FagSystem, response);

            var customers = new List<CrmCustomer>();
            customers.AddRange(crmCustomersBySourceId);
            customers.AddRange(crmCustomersByCustomerNumber);
            customers = customers.OrderBy(e => e.StateCode).ToList();
            if (customers.Count == 0) return new CrmCustomer();

            customers = SortCustomerListByPriority(customers);

            var customer = customers.First();
            customer.Role = evt.Interessent.Rolle;

            cust.CustomerId = customer.Id;
            return customer;
        }
        
        public static bool HasSameSource(TerritoryCode crmCustomerCardSource, Source eventSource)
        {
            return (int) crmCustomerCardSource == Helper.GetTerritoryCode(eventSource).Value;
        }


        public static bool UpdateCustomerSourceId(CrmCustomer crmCustomerCard, string source, string sourceId, ProcessedCustomerResponse response, QueryExpressionHelper query, bool hendelse = false)
        {
            if (string.IsNullOrEmpty(sourceId)) return true;
            if (crmCustomerCard.SourceId.Equals(sourceId)) return true;
            if (hendelse && !string.IsNullOrEmpty(crmCustomerCard.SourceId)) return true; // TODO, decide to use this for kundehendelse

            var entity = new Entity(crmCustomerCard.EntityName.ToString()) { Id = crmCustomerCard.Id };
            RequestBuilder.UpdateCustomerSourceId(source, sourceId, entity);

            return query.UpdateEntity(entity, response);
        }

        

        public static void CreatePotentialCustomer(KundeHendelse evt, CustomerExtra cust, ProcessedCustomerResponse response, QueryExpressionHelper query)
        {
            if (cust.CustomerId != Guid.Empty) return;

            var ent = RequestBuilder.BuildPotentialCustomerForKundeHendelse(evt, cust);
            cust.CustomerId = query.CreateEntity(ent, response);

            Log.Add($"Creating a potential customer. '{evt.KundeId}::{evt.Brukersted}'. Owner=>{cust.CustomerCardSystemuserOwnerId} / {cust.DefaultTeamId}");

            cust.IsNewCustomer = true;
        }
        public static void CreatePotentialCustomer(SakHendelse evt, CustomerExtra cust, ProcessedCustomerResponse response, QueryExpressionHelper query)
        {
            if (cust.CustomerId != Guid.Empty) return;

            var ent = RequestBuilder.BuildPotentialCustomerForKundeHendelse(evt, cust);
            cust.CustomerId = query.CreateEntity(ent, response);

            Log.Debug($"Creating a potential customer. '{evt.Interessent.OffentligId}::{evt.Bankregnr}'. Owner=>{cust.CustomerCardSystemuserOwnerId} / {cust.DefaultTeamId}");

            cust.IsNewCustomer = true;
        }


        public static void CreateOutcomeForSparing(KundeHendelse evt, CustomerExtra cust, ProcessedCustomerResponse response, QueryExpressionHelper query)
        {
     
            var phase = EnumUtils.Parse<HendelseStatusEnum>(evt.Fase);
            var action = EnumUtils.Parse<HendelseAction>(evt.Handling);
            var transaction = EnumUtils.Parse<HendelseTransaction>(evt.TransaksjonsType);
           
            //Error on evt.Handling 
            Log.AppendLine($"CreateOutcomeForSparing1: {evt.Fase}, {evt.Handling}, {evt.TransaksjonsType}");
            //Salg, NotFound, EngangsInskudd
            Log.AppendLine($"CreateOutcomeForSparing2: {phase}, {action}, {transaction}");

            switch (phase)
            {
                //case HendelseStatusEnum.Soknad:
                case HendelseStatusEnum.Interesse:
                case HendelseStatusEnum.Interesse_For_Produkt:
                case HendelseStatusEnum.Interesse_Pris_Og_Produkt:
                    // SO OPEN
                    Log.Add($"{Util.GetMethodName()}. Gonna make SO with keep open true");
                    OpportunityManager.HandleSalesOpportunityForSparing(evt, cust, response, query, keepOpen: true);
                    break;
                case HendelseStatusEnum.Salg:
                    // SO WON
                    Log.Add($"{Util.GetMethodName()}. Gonna make SO as won. Next HandleSalesOpportunityForSparing...");
                    OpportunityManager.HandleSalesOpportunityForSparing(evt, cust, response, query);
                    break;
                case HendelseStatusEnum.Avsluttet:
                    IncidentManager.HandleIncident(evt, cust, response, query);
                    //CreateAndResolveIncident(evt, cust, response, query);
                    break;
                case HendelseStatusEnum.Endret:
                    if (action == HendelseAction.Utvidet)
                    {
                        // SO WON
                        Log.Add($"{Util.GetMethodName()}. Gonna make SO as won");
                        OpportunityManager.HandleSalesOpportunityForSparing(evt, cust, response, query);
                    }
                    else if (action == HendelseAction.Redusert)
                    {
                        Log.Add($"{Util.GetMethodName()}. HandleIncident...");
                        IncidentManager.HandleIncident(evt, cust, response, query);
                        //CreateAndResolveIncident(evt, cust, response, query);
                    }
                    else
                    {
                        Log.Add($"Sparing event for phase 'endret' with action 'other'. Action: '{evt.Handling}'");
                        // Create info activity
                        InfoActivityManager.CreateUndefinedInfoActivity(evt, cust, response, query);
                    }
                    break;
                case HendelseStatusEnum.Annet:
                    if (transaction == HendelseTransaction.Spareavtale || transaction == HendelseTransaction.Engangsinnskudd)
                    {
                        // SO WON
                        OpportunityManager.HandleSalesOpportunityForSparing(evt, cust, response, query);
                    }
                    else
                    {
                        IncidentManager.HandleIncident(evt, cust, response, query);
                        //CreateAndResolveIncident(evt, cust, response, query);
                    }
                    break;
                default:
                    Log.AppendLine($"Sparing event. Phase not defined: '{evt.Fase}'");
                    InfoActivityManager.CreateUndefinedInfoActivity(evt, cust, response, query);
                    break;
            }
            Log.Add($"CreateOutcomeForSparing done. Success: {response.Success} {response.ErrorMessage}");
        }

        

        public static void CreateOutcomeForSparingAndRadgiverPluss(SakHendelse evt, CustomerExtra cust, ProcessedCustomerResponse response, QueryExpressionHelper query, MsmqLogger log)
        {
            var result = cust.SakHendelseAttributes.Result.ToLower();

            switch (result)
            {
                case "salg":
                    // SO WON
                    OpportunityManager.HandleSalesOpportunityForSparingSakHendelse(evt, cust, response, query);
                    break;
                case "ikke salg":
                    // SO LOST 
                    OpportunityManager.HandleSalesOpportunityForSparingSakHendelse(evt, cust, response, query, lost: true);
                    break;
                case "":
                    // SO OPEN (interesse)
                    OpportunityManager.HandleSalesOpportunityForSparingSakHendelse(evt, cust, response, query, keepOpen: true);
                    break;
                default:
                    log.Warn($"Sparing event (SH). Result not defined: '{result}'");
                    // Create info activity
                    InfoActivityManager.CreateUndefinedInfoActivity(evt, cust, response, query);
                    break;
            }
        }

        public static void CreateOutcomeForSparingAndRadgiverPluss(KundeHendelse evt, CustomerExtra cust, ProcessedCustomerResponse response, QueryExpressionHelper query, StringBuilder log)
        {
            var phase = evt.Fase.ToLower(); // TODO - blir nytt felt 'resultat'  :: SALG :: IKKE SALG :: TOM
            var action = evt.Handling.ToLower();
            var transaction = evt.TransaksjonsType.ToLower();

            switch (phase)
            {
                case "salg":
                    // SO WON
                    OpportunityManager.HandleSalesOpportunityForSparing(evt, cust, response, query);
                    break;
                case "ikke salg":
                    // SO LOST 
                    // TODO, decide what to do here
                    // Define fields

                    //IncidentManager.HandleIncident(evt, cust, response, query);
                    //OpportunityManager.SalesOpportunityZeroRevenueToLost(evt, cust, response, query);
                    break;
                case "":
                    // SO OPEN (interesse)
                    OpportunityManager.HandleSalesOpportunityForSparing(evt, cust, response, query, keepOpen: true);
                    break;
                default:
                    log.AppendLine($"Sparing event. Phase not defined: '{evt.Fase}'");
                    // Create info activity
                    InfoActivityManager.CreateUndefinedInfoActivity(evt, cust, response, query);
                    break;
            }
        }
        public static void CreateOutcomeForInsurance(KundeHendelse evt, CustomerExtra cust, ProcessedCustomerResponse response, QueryExpressionHelper query)
        {
            var phase = EnumUtils.Parse<HendelseStatusEnum>(evt.Fase);
            var action = EnumUtils.Parse<HendelseAction>(evt.Handling);
            var transaction = EnumUtils.Parse<HendelseTransaction>(evt.TransaksjonsType);

            Log.Add($"CreateOutcomeForInsurance {evt.Fase} {phase}");

            switch (phase)
            {
                case HendelseStatusEnum.Soknad:
                case HendelseStatusEnum.Interesse:
                case HendelseStatusEnum.Interesse_For_Produkt:
                case HendelseStatusEnum.Interesse_Pris_Og_Produkt:
                    // SO OPEN
                    OpportunityManager.HandleSalesOpportunityForInsurance(evt, cust, response, query, true);
                    break;
                case HendelseStatusEnum.Salg:
                    // SO WON
                    OpportunityManager.HandleSalesOpportunityForInsurance(evt, cust, response, query);
                    break;
                case HendelseStatusEnum.Avsluttet:
                    IncidentManager.HandleIncident(evt, cust, response, query);
                    //CreateAndResolveIncident(evt, cust, response, query);
                    break;
                case HendelseStatusEnum.Endret:
                    if (action == HendelseAction.Utvidet)
                    {
                        // SO WON
                        Log.AppendLine($"HendelseAction.Utvidet");
                        OpportunityManager.HandleSalesOpportunityForInsurance(evt, cust, response, query);
                    }
                    else if (action == HendelseAction.Redusert || action == HendelseAction.Annet)
                    {
                        IncidentManager.HandleIncident(evt, cust, response, query);
                        //CreateAndResolveIncident(evt, cust, response, query);
                    }
                    else
                    {
                        Log.AppendLine($"Insurance event for phase 'endret' with action 'other'. Action: '{evt.Handling}'");
                        // Create info activity
                        InfoActivityManager.CreateUndefinedInfoActivity(evt, cust, response, query);
                    }
                    break;
                case HendelseStatusEnum.Annet:
                    if (transaction == HendelseTransaction.Salg)
                    {
                        // SO WON
                        OpportunityManager.HandleSalesOpportunityForInsurance(evt, cust, response, query);
                    }
                    else
                    {
                        IncidentManager.HandleIncident(evt, cust, response, query);
                        //CreateAndResolveIncident(evt, cust, response, query);
                    }
                    break;
                default:
                    Log.AppendLine($"Insurance event. Phase not defined: '{evt.Fase}'");
                    InfoActivityManager.CreateUndefinedInfoActivity(evt, cust, response, query);
                    break;
            }
        }
        public static void CreateOutcomeForKreditt(SakHendelse evt, CustomerExtra cust, ProcessedCustomerResponse response, QueryExpressionHelper query, MsmqLogger log)
        {
            //var phase = evt.SakStatus.ToLower();


            OpportunityManager.HandleSalesOpportunityForKreditt(evt, cust, response, query);

            //switch (EnumUtils.Parse<HendelseStatusEnum>(evt.SakStatus))
            //{
            //    // SO WON
            //    case HendelseStatusEnum.Ferdig_behandlet:
            //        OpportunityManager.HandleSalesOpportunityForKreditt(evt, cust, response, query);
            //        break;
            //    // SO LOST
            //    case HendelseStatusEnum.Behov_Opphoert:
            //    case HendelseStatusEnum.Avslatt:
            //        OpportunityManager.HandleSalesOpportunityForKreditt(evt, cust, response, query);
            //        break;
            //    // SO OPEN
            //    default:
            //        //log.Warn($"Insurance event. Phase not defined: '{evt.Fase}'");
            //        OpportunityManager.HandleSalesOpportunityForKreditt(evt, cust, response, query, true);
            //        break;
            //}
        }

        public static OptionSetValue GetOpportunityStatusCodeForRaadgiverpluss(string status)
        {
            var check = status.ToLower();
            switch (check)
            {
                case "":
                    return new OptionSetValue((int)OpportunityStatusCode.Interesse);
                case "ikke salg":
                    return new OptionSetValue((int)OpportunityStatusCode.SlettetIFagsystem); //Lost
                case "salg":
                    return new OptionSetValue((int)OpportunityStatusCode.Won); //Won
                default:
                    return new OptionSetValue((int)OpportunityStatusCode.NotFound); // TODO SOME SHIHIHTIHTIHT
            }
        }

        public static OptionSetValue GetOpportunityStatusCode(string status)
        {
            Log.Add($"GetOpportunityStatusCode.Status: {EnumUtils.Parse<HendelseStatusEnum>(status)}");

            switch (EnumUtils.Parse<HendelseStatusEnum>(status))
            {
                case HendelseStatusEnum.Soknad:
                case HendelseStatusEnum.Interessert:
                case HendelseStatusEnum.Interesse:
                    return new OptionSetValue((int)OpportunityStatusCode.Interesse);
                case HendelseStatusEnum.Interesse_For_Produkt:
                    return new OptionSetValue((int)OpportunityStatusCode.InteresseForProdukt);
                case HendelseStatusEnum.Interesse_Pris_Og_Produkt:
                    return new OptionSetValue((int)OpportunityStatusCode.PrisOgProdukt);
                case HendelseStatusEnum.Ny:
                    return new OptionSetValue((int)OpportunityStatusCode.Ny);
                case HendelseStatusEnum.Fra_lanesoknad:
                    return new OptionSetValue((int)OpportunityStatusCode.FraLaanesoknad);
                case HendelseStatusEnum.Under_arbeid:
                    return new OptionSetValue((int)OpportunityStatusCode.UnderArbeid);
                case HendelseStatusEnum.Til_beslutning:
                    return new OptionSetValue((int)OpportunityStatusCode.TilBeslutning);
                case HendelseStatusEnum.Innstilt:
                    return new OptionSetValue((int)OpportunityStatusCode.Innstilt);
                case HendelseStatusEnum.Besluttet:
                    return new OptionSetValue((int)OpportunityStatusCode.Besluttet);
                case HendelseStatusEnum.Under_produksjon:
                    return new OptionSetValue((int)OpportunityStatusCode.UnderProduksjon);
                case HendelseStatusEnum.Til_signering:
                    return new OptionSetValue((int)OpportunityStatusCode.TilSignering);
                case HendelseStatusEnum.Signert:
                    return new OptionSetValue((int)OpportunityStatusCode.Signert);
                case HendelseStatusEnum.Klar_for_depotkontroll_og_utbetaling:
                    return new OptionSetValue((int)OpportunityStatusCode.KlarForDepotkontrollOgUtbetaling);
                case HendelseStatusEnum.Under_depotkontroll_og_utbetaling:
                    return new OptionSetValue((int)OpportunityStatusCode.UnderDepotKontrollOgUtbetaling);
                case HendelseStatusEnum.Under_depotkontroll_utbetalt:
                    return new OptionSetValue((int)OpportunityStatusCode.UnderDepotkontrollUtbetalt);
                case HendelseStatusEnum.Behov_Opphoert:
                    return new OptionSetValue((int)OpportunityStatusCode.BehovOpphoert); //Lost
                case HendelseStatusEnum.Avslatt:
                    return new OptionSetValue((int)OpportunityStatusCode.Avslatt); //Lost
                case HendelseStatusEnum.Ferdig_behandlet:
                    return new OptionSetValue((int)OpportunityStatusCode.Won); //Won
                default:
                    return new OptionSetValue((int)OpportunityStatusCode.NotFound); // TODO SOME SHIHIHTIHTIHT
            }
        }





        public static void GetOwnerIfExist(CrmCustomer crmCustomerCard, CustomerExtra cust)
        {
            if (crmCustomerCard.Id == Guid.Empty) return;

            if (crmCustomerCard.OwnerReference.LogicalName.Equals(EntityName.systemuser.ToString()))
            {
                cust.CustomerCardSystemuserOwnerId = crmCustomerCard.OwnerReference.Id;
            }
        }
        
        public static double DayDiff(DateTime date)
        {
            return (DateTime.UtcNow - date).TotalDays;
        }


        #region Private Methods

        private static List<CrmCustomer> SortCustomerListByPriority(List<CrmCustomer> customers)
        {
            return customers.OrderBy(e => e.StateCode)
                            .ThenByDescending(e => e.Source == TerritoryCode.Kjerne)
                            .ThenByDescending(e => e.Source == TerritoryCode.Nice)
                            .ThenByDescending(e => e.Source == TerritoryCode.Tradex)
                            .ThenByDescending(e => e.Source == TerritoryCode.BanQsoft)
                            .ThenByDescending(e => !string.IsNullOrEmpty(e.SourceId)).ToList();
        }
        
        private static Guid CreateDepartment(string departmentName, string departmentNumber, Guid defaultTeamId, ProcessedCustomerResponse response, QueryExpressionHelper query)
        {
            var entity = RequestBuilder.CreateDepartment(departmentName, departmentNumber, defaultTeamId);
            return query.CreateEntity(entity, response);
        }
        private static bool UpdateDepartment(string departmentName, Guid depId, ProcessedCustomerResponse response, QueryExpressionHelper query)
        {
            var entity = RequestBuilder.UpdateDepartment(departmentName);
            entity.Id = depId;
            return query.UpdateEntity(entity, response);
        }
       
        #endregion





    }
}
