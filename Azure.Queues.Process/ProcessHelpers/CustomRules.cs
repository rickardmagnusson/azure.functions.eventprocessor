﻿using System.Collections.Generic;

namespace Azure.Queues.Process.ProcessHelpers
{
    /// <summary>
    /// ???
    /// </summary>
    public class CustomRules
    {
        public static bool IsBankSpecificEmail(List<string> bankSpecificEmails, string email)
        {
            if (string.IsNullOrEmpty(email) || bankSpecificEmails == null || bankSpecificEmails.Count == 0) return false;

            return bankSpecificEmails.Contains(email.ToLower());
        }
    }
}
