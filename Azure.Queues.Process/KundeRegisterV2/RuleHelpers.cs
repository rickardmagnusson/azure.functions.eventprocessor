﻿using Common.Entities;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Azure.Queues.Process
{
    /// <summary>
    /// Contains helpers for the RuleEngine 
    /// </summary>
    public static class RuleHelper
    {
        private static readonly string CRM = "1";
        private static readonly string TRADEX = "809020005";
        private static readonly string KERNE = "809020000";
        private static readonly string BREEG = "809020003";
        private static readonly string DSF = "809020003";
        private static readonly string MIGRATE = "809020004";
        private static readonly string ARCHIVE = "809020000";
        private static readonly string BLI_KUNDE = "809020001";
        private static readonly string STATUS_REASON_ACTIVE = "1";
        private static readonly string STATUS_REASON_INACTIVE = "2";
        private static readonly string INVALID_PUBLICID_FIELD = "cap_invalidpublicid";
        private static readonly string SMS_FIELD = "Contact_Faxes";


        /// <summary>
        /// Load contact from cache.
        /// </summary>
        /// <param name="ssn"></param>
        /// <returns>A contact from the ssn, if it's is found</returns>
        public static List<CrmCustomer> CRMContacts(string ssn)
        {
            var cache = CRMCache.Instance();
            return cache.Contacts.Where(c => c.CustomerNumber == ssn).ToList();
        }


        /// <summary>
        /// Load all BusinessUnits.
        /// </summary>
        /// <returns>List of active BusinessUnits</returns>
        public static List<BusinessUnit> BusinessUnits()
        {
            var cache = CRMCache.Instance();
            return cache.BusinessUnits;
        }


        /// <summary>
        /// Load all SMS activities.
        /// </summary>
        /// <returns>List of active SMS activities</returns>
        public static List<SMS> GetSMS()
        {
            var cache = CRMCache.Instance();
            return cache.SMS;
        }


        #region Loaders

        /// <summary>
        /// Load customers into cache for performance
        /// </summary>
        /// <param name="items"></param>
        /// <returns>A list of CrmContacts</returns>
        public static List<CrmCustomer> LoadContacts(this List<KundeRegisterEvent> items)
        {
            var customers = new List<CrmCustomer>();

            foreach (var item in items)
            {
                var customer = new CRMQuery()
                    .GetBySSN<CrmCustomer>(item.CustomerNumber);


                foreach (var c in customer)
                {
                    var cust = new CrmCustomer
                    {
                        CustomerNumber = c.governmentid,
                        fullname = c.fullname,
                        cap_lastactivitycreatedon_date = c.cap_lastactivitycreatedon_date,
                        cap_lastcasecreatedon_date = c.cap_lastcasecreatedon_date,
                        cap_sourceid = c.cap_sourceid,
                        cap_nicesourceid = c.cap_nicesourceid,
                        cap_nice2sourceid = c.cap_nice2sourceid,
                        cap_banqsoftsourceid = c.cap_banqsoftsourceid,
                        cap_tradexsourceid = c.cap_tradexsourceid,
                        contactid = c.governmentid,
                        statuscode = c.statuscode,
                        statecode = c.statecode,
                        territorycode = c.territorycode
                    };

                    customers.Add(cust);
                }
            }
            return customers;
        }


        /// <summary>
        /// Load customers into cache for performance
        /// </summary>
        /// <param name="items"></param>
        /// <returns>A list of CrmContacts</returns>
        public static List<SMS> LoadSMS(this List<KundeRegisterEvent> items)
        {
            var SMS = new List<SMS>();
            var cache = CRMCache.Instance();
            var customers = cache.Contacts;

            foreach (var item in customers)
            {
#if DEBUG
                Console.WriteLine("Cust: ----> " + item.fullname);
#endif
                var customerActivities = new CRMQuery().SMS<SMS>(item.contactid);

                if (customerActivities.Any())
                    foreach (var a in customerActivities)
                        SMS.Add(new SMS { contactid = item.contactid, scheduledend = a.scheduledend });

            }
#if DEBUG
            Console.WriteLine("SMS contains: " + SMS.Any());
#endif
            return SMS;
        }


        /// <summary>
        /// Load all BusinessUnits into cache for performance.
        /// </summary>
        /// <param name="items"></param>
        /// <returns>List<string> of </returns>
        public static List<BusinessUnit> LoadBusinessUnits(this List<KundeRegisterEvent> items)
        {
            var cache = CRMCache.Instance();
            var queryAllBanks = new CRMQuery().BusinessUnits<BusinessUnit>();
            var bankList = new List<BusinessUnit>();

            queryAllBanks.ForEach(b => {
                if (b.divisionname != null && b.businessunitid != null)
                    bankList.Add(new BusinessUnit { businessunitid = b.businessunitid, divisionname = b.divisionname });
            });

            return bankList;
        }


        #endregion


        /// <summary>
        /// Get all BusinessUnits
        /// </summary>
        /// <param name="items"></param>
        /// <returns>List<string> of active BusinessUnits</returns>
        public static List<string> GetBusinessUnits(this List<KundeRegisterEvent> items)
        {
            var queryAllBanks = BusinessUnits();
            var bankList = new List<string>();

            queryAllBanks.ForEach(b => {
                if (b.divisionname != null)
                    bankList.Add(b.divisionname);
            });

            return bankList;
        }


        /// <summary>
        /// Get all contacts where statcode = (Active) or (Inactive)
        /// </summary>
        /// <param name="items">List<KunderRegister> items to check</param>
        /// <returns>List of active SNN</returns>
        public static List<string> GetSSN(this List<KundeRegisterEvent> items)
        {
            var ssnList = new List<string>();
            foreach (var item in items)
            {
                CRMContacts(item.CustomerNumber)
                .ForEach(i => {
                    ssnList.Add(item.CustomerNumber);
                });
            }
            return ssnList;
        }


        /// <summary>
        /// Get all contacts where statcode = 0 (Active)
        /// </summary>
        /// <param name="items">List<KunderRegister> items to check</param>
        /// <returns>List of active SNN</returns>
        public static List<string> GetActiveSSN(this List<KundeRegisterEvent> items)
        {
            var ssnList = new List<string>();
            foreach (var item in items)
            {
                CRMContacts(item.CustomerNumber)
                .ForEach(i => {
                    if (Convert.ToInt32(i.StateCode) == 0)
                        ssnList.Add(item.CustomerNumber);
                });
            }
            return ssnList;
        }


        /// <summary>
        /// Get all contacts where statcode = 0 (Active) or 1 InActive
        /// </summary>
        /// <param name="items">List<KunderRegister> items to check</param>
        /// <returns>List of active SNN</returns>
        public static List<string> CustomerStatus(this List<KundeRegisterEvent> items)
        {
            var ssnList = new List<string>();
            foreach (var item in items)
            {
                CRMContacts(item.CustomerNumber)
                .ForEach(i => {
                    var stateCode = Convert.ToInt32(i.StateCode);
                    if (stateCode == 0 || stateCode == 1)
                        ssnList.Add(item.CustomerNumber);
                });
            }
            return ssnList;
        }



        /// <summary>
        /// Applies to #1
        /// Creates a list of ActivityStatus as strings
        /// </summary>
        /// <returns>A list of ActivityStatus enum as strings</returns>
        public static List<string> ActivityStatusFields()
        {
            var list = new List<string>();
            var activityStatus = new List<EventType> { EventType.AKTIV, EventType.AKTIV_RELASJON, EventType.AVVENTER_SJEKK };

            foreach (EventType pa in activityStatus)
                list.Add(pa.ToString());
            return list;
        }


        /// <summary>
        /// Check to see if contact has other sources
        /// </summary>
        /// <param name="items"></param>
        /// <returns>List<string> SSN</returns>
        public static List<string> HasOtherSources(this List<KundeRegisterEvent> items)
        {
            //cap_sourceid(Kerne): BU: Any bank
            //cap_tradexsourceid(Tradex) : BU: Kapitalforvaltning
            //cap_nicesourceid(NICE EFS): BU Eika Skadeforsikring
            //cap_nice2sourceid(NICE EFP): BU: Eika Personforsikring
            //cap_banqsoftsourceid(BanQsoft) BU: Eika Kredittbank

            //By priority
            //  Kerne
            //  - NICE
            //  - BanQsoft
            //  - Tradex

            var ssnList = new List<string>();
            foreach (var item in items)
            {
                var customers = CRMContacts(item.CustomerNumber);
                customers.ForEach(i =>
                {
                    if (!string.IsNullOrEmpty(i.SourceId) ||
                        !string.IsNullOrEmpty(i.NicePersonSourceId) ||
                        !string.IsNullOrEmpty(i.NiceSkadeSourceId) ||
                        !string.IsNullOrEmpty(i.BanQsoftSourceId) ||
                        !string.IsNullOrEmpty(i.TradexSourceId))
                    {
                        if (!ssnList.Contains(i.CustomerNumber)) //Distinct
                        {
#if DEBUG
                            Console.Write("Customer " + i.fullname);
                            Console.Write(" Added: " + i.governmentid);
                            Console.WriteLine("");
#endif
                            ssnList.Add(item.CustomerNumber);
                        }
                    }
                });
            }

            return ssnList;
        }


        /// <summary>
        /// Check of recent activity for a contact
        /// </summary>
        /// <param name="items"></param>
        /// <returns>List<string> SSN</returns>
        public static List<string> RecentActivities(this List<KundeRegisterEvent> items)
        {
            // Recent Activities
            // -Any Case or SO where Created On<last 12 months
            // - Task, Email, Phone Call, Letter, SMS, Appointment where Created On < last 12 months
            // - Task, Email, Phone Call, Letter, SMS, where Due Date in the future(no limit)
            // - Appointment where Start Time in the future
            // - Customer Created On<last 12 month

            var ssnList = new List<string>();
            var now = DateTime.Now;

            foreach (var item in items)
            {
                CRMContacts(item.CustomerNumber)
                .ForEach(i =>
                {
                    if (i.cap_lastactivitycreatedon_date != null)
                    {
                        DateTime lastactivityDate;
                        if (i.cap_lastactivitycreatedon_date.TryParseDate(out lastactivityDate))
                        {
                            if (lastactivityDate.HasRecentActivity())
                                ssnList.Add(item.CustomerNumber);
                        }
                        else
                        {
#if DEBUG
                            Console.WriteLine($"cap_lastactivitycreatedon_date ({i.cap_lastactivitycreatedon_date}) is not valid");
#endif
                        }
                    }

                    if (i.cap_lastcasecreatedon_date != null)
                    {
                        DateTime lastactivitycasecreate;
                        if (i.cap_lastcasecreatedon_date.TryParseDate(out lastactivitycasecreate))
                        {
                            if (lastactivitycasecreate.HasRecentActivity())
                                ssnList.Add(item.CustomerNumber);
                        }
                        else
                        {
#if DEBUG
                            Console.WriteLine($"cap_lastcasecreatedon_date ({i.cap_lastcasecreatedon_date}) is not valid");
#endif
                        }
                    }

                    if (i.cap_lastoptycreatedon_date != null)
                    {
                        DateTime lastoptycreated;
                        if (i.cap_lastoptycreatedon_date.TryParseDate(out lastoptycreated))
                        {
                            if (lastoptycreated.HasRecentActivity())
                                ssnList.Add(item.CustomerNumber);
                        }
                        else
                        {
#if DEBUG
                            Console.WriteLine($"cap_lastoptycreatedon_date ({i.cap_lastoptycreatedon_date}) is not valid");
#endif
                        }
                    }

                    //SMS
                    var sms = GetSMS();

                    if (sms != null && sms.Any())
                    {
                        var schedule = sms.Where(c => c.contactid == i.contactid);
                        if (schedule.Any())
                        {
                            foreach (var s in schedule)
                            {
                                DateTime scheduledend;
                                if (s.scheduledend.TryParseDate(out scheduledend))
                                {
#if DEBUG
                                    Console.WriteLine($"Found user i SMS with activitydate '{s.scheduledend}'");
#endif
                                    if (scheduledend.HasRecentActivity())
                                        ssnList.Add(item.CustomerNumber);
                                }
                                else
                                {
#if DEBUG
                                    Console.WriteLine($"scheduledend datetime ({s.scheduledend}) is not valid");
#endif
                                }
                            }
                        }//IF
                    } //IF

                }); //Foreach
            }
            return ssnList;
        }




        /// <summary>
        /// Check to see if a date is older than one year        
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns>True if so</returns>
        public static bool HasRecentActivity(this DateTime dateTime)
        {
            var last = dateTime.AddYears(-1);
            var now = DateTime.Now;
            if (last < now)
                return true;
            return false;
        }


        /// <summary>
        /// Try parse a date to a valid DateTime 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static bool TryParseDate(this string value, out DateTime dateTime)
        {
            try
            {
                string format = "MM/dd/yyyy hh:mm:ss";
                dateTime = DateTime.ParseExact(value, format, CultureInfo.InvariantCulture);
                if (dateTime != null)
                    return true;
                else
                    return false;
            }
            catch{}

            dateTime = DateTime.MinValue;
            return false;
        }


        /// <summary>
        /// Removes the ignored items from event
        /// </summary>
        /// <param name="eventList"></param>
        /// <param name="items"></param>
        /// <returns>A list of ignored items</returns>
        public static List<KundeRegisterEvent> AddToIgnore(this List<KundeRegisterEvent> items, List<KundeRegisterEvent> oldList)
        {
            if (items.Any())
                items.ForEach(i => { oldList.Remove(i); });

            return oldList;
        }


        /// <summary>
        /// Merge two list into one unique.
        /// </summary>
        /// <param name="one"></param>
        /// <param name="two"></param>
        /// <returns>A merged list</returns>
        public static List<KundeRegisterEvent> Merge(this List<KundeRegisterEvent> one, List<KundeRegisterEvent> two)
        {
            if (two.Any())
                return one.Union(two).ToList();
            else
                return one;
        }


        /// <summary>
        /// SourceEqualsSourceId
        /// Function to check if source belongs to Kerne
        /// </summary>
        /// <param name="items"></param>
        /// <returns></returns>
        public static List<string> SourceEqualsSourceInKerne(this List<KundeRegisterEvent> items)
        {
            var ssnList = new List<string>();
            foreach (var item in items)
            {
                CRMContacts(item.CustomerNumber)
                .ForEach(i => {
                    var src = i.territorycode.Trim();
                    if (src.Equals(KERNE))
                        ssnList.Add(item.CustomerNumber);
                });
            }
            return ssnList;
        }



        /// <summary>
        /// Function to check if source is Migrate
        /// </summary>
        /// <param name="items"></param>
        /// <returns></returns>
        public static List<string> SourceMigrating(this List<KundeRegisterEvent> items)
        {
            var ssnList = new List<string>();
            foreach (var item in items)
            {
                CRMContacts(item.CustomerNumber)
                .ForEach(i => {
                    var src = i.territorycode.Trim();
                    if (src.Equals(MIGRATE))
                        ssnList.Add(item.CustomerNumber);
                });
            }
            return ssnList;
        }


        /// <summary>
        /// Function to check if source is in Archive
        /// </summary>
        /// <param name="items"></param>
        /// <returns></returns>
        public static List<string> Archieved(this List<KundeRegisterEvent> items)
        {
            // CRM Customer  is in (Archieved)?

            var ssnList = new List<string>();
            foreach (var item in items)
            {
                CRMContacts(item.CustomerNumber)
                .ForEach(i => {
                    if (i.territorycode.Trim() == ARCHIVE)
                        ssnList.Add(item.CustomerNumber);
                });
            }
            return ssnList;
        }


        /// <summary>
        /// If customer is inactive...
        /// </summary>
        /// <param name="items"></param>
        /// <returns></returns>
        public static List<string> Inactive(this List<KundeRegisterEvent> items)
        {
            // CRM Customer  is Inactive(Archieved)?
            //statuscode

            var ssnList = new List<string>();
            foreach (var item in items)
            {
                CRMContacts(item.CustomerNumber)
                .ForEach(i => {
                    if (i.statuscode.Trim() == "1")
                        ssnList.Add(item.CustomerNumber);
                });
            }
            return ssnList;
        }


        /// <summary>
        /// If customer is inactive...
        /// </summary>
        /// <param name="items"></param>
        /// <returns></returns>
        public static List<string> IsActive(this List<KundeRegisterEvent> items)
        {
            // CRM Customer  is Inactive(Archieved)?
            //statuscode

            var ssnList = new List<string>();
            foreach (var item in items)
            {
                CRMContacts(item.CustomerNumber)
                .ForEach(i => {
                    if (i.statuscode.Trim() == "0")
                        ssnList.Add(item.CustomerNumber);
                });
            }
            return ssnList;
        }


        /// <summary>
        /// Dont think we need this function due to function: HasOtherSources!! -----------------------------------------------------<
        /// #4 
        /// </summary>
        /// <param name="items"></param>
        /// <returns></returns>
        public static List<string> OtherActiveSources(this List<KundeRegisterEvent> items)
        {
            var otherActiveSourcesList = new List<string>();
            var customers = items.GetSSN();

            return otherActiveSourcesList;
        }


        /// <summary>
        /// Check to see if customer is in BREEG or DSF.
        /// </summary>
        /// <param name="items"></param>
        /// <returns>A list of items containing items found in BREEG</returns>
        public static List<string> CustomerInCRMDsfBreeg(this List<KundeRegisterEvent> items)
        {
            var bu = items.GetBusinessUnits();
            var customers = items.GetSSN();

            var ssnList = new List<string>();
            foreach (var item in items)
            {
                CRMContacts(item.CustomerNumber)
                .ForEach(i => {
                    var src = i.territorycode;
                    if (src.Equals(BREEG) || src.Equals(DSF) || src.Equals(CRM))
                        if (i.owningbusinessunit == item.DistributorBankNumber)
                            ssnList.Add(item.CustomerNumber);
                });
            }
            return ssnList;
        }


        /// <summary>
        /// Compare bank in CRM to Bank in KR
        /// </summary>
        /// <param name="items"></param>
        /// <returns>A list of SSN of items passed</returns>
        public static List<string> BankEqualsBank(this List<KundeRegisterEvent> items)
        {
            //OwnerBankNumber
            var customers = items.GetSSN();
            var ssnList = new List<string>();
            foreach (var item in items)
            {
                CRMContacts(item.CustomerNumber)
                .ForEach(i => {
                    if (i.owningbusinessunit == item.DistributorBankNumber)
                        ssnList.Add(item.CustomerNumber);
                });
            }
            return ssnList;
        }


        /// <summary>
        /// Compare bank in CRM to Bank in KR
        /// </summary>
        /// <param name="items"></param>
        /// <returns>A list of SSN of items passed</returns>
        public static List<string> BankCrmEqualsBankKr(this List<KundeRegisterEvent> items)
        {
            var equalList = new List<string>();

            foreach (var cust in items)
            {
                var crmcustomers = CRMContacts(cust.CustomerNumber);
                //OwnerBankNumber (from the event) = Bank where customer belongs in CRM
                if (crmcustomers.Where(c => c.owningbusinessunit == cust.OwnerBankNumber).Any())
                    equalList.Add(cust.CustomerNumber);
            }
            return equalList;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="items"></param>
        /// <returns></returns>
        public static List<string> SSNEqualsToKr(this List<KundeRegisterEvent> items)
        {
            var equalList = new List<string>();
            var customers = items.GetSSN();
            var bu = items.GetBusinessUnits();

            //OrgNum/FødNum(CRM) = OrgNum/FødNum (KR)

            foreach (var item in items)
            {
                var customer = CRMContacts(item.CustomerNumber);
                foreach (var cust in customer)
                {
                    if (cust.territorycode.Equals(CRM)) //need crm number here!!!!!!!!
                        if (cust.governmentid.Equals(item.CustomerNumber))
                            equalList.Add(item.CustomerNumber);
                }
            }

            return equalList;
        }


        /// <summary>
        /// Has the customer a Blikunde statecode of 0, 1 (Active / Inactive)
        /// </summary>
        /// <param name="items"></param>
        /// <returns>A list of SSN belonging to customers</returns>
        public static List<string> BecomeCustomerStatus(this List<KundeRegisterEvent> items)
        {
            var list = new List<string>();

            foreach (var item in items)
            {
                var customer = CRMContacts(item.CustomerNumber);
                foreach (var cust in customer)
                    if (cust.territorycode.Trim().Equals(BLI_KUNDE))
                    {
                        if (cust.StateCode.Equals("0")
                            || cust.StateCode.Equals("1"))
                            list.Add(item.CustomerNumber);
                    }
            }
            return list;
        }


        /// <summary>
        /// Compares the state of items in list
        /// </summary>
        /// <param name="items"></param>
        /// <param name="status">Status to match</param>
        /// <returns>A list of valid SSN</returns>
        public static List<string> WhereActivityTypeIs(this List<KundeRegisterEvent> items, EventType status)
        {
            var list = new List<string>();
            foreach (var item in items)
            {
                if (item.EventType == status)
                    list.Add(item.CustomerNumber);
            }
            return list;
        }


        /// <summary>
        /// This should be the first rule to run, since we are only interrested in the new ActivityStatus.
        /// If the status from the event contains one of these, add it list of items to send thru the new rules.
        /// The status of an Event. 
        /// </summary>
        /// <param name="items">KundeRegisterEvent items</param>
        /// <returns>A list of items, if any with item to perform run cleanup rules</returns>
        public static List<string> ShouldRunRulesForCustomer(this List<KundeRegisterEvent> items)
        {
            var list = new List<string>();

            foreach (var item in items)
            {
                var statusToCompare = item.EventType;
                switch (statusToCompare)
                {
                    case EventType.AKTIV:
                    case EventType.AKTIV_RELASJON:
                    case EventType.AVVENTER_SJEKK:
                        list.Add(item.CustomerNumber);
                        break;
                    case EventType.IKKE_FUNNET_AKTIV_I_KERNE:
                        list.Add(item.CustomerNumber);
                        break;
                    case EventType.KERNEID_IKKE_GYLDIG:
                        list.Add(item.CustomerNumber);
                        break;
                    case EventType.OFFENTLIGID_IKKE_I_KERNE:
                    case EventType.BANKREGNR_IKKE_EIKABANK:
                    case EventType.FIKTIVID_IKKE_I_KERNE:
                        list.Add(item.CustomerNumber);
                        break;
                }
            }

            return list;
        }
    }
}
