﻿using System;
using System.Collections.Generic;
using Common.Entities;

namespace Azure.Queues.Process
{
    //Applies to (AVVENTER_SJEKK, AKTIV_RELASJON, AKTIV)
    public class ActivityStatusRule<T> : CompositeRule<T>
    {
        List<string> targets;

        public ActivityStatusRule(List<T> source, List<string> targets) : base(source)
        {
            this.targets = targets;
        }

        //Businessrule
        public override bool IsSatisfiedBy(T o)
        {
            //cap_sourceid(Kerne): BU: Any bank
            //cap_tradexsourceid(Tradex) : BU: Kapitalforvaltning
            //cap_nicesourceid(NICE EFS): BU Eika Skadeforsikring
            //cap_nice2sourceid(NICE EFP): BU: Eika Personforsikring
            //cap_banqsoftsourceid(BanQsoft) BU: Eika Kredittbank           
            
            //By priority
            //  Kerne
            //  - NICE
            //  - BanQsoft
            //  - Tradex


          dynamic item;
            if (typeof(T) == typeof(KundeRegisterEvent)) //Maybe make this more generic
                item = (o as KundeRegisterEvent);
            else
                throw new Exception("Type is not valid");

            if (targets.Contains(item.Status.ToString()))
                return true;
            return false;
        }
    }
}
