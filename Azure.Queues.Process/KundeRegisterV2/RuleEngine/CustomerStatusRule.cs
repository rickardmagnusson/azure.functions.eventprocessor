﻿using System;
using System.Collections.Generic;
using Common.Entities;

namespace Azure.Queues.Process
{
    public class CustomerStatusRule<T> : CompositeRule<T>
    {
        private List<string> targets;

        public CustomerStatusRule(List<T> source, List<string> targets) : base(source)
        {
            if(this.targets!=null)
                this.targets.Clear();
            this.targets = targets;
        }

        //Businessrule
        public override bool IsSatisfiedBy(T o)
        {
            dynamic item;
            if (typeof(T) == typeof(KundeRegisterEvent)) //Maybe make this more generic
                item = (o as KundeRegisterEvent);
            else
                throw new Exception("Type is not valid");

            if (targets.Contains(item.CustomerNumber)) {
                return true;
            }
            return false;
        }
    }
}
