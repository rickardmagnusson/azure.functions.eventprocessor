﻿using System;
using System.Collections.Generic;
using Common.Entities;
using Common.Enums;

namespace Azure.Queues.Process
{
    public class CheckStatusByGroupRule<T> : CompositeRule<T>
    {
        List<EventType> targets = new List<EventType> {
            EventType.AKTIV,
            EventType.AKTIV_RELASJON,
            EventType.AVVENTER_SJEKK };


        public CheckStatusByGroupRule(List<T> source) : base(source)
        {
        }

        //Businessrule
        public override bool IsSatisfiedBy(T o)
        {
            dynamic item;
            if (typeof(T) == typeof(KundeRegisterEvent)) //Maybe make this more generic
                item = (o as KundeRegisterEvent);
            else
                throw new Exception("Type is not valid");

            if (targets.Contains(item.Status))
                return true;
            return false;
        }
    }
}
