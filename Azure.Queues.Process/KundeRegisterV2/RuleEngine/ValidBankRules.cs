﻿using System;
using System.Collections.Generic;
using Common.Entities;

namespace Azure.Queues.Process
{
    public class ValidBanksRule<T> : CompositeRule<T>
    {
        private List<string> targets;

        public ValidBanksRule(List<T> source, List<string> targets) : base(source)
        {
            this.targets = targets;
        }

        //Businessrule
        public override bool IsSatisfiedBy(T o)
        {
            dynamic item;
            if (typeof(T) == typeof(KundeRegisterEvent)) //Maybe make this more generic
                item = (o as KundeRegisterEvent);
            else
                throw new Exception("Type is not valid");
            if (this.targets.Contains(item.BankNo)) //Fault
                return true;
            return false;
        }
    }
}
