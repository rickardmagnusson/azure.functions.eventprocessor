﻿using System;
using System.Collections.Generic;
using Common.Entities;

namespace Azure.Queues.Process
{

    /// <summary>
    /// Validates that the Source of this event is in range of the valid Source to be processed.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class heckCustomerStatusRule<T> : CompositeRule<T>
    {
        private List<string> targets;

        public heckCustomerStatusRule(List<T> source, List<string> target) : base(source)
        {
            targets = target;
        }

        //Businessrule
        public override bool IsSatisfiedBy(T o)
        {
            dynamic item;
            if (typeof(T) == typeof(KundeRegisterEvent))
                item = (o as KundeRegisterEvent);
            else
                throw new Exception("Type is not valid");
            return targets.Contains(item.SSN.ToString());
        }
    }
}
