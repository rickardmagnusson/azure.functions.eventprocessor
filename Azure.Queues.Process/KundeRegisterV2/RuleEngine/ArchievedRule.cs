﻿using System;
using System.Collections.Generic;
using Common.Entities;

namespace Azure.Queues.Process
{
    public class ArchievedRule<T> : CompositeRule<T>
    {
        private List<string> targets;

        public ArchievedRule(List<T> source, List<string> target) : base(source)
        {
            targets = target;
        }

      
        /// <summary>
        /// Business rule execution
        /// </summary>
        /// <param name="o"></param>
        /// <returns>True if passed</returns>
        public override bool IsSatisfiedBy(T o)
        {
            dynamic item;
            if (typeof(T) == typeof(KundeRegisterEvent))
                item = (o as KundeRegisterEvent);
            else
                throw new Exception("Type is not valid");
            return targets.Contains(item.Source.ToString());
        }
    }
}
