﻿using System;
using System.Collections.Generic;
using Common.Entities;

namespace Azure.Queues.Process
{

    public class OtherSourcesRule<T> : CompositeRule<T>
    {
        List<string> targets;

        public OtherSourcesRule(List<T> source, List<string> targets) : base(source)
        {
            this.targets = targets;
        }

        //Businessrule
        public override bool IsSatisfiedBy(T o)
        {
            //*Recent Activities
            //- Any Case or SO where Created On<last 12 months
            //- Task, Email, Phone Call, Letter, SMS, Appointment where Created On < last 12 months
            //- Task, Email, Phone Call, Letter, SMS,
            //    where Due Date in the future(no limit)
            //-Appointment where Start Time in the future
            //-Customer Created On<last 12 month

            dynamic item;
            if (typeof(T) == typeof(KundeRegisterEvent)) //Maybe make this more generic
                item = (o as KundeRegisterEvent);
            else
                throw new Exception("Type is not valid");

            if (targets.Contains(item.CustomerNumber))
                return true;
            return false;
        }
    }
}
