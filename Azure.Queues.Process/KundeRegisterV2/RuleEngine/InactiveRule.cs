﻿using System;
using System.Collections.Generic;
using Common.Entities;

namespace Azure.Queues.Process
{
    public class InactiveRule<T> : CompositeRule<T>
    {
        private List<string> targets;

        public InactiveRule(List<T> source, List<string> target) : base(source)
        {
            targets = target;
        }

        //Businessrule
        public override bool IsSatisfiedBy(T o)
        {
            dynamic item;
            if (typeof(T) == typeof(KundeRegisterEvent))
                item = (o as KundeRegisterEvent);
            else
                throw new Exception("Type is not valid");
            return targets.Contains(item.CustomerNumber);
        }
    }
}
