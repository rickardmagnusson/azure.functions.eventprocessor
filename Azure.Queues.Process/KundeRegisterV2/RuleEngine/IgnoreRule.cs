﻿using System;
using System.Collections.Generic;
using Common.Entities;
using Common.Enums;

namespace Azure.Queues.Process
{
    /// <summary>
    /// Validates if ActivityStatus is of type BANKREGNR_IKKE_EIKABANK or FIKTIVID_IKKE_I_KERNE.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class IgnoreRule<T> : CompositeRule<T>
    {
        public IgnoreRule(List<T> source) : base(source)
        {
        }

        //Businessrule
        //Return true if item has flag BANKREGNR_IKKE_EIKABANK or FIKTIVID_IKKE_I_KERNE
        public override bool IsSatisfiedBy(T o)
        {
            dynamic item;
            if (typeof(T) == typeof(KundeRegisterEvent))
                item = (o as KundeRegisterEvent);
            else
                throw new Exception("Type is not valid");

            switch (item.Status)
            {
                case EventType.BANKREGNR_IKKE_EIKABANK:
                    return true;
                case EventType.FIKTIVID_IKKE_I_KERNE:
                    return true;
            }
            return false;
        }
    }
}
