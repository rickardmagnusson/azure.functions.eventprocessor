﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Azure.Queues.Process
{
    public class ExpressionRule<T> : CompositeRule<T>
    {
        private Func<T, bool> expression;

        /// <summary>
        /// Add a Rule with expression to Group 
        /// </summary>
        /// <param name="source"></param>
        /// <param name="expression"></param>
        public ExpressionRule(List<T> source, Func<T, bool> expression) : base(source)
        {
            if (expression == null)
                throw new ArgumentNullException();
            else
                this.expression = expression;
        }
        /// <summary>
        /// Business rule execution
        /// </summary>
        /// <param name="o"></param>
        /// <returns>True if passed</returns>
        public override bool IsSatisfiedBy(T o)
        {
            return this.expression(o);
        }
    }
}
