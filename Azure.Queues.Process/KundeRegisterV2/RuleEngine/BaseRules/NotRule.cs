﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Azure.Queues.Process
{
    /// <summary>
    /// Not Rule
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class NotRule<T> : CompositeRule<T>
    {

        IRule<T> Rule;

        /// <summary>
        /// Add a Not statement to a group of rules
        /// </summary>
        /// <param name="source"></param>
        /// <param name="spec"></param>
        public NotRule(List<T> source, IRule<T> spec) : base(source)
        {
            this.Rule = spec;
        }

        /// <summary>
        /// Execute statement rule
        /// </summary>
        /// <param name="o">True if passed</param>
        /// <returns></returns>
        public override bool IsSatisfiedBy(T o)
        {
            return !this.Rule.IsSatisfiedBy(o);
        }
    }
}
