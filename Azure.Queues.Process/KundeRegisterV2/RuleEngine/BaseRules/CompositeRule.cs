﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Azure.Queues.Process
{
    /// <summary>
    /// Base class for a Rule
    /// </summary>
    /// <typeparam name="T">The type to run a statment on</typeparam>
    public abstract class CompositeRule<T> : IRule<T>
    {
        private List<T> targets = new List<T>();

        public CompositeRule(List<T> source)
        {
            this.targets = source;
        }

        /// <summary>
        /// Make sure to set base in class : base(source)
        /// Important that source is defined becuase this generates the Passed / NotPassed items
        /// </summary>
        /// <param name="source"></param>
        /// <param name="left"></param>
        /// <param name="right"></param>
        public CompositeRule(List<T> source, IRule<T> left, IRule<T> right)
        {
            this.targets = source;
        }


        /// <summary>
        /// Business rule execution
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        public abstract bool IsSatisfiedBy(T o);


        /// <summary>
        /// Return a list of items that not passed validation.
        /// </summary>
        public List<T> NotPassedItems
        {
            get { return targets.Where(c => !IsSatisfiedBy(c)).ToList(); }
        }


        /// <summary>
        /// Return a list of items that passed validation.
        /// </summary>
        public List<T> PassedItems
        {
            get { return targets.Where(c => IsSatisfiedBy(c)).ToList(); }
        }

        /// <summary>
        /// Add an And statement to Group.
        /// </summary>
        /// <param name="Rule"></param>
        /// <returns></returns>
        public CompositeRule<T> And(IRule<T> Rule) 
        {
            return new AndRule<T>(targets, this, Rule);
        }


        /// <summary>
        /// Add an Or statement to Group
        /// </summary>
        /// <param name="Rule"></param>
        /// <returns></returns>
        public CompositeRule<T> Or(IRule<T> Rule)
        {
            return new OrRule<T>(targets, this, Rule);
        }


        /// <summary>
        /// Add an Not statement to Rule.
        /// </summary>
        /// <param name="Rule"></param>
        /// <returns></returns>
        public CompositeRule<T> Not(IRule<T> Rule)
        {
            return new NotRule<T>(targets, Rule);
        }
    }
}
