﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Azure.Queues.Process
{
    /// <summary>
    /// Main interface for all rules
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IRule<T>
    {
        List<T> PassedItems { get; }
        List<T> NotPassedItems { get; }

        bool IsSatisfiedBy(T o);

        CompositeRule<T> And(IRule<T> Rule);
        CompositeRule<T> Or(IRule<T> Rule);
        CompositeRule<T> Not(IRule<T> Rule);
    }
}
