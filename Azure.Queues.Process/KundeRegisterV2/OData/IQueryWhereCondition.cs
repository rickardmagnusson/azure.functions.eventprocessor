﻿

namespace Azure.Queues.Process
{
    public interface IQueryWhereCondition : IQueryCondition
    {
        IQueryOrCondition EqualsTo(string compare);
    }
}
