﻿using System;

namespace Azure.Queues.Process
{
    public class NotImplementedAttribute : Attribute
    {
        public string Message { get; set; }
        public NotImplementedAttribute(string message)
        {
            Message = message;
            throw new NotImplementedException(message);
        }
    }
}
