﻿
namespace Azure.Queues.Process
{
    public interface IQueryOrCondition : IQueryCondition
    {
        IQueryOrCondition Or(string compare);
    }
}
