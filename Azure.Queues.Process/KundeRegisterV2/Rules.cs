﻿using Common.Entities;
using Common.Enums;
using System;
using System.Collections.Generic;

namespace Azure.Queues.Process
{
    /// <summary>
    /// RuleProcessor functions
    /// </summary>
    public static partial class RuleProcessor
    {

        /// <summary>
        /// Contains predefined Rules, ready for events to be processed.
        /// TODO!! NEED TO CHECK VALIDITY OF ITEMS (strings not empty and so on) BEFORE RUN
        /// </summary>


        //A list of valid sources to run thru Rules below
        private static List<string> ValidSourceList = new List<string> { "Kerne" };


        //A list of valid banks in CRM. Need to set!!
        private static List<string> ValidBankList = new List<string> { "4730" };


        /// <summary>
        /// This is the first rule to run. The ActivityStatus needs to be of new type.
        /// </summary>
        public static Func<List<KundeRegisterEvent>, CompositeRule<KundeRegisterEvent>> RunRulesForCustomer =
            list => new RunRulesForCustomer<KundeRegisterEvent>(list, list.ShouldRunRulesForCustomer());


        /// <summary>
        /// Source = Kerne
        /// </summary>
        public static Func<List<KundeRegisterEvent>, CompositeRule<KundeRegisterEvent>> ValidateSource =
            list => new SourceRule<KundeRegisterEvent>(list, ValidSourceList);


        /// <summary>
        /// SourceId = SourceId (Kerne) && Source = Kerne
        /// </summary>
        public static Func<List<KundeRegisterEvent>, CompositeRule<KundeRegisterEvent>> ValidateSourceId =
            list => new SourceEqualsSourceId<KundeRegisterEvent>(list, list.SourceEqualsSourceInKerne());


        /// <summary>
        /// Bank = Bank
        /// </summary>
        public static Func<List<KundeRegisterEvent>, CompositeRule<KundeRegisterEvent>> ValidateBanks =
            list => new ValidBanksRule<KundeRegisterEvent>(list, ValidBankList);


        /// <summary>
        /// Bank equals bank in CRM
        /// </summary>
        public static Func<List<KundeRegisterEvent>, CompositeRule<KundeRegisterEvent>> ValidateBankIsBank =
            list => new ValidBanksRule<KundeRegisterEvent>(list, list.BankEqualsBank());


        /// <summary>
        /// Status (statecode) Whats the customer Status
        /// </summary>
        public static Func<List<KundeRegisterEvent>, CompositeRule<KundeRegisterEvent>> ValidateStatus =
            list => new CustomerStatusRule<KundeRegisterEvent>(list, list.CustomerStatus());


        /// <summary>
        /// ActivityStatus enum from Event, on how where to proceed #1 or #9
        /// </summary>
        public static Func<List<KundeRegisterEvent>, CompositeRule<KundeRegisterEvent>> ValidateActivity =
            list => new ActivityStatusRule<KundeRegisterEvent>(list, RuleHelper.ActivityStatusFields());


        /// <summary>
        /// Has Other Sources
        /// </summary>
        public static Func<List<KundeRegisterEvent>, CompositeRule<KundeRegisterEvent>> ValidateHasOtherSources =
            list => new OtherSourcesRule<KundeRegisterEvent>(list, list.HasOtherSources());


        /// <summary>
        /// Has recent activities
        /// </summary>
        public static Func<List<KundeRegisterEvent>, CompositeRule<KundeRegisterEvent>> ValidateRecentActivities =
            list => new RecentActivityRule<KundeRegisterEvent>(list, list.RecentActivities());


        /// <summary>
        /// Group by Next function to call (Need to check when having two or more equal SSN) These goes to #1
        /// </summary>
        public static Func<List<KundeRegisterEvent>, CompositeRule<KundeRegisterEvent>> ValidateActiveOrRelasjonOrAvventer =
            list => new CheckStatusByGroupRule<KundeRegisterEvent>(list);


        /// <summary>
        /// Is source Migrate
        /// </summary>
        public static Func<List<KundeRegisterEvent>, CompositeRule<KundeRegisterEvent>> ValidateSourceIsMigrate =
            list => new MigrateRule<KundeRegisterEvent>(list, list.SourceMigrating());

        /// <summary>
        /// BankId(CRM) = BankId(KR)
        /// </summary>
        public static Func<List<KundeRegisterEvent>, CompositeRule<KundeRegisterEvent>> BankCrmEqualsBankKr =
            list => new ValidateBankExistInKrRule<KundeRegisterEvent>(list, list.BankCrmEqualsBankKr());


        /// <summary>
        /// OrgNum/FødNum(CRM) = OrgNum/FødNum (KR)
        /// </summary>
        public static Func<List<KundeRegisterEvent>, CompositeRule<KundeRegisterEvent>> ValidateSSNEqualsToKr =
            list => new ValidCustomerSSNRule<KundeRegisterEvent>(list, list.SSNEqualsToKr());


        /// <summary>
        /// WhereActivityTypeIs(EventType)
        /// </summary>
        public static Func<List<KundeRegisterEvent>, CompositeRule<KundeRegisterEvent>> Active =
            list => new CustomerStatusRule<KundeRegisterEvent>(list, list.WhereActivityTypeIs(EventType.AKTIV));


        /// <summary>
        /// ---> #3 BANKREGNR_IKKE_EIKABANK, Deside where to go -->
        /// </summary>
        public static Func<List<KundeRegisterEvent>, CompositeRule<KundeRegisterEvent>> CoreIdNotValid =
            list => new CustomerStatusRule<KundeRegisterEvent>(list, list.WhereActivityTypeIs(EventType.BANKREGNR_IKKE_EIKABANK));

        //#2
        public static Func<List<KundeRegisterEvent>, CompositeRule<KundeRegisterEvent>> PublicIdNotInCore =
            list => new CustomerStatusRule<KundeRegisterEvent>(list, list.WhereActivityTypeIs(EventType.OFFENTLIGID_IKKE_I_KERNE));

        //#3
        public static Func<List<KundeRegisterEvent>, CompositeRule<KundeRegisterEvent>> CoreNotValid =
            list => new CustomerStatusRule<KundeRegisterEvent>(list, list.WhereActivityTypeIs(EventType.KERNEID_IKKE_GYLDIG));

        //#4
        public static Func<List<KundeRegisterEvent>, CompositeRule<KundeRegisterEvent>> NotFoundActiveInCore =
          list => new CustomerStatusRule<KundeRegisterEvent>(list, list.WhereActivityTypeIs(EventType.IKKE_FUNNET_AKTIV_I_KERNE));


        /// <summary>
        /// Id ActivityStatus is BANKREGNR_IKKE_EIKABANK or FIKTIVID_IKKE_I_KERNE
        /// </summary>
        public static Func<List<KundeRegisterEvent>, CompositeRule<KundeRegisterEvent>> ItemsToIgnore =
            list => new IgnoreRule<KundeRegisterEvent>(list); // If item flag is BANKREGNR_IKKE_EIKABANK or FIKTIVID_IKKE_I_KERNE


        /// <summary>
        /// Is customer/account in archive
        /// </summary>
        public static Func<List<KundeRegisterEvent>, CompositeRule<KundeRegisterEvent>> InArchive =
            list => new ArchievedRule<KundeRegisterEvent>(list, list.Archieved());


        /// <summary>
        /// Is customer/account inactive
        /// </summary>
        public static Func<List<KundeRegisterEvent>, CompositeRule<KundeRegisterEvent>> InActive =
            list => new InactiveRule<KundeRegisterEvent>(list, list.Inactive());


        /// <summary>
        /// Is customer/account active
        /// </summary>
        public static Func<List<KundeRegisterEvent>, CompositeRule<KundeRegisterEvent>> IsActive =
         list => new InactiveRule<KundeRegisterEvent>(list, list.IsActive());


        /// <summary>
        /// If customer source is BREEG, DSF OR CRM
        /// </summary>
        public static Func<List<KundeRegisterEvent>, CompositeRule<KundeRegisterEvent>> CustomerIsInBreeg =
            list => new CustomerSourceIsBreeg<KundeRegisterEvent>(list, list.CustomerInCRMDsfBreeg());


        /// <summary>
        /// BliKundeStatus
        /// </summary>
        public static Func<List<KundeRegisterEvent>, CompositeRule<KundeRegisterEvent>> BecomeCustomerStatus =
           list => new BliKundeStatusRule<KundeRegisterEvent>(list, list.BecomeCustomerStatus());
    }
}
