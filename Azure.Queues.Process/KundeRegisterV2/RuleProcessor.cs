﻿using Common.Entities;
using System.Collections.Generic;
using System.Linq;

namespace Azure.Queues.Process
{
    public static partial class RuleProcessor
    {
        /// <summary>
        /// Run rules for cleanup in Kerne
        /// https://confluence.intra.eika.no/display/PTCDE/Kunderegister+%28KR%29++-+processing+Kerne+customer+data+per+customer+status
        /// </summary>
        /// <param name="items"></param>
        public static void Process(List<KundeRegisterEvent> items)
        {
            //Preload data
            CRMCache.Instance(items);
            var cache = items;

            #region Rules

            var validateCore = ValidateSource(items);
            var validateSourceId = ValidateSourceId(items);
            var itemsToIgnore = ItemsToIgnore(items);
            var validateBank = ValidateBanks(items);
            var validateStatus = ValidateStatus(items);
            var validateActivity = ValidateActivity(items);
            var bankCrmEqualsBankKr = BankCrmEqualsBankKr(items);
            var validateSourceIsMigrate = ValidateSourceIsMigrate(items);
            var validateHasOtherSources = ValidateHasOtherSources(items);
            var validateRecentActivities = ValidateRecentActivities(items);
            var validateActiveOrRelasjonOrAvventer = ValidateActiveOrRelasjonOrAvventer(items);
            var coreIdNotValid = CoreIdNotValid(items);
            var activeCoreIdNotFound = NotFoundActiveInCore(items);
            var inArchive = InArchive(items);
            var inActive = InActive(items);
            var customerIsInBreeg = CustomerIsInBreeg(items);
            var validateSSNEqualsToKr = ValidateSSNEqualsToKr(items);
            var validateBliKunde = BecomeCustomerStatus(items);
            var itemsWithNewStatus = RunRulesForCustomer(items);

            #endregion


            #region Grouped Rules


            //#IGNORE (Items with wrong status(Not new status))
            items = itemsWithNewStatus.NotPassedItems.AddToIgnore(cache);
            items = itemsToIgnore.PassedItems.AddToIgnore(cache);
            if (!items.Any())
                return;

            //Find customer
            var GroupRule = 
                validateCore
                .And(validateSourceId)
                .And(validateBank)
                .And(validateStatus);

            //Not found customer in previous Rule ? -->
            var NotFound = BankCrmEqualsBankKr(GroupRule.NotPassedItems)
                .And(validateCore)
                .And(validateStatus);

            //Did the previous rule find any customers? Merge them back into list.
            items = items.Merge(NotFound.PassedItems);

            //Customer status -->

            //Found with status --> #1
            var GroupRuleCustomerFoundStatus = ValidateActiveOrRelasjonOrAvventer(GroupRule.PassedItems);

            //Found with status --> #2
            var GroupRulePublicIdNotInCore = PublicIdNotInCore(GroupRule.PassedItems);

            //Found with status --> #3
            var GroupRuleCoreIdNotValid = CoreNotValid(GroupRule.PassedItems);           
            
            //Found with status --> #4
            var GroupRuleNotFoundActiveInCore = NotFoundActiveInCore(GroupRule.PassedItems);     
            
            //Customers not found --> #9
            var GroupRuleCustomerNotFound = GroupRule.NotPassedItems.Merge(NotFound.NotPassedItems);



            //########### #1 ############
            var customerList = GroupRuleCustomerFoundStatus.NotPassedItems;
            var customerIsActive = 
                InActive(GroupRuleCustomerFoundStatus.NotPassedItems)
                .And(ValidateHasOtherSources(customerList))
                .And(ValidateRecentActivities(customerList));

            customerIsActive.PassedItems.ForEach(p=> { UpdateCustomer(p, true); });      
            customerIsActive.NotPassedItems.ForEach(f => { UpdateCustomer(f, false); }); 



            //########### #2 ############
            var customerHasOtherSources = GroupRulePublicIdNotInCore
                .And(ValidateRecentActivities(customerList));

            customerHasOtherSources.PassedItems.ForEach(e=> { UpdateCustomerExistingCard(e);  }); //Update
            customerHasOtherSources.NotPassedItems.ForEach(e=> { DeleteCustomer(e); }); //Delete



            //########### #3 ############
            GroupRuleCoreIdNotValid.PassedItems.ForEach(e=> { UpdateWithInvalidPublicId(e); });



            //########### 4 ############
            GroupRuleNotFoundActiveInCore
                .And(ValidateHasOtherSources(GroupRuleNotFoundActiveInCore.PassedItems))
                .PassedItems.ForEach(e=> { UpdateCustomerEssentials(e); }); //Update

            GroupRuleNotFoundActiveInCore
                .And(InArchive(GroupRuleNotFoundActiveInCore.PassedItems))
                .And(InActive(GroupRuleNotFoundActiveInCore.PassedItems))
                .PassedItems.ForEach(e=> { /*Log*/ }); //Add to completed, Reason: NotFoundActiveInCore

            GroupRuleNotFoundActiveInCore
                 .And(InArchive(GroupRuleNotFoundActiveInCore.NotPassedItems))
                 .And(InActive(GroupRuleNotFoundActiveInCore.NotPassedItems))
                 .PassedItems.ForEach(e => { UpdateCustomerAltSource(e); });



            //########### 9 ############
            //CRM Customer found 
            var customersmigraring = 
                ValidateSourceIsMigrate(GroupRuleCustomerNotFound)
                .And(InActive(GroupRuleCustomerNotFound));

            var list = customersmigraring.NotPassedItems;

            var foundcustomers = 
                 BankCrmEqualsBankKr(list)
                .And(ValidateSSNEqualsToKr(list))
                .And(IsActive(list))
                .Or(InActive(list))
                .And(ValidateHasOtherSources(list))
                .And(BecomeCustomerStatus(list));


            //Check customer status (KR) of each items found
            var found = foundcustomers.PassedItems;

            //OFFENTLIGID_IKKE_I_KERNE 
            foundcustomers
                .And(Active(found))
                .PassedItems.ForEach(e=> { UpdateCustomerActive(e); });

            
            //OFFENTLIGID_IKKE_I_KERNE 
            var publicIdNotValid = foundcustomers
                .And(PublicIdNotInCore(found))
                .And(ValidateRecentActivities(found));


            //IKKE_FUNNET_AKTIV_I_KERNE
            foundcustomers
                .And(NotFoundActiveInCore(found))
                .And(ValidateRecentActivities(found))
                .PassedItems.ForEach(e=> { UpdateCustomerActive(e); } );

                //Dont apply
                foundcustomers
                    .And(NotFoundActiveInCore(found))
                    .And(ValidateRecentActivities(found))
                    .NotPassedItems.ForEach(e => { DeleteCustomer(e); });


            //OFFENTLIGID_IKKE_I_KERNE and no recent activities, delete -->
            foundcustomers
                .And(NotFoundActiveInCore(found))
                .And(ValidateRecentActivities(found))
                .NotPassedItems.ForEach(e => { DeleteCustomer(e); });


            //OFFENTLIGID_IKKE_GYLDIG 
            foundcustomers
                .And(CoreNotValid(found))
                .PassedItems.ForEach(e=> { UpdateWithInvalidPublicId(e); });

            #endregion
        }


        #region Customer functions
        //Functions below need to be grouped!!


        public static void Update(KundeRegisterEvent evt, CustomerConfig config)
        {

        }


        /// <summary>
        // True: Update Customer with all information provided from the event. In addtion set: Status = Active, Type = Customer 
        ///False: Update Customer with all information provided from the event. In addtion set: Type = Customer
        /// </summary>
        /// <param name="evt">KundeRegisterEvent evt</param>
        /// <param name="Status">Status of Active</param>
        public static void UpdateCustomer(KundeRegisterEvent evt, bool Status)
        {

        }


        //Update Customer data with personal info from the event. In addition: 
        //Type = Customer
        //Status = Active
        //Source = Kerne
        //SourceId = SourceId(KR)
        public static void UpdateCustomerActive(KundeRegisterEvent evt)
        {

        }


        /// <summary>
        /// Update Customer  with all personal information provided from the retrieved customer card. 
        // In addtion set:
        //- Source = found source
        //- KerneId = null;
        //- Status = Active
        /// </summary>
        /// <param name="evt"></param>
        /// <param name="Status"></param>
        public static void UpdateCustomerAltSource(KundeRegisterEvent evt)
        {

        }

        /// <summary>
        //Update Customer:
        //- Type = Potential Customer
        //- Source = CRM
        //- KerneId = null;
        //- Set as Active(if Inactive)
        /// </summary>
        /// <param name="evt"></param>
        public static void UpdateCustomerPontentialCustomer(KundeRegisterEvent evt)
        {

        }

        public static void UpdateCustomerExistingCard(KundeRegisterEvent evt)
        {

        }

        public static void DeleteCustomer(KundeRegisterEvent evt)
        {

        }


        /// <summary>
        //Update Customer personal information provided from the event. In addtion set:
        //Ugyldig Offentlig ID = Yes
        //Keep customer status and type as it is.
        /// </summary>
        /// <param name="evt"></param>
        public static void UpdateWithInvalidPublicId(KundeRegisterEvent evt)
        {

        }


        /// <summary>
        /// Do not change status, source and type.
        /// </summary>
        /// <param name="evt"></param>
        public static void UpdateCustomerEssentials(KundeRegisterEvent evt)
        {

        }

        #endregion
    }
}
