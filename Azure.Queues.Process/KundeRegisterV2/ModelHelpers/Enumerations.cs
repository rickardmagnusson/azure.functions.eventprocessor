﻿

namespace Azure.Queues.Process
{
  
    /// <summary>
    /// What type of customer should be inserted/created.
    /// Customer or PotentialCustomer
    /// </summary>
    public enum CustomerStatusType
    {
        Customer,
        PotentialCustomer
    }


    /// <summary>
    /// Status if Customer is Archieved, Inactive
    /// </summary>
    public enum Status
    {
        Archieved,
        Inactive
    }
}
