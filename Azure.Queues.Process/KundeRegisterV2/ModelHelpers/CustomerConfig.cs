﻿
namespace Azure.Queues.Process
{
    /// <summary>
    /// Helper for RuleEngine 
    /// </summary>
    public class CustomerConfig
    {
        public CustomerStatusType StatusType { get; set; }
        public Status Status { get; set; }
        public string Source { get; set; }
        public string SourceId { get; set; }
        public string KerneId { get; set; }
        public bool PublicId { get; set; }
    }
}
