﻿

namespace Azure.Queues.Process
{
    /// <summary>
    /// Helper for RuleEngine 
    /// </summary>
    public class SMS
    {
        public string contactid { get; set; }
        public string scheduledend { get; set; }
        public string _regardingobjectid_value { get; set; }
    }
}
