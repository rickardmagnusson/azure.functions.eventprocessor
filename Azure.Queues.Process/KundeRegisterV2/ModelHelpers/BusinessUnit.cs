﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Azure.Queues.Process
{
    /// <summary>
    /// Helper for RuleEngine 
    /// </summary>
    public class BusinessUnit
    {
        public string businessunitid { get; set; }
        public string divisionname { get; set; }
    }
}
