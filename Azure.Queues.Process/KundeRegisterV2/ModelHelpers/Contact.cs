﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Azure.Queues.Process
{
    public class Contact
    {
        public Guid contactid { get; set; }

        public int territorycode { get; set; }
        public int statuscode { get; set; }
        public int statecode { get; set; }

        //SSN
        public string governmentid { get; set; }

        public int customertypecode { get; set; }
        public string _owningbusinessunit_value { get; set; }



        //SourceId
        public string cap_tradexsourceid { get; set; }
        public string cap_nice2sourceid { get; set; }
        public string cap_nicesourceid { get; set; }
        public string cap_sourceid { get; set; }
        public string cap_banqsoftsourceid { get; set; }


        //Activities
        public DateTime cap_eventdate { get; set; }
        public Guid cap_eventid { get; set; }
        public DateTime cap_lastcasecreatedon { get; set; }
        public int cap_lastcasecreatedon_state { get; set; }

        public DateTime cap_lastactivitycreatedon { get; set; }
        public int cap_lastactivitycreatedon_state { get; set; }
        
        public DateTime modifiedon { get; set; }

        public DateTime cap_lastcasecreatedon_date { get; set; }

        public DateTime cap_lastoptycreatedon_date { get; set; }
        public int cap_lastoptycreatedon_state { get; set; }
        
        public DateTime cap_lastactivitycreatedon_date { get; set; }
        public DateTime createdon { get; set; }
        public string cap_lastoptycreatedon { get; set; }

        public string _cap_departmentid_value { get; set; }
        

        //Name
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string fullname { get; set; }

        //Email
        public string emailaddress1 { get; set; }

        public string yomifirstname { get; set; }
        public string yomifullname { get; set; }
        public string yomilastname { get; set; }


        //Phone
        public string mobilephone { get; set; }

       
        //Address
        public string address1_addressid { get; set; }
        public string address1_postalcode { get; set; }
        public string address1_city { get; set; }
        public string telephone1 { get; set; }
        public string address1_telephone1 { get; set; }
        public string address1_line1 { get; set; }
        public string address1_country { get; set; }
        public string address1_stateorprovince { get; set; }      

    }
}
