﻿using System;

namespace Azure.Queues.Process.KundeRegisterV2.ModelHelpers
{
    //SMS
    public class ContactFax
    {
        public DateTime scheduledend { get; set; }
        public string _regardingobjectid_value { get; set; }
        public string activityid { get; set; }

        public string Query
        {
            get {
                return "/contacts({contact_id})$select=fullname,governmentid&$expand=Contact_Faxes($select=scheduledend)";
            }
        }
    }
}
