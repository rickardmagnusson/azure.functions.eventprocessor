﻿using Common.Entities;
using System.Collections.Generic;


namespace Azure.Queues.Process
{
    /// <summary>
    /// Load all instances of queries to database
    /// when loading the KundeRegisterEvents
    /// </summary>
    public class CRMCache
    {
        static CRMCache _instance;
        private CRMCache() { }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static CRMCache Instance()
        {
            //Need an initialized instance here to grab the lists.
            return _instance;
        }


        /// <summary>
        /// Creates a new instance of CRMCache.
        /// </summary>
        /// <returns>CRMCache instance</returns>
        public static CRMCache Instance(List<KundeRegisterEvent> list)
        {
            if (_instance == null)
            {
                _instance = new CRMCache();
                _instance.Contacts = RuleHelper.LoadContacts(list);
                _instance.SMS = RuleHelper.LoadSMS(list);
                _instance.BusinessUnits = RuleHelper.LoadBusinessUnits(list);
            }
                
            return _instance;
        }

        /// <summary>
        /// A list of BusinessUnits from cache
        /// </summary>
        public List<BusinessUnit> BusinessUnits { get; private set; }

        /// <summary>
        /// A list of Contacts from cache
        /// </summary>
        public List<CrmCustomer> Contacts { get; private set; }

        /// <summary>
        /// A list of recent activities in SMS(FAX)
        /// </summary>
        public List<SMS> SMS { get; private set; }
    }
}
