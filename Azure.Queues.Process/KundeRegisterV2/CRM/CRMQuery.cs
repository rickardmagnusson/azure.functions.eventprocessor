﻿
using System;
using System.Collections.Generic;
using API = Crm.Services.Extensions.OrganizationServiceHelper;

namespace Azure.Queues.Process
{
    /// <summary>
    /// Contains methods to query CRM.
    /// </summary>
    public class CRMQuery
    {
        private string crmQuery = string.Empty;

        public CRMQuery()
        {
            //Reset query each new instance
            this.crmQuery = string.Empty;
        }


        /// <summary>
        /// Instances CRMQueries and creates add the query.
        /// </summary>
        /// <param name="query">Add a query to CRMQueries</param>
        public CRMQuery(string query)
        {
            this.crmQuery = query;
        }


        /// <summary>
        /// Executes the current request.
        /// </summary>
        /// <typeparam name="T">Type to return</typeparam>
        /// <returns>A List<T> of type T</returns>
        public List<T> ExecuteQuery<T>()
        {
            if (string.IsNullOrEmpty(this.crmQuery))
                throw new Exception("Nothing to execute. You need to specify a query to execute!");

            var task = API.ToEntity<T>(crmQuery);
            task.Wait(); //Force webapi to finish.
#if DEBUG
            Console.WriteLine(crmQuery);
#endif
            return task.Result;
        }


        /// <summary>
        /// Executes the current request.
        /// </summary>
        /// <typeparam name="T">Type to return</typeparam>
        /// <returns>A List<T> of type T</returns>
        public List<T> ExecuteQuery<T>(string oftype)
        {
            if (string.IsNullOrEmpty(this.crmQuery))
                throw new Exception("Nothing to execute. You need to specify a query to execute!");

            var task = API.ToEntity<T>(crmQuery, oftype);
            task.Wait(); //Force webapi to finish.
            Console.WriteLine(crmQuery);
            return task.Result;
        }


        /// <summary>
        /// Executes the current request.
        /// </summary>
        /// <typeparam name="T">Type to return</typeparam>
        /// <param name="builder">QueryBuilder query</param>
        /// <returns>A List<T> of type T</returns>
        public List<T> ExecuteQuery<T>(QueryBuilder builder)
        {
            if (string.IsNullOrEmpty(builder.ToString()))
                throw new Exception("Nothing to execute. You need to specify a query to execute!");

            crmQuery = builder.ToString();

            return ExecuteQuery<T>();
        }


        public List<T> GetSMSActivity<T>(string customerid)
        {
            crmQuery = QueryBuilder.AddEntity($"/contacts({customerid})?$select=fullname,governmentid&$expand=Contact_Faxes($select=scheduledend)").ToString();
            return ExecuteQuery<T>("Contact_Faxes");
        }


        /// <summary>
        /// Predefiend query to get contacts details from ssn (Social security number)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="ssn">Contact social security number (SSN)</param>
        /// <returns>A List<T> of type T</returns>
        public List<T> GetBySSN<T>(string ssn)
        {
            //Get default fields
            crmQuery = QueryBuilder
                      .AddEntity("contacts")
                      .AddField("fullname")
                      .AddRange(new string[] {
                            "cap_lastactivitycreatedon_date",
                            "cap_lastcasecreatedon_date",
                            "cap_sourceid",
                            "cap_nicesourceid",
                            "cap_nice2sourceid",
                            "cap_banqsoftsourceid",
                            "cap_tradexsourceid",
                            "contactid",
                            "statuscode",
                            "statecode",
                            "territorycode",
                            "governmentid"
                      }).Where("governmentid").EqualsTo($"{ssn}")
                      .ToString();

            return ExecuteQuery<T>();
        }


        /// <summary>
        /// Get all fields required for a contact.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="ssn"></param>
        /// <returns></returns>
        public List<T> GetCustomerInfo<T>(string ssn)
        {
            //Get default fields
            crmQuery = QueryBuilder
                    .AddEntity("contacts")
                    .AddField("fullname")
                    .AddRange(new string[] {
                            "cap_lastactivitycreatedon_date",
                            "cap_lastcasecreatedon_date",
                            "cap_sourceid",
                            "cap_nicesourceid",
                            "cap_nice2sourceid",
                            "cap_banqsoftsourceid",
                            "cap_tradexsourceid",
                            "contactid",
                            "statuscode",
                            "statecode",
                            "territorycode",
                            "governmentid"
                    }).Where("governmentid").EqualsTo($"{ssn}")
                    .ToString();

            return ExecuteQuery<T>();
        }


        /// <summary>
        /// Get all fields required for an account.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="ssn"></param>
        /// <returns></returns>
        public List<T> GetAccountInfo<T>(string ssn)
        {
            //Get default fields
            crmQuery = QueryBuilder
                    .AddEntity("accounts")
                    .AddField("fullname")
                    .AddRange(new string[] {
                            "fax",
                            "archive",
                            "contactid",
                            "statuscode",
                            "statecode",
                            "governmentid"
                    }).Where("governmentid").EqualsTo($"{ssn}")
                    .ToString();

            return ExecuteQuery<T>();
        }


        /// <summary>
        /// Get all banks
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public List<T> BusinessUnits<T>()
        {
            crmQuery = QueryBuilder
                .AddEntity("businessunits")
                .AddField("businessunitid")
                .AddField("divisionname")
                .ToString();

            return ExecuteQuery<T>();
        }


        /// <summary>
        /// Get the scheduledend in fax for (SMS)*
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="contactid"></param>
        /// <returns></returns>
        public List<T> SMS<T>(string contactid)
        {
            ///contacts(88c7da47-de38-e911-a859-000d3a290633)?$select=contactid&$expand=Contact_Faxes($select=_regardingobjectid_value,scheduledend)
            ///contacts(88c7da47-de38-e911-a859-000d3a290633)?$select=contactid&$expand=Contact_Faxes($select=scheduledend)
            crmQuery = $"/contacts({contactid})?$select=contactid&$expand=Contact_Faxes($select=scheduledend,_regardingobjectid_value)";

            return ExecuteQuery<T>("Contact_Faxes");
        }
    }
}
