﻿using Azure.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Azure.Queues.Process
{
    public class LogItem
    {
        public static void Add(string message)
        {
            SingletonLogger.Instance().Add(message);
        }
    }
}
