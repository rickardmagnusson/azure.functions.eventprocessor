﻿using System;
using System.Collections.Generic;
using Common;
using Common.Entities;
using Common.Enums;
using Common.Helpers;
using Common.Logging;
using Common.Models;

namespace Azure.Queues.Process.ProcessHelpers
{
    public class EtlDatabaseHelper
    {
        private static readonly MsmqLogger Log =  MsmqLogger.CreateLogger("etl-database-helper");


        public static void SetMessagesToIgnore(List<MsmqMessageInbox> items, string reason, DataWriter dataWriter)
        {
            foreach (var i in items) i.ErrorMessage = reason;
            UpdateMessagesInDatabaseToIgnore(items, dataWriter);
        }


        public static void UpdateMessagesInDatabase(List<ProcessedCustomerResponse> processedEventResponses, DataWriter dataWriter)
        {
            foreach (var response in processedEventResponses)
            {
                if (response.IsIgnored)
                {
                    SetInboxStatusForEventRecord(response.MsmqInboxId, MessageInboxStatus.Ignored, response.ErrorMessage, dataWriter);
                    continue;
                }
                SetInboxStatusForEventRecord(response.MsmqInboxId, response.Success ? MessageInboxStatus.Completed : MessageInboxStatus.Failed, response.ErrorMessage, dataWriter);
            }
        }
        public static void UpdateDuplicateMessagesInDatabaseToIgnore(List<MsmqMessageInbox> ignoredMessages, DataWriter dataWriter)
        {
            var msg = "Skipped due to duplicate change in the same batch. Oldest event ignored.";
            foreach (var item in ignoredMessages)
            {
                Log.Warn(msg);
                dataWriter.SetStatusForMessage(item.Id, MessageInboxStatus.Ignored, msg);
            }
        }
        public static void UpdateMessagesInDatabaseToIgnore(List<MsmqMessageInbox> ignoredMessages, DataWriter dataWriter)
        {
            foreach (var item in ignoredMessages)
            {
                Log.Warn(item.ErrorMessage);
                dataWriter.SetStatusForMessage(item.Id, MessageInboxStatus.Ignored, item.ErrorMessage);
            }
        }

        public static void SetInboxStatusForEventRecord(long id, MessageInboxStatus status, string errorMessage, DataWriter dataWriter)
        {
            dataWriter.SetStatusForMessage(id, status, errorMessage);
        }

        public static MsmqCompletedMessages GetAlternateCustomerCard(KundeRegisterEvent evt, ProcessedCustomerResponse response, DataReader etlDb, bool productCompanyCustomerCard)
        {
            try
            {
                if (productCompanyCustomerCard && evt.Source != Source.NicePerson) return null;

                var alternativeSources = productCompanyCustomerCard
                    ? new List<Source> { Source.NicePerson }
                    : GetAlternativeSourcePrioList(evt.Source);

                if (!evt.DistributorBankNumber.Equals("9811"))
                {
                    alternativeSources.Remove(Source.BanQsoft);
                }

                return etlDb.GetAlternativeCustomerCard(evt, alternativeSources, productCompanyCustomerCard);
            }
            catch (Exception ex)
            {
                LogHelper.Error(Util.GetMethodName(), ex, response, Log);
                return null;
            }
        }

        private static List<Source> GetAlternativeSourcePrioList(Source source)
        {
            var list = new List<Source>();

            foreach (var item in SourceSpecific.PrioList.Keys)
            {
                if (item == source) continue;
                
                list.Add(item);
            }

            return list;
        }

    }
}
