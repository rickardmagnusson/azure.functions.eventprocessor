﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Azure.Logging;
using Common.Adapters;
using Common.Entities;
using Common.Helpers;
using Serilog;
using Serilog.Core;
using Serilog.Events;

namespace Common.Initializer
{

    /// <summary>
    /// This class need a massive cleanup. 
    /// </summary>
    public class Initialize
    {
        #region variables

        public static Dictionary<string, string> EtlConfig = new Dictionary<string, string>();
        public static Dictionary<string, string> EtlCrmSetting = new Dictionary<string, string>();
        public static string EtlConnStr = string.Empty;
        public static string CrmDbConnStr = string.Empty;
        public static string ServiceUrl = string.Empty;
        public static string MsmqMessageInboxConnectionString = string.Empty;
        public static string CrmServiceConnectionString = string.Empty;

        public static Dictionary<string, Guid> ConnectionRoleByType = new Dictionary<string, Guid>();
        public static Dictionary<string, string> EtlConfigBankNos = new Dictionary<string, string>();
        public static Dictionary<string, string> ProductCodesByBank = new Dictionary<string, string>();
        public static Dictionary<string, string> KundehendelseAppNames = new Dictionary<string, string>();
        private static SingletonLogger log = SingletonLogger.Instance();

        private static string _crmConnStr = string.Empty;
        private static string _tableName;
        private static LoggingLevelSwitch _levelSwitch;
        private static StringBuilder Logger = new StringBuilder();


        #endregion

        #region public functions


        public static async Task<string> InitSettings()
        {
            await Task.Run(() =>
            {
                LoadSettings();
            });

            return $"{Logger.ToString()}";
        }


        /// <summary>
        /// This is halfed cleanup, most moved into ConfigurationExtensions
        /// </summary>
        public static void LoadSettings()
        {
            Logger.AppendLine("Started LoadSettings");
            _tableName = Environment.GetEnvironmentVariable("LogTable");
            Logger.AppendLine($"_tableName: {_tableName}");
            //XmlConfigurator.Configure(new FileInfo("log4net.config"));
            EtlConnStr = Environment.GetEnvironmentVariable("crm-sqldb-connectionstring-secret");

            SetupSerilog(EtlConnStr);
            //SetupLog4Net(EtlConnStr);

            //Note used
            MsmqMessageInboxConnectionString = Environment.GetEnvironmentVariable("CRM_ETLEntities");
            Logger.AppendLine($"MsmqMessageInboxConnectionString: {MsmqMessageInboxConnectionString}");

            LoadConfigSettings();
            LoadCrmSettings();

            ServiceUrl = InitializeHelper.SetServiceUrl(EtlCrmSetting);
            CrmDbConnStr = EtlCrmSetting["CrmSqlConnectionString"];
            CrmServiceConnectionString = EtlCrmSetting["CrmConnectionString"];
            Logger.AppendLine("Ended LoadSettings");
        }


        public static void LoadConfigSettings()
        {
            Console.WriteLine("Begining to Load Config Settings.....");
            try
            {
                EtlConfig = InitializeHelper.LoadFromDb("Config");
                InitializeHelper.IntializeEtlVariables(EtlConfig);
                Log.Debug("Config settings loaded");
            }
            catch (Exception ex)
            {
                Logger.AppendLine("Unable to load config setting");
                throw ex;
            }
        }


        public static void LoadCrmSettings()
        {
            Logger.AppendLine("Begining to load CRM settings...");
            EtlCrmSetting = InitializeHelper.LoadFromDb("CRM");
        }


        public static void LoadProductCodes()
        {
            ProductCodesByBank = InitializeHelper.LoadFromDb("ProductCodes");
        }


        public static void LoadAppNames()
        {
            KundehendelseAppNames = InitializeHelper.LoadFromDb("KundehendelseAppName");
        }


        public static Dictionary<string, string> GetInsuranceRoleNames()
        {
            try
            {
                return InitializeHelper.LoadFromDb("InsuranceRole");
            }
            catch (Exception)
            {
                return new Dictionary<string, string>();
            }
        }


        public static void GetConnectionRoleIds(QueryExpressionHelper query)
        {
            try
            {
                var connectionRoleNames = InitializeHelper.LoadFromDb("ConnectionRole");
                ConnectionRoleByType = query.GetConnectionRoleIds(connectionRoleNames);
            }
            catch (Exception)
            {
                ConnectionRoleByType = new Dictionary<string, Guid>();
            }
        }


        public static void UpdateVendorConfigValues(QueryCrm query)
        {
            GetBankNumbers();
            GetConnectionRoleIds(query);
        }


        public static void GetConnectionRoleIds(QueryCrm query)
        {
            try
            {
                var connectionRoleNames = InitializeHelper.LoadFromDb("ConnectionRole");
                ConnectionRoleByType = query.GetConnectionRoleIds(connectionRoleNames);
            }
            catch (Exception)
            {
                ConnectionRoleByType = new Dictionary<string, Guid>();
            }
        }


        public static void GetBankNumbers()
        {
            try
            {
                EtlConfigBankNos = InitializeHelper.LoadFromDb("BankNo");
            }
            catch (Exception)
            {
                EtlConfigBankNos = new Dictionary<string, string>();
            }
        }


        public static List<string> GetBankToRunForKundeHendelse()
        {
            var result = new List<string>();
            try
            {
                EtlCrmSetting = InitializeHelper.LoadFromDb("CRM");
                foreach (var bankNo in EtlCrmSetting["RunKundeHendelseForBank"].Split(','))
                {
                    if (!result.Contains(bankNo))
                    {
                        log.Add($"Adding: {bankNo}");
                        result.Add(bankNo);
                    }
                }
            }
            catch (Exception ex)
            {
               Logger.AppendLine($"Error while processing {Util.GetMethodName()}. Error message: {ManagerHelper.GetExceptionMessage(ex)}");
            }
            return result;
        }


        public static List<string> GetBankToRunForSakHendelse()
        {
            var result = new List<string>();
            try
            {
                EtlCrmSetting = InitializeHelper.LoadFromDb("CRM");
                foreach (var bankNo in EtlCrmSetting["RunSakHendelseForBank"].Split(','))
                {
                    if (!result.Contains(bankNo)) result.Add(bankNo);
                }
            }
            catch (Exception ex)
            {
                Logger.AppendLine($"Error while processing {Util.GetMethodName()}. Error message: {ManagerHelper.GetExceptionMessage(ex)}");
            }
            return result;
        }

        public static void InitSerializers(EventSerializerCollection serializers)
        {
            serializers = new EventSerializerCollection(
                typeof(EventMaster), 
                typeof(DataWarehouseTempEvent), 
                typeof(CustomerEvent),
                typeof(KundeRegisterEvent),
                typeof(KundeHendelseEvent),
                typeof(KundeHendelse),
                typeof(SakHendelse)
            );
        }




        #endregion

        #region Private Methods


        public static Dictionary<string, string> _logLevelConfig;

        public static void InitLogLevelFromDb()
        {
            if (!_logLevelConfig.TryGetValue(_tableName, out var serilogLevel))
                Log.Error($"Key: {_tableName} not found in _logLevelConfig");

            switch (serilogLevel?.ToLower())
            {
                case "debug":
                    _levelSwitch.MinimumLevel = LogEventLevel.Debug;
                    break;
                case "info":
                case "information":
                    _levelSwitch.MinimumLevel = LogEventLevel.Information;
                    break;
                case "warn":
                case "warning":
                    _levelSwitch.MinimumLevel = LogEventLevel.Warning;
                    break;
                case "error":
                    _levelSwitch.MinimumLevel = LogEventLevel.Error;
                    break;
                case "fatal":
                    _levelSwitch.MinimumLevel = LogEventLevel.Fatal;
                    break;
                default:
                    _levelSwitch.MinimumLevel = LogEventLevel.Verbose;
                    break;
            }
        }

        
        private static void SetupSerilog(string connectionString)
        {
            _levelSwitch = new LoggingLevelSwitch(LogEventLevel.Verbose);

            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.ControlledBy(_levelSwitch)
                .CreateLogger();
        }
        
     

        #endregion

    }
}
