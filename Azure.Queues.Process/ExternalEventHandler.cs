﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common;
using Common.Adapters;
using Common.Enums;
using Common.Helpers;
using Common.Logging;
using Common.Initializer;
using Azure.Queues.Process.Kundehendelse;
using Azure.Queues.Process.Kunderegister;
using Azure.Queues.Process.ProcessHelpers;
using ProcessManager.Sakhendelse;
using System.Threading.Tasks;
using System.Text;
using Common.Models;
using Azure.Queues.Process;


namespace ProcessManager
{
    public class ExternalEventHandler
    {
        //Used for function logging
        private StringBuilder Logger;

        private static ExternalEventHandler _instance;
        private static MsmqLogger Log = MsmqLogger.CreateLogger("external-event-handler");
        private const int BatchSize = 100;
        private DataReader _dataReader;
        private DataWriter _dataWriter;
        private Guid _sessionId;
        private QueryCrm _query;
        private bool _processing;


        #region Init Methods
        private ExternalEventHandler()
        {
            Logger = new StringBuilder();
            _dataReader = new DataReader(Logger);
            _dataWriter = new DataWriter(Logger);
            _processing = false;
        }


        /// <summary>
        /// Runs the same batch thrue all types in order of EventMessageType.
        /// </summary>
        /// <returns>Logger string</returns>
        public async Task<string> RunProcessBatch()
        {
            string evt = "OK";
            try
            {
                await Task.Run(() => {
                    var batch = GetBatch();

                    var KR = batch.Where(t => t.EventType == 4).ToList();
                    var KH = batch.Where(t => t.EventType == 5).ToList();
                    var SH = batch.Where(t => t.EventType == 6).ToList();

                    var kr = ProcessEventBatch(KR, EventMessageType.KundeRegisterEvent); kr.Wait();
                    var kh = ProcessEventBatch(KH, EventMessageType.KundeHendelseEvent); kh.Wait();
                    var sh = ProcessEventBatch(SH, EventMessageType.SakHendelseEvent);   sh.Wait();
                });

                #region Logged run


                //if (!batch.Any())
                //    return "Nothing to run(Batch is empty)";

                //if (KR.Any())
                //    evt = await ProcessEventBatch(KR, EventMessageType.KundeRegisterEvent);

                //if (evt != "OK")
                //    Logger.AppendLine($"Error processing KundeRegisterEvent, {evt}");

                //if (KH.Any())
                //    evt = await ProcessEventBatch(KH, EventMessageType.KundeHendelseEvent);

                //if (evt != "OK")
                //    Logger.AppendLine($"Error processing KundeHendelseEvent, {evt}");

                //if (SH.Any())
                //    evt = await ProcessEventBatch(SH, EventMessageType.SakHendelseEvent);

                //if (evt != "OK")
                //    Logger.AppendLine($"Error processing SakHendelseEvent, {evt}");

                #endregion
            }
            catch (Exception e)
            {
                evt = $"{e.Message} | Logger: {Logger.ToString()} ErrorMessage: {evt}";
            }

            return $"RunProcessBatch run complete: {Logger.ToString()}";
        }


        /// <summary>
        /// Get the lastest batch()
        /// </summary>
        /// <returns></returns>
        public List<MsmqMessageInbox> GetBatch()
        {
            return _dataReader.GetNextRange(BatchSize);
        }


        public static ExternalEventHandler GetInstance(Guid sessionId)
        {
            if (_instance == null)
                _instance = new ExternalEventHandler {_sessionId = sessionId};

            ValidateSession(sessionId, "The instance object of the CrmExternalEventHandler is currently used by another session.");

            return _instance;
        }

        #endregion


        /// <summary>
        /// Extension to let same batch run thrue each event
        /// </summary>
        /// <param name="batch"></param>
        /// <param name="eventType"></param>
        private async Task<string> ProcessEventBatch(List<MsmqMessageInbox> batch, EventMessageType eventType)
        {
            _processing = true;

            try
            {
                Logger.AppendLine($"Logging: ProcessEventBatch ({eventType})");

                if (batch.Any())
                {
                    await Task.Run(() => {
                        Logger.AppendLine($"Logging: ProcessEventBatch on({eventType}) (Batch {batch.Count})");
                        var inboxMessages = CheckDuplicates(batch);
                        batch = inboxMessages.ToProcess;
                        Logger.AppendLine($"Batch: { batch.Count}");
                        EtlDatabaseHelper.UpdateDuplicateMessagesInDatabaseToIgnore(inboxMessages.Duplicates, _dataWriter);
                        Logger.AppendLine($"Added batch with size: {batch.Count}");
                        Log.Info($"Added batch with size: {batch.Count}");
                        //---->
                        ProcessDataBatch(batch, eventType);
                    });
                }
            }
            catch (Exception e)
            {
                Logger.AppendLine($"Unexpected exception when processing batch of events. ({eventType}) {e}");
                Log.Error("Unexpected exception when processing batch of events.", e);
                return $"FAILED: ({eventType}, {e})";
            }
            finally
            {
                _processing = false;
            }

            return "OK";

        }


        public void ProcessBatch(EventMessageType eventType)
        {
            _processing = true;

            try
            {
                var batch = _dataReader.GetNextRange(BatchSize, eventType);

                if (batch.Any())
                {
                    var inboxMessages = CheckDuplicates(batch);
                    batch = inboxMessages.ToProcess;

                    EtlDatabaseHelper.UpdateDuplicateMessagesInDatabaseToIgnore(inboxMessages.Duplicates, _dataWriter);

                    Log.Info($"Added batch with size: {batch.Count()}");
                    Logger.AppendLine($"Added batch with size: {batch.Count()}");

                    //---->
                    ProcessDataBatch(batch, eventType);
                }
            }
            catch (Exception e)
            {
                Log.Error("Unexpected exception when processing batch of events.", e);
            }
            finally
            {
                _processing = false;
            }
        }


        private void ProcessDataBatch(List<MsmqMessageInbox> items, EventMessageType eventType)
        {
            LogProcessingBatch(items, eventType);

            Logger.AppendLine($"ProcessDataBatch for {eventType}");

            _query = new QueryCrm(Initialize.EtlCrmSetting);

            switch (eventType)
            {
                case EventMessageType.KundeRegisterEvent:
                    KundeRegisterProcess.ProcessMessages(items, _dataReader, _dataWriter, _query);
                    break;
                case EventMessageType.KundeHendelseEvent:
                    KundeHendelseProcess.ProcessMessages(items, _dataWriter, _query);
                    break;
                case EventMessageType.SakHendelseEvent:
                    SakHendelseProcess.ProcessMessages(items, _dataWriter, _query);
                    break;
                default:
                    Log.Error($"{Util.GetMethodName()}: eventtype is not valid: '{eventType}'");
                    break;
            }

            _query = null;
            Logger.AppendLine($"Done ProcessDataBatch for {eventType}");
            Log.Info("Done!");
        }


        #region Helpers
        

        private InboxMessages CheckDuplicates(List<MsmqMessageInbox> items)
        {
            items = items.OrderByDescending(e => e.Id).ToList();

            var newItemsList = new List<MsmqMessageInbox>();
            var duplicates = new List<MsmqMessageInbox>();
            var addedList = new List<string>();
            foreach (var e in items)
            {
                var curr = $"{e.CustomerNumber}_{e.BankId}_{e.EventType}_{e.ActionType}_{e.Source}";
                if (e.EventType == 5 || e.EventType == 6 || !addedList.Contains(curr))
                {
                    newItemsList.Add(e);
                    addedList.Add(curr);
                }
                else
                {
                    Logger.AppendLine($"A duplicate is found in the duplicate check{e.SourceId}");
                    Log.Debug($"A duplicate is found in the duplicate check: {e.SourceId}");
                    duplicates.Add(e);
                }
            }

            items = newItemsList.OrderBy(e => e.Id).ToList();

            return new InboxMessages
            {
                ToProcess = items,
                Duplicates = duplicates
            };
        }
        private void LogProcessingBatch(List<MsmqMessageInbox> items, EventMessageType eventType)
        {
            //Ignored due to ApplicationInsights
            // LogHelper.LogWithAdditionalMessage($"Processing batch '{items.Min(i => i.Id)} - {items.Max(i => i.Id)}'...", 
            //    $"{items.Count} -> {eventType}", LogLevel.Info, Log);
        }


        private static void ValidateSession(Guid sessionId, string messageOnFail)
        {
            if (sessionId != _instance._sessionId)
            {
                throw new InvalidOperationException(messageOnFail);
            }
        }

        #endregion

    }
}
