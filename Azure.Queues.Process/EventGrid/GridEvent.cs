﻿using System;
using Newtonsoft.Json;

namespace Azure.Queues.Process.EventGrid
{
    /// <summary>
    /// EventGrid message. Used in Azure function 
    /// to send messages to eventgridtrigger object.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class GridEvent<T> where T : class
    {
        [JsonProperty(PropertyName = "dataVersion")]
        public string DataVersion { get; set; }

        [JsonProperty(PropertyName = "subject")]
        public string Subject { get; set; }

        [JsonProperty(PropertyName = "eventType")]
        public string EventType { get; set; }

        [JsonProperty(PropertyName = "eventTime")]
        public DateTime EventTime { get; set; }

        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }
    }
}
