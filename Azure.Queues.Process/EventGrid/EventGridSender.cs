﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Azure.Queues.Process.EventGrid
{
    /// <summary>
    /// Eventgrid sender
    /// </summary>
    public class EventGridSender
    {
        /// <summary>
        /// Send a GridEvent to an Azure Function
        /// </summary>
        /// <param name="subject">The message to send (For eg. Start or Restart)</param>
        /// <returns></returns>
        public static async Task<HttpResponseMessage> Send(string subject)
        {
            var endpoint  = ConfigurationManager.AppSettings["crm-grid-eventprocess-endpoint"];
            var eventType = ConfigurationManager.AppSettings["crm-grid-eventprocess-eventtype"];
            var aegsaskey = ConfigurationManager.AppSettings["crm-grid-eventprocess-secret"];

            var httpClient = new HttpClient();
            var eventList = new List<GridEvent<object>>();

            var gridEvent = new GridEvent<object>
            {
                Subject = subject,
                EventType = eventType,
                EventTime = DateTime.UtcNow,
                Id = Guid.NewGuid().ToString(),
                DataVersion = "1.0"
            };

            eventList.Add(gridEvent);
            var json = JsonConvert.SerializeObject(eventList);

            httpClient.DefaultRequestHeaders.Add("aeg-sas-key", aegsaskey);
            httpClient.DefaultRequestHeaders.UserAgent.ParseAdd("eventgridtriggerclient");

            var request = new HttpRequestMessage(HttpMethod.Post, endpoint)
            {
                Content = new StringContent(json, Encoding.UTF8, "application/json")
            };

            return await httpClient.SendAsync(request);
        }
    }
}
