﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common.Adapters;
using Common.Entities;
using Common.Initializer;
using Common.Logging;
using Common.Models;
using Azure.Queues.Process.ProcessHelpers;
using System.Text;
using Azure.Logging;

namespace Azure.Queues.Process.Kundehendelse
{
    public class KundeHendelseProcess
    {
        private static MsmqLogger Log = MsmqLogger.CreateLogger("kundehendelse-process");
        private static SingletonLogger log = SingletonLogger.Instance();
        static StringBuilder Logger;


        public static void ProcessMessages(List<MsmqMessageInbox> items, DataWriter dataWriter, QueryCrm query)
        {
            Logger = dataWriter.Logger;
            var oddCases = items.Where(m =>
                    !m.Source.Equals("TRADEX", StringComparison.CurrentCultureIgnoreCase) &&
                    !m.Source.Equals("NICE_SKADE", StringComparison.CurrentCultureIgnoreCase)
            ).ToList();

            if (oddCases.Count > 0) Logger.AppendLine($"An unexpected source of KundeHendelse arrived: '{oddCases[0].Source}'");

            var oddCaseIds = oddCases.Select(e => e.Id).ToList();
            items = items.Where(e => !oddCaseIds.Contains(e.Id)).ToList();

            EtlDatabaseHelper.SetMessagesToIgnore(oddCases, "Source is not ready to be processed yet.", dataWriter);
            var processedEventResponses = ProcessEvents(items, query);
            EtlDatabaseHelper.UpdateMessagesInDatabase(processedEventResponses, dataWriter);
        }

        
        #region Private Methods
        private static List<ProcessedCustomerResponse> ProcessEvents(List<MsmqMessageInbox> items, QueryCrm queryToUse)
        {

            var result = new List<ProcessedCustomerResponse>();

            using (var query = new QueryExpressionHelper())
            {
                var validBanksToRun = Initialize.GetBankToRunForKundeHendelse();

                Initialize.LoadProductCodes();
                Initialize.LoadAppNames();

                foreach (var item in items)
                {
                    var response = new ProcessedCustomerResponse { MsmqInboxId = item.Id, RetryCount = item.RetryCount };
                    var evt = SerializeHelper.DeserializeEventMessageContent<KundeHendelse>(item.MessageContent);

                    if (validBanksToRun.Contains(evt.Brukersted))
                    {
                        KundeHendelseManager.HandleKundeHendelseEvent(evt, response, query);
                    }
                    else
                    {
                        response.ErrorMessage = $"Skipping KundeHendelse for bank '{evt.Brukersted}.'";
                        response.IsIgnored = true;
                    }

                    result.Add(response);
                }
            }
            return result;
        }

        #endregion
    }
}
