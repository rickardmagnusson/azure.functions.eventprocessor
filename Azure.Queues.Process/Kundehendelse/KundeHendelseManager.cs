﻿using System;
using Common;
using Common.Adapters;
using Common.Entities;
using Common.Enums;
using Common.Initializer;
using Common.Logging;
using Azure.Queues.Process.ProcessEntityHelper;
using Azure.Queues.Process.ProcessHelpers;
using System.Text;
using Azure.Logging;

namespace Azure.Queues.Process.Kundehendelse
{
    public class KundeHendelseManager
    {
        #region Private Members

        private static readonly MsmqLogger Log = MsmqLogger.CreateLogger("kundehendelse-manager");

        #endregion

        #region Public Methods


        private static SingletonLogger log = SingletonLogger.Instance();



        public static void HandleKundeHendelseEvent(KundeHendelse evt, ProcessedCustomerResponse response, QueryExpressionHelper query)
        {
            try
            {
                var cust = ValidateAndPrepare(evt, response, query);
                if (cust == null) return;

#if DEBUG
                log.Add($"CustomerXtra: " +
                    $"{cust.BankId}," +
                    $"{cust.EntityName}," +
                    $"{cust.LoggedInUserOwnerId}," +           
                    $"{cust.DefaultTeamId}," +
                    $"{cust.SubjectId}," +
                    $"{cust.PriceListId}," +
                    $"{cust.VendorBankNumber}," +
                    $"{cust.Product.Number}," +
                    $"{cust.Product.Name}");


                log.Add("Done with ValidateAndPrepare.");
#endif
                var crmCustomerCard = ProcessHelper.GetCustomerCard(evt, cust, response, query);
                if (response.FailedProcessing) return;

                log.Add("Done with GetCustomerCard.");


                ProcessHelper.GetOwnerIfExist(crmCustomerCard, cust);

                log.Add("Done with GetOwnerIfExist.");

                if (cust.CustomerId == Guid.Empty)
                {
                    ProcessHelper.CreatePotentialCustomer(evt, cust, response, query);
                    if (response.FailedProcessing) return;
                }
                else ProcessHelper.UpdateCustomerSourceId(crmCustomerCard, evt.FagSystem, evt.FagsystemKundenummer, response, query, true);

                log.Add("Done with CreatePotentialCustomer.");

                var success = ProductManager.UpsertProduct(evt, cust, response, query, Log);
#region return failure
                if (!success)
                {
                    response.ErrorMessage = "Error during CreateOrUpdateProduct.";
                    return;
                }
#endregion

                log.Add("Done with CreateOrUpdateProduct.");


                if (evt.ProsessOmraade == ProcessArea.Sparing)
                {
                    log.Add("ProsessOmraade... Sparing.");


                    ProcessHelper.CreateOutcomeForSparing(evt, cust, response, query); //<------------------FAILS---------------------------------------------------------------------------------------------------------------------------------------------------->
                    if (response.FailedProcessing)
                    {
                        log.Add($"ProcessArea.Sparing.CreateOutcomeForSparing failed: {response.FailedProcessing} {response.ErrorMessage}");
                        return;
                    }
                }
                else if (evt.ProsessOmraade == ProcessArea.Forsikring)
                {
                    log.Add("CreateOutcomeForInsurance... Forsikring...");

                    ProcessHelper.CreateOutcomeForInsurance(evt, cust, response, query);

                    if (response.FailedProcessing) {
                        log.Add($"ProcessArea.Forsikring.CreateOutcomeForInsurance failed: {response.ErrorMessage}");
                        return;
                    }
                }
                else
                {
                    log.Add($"Skipping any outcome of the 'kundehendelse' due to process area {evt.ProsessOmraade} or {evt.Applikasjon} is not yet supported. {response}");
                    return;
                }

                log.Add("Done with ProsessOmraade.");
                response.Success = true;
            }
            catch (Exception ex)
            {
                log.Add($"Error while processing { System.Reflection.MethodBase.GetCurrentMethod().Name}\n Message: \n{ex}\n{ex.InnerException}");
            }
        }

#endregion

#region Private  Methods


        /// <summary>
        /// This is a businessrule and should be moved into the newer type of RuleEngine in future
        /// </summary>
        /// <param name="evt"></param>
        /// <param name="response"></param>
        /// <param name="query"></param>
        /// <returns></returns>
        private static CustomerExtra ValidateAndPrepare(KundeHendelse evt, ProcessedCustomerResponse response, QueryExpressionHelper query)
        {
            var bankId = query.GetBankIdByBankNo(evt.Brukersted);
         
            if (!ProcessHelper.ValidateBank(bankId, evt.Brukersted, response, Log))
            {
                response.ErrorMessage = "Kundehendelse not valid. Can't find 'Brukersted' in Crm.";
                return null;
            }

            var cust = new CustomerExtra
            {
                BankId = bankId,
                EntityName = Helper.GetEntityName(evt),
                LoggedInUserOwnerId = query.GetUserIdByUsernameAndBankId(evt.InnloggetRaadgiver, bankId),
                DefaultTeamId = query.GetDefaultTeamIdByBankId(bankId),
                SubjectId = query.GetSubjectIdByTitle(Helper.MapSubject(evt.ProsessOmraade, evt.FagSystem)),
                PriceListId = query.GetPriceListIdByTitle(Helper.MapSubject(evt.ProsessOmraade, evt.FagSystem)),
                VendorBankNumber = Helper.GetVendorBankNumber(evt.FagSystem),
                Product = query.GetProductByCode(evt.ProduktKode)
            };

#if DEBUG
            log.Add($"Inside CustomerExtra ------ START");
            log.Add($"BankId "+ bankId);
            log.Add($"LoggedInUserOwnerId: {cust.LoggedInUserOwnerId}");
            log.Add($"DefaultTeamId " + cust.DefaultTeamId);
            log.Add($"Inside CustomerExtra ------ END");
#endif
            if (cust.EntityName == EntityName.Undefined)
            {
                Helper.SetLogAndResponse("Kundehendelse not valid. Can't find a valid EntityName from 'KundeId'.", response, LogLevel.Warn, Log, true);
                log.Add($"Kundehendelse not valid. Can't find a valid EntityName from 'KundeId'. {response}");
                return null;
            }

            if (cust.SubjectId == Guid.Empty)
            {
                Helper.SetLogAndResponse("Kundehendelse not valid. Can't find 'Subject' (ProsessOmraade) in Crm.", response, LogLevel.Warn, Log);
                log.Add($"Kundehendelse not valid. Can't find 'Subject' (ProsessOmraade) in Crm. {response}");
                return null;
            }

            if (cust.PriceListId == Guid.Empty)
            {
                Helper.SetLogAndResponse("Kundehendelse not valid. Can't find 'Price List' (ProsessOmraade) in Crm.", response, LogLevel.Warn, Log);
                log.Add($"Kundehendelse not valid. Can't find 'Price List' (ProsessOmraade) in Crm. {response}");
                return null;
            }

            if (cust.Product.Id != Guid.Empty && !cust.Product.Active)
            {
                Helper.SetLogAndResponse("Kundehendelse not valid. Product is inactive in Crm.", response, LogLevel.Warn, Log);
                log.Add($"Kundehendelse not valid. Can't find 'Price List' (ProsessOmraade) in Crm. {response}");
                return null;
            }

            if (string.IsNullOrEmpty(cust.VendorBankNumber))
            {
                Helper.SetLogAndResponse("Kundehendelse not valid. Can't find 'VendorBankNumber' given 'FagSystem'.", response, LogLevel.Warn, Log);
                log.Add($"Kundehendelse not valid. Can't find 'VendorBankNumber' given 'FagSystem'. {response}");
                return null;
            }

            if (!Helper.ValidateKundehendelseAppNames(Initialize.KundehendelseAppNames))
            {
                Helper.SetLogAndResponse("Kundehendelse not valid. KundehendelseAppNames in 'ETL_Config' is missing one of the Enum defined AppNames in this code.", response, LogLevel.Warn, Log);
                log.Add($"Kundehendelse not valid. KundehendelseAppNames in 'ETL_Config' is missing one of the Enum defined AppNames in this code. {response}");
                return null;
            }

            if (!(evt.ProsessOmraade == ProcessArea.Sparing && Helper.ApplicationIsFromWeb(evt.Applikasjon, Initialize.KundehendelseAppNames)) &&
                !(evt.ProsessOmraade == ProcessArea.Forsikring && Helper.ApplicationIsFromWeb(evt.Applikasjon, Initialize.KundehendelseAppNames)))
            {
                Helper.SetLogAndResponse("Kundehendelse not valid. 'ProsessOmraade' and 'Applikasjon' is not within the defined values.", response, LogLevel.Warn, Log);
                log.Add($"Kundehendelse not valid. 'ProsessOmraade' and 'Applikasjon' is not within the defined values. {response}");
                return null;
            }

            log.Add($"ValidateAndPrepare complete. {response}");
            Helper.SetLogAndResponse("ValidateAndPrepare complete.", response, LogLevel.Warn, Log);

            return cust;
        }
#endregion

    }
}
