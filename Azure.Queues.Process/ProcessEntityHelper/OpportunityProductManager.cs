﻿using System;
using System.Collections.Generic;
using System.Linq;
using Azure.Logging;
using Common;
using Common.Adapters;
using Common.Entities;
using Common.Enums;
using Common.Logging;
using Common.RequestBuilders;

namespace Azure.Queues.Process.ProcessEntityHelper
{
    public class OpportunityProductManager
    {

        private static readonly MsmqLogger Log = MsmqLogger.CreateLogger("opportunity-product-manager");
        private static SingletonLogger log = SingletonLogger.Instance();

        // TODO extend this method to have value or evt as input (or put new value on ProductInfo)
        public static void HandleOpportunityProduct(Guid opportunityId, ProductInfo product, ProcessedCustomerResponse response, QueryExpressionHelper query, decimal value, bool deleteFromOpportunity = false, bool singleProduct = true)
        {
            log.Add("A3 - Start Opportunity Prod Manager"  );
            if (product.Id == Guid.Empty) return;

            if (value < 0) value = 0;

            var opportunityProducts = query.GetOpportunityProducts(opportunityId, response);
            if (response.FailedProcessing) return;

            if (!deleteFromOpportunity)
            {
                log.Add("A5 - Inside Add Opp Product. Here are some values: Product " +product.Name);
                AddOpportunityProduct(opportunityId, product, opportunityProducts, value, response, query, singleProduct);
            }
            else
            {
                RemoveOpportunityProduct(product, opportunityProducts, response, query);
            }

            log.Add("A4 - End Opportunity Prod Manager");
        }


        private static void AddOpportunityProduct(Guid opportunityId, ProductInfo product, List<OpportunityProduct> products, decimal value, ProcessedCustomerResponse response, QueryExpressionHelper query, bool singleProduct)
        {
            var needsToAddProduct = !products.Select(e => e.ProductId).ToList().Contains(product.Id);

            if (needsToAddProduct)
            {
                if (singleProduct) DeleteAllCurrentProducts(products, response, query);
                CreateOpportunityProduct(opportunityId, product, value, response, query);
            }
            else
            {
                var opportunityProductId = products.First(e => e.ProductId.Equals(product.Id)).Id;

                UpdateOpportunityProduct(opportunityProductId, product, value, response, query);
            }
        }
        private static void RemoveOpportunityProduct(ProductInfo product, List<OpportunityProduct> products, ProcessedCustomerResponse response, QueryExpressionHelper query)
        {
            var opportunityProduct = products.FirstOrDefault(e => e.ProductId.Equals(product.Id));

            if (opportunityProduct != null)
            {
                DeleteOpportunityProduct(opportunityProduct.Id, response, query);
            }
        }

        private static void DeleteAllCurrentProducts(List<OpportunityProduct> products, ProcessedCustomerResponse response, QueryExpressionHelper query)
        {
            foreach (var opportunityProduct in products)
            {
                DeleteOpportunityProduct(opportunityProduct.Id, response, query);
            }
        }


        private static void CreateOpportunityProduct(Guid opportunityId, ProductInfo product, decimal value, ProcessedCustomerResponse response, QueryExpressionHelper query)
        {
            var ent = RequestBuilder.CreateOpportunityProductsEntity(opportunityId, product, value);

            Log.Debug($"Adding product with name: '{product.Name}'");
            query.CreateEntity(ent, response);
        }
        private static void UpdateOpportunityProduct(Guid opportunityProductId, ProductInfo product, decimal value, ProcessedCustomerResponse response, QueryExpressionHelper query)
        {
            var ent = RequestBuilder.UpdateOpportunityProductsEntity(opportunityProductId, value);

            Log.Debug($"Updating product with name: '{product.Name}'");
            query.UpdateEntity(ent, response);
        }
        private static void DeleteOpportunityProduct(Guid opportunityProductId, ProcessedCustomerResponse response, QueryExpressionHelper query)
        {
            Log.Debug($"Removing product with opportunityProductId: '{opportunityProductId}'");
            query.DeleteEntity(EntityName.opportunityproduct, opportunityProductId, response);
        }
    }
}
