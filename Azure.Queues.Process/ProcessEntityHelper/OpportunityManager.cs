﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common;
using Common.Adapters;
using Common.Constants;
using Common.Entities;
using Common.Enums;
using Common.Helpers;
using Common.Initializer;
using Common.Logging;
using Common.RequestBuilders;
using Microsoft.Xrm.Sdk;
using Azure.Queues.Process.ProcessHelpers;
using System.Text;
using Azure.Logging;

namespace Azure.Queues.Process.ProcessEntityHelper
{
    public class OpportunityManager
    {
        private static readonly MsmqLogger Log = MsmqLogger.CreateLogger("opportunity-manager");
        private static SingletonLogger log = SingletonLogger.Instance();

        public static void HandleSalesOpportunityForSparingSakHendelse(SakHendelse evt, CustomerExtra cust, ProcessedCustomerResponse response, QueryExpressionHelper query, bool keepOpen = false, bool lost = false)
        {
            var opportunities = new List<SalesOpportunity>();
            opportunities = query.GetSalesOpportunitiesBySakId(evt.SakId, cust.CustomerId, response);
            query.GetOpportunityProducts(opportunities, response);
            if (response.FailedProcessing) return;


            //TODO consider here if we need to filter on it
            //opportunities.ForEach(e => e.OpportunityProducts = query.GetOpportunityProducts(e.Id));

            log.Add($"is new cust: {cust.IsNewCustomer}");
            opportunities = opportunities.OrderByDescending(e => e.CreatedOn).Where(e => e.SubjectId == cust.SubjectId).ToList(); // ProsessOmråde er likt.

            if (opportunities.Count == 0)
            {
                if(cust.CustomerId == Guid.Empty) return;

                log.Add($"No opportunities found for {evt.Interessent.OffentligId} related to '{evt.Prosessomraade}'");
                CreateSalesOpportunityRadgiverPluss(evt, cust, keepOpen, lost, response, query);
            }
            else if (Helper.ApplicationIsFromRaadgiverPluss(evt.Applikasjon, Initialize.KundehendelseAppNames))
            {
                HandleSalesOpportunityForSparingAndRaadgiverPluss(evt, cust, opportunities, response, query, keepOpen, lost);
            }
            else
            {
                log.Add($"The application '{evt.Applikasjon}' is not defined as either web or source. The event will be ignored. {response}");
            }
        }
       

        public static void HandleSalesOpportunityForSparing(KundeHendelse evt, CustomerExtra cust, ProcessedCustomerResponse response, QueryExpressionHelper query, bool keepOpen = false)
        {
            

            var opportunities = new List<SalesOpportunity>();
            if (!cust.IsNewCustomer)
                opportunities = query.GetSalesOpportunitiesByCustomerId(cust.CustomerId, response);

            log.AppendLine($"Is new customer: {cust.IsNewCustomer}. Next: GetOpportunityProducts...");

            query.GetOpportunityProducts(opportunities, response);
            log.Add($"HandleSalesOpportunityForSparing Response success: {response.Success}");
            if (response.FailedProcessing)
            {
                log.Add($"HandleSalesOpportunityForSparing error: { response.ErrorMessage }");
                return;
            }

            //TODO consider here if we need to filter on it
            //opportunities.ForEach(e => e.OpportunityProducts = query.GetOpportunityProducts(e.Id));

            
            opportunities = opportunities.OrderByDescending(e => e.CreatedOn).Where(e => e.SubjectId == cust.SubjectId).ToList(); // ProsessOmråde er likt.

            if (opportunities.Count == 0)
            {
                log.AppendLine($"No opportunities found for {evt.KundeId} related to '{evt.ProsessOmraade}'");
                CreateSalesOpportunity(evt, cust, keepOpen, response, query);
            }
            else if (Helper.ApplicationIsFromWeb(evt.Applikasjon, Initialize.KundehendelseAppNames))
            {
                log.AppendLine($"HandleSalesOpportunityForSparingAndWeb...");
                HandleSalesOpportunityForSparingAndWeb(evt, cust, opportunities, response, query, keepOpen);
            }

            //else if (Helper.ApplicationIsFromSource(evt.Applikasjon, Initialize.KundehendelseAppNames))
            //{
            //    HandleSalesOpportunityForSparingAndSource(evt, cust, opportunities, response, query, keepOpen);
            //}
            //else if (Helper.ApplicationIsFromRaadgiverPluss(evt.Applikasjon, Initialize.KundehendelseAppNames))
            //{
            //    HandleSalesOpportunityForSparingAndRaadgiverPluss(evt, cust, opportunities, response, query, keepOpen);
            //}
            else
            {
                log.AppendLine($"The application '{evt.Applikasjon}' is not defined as either web or source. The event will be ignored. { response}");
            }
        }
        public static void HandleSalesOpportunityForInsurance(KundeHendelse evt, CustomerExtra cust, ProcessedCustomerResponse response, QueryExpressionHelper query, bool keepOpen = false)
        {
            log.Add($"HandleSalesOpportunityForInsurance hasFailed: {response.FailedProcessing}");
            log.Debug($"Is new cust: {!cust.IsNewCustomer}");

            var opportunities = new List<SalesOpportunity>();
            if (!cust.IsNewCustomer)
                opportunities = query.GetSalesOpportunitiesByCustomerId(cust.CustomerId, response);

            query.GetOpportunityProducts(opportunities, response);

            if (response.FailedProcessing) {

                log.Debug($"HandleSalesOpportunityForInsurance failed: Reason: {response.ErrorMessage}");
                return;
            }

            opportunities = opportunities.OrderByDescending(e => e.CreatedOn).Where(e => e.SubjectId == cust.SubjectId).ToList(); // ProsessOmråde er likt. 


            if (opportunities.Count == 0)
            {
                log.Debug($"No opportunities found for {evt.KundeId} related to '{evt.ProsessOmraade}'");
                CreateSalesOpportunity(evt, cust, keepOpen, response, query);
            }
            else if (Helper.ApplicationIsFromWeb(evt.Applikasjon, Initialize.KundehendelseAppNames))
            {
                HandleSalesOpportunityForInsuranceAndWeb(evt, cust, opportunities, response, query, keepOpen);
            }
            else if (Helper.ApplicationIsFromSource(evt.Applikasjon, Initialize.KundehendelseAppNames))
            {
                //HandleSalesOpportunityForInsuranceAndSource(evt, cust, opportunities, response, query, keepOpen);
            }
            else
            {
               log.Add($"The application '{evt.Applikasjon}' is not defined as either web or source. The event will be ignored. {response}");
            }
        }
        public static void HandleSalesOpportunityForKreditt(SakHendelse evt, CustomerExtra cust, ProcessedCustomerResponse response, QueryExpressionHelper query)
        {
            var opportunities = query.GetSalesOpportunitiesByExtEventId(evt.SakId, cust.BankId, response);
            //var opportunities = query.GetSalesOpportunitiesKreditt(cust.BankId, response);

            // TODO change it to first query by SakId - and if no hit - query by customer Id

            query.GetOpportunityProducts(opportunities, response);
            if (response.FailedProcessing) return;

            if (opportunities.Count == 0)
            {
                log.Debug($"No opportunities found for {evt.Interessent?.OffentligId} related to '{evt.Prosessomraade}'");
                CreateSalesOpportunityKreditt(evt, cust, response, query);
            }
            else
            {
                if (KredittExtIdSource(evt, cust, response, query, opportunities)) return;
                if (StandardKredittOpen(evt, cust, response, query, opportunities)) return;

                log.Debug($"No opportunities found for {evt.Interessent?.OffentligId} related to '{evt.Prosessomraade}'");
                CreateSalesOpportunityKreditt(evt, cust, response, query);
            }
        }
        public static void HandleSalesOpportunityForKreditt_Old(SakHendelse evt, CustomerExtra cust, ProcessedCustomerResponse response, QueryExpressionHelper query, bool keepOpen = false)
        {
            var opportunities = new List<SalesOpportunity>();
            if (!cust.IsNewCustomer) opportunities = query.GetSalesOpportunitiesByExtEventId(evt.SakId, cust.BankId, response);
            query.GetOpportunityProducts(opportunities, response);
            if (response.FailedProcessing) return;

            log.Debug($"is new cust: {cust.IsNewCustomer}");

            //opportunities = opportunities.OrderByDescending(e => e.CreatedOn).Where(e => e.SubjectId == cust.SubjectId &&
            //                                                                             e.Source != TerritoryCode.Crm).ToList(); // ProsessOmråde er likt. 


            if (opportunities.Count == 0)
            {
                log.Debug($"No opportunities found for {evt.Interessent?.OffentligId} related to '{evt.Prosessomraade}'");
                //CreateSalesOpportunityKreditt(evt, cust, keepOpen, response, query);
            }





            //else if (Helper.ApplicationIsFromWeb(evt.Applikasjon, Initialize.KundehendelseAppNames))
            //{
            //    HandleSalesOpportunityForInsuranceAndWeb(evt, cust, opportunities, response, query, keepOpen);
            //}
            //else if (Helper.ApplicationIsFromSource(evt.Applikasjon, Initialize.KundehendelseAppNames))
            //{
            //    //HandleSalesOpportunityForInsuranceAndSource(evt, cust, opportunities, response, query, keepOpen);
            //}
            //else
            //{
            //    Helper.SetLogAndResponse($"The application '{evt.Applikasjon}' is not defined as either web or source. The event will be ignored.", response, LogLevel.Warn, Log, true);
            //}
        }

        public static bool SalesOpportunityZeroRevenueToLost(KundeHendelse evt, CustomerExtra cust, ProcessedCustomerResponse response, QueryExpressionHelper query, List<SalesOpportunity> opportunities)
        {
            //var opportunities = new List<SalesOpportunity>();
            //if (!cust.IsNewCustomer) opportunities = query.GetSalesOpportunitiesByCustomerId(cust.CustomerId, response);
            //if (response.FailedProcessing) return false;

            var opportunity = opportunities.FirstOrDefault(e => e.SubjectId == cust.SubjectId &&
                                                            e.StateCode == 0 &&
                                                            e.ActualRevenue == 0 &&
                                                            //e.ProductId == cust.Product.Id &&
                                                            e.App.Equals(evt.Applikasjon, StringComparison.CurrentCultureIgnoreCase));

            if (opportunity != null)
            {
                log.Add("Case check => true");

                //OpportunityProductManager.HandleOpportunityProduct(opportunity.Id, cust.Product, response, query); //TODO. Do we need to update product here ?
                CloseSalesOpportunityLost(evt, opportunity, response, query);
                return true;
            }

            return false;
        }
        public static bool HandleUnsharingOfRelatedSalesOpportunities(Guid customerId, Guid subjectId, Guid vendorBankId, string bankNumber, ProcessedCustomerResponse response, QueryCrm query)
        {
            var bankName = query.GetBankName(bankNumber);
            var collaborationTeamId = query.GetCollaborationTeam(bankName, vendorBankId);

            var opportunities = query.GetSharedOpportunitiesOnCustomerForCollaborationTeam(customerId, subjectId, collaborationTeamId);

            foreach (var id in opportunities)
            {
                query.UnshareTeamAccessOnOpportunity(id, collaborationTeamId, response);
            }
            if (opportunities.Count > 0) log.Debug($"Related Sales Opportunities ({opportunities.Count}) are unshared from card.");

            return true;
        }

     

        #region Private Methods

        private static void HandleSalesOpportunityForSparingAndWeb(KundeHendelse evt, CustomerExtra cust, List<SalesOpportunity> opportunities, ProcessedCustomerResponse response, QueryExpressionHelper query, bool keepOpen)
        {
            log.AppendLine($"HandleSalesOpportunityForSparingAndWeb {evt.KundeId} related to '{evt.ProsessOmraade}'");

            if (StandardWeb(evt, cust, keepOpen, response, query, opportunities)) {
                log.Add("StandardWeb");
                return; }

            if (RaadgiverPlussTwentyDaysSinceCloseWeb(evt, cust, keepOpen, response, query, opportunities)) {
                log.Add("RaadgiverPlussTwentyDaysSinceCloseWeb");
                return; }
            if (OpenAndEmptyWeb(evt, cust, keepOpen, response, query, opportunities)) {
                log.Add("OpenAndEmptyWeb");
                return; }


            if (!keepOpen)
            {
                if (TradexSourceWonFromWeb(evt, cust, response, opportunities)) {
                    log.Add("TradexSourceWonFromWeb");
                    return; }
            }

            log.Debug("We have gone through all duplicate checks => Will now Create new SO.");

            CreateSalesOpportunity(evt, cust, keepOpen, response, query);
        }

        private static void HandleSalesOpportunityForSparingAndSource(KundeHendelse evt, CustomerExtra cust, List<SalesOpportunity> opportunities, ProcessedCustomerResponse response, QueryExpressionHelper query, bool keepOpen)
        {
            if (StandardWonSource(evt, cust, response, query, opportunities)) return;
            if (StandardOpenSource(evt, cust, response, query, opportunities)) return;

            if (RaadgiverPlussOpenSource(evt, cust, response, query, opportunities)) return;
            if (RaadgiverPlussClosedWonSource(evt, cust, response, query, opportunities)) return;

            if (OpenAndEmptySource(evt, cust, response, query, opportunities)) return;


            log.Debug("We have gone through all duplicate checks => Will now Create new SO.");
            CreateSalesOpportunity(evt, cust, false, response, query);
        }

        private static void HandleSalesOpportunityForInsuranceAndWeb(KundeHendelse evt, CustomerExtra cust, List<SalesOpportunity> opportunities, ProcessedCustomerResponse response, QueryExpressionHelper query, bool keepOpen)
        {
            if (StandardWeb(evt, cust, keepOpen, response, query, opportunities)) return;

            if (keepOpen)
            {
                if (NiceSkadeSourceNewlyCreatedOneDayWeb(evt, cust, response, opportunities)) return;
                //if (SalesOpportunityWithFamilyProduct(evt, cust, response, query, opportunities, KundehendelseAppName.nice_skade_source)) return;
            }

            if (OpenAndCompletelyEmptyWeb(evt, cust, keepOpen, response, query, opportunities)) return;

            log.Debug("We have gone through all duplicate checks => Will now Create new SO.");
            CreateSalesOpportunity(evt, cust, keepOpen, response, query);
        }


        #region SH :: Sparing :: Raadgiver+

        private static void HandleSalesOpportunityForSparingAndRaadgiverPluss(SakHendelse evt, CustomerExtra cust, List<SalesOpportunity> opportunities, ProcessedCustomerResponse response, QueryExpressionHelper query, bool keepOpen, bool lost)
        {
            if (StandardExtIdSource(evt, cust, keepOpen, lost, response, query, opportunities)) return;
            if (EmptyExtIdSource(evt, cust, keepOpen, lost, response, query, opportunities)) return;

            CreateSalesOpportunityRadgiverPluss(evt, cust, keepOpen, lost, response, query);
        }

        private static bool StandardExtIdSource(SakHendelse evt, CustomerExtra cust, bool keepOpen, bool lost, ProcessedCustomerResponse response, QueryExpressionHelper query, List<SalesOpportunity> opportunities)
        {
            var opportunity = opportunities.FirstOrDefault(x => x.StateCode == (int)OpportunityStateCode.Open &&
                                                                x.ExternalEventId.Equals(evt.SakId) &&
                                                                x.App.Equals(evt.Applikasjon));

            if (opportunity != null)
            {
                Log.Debug("Check 1 Source => true");
                UpdateSalesOpportunityForRadgiverPluss(evt, cust, opportunity, keepOpen, lost, response, query);
                //UpdateSalesOpportunity(evt, cust, opportunityCurrentProduct, keepOpen, response, query);

                return true;
            }
            Log.Debug("Check 1 Source => false");
            return false;
        }

        private static bool EmptyExtIdSource(SakHendelse evt, CustomerExtra cust, bool keepOpen, bool lost, ProcessedCustomerResponse response, QueryExpressionHelper query, List<SalesOpportunity> opportunities)
        {
            var opportunity = opportunities.FirstOrDefault(x => x.StateCode == (int)OpportunityStateCode.Open &&
                                                                string.IsNullOrEmpty(x.ExternalEventId) &&
                                                                string.IsNullOrEmpty(x.App) &&
                                                                string.IsNullOrEmpty(x.InitialApp));

            if (opportunity != null)
            {
                Log.Debug("Check 1.2 Source => true");
                UpdateSalesOpportunityForRadgiverPluss(evt, cust, opportunity, keepOpen, lost, response, query);
                //UpdateSalesOpportunity(evt, cust, opportunityCurrentProduct, keepOpen, response, query);

                return true;
            }
            Log.Debug("Check 1.2 Source => false");
            return false;
        }

        #endregion

        #region KH :: Sparing :: Source

        private static bool StandardWonSource(KundeHendelse evt, CustomerExtra cust, ProcessedCustomerResponse response, QueryExpressionHelper query, List<SalesOpportunity> opportunities)
        {
            var appNameWeb = Helper.GetApplicationNameForWeb(Initialize.KundehendelseAppNames, evt.Applikasjon);
            var newEstValue = evt.Verdi - evt.TidligereVerdi;
            if (newEstValue < 0) newEstValue = 0;

            var opportunity = opportunities.FirstOrDefault(x => x.StateCode == (int)OpportunityStateCode.Won &&
                                                                x.App.Equals(appNameWeb, StringComparison.CurrentCultureIgnoreCase) &&
                                                                (x.InitialApp.Equals(appNameWeb, StringComparison.CurrentCultureIgnoreCase) || string.IsNullOrEmpty(x.InitialApp)) &&
                                                                x.OpportunityProducts.Where(e => e.ProductId == cust.Product.Id).ToList().Count > 0 &&
                                                                x.ActualRevenue == newEstValue &&
                                                                ProcessHelper.DayDiff(x.EventDate) < 2);

            if (opportunity != null)
            {
                log.Debug("Check 1 Source => true");
                UpdateSalesOpportunityApp(evt, cust, opportunity, response, query);

                return true;
            }
            log.Debug("Check 1 Source => false");
            return false;
        }
        private static bool StandardOpenSource(KundeHendelse evt, CustomerExtra cust, ProcessedCustomerResponse response, QueryExpressionHelper query, List<SalesOpportunity> opportunities)
        {
            var appNameWeb = Helper.GetApplicationNameForWeb(Initialize.KundehendelseAppNames, evt.Applikasjon);

            var opportunity = opportunities.FirstOrDefault(x => x.StateCode == (int)OpportunityStateCode.Open &&
                                                                x.App.Equals(appNameWeb, StringComparison.CurrentCultureIgnoreCase) &&
                                                                (x.InitialApp.Equals(appNameWeb, StringComparison.CurrentCultureIgnoreCase) || string.IsNullOrEmpty(x.InitialApp)) &&
                                                                ProcessHelper.DayDiff(x.EventDate) < 2);

            if (opportunity != null)
            {
                log.Debug("Check 2 Source => true");
                UpdateAndCloseSalesOpportunityWon(evt, cust, opportunity, response, query);

                return true;
            }
            log.Debug("Check 2 Source => false");
            return false;
        }
        private static bool RaadgiverPlussOpenSource(KundeHendelse evt, CustomerExtra cust, ProcessedCustomerResponse response, QueryExpressionHelper query, List<SalesOpportunity> opportunities)
        {
            //var raadgiverAppName = Initialize.KundehendelseAppNames[KundehendelseAppName.tradex_raadgiver.ToString()];

            var opportunity = opportunities.FirstOrDefault(x => x.StateCode == (int)OpportunityStateCode.Open &&
                                                                IsApplication(x.App, KundehendelseAppName.tradex_raadgiver)); //x.App.Equals(raadgiverAppName, StringComparison.CurrentCultureIgnoreCase));


            if (opportunity != null)
            {
                log.Debug("Check 3.1 Source => true");
                HandleSalesOpportunityChild(evt, cust, false, opportunity.Id, response, query);

                return true;
            }
            log.Debug("Check 3.1 Source => false");
            return false;
        }
        private static bool RaadgiverPlussClosedWonSource(KundeHendelse evt, CustomerExtra cust, ProcessedCustomerResponse response, QueryExpressionHelper query, List<SalesOpportunity> opportunities)
        {
            //var raadgiverAppName = Initialize.KundehendelseAppNames[KundehendelseAppName.tradex_raadgiver.ToString()];

            var opportunityList = opportunities.Where(x => x.StateCode == (int)OpportunityStateCode.Won &&
                                                           IsApplication(x.App, KundehendelseAppName.tradex_raadgiver)).ToList();

            var opportunity = query.GetFirstOpportunityWithClosedLast20Days(opportunityList, response);


            if (opportunity != null)
            {
                log.Debug("Check 3.2 Source => true");
                HandleSalesOpportunityChild(evt, cust, false, opportunity.Id, response, query);

                return true;
            }
            log.Debug("Check 3.2 Source => false");
            return false;
        }
        private static bool OpenAndEmptySource(KundeHendelse evt, CustomerExtra cust, ProcessedCustomerResponse response, QueryExpressionHelper query, List<SalesOpportunity> opportunities)
        {
            //Check #2.2
            var opportunityList = opportunities.Where(x => x.StateCode == (int)OpportunityStateCode.Open &&
                                                            string.IsNullOrEmpty(x.InitialApp) &&
                                                            string.IsNullOrEmpty(x.App)).ToList();

            var opportunity = GetFirstOpportunityWithChild(opportunityList, response, query);

            if (opportunity != null)
            {
                Log.Debug("Check 4 Web => true (Making child opportunity)");
                HandleSalesOpportunityChild(evt, cust, false, opportunity.Id, response, query);
                return true;
            }


            Log.Debug("Check 4 Web => false");
            return false;
        }

        #region hide old
        private static bool RaadgiverPlussOldSource(KundeHendelse evt, CustomerExtra cust, bool keepOpen, ProcessedCustomerResponse response, QueryExpressionHelper query, List<SalesOpportunity> opportunities)
        {
            var raadgiverAppName = Initialize.KundehendelseAppNames[KundehendelseAppName.tradex_raadgiver.ToString()];

            var opportunitiesFiltered = opportunities.Where(x => x.StateCode == 0 && // Open
                                                x.App.Equals(raadgiverAppName, StringComparison.CurrentCultureIgnoreCase) &&
                                                ProcessHelper.DayDiff(x.EventDate) < 2).ToList();

            if (WithCurrentProductByFilteredOpportunities(evt, cust, keepOpen, response, query, opportunitiesFiltered)) return true;
            if (WithFamilyProductByFilteredOpportunities(evt, cust, keepOpen, response, query, opportunitiesFiltered)) return true;

            Log.Debug("Check 2 Source => false");
            return false;
        }
        private static bool WithCurrentProductByFilteredOpportunities(KundeHendelse evt, CustomerExtra cust, bool keepOpen, ProcessedCustomerResponse response, QueryExpressionHelper query, List<SalesOpportunity> opportunitiesFiltered)
        {
            var opportunityCurrentProduct = opportunitiesFiltered.FirstOrDefault(x => x.ProductId == cust.Product.Id);
            if (opportunityCurrentProduct == null) return false;

            Log.Debug("Check 2.1 Source => true");
            UpdateSalesOpportunity(evt, cust, opportunityCurrentProduct, keepOpen, response, query);
            return true;
        }
        private static bool WithFamilyProductByFilteredOpportunities(KundeHendelse evt, CustomerExtra cust, bool keepOpen, ProcessedCustomerResponse response, QueryExpressionHelper query, List<SalesOpportunity> opportunitiesFiltered)
        {
            if (!Initialize.ProductCodesByBank.ContainsKey(cust.VendorBankNumber)) return false;

            var familyProduct = ProductManager.GetFamilyProductByVendorBankNumber(cust.VendorBankNumber, query);
            if (familyProduct.Id == Guid.Empty) return false;

            var opportunityFamilyProduct = opportunitiesFiltered.FirstOrDefault(x => x.ProductId == familyProduct.Id);
            if (opportunityFamilyProduct == null) return false;

            Log.Debug("Check 2.2 Source => true");
            UpdateSalesOpportunity(evt, cust, opportunityFamilyProduct, keepOpen, response, query);
            return true;
        }
        private static bool CreatedInCrmSource(KundeHendelse evt, CustomerExtra cust, bool keepOpen, ProcessedCustomerResponse response, QueryExpressionHelper query, List<SalesOpportunity> opportunities)
        {
            //if (!Initialize.ProductCodesByBank.ContainsKey(cust.VendorBankNumber)) return false;

            //var familyProduct = ProcessHelper.GetFamilyProductByVendorBankNumber(cust.VendorBankNumber, query);
            //if (familyProduct.Id == Guid.Empty) return false;

            var opportunity = opportunities.FirstOrDefault(x => x.StateCode == 0 && // Open 
                                                                                    //x.ProductId == familyProduct.Id &&
                                                           x.ProductId == Guid.Empty && // It will most likely be filtered on empty product
                                                           string.IsNullOrEmpty(x.App) &&
                                                           x.Source == TerritoryCode.Crm);
            if (opportunity != null)
            {
                Log.Debug("Check 3 Source => true");
                UpdateSalesOpportunity(evt, cust, opportunity, keepOpen, response, query);
                return true;
            }

            Log.Debug("Check 3 Source => false");
            return false;
        }
        #endregion

        #endregion

        #region KH :: Sparing + Forsikring :: Web

        private static bool StandardWeb(KundeHendelse evt, CustomerExtra cust, bool keepOpen, ProcessedCustomerResponse response, QueryExpressionHelper query, List<SalesOpportunity> opportunities)
        {
            //Check #1 - default to sparing for now
            var opportunity = opportunities.FirstOrDefault(x => x.StateCode == (int)OpportunityStateCode.Open &&
                                                                x.App.Equals(evt.Applikasjon));

            if (opportunity != null)
            {
                log.Add("StandardWeb Check 1 Web => true");
                UpdateSalesOpportunity(evt, cust, opportunity, keepOpen, response, query);

                return true;
            }
            log.Add("StandardWeb Check 1 Web => false");
            return false;
        }


        private static bool NiceSkadeSourceNewlyCreatedOneDayWeb(KundeHendelse evt, CustomerExtra cust, ProcessedCustomerResponse response, List<SalesOpportunity> opportunities)
        {
            var newEstValue = evt.Verdi - evt.TidligereVerdi;
            if (newEstValue < 0) newEstValue = 0;

            //Check #2
            var opportunity = opportunities.FirstOrDefault(x => x.StateCode == (int)OpportunityStateCode.Open &&
                                                            IsApplication(x.App, KundehendelseAppName.nice_skade_source) &&
                                                            x.EstimatedRevenue == newEstValue &&
                                                            x.OpportunityProducts.Where(e => e.ProductId == cust.Product.Id).ToList().Count > 0 &&
                                                            ProcessHelper.DayDiff(x.EventDate) < 1);
            if (opportunity != null)
            {
                Log.Debug("Check 2 => true (Doing nothing - Keeping the one in CRM - Setting event to 'Failed'/Ignored. )");

                response.ErrorMessage = "Ignoring event. Opportunity already exist as 'open' and created within a day.";
                response.IsIgnored = true;

                return true;
            }

            Log.Debug("Check 2 => false");
            return false;
        }
        private static bool OpenAndCompletelyEmptyWeb(KundeHendelse evt, CustomerExtra cust, bool keepOpen, ProcessedCustomerResponse response, QueryExpressionHelper query, List<SalesOpportunity> opportunities)
        {
            var opportunityList = opportunities.Where(x => x.StateCode == (int)OpportunityStateCode.Open &&
                                                            string.IsNullOrEmpty(x.App) &&
                                                            string.IsNullOrEmpty(x.InitialApp) &&
                                                            x.OpportunityProducts.Count == 0).ToList();

            var opportunity = GetFirstOpportunityWithChild(opportunityList, response, query);

            if (opportunity != null)
            {
                Log.Debug("Check 3 Web => true (Making child opportunity)");
                HandleSalesOpportunityChild(evt, cust, keepOpen, opportunity.Id, response, query);
                return true;
            }


            Log.Debug("Check 3 Web => false");
            return false;
        }

        #region hide old
        private static bool ClosedByAppNameWeb(KundeHendelse evt, CustomerExtra cust, ProcessedCustomerResponse response, List<SalesOpportunity> opportunities)
        {
            var newEstValue = evt.Verdi - evt.TidligereVerdi;
            //var sourceAppName = Initialize.KundehendelseAppNames[appNameEnum.ToString()];
            var appNameSource = Helper.GetApplicationNameForSource(Initialize.KundehendelseAppNames, evt.Applikasjon);

            //Check #2
            var opportunity = opportunities.FirstOrDefault(x => (x.StateCode == 1 || x.StateCode == 2) && // Won || Lost
                                                                x.App.Equals(appNameSource, StringComparison.CurrentCultureIgnoreCase) &&
                                                                (int)x.Channel == Helper.GetChannel(evt.Channel).Value &&
                                                                x.ProductId == cust.Product.Id &&
                                                                x.ActualRevenue == newEstValue &&
                                                                ProcessHelper.DayDiff(x.EventDate) < 2);
            if (opportunity != null)
            {
                Log.Debug("Check 2 Web => true (Doing nothing - Keeping the one in CRM)");
                response.ErrorMessage = "Ignoring event. Opportunity already closed within last two days.";
                response.IsIgnored = true;
                return true;
            }

            Log.Debug("Check 2 Web => false");
            return false;
        }
        private static bool WithFamilyProductWeb(KundeHendelse evt, CustomerExtra cust, ProcessedCustomerResponse response, QueryExpressionHelper query, List<SalesOpportunity> opportunities, KundehendelseAppName appNameEnum)
        {
            if (!Initialize.ProductCodesByBank.ContainsKey(cust.VendorBankNumber)) return false;

            var appName = Initialize.KundehendelseAppNames[appNameEnum.ToString()];
            var familyProduct = ProductManager.GetFamilyProductByVendorBankNumber(cust.VendorBankNumber, query);
            if (familyProduct.Id == Guid.Empty) return false;

            //Check #4
            var opportunity = opportunities.FirstOrDefault(x => x.StateCode == 0 &&
                                            x.App.Equals(appName, StringComparison.CurrentCultureIgnoreCase) &&
                                            x.ProductId == familyProduct.Id &&
                                             ProcessHelper.DayDiff(x.EventDate) < 2);
            if (opportunity != null)
            {
                Log.Debug("Check 4 => true");
                UpdateSalesOpportunity(evt, cust, opportunity, true, response, query, cust.Product.Id);
                return true;
            }

            Log.Debug("Check 4 => false");
            return false;
        }
        private static bool NewlyCreatedTwoDaysWeb(KundeHendelse evt, CustomerExtra cust, ProcessedCustomerResponse response, QueryExpressionHelper query, List<SalesOpportunity> opportunities, KundehendelseAppName appNameEnum)
        {
            var appName = Initialize.KundehendelseAppNames[appNameEnum.ToString()];

            //Check #5
            var opportunity = opportunities.FirstOrDefault(x => x.StateCode == 0 &&
                                                            x.App.Equals(appName, StringComparison.CurrentCultureIgnoreCase) &&
                                                            x.ProductId == cust.Product.Id &&
                                                             ProcessHelper.DayDiff(x.EventDate) < 2);
            if (opportunity != null)
            {
                Log.Debug("Check 5 => true");
                UpdateSalesOpportunity(evt, cust, opportunity, false, response, query);
                return true;
            }

            Log.Debug("Check 5 => false");
            return false;
        }
        private static bool CreatedInCrmWeb(KundeHendelse evt, CustomerExtra cust, bool keepOpen, ProcessedCustomerResponse response, QueryExpressionHelper query, List<SalesOpportunity> opportunities)
        {
            //Check #6.1 - Source CRM
            var opportunity = opportunities.FirstOrDefault(x => x.StateCode == 0 &&
                                                           x.ProductId == cust.Product.Id &&
                                                           string.IsNullOrEmpty(x.App) &&
                                                           x.Source == TerritoryCode.Crm);
            if (opportunity != null)
            {
                Log.Debug("Check 6.1 => true");
                //UpdateSO(opportunity.Id, keepOpen, newEstValue, e.ProductCode);
                UpdateSalesOpportunity(evt, cust, opportunity, keepOpen, response, query);
                return true;
            }

            Log.Debug("Check 6.1 => false");
            return false;
        }
        private static bool CreatedInCrmWithFamiliyProductWeb(KundeHendelse evt, CustomerExtra cust, bool keepOpen, ProcessedCustomerResponse response, QueryExpressionHelper query, List<SalesOpportunity> opportunities)
        {
            if (!Initialize.ProductCodesByBank.ContainsKey(cust.VendorBankNumber)) return false;

            var familyProductCode = Initialize.ProductCodesByBank[cust.VendorBankNumber];
            var familyProduct = query.GetProductByCode(familyProductCode); // Ie: Eika Kapital Forvaltning (parent product)

            //Check #6.2 - Source CRM or EMPTY
            var opportunity = opportunities.FirstOrDefault(x => x.StateCode == 0 &&
                                                           x.ProductId == familyProduct.Id &&
                                                           string.IsNullOrEmpty(x.App) &&
                                                           x.Source == TerritoryCode.Crm);

            if (opportunity != null)
            {
                Log.Debug("Check 6.2 => true");
                //UpdateSO(opportunity.Id, openStatusEvent, newEstValue, e.ProductCode);
                UpdateSalesOpportunity(evt, cust, opportunity, keepOpen, response, query, cust.Product.Id);
                return true;
            }

            Log.Debug("Check 6.2 => false");
            return false;
        }
        #endregion

        #region New for V2

        private static bool RaadgiverPlussTwentyDaysSinceCloseWeb(KundeHendelse evt, CustomerExtra cust, bool keepOpen, ProcessedCustomerResponse response, QueryExpressionHelper query, List<SalesOpportunity> opportunities)
        {
            //var raadgiverAppName = Initialize.KundehendelseAppNames[KundehendelseAppName.tradex_raadgiver.ToString()];
            //var appName = Initialize.KundehendelseAppNames[appNameEnum.ToString()];

            var opportunityListInitial = opportunities.Where(x => IsApplication(x.App, KundehendelseAppName.tradex_raadgiver)).ToList();

            var opportunityList = opportunityListInitial.Where(x => x.StateCode == (int)OpportunityStateCode.Open).ToList();
            if (opportunityList.Count == 0) opportunityList = query.GetOpportunitiesWithClosedLast20Days(opportunityListInitial, response);

            var opportunity = GetFirstOpportunityWithChild(opportunityList, response, query);

            if (opportunity != null)
            {
                Log.Debug("Check 2.1 Web => true (Making child opportunity)");
                HandleSalesOpportunityChild(evt, cust, keepOpen, opportunity.Id, response, query);
                return true;
            }

            Log.Debug("Check 2.1 Web => false");
            return false;
        }
        private static bool OpenAndEmptyWeb(KundeHendelse evt, CustomerExtra cust, bool keepOpen, ProcessedCustomerResponse response, QueryExpressionHelper query, List<SalesOpportunity> opportunities)
        {
            //Check #2.2
            var opportunityList = opportunities.Where(x => x.StateCode == (int)OpportunityStateCode.Open &&
                                                            string.IsNullOrEmpty(x.InitialApp) &&
                                                            string.IsNullOrEmpty(x.App)).ToList();

            var opportunity = GetFirstOpportunityWithChild(opportunityList, response, query);

            if (opportunity != null)
            {
                Log.Debug("Check 2.2 Web => true (Making child opportunity)");
                HandleSalesOpportunityChild(evt, cust, keepOpen, opportunity.Id, response, query);
                return true;
            }

            Log.Debug("Check 2.2 Web => false");
            return false;
        }
        private static bool TradexSourceWonFromWeb(KundeHendelse evt, CustomerExtra cust, ProcessedCustomerResponse response, List<SalesOpportunity> opportunities)
        {
            //var sourceAppName = Initialize.KundehendelseAppNames[KundehendelseAppName.tradex_source.ToString()];
            //var appName = Initialize.KundehendelseAppNames[appNameEnum.ToString()];
            var newEstValue = evt.Verdi - evt.TidligereVerdi;
            if (newEstValue < 0) newEstValue = 0;

            var opportunity = opportunities.FirstOrDefault(x => x.StateCode == (int)OpportunityStateCode.Won &&
                                                        IsApplication(x.App, KundehendelseAppName.tradex_source) && //x.App.Equals(sourceAppName, StringComparison.CurrentCultureIgnoreCase) &&
                                                        x.ActualRevenue == newEstValue &&
                                                        x.OpportunityProducts.Where(e => e.ProductId == cust.Product.Id).ToList().Count > 0 &&
                                                        ProcessHelper.DayDiff(x.EventDate) < 2);

            if (opportunity != null)
            {
                Log.Debug("Check 3 Web => true (Doing nothing - Keeping the one in CRM - Setting event to 'Failed'/Ignored. )");

                response.ErrorMessage = "Ignoring event. Opportunity already closed as Won from source. ";
                response.IsIgnored = true;

                return true;
            }

            Log.Debug("Check 3 Web => false");
            return false;
        }


        #endregion

        #endregion

        #region KH :: Sparing :: Radgiver Pluss

        private static void HandleSalesOpportunityForSparingAndRaadgiverPluss(KundeHendelse evt, CustomerExtra cust, List<SalesOpportunity> opportunities, ProcessedCustomerResponse response, QueryExpressionHelper query, bool keepOpen)
        {
            if (StandardExtIdSource(evt, cust, keepOpen, response, query, opportunities)) return;

            var id = CreateSalesOpportunityRadgiverPluss(evt, cust, false, response, query);

            if (OpenAndEmptyLastThirtyDaysWeb(evt, cust, keepOpen, response, query, opportunities, id, KundehendelseAppName.tradex_web)) return;
        }

        private static bool StandardExtIdSource(KundeHendelse evt, CustomerExtra cust, bool keepOpen, ProcessedCustomerResponse response, QueryExpressionHelper query, List<SalesOpportunity> opportunities)
        {
            var opportunity = opportunities.FirstOrDefault(x => x.StateCode == (int)OpportunityStateCode.Open &&
                                                                x.ExternalEventId.Equals(evt.AvtaleId) && //TODO, bekreftet at dette feltet er riktig eller om det skal være nytt felt 'SakId' / 'Id'
                                                                x.App.Equals(evt.Applikasjon));

            if (opportunity != null)
            {
                Log.Debug("Check 1 Source => true");
                UpdateSalesOpportunityForRadgiverPluss(evt, cust, opportunity, keepOpen, response, query);
                //UpdateSalesOpportunity(evt, cust, opportunityCurrentProduct, keepOpen, response, query);

                return true;
            }
            Log.Debug("Check 1 Source => false");
            return false;
        }
        private static bool OpenAndEmptyLastThirtyDaysWeb(KundeHendelse evt, CustomerExtra cust, bool keepOpen, ProcessedCustomerResponse response, QueryExpressionHelper query, List<SalesOpportunity> opportunities, Guid parentId, KundehendelseAppName appNameEnum)
        {
            if (parentId == Guid.Empty) return false;

            //var appName = Initialize.KundehendelseAppNames[appNameEnum.ToString()];
            var opportunity = opportunities.FirstOrDefault(x => x.StateCode == 0 &&
                                                            IsApplication(x.App, KundehendelseAppName.tradex_web) && //x.App.Equals(appName, StringComparison.CurrentCultureIgnoreCase) &&
                                                            ProcessHelper.DayDiff(x.EventDate) < 30);

            if (opportunity != null)
            {
                Log.Debug("Check 2 Web => true (Setting child opportunity)");
                SetParentOfOpportunity(opportunity.Id, parentId, response, query);
                return true;
            }

            Log.Debug("Check 2 Web => false");
            return false;
        }

        #endregion

        #region KH :: Kredittportal
        private static bool KredittExtIdSource(SakHendelse evt, CustomerExtra cust, ProcessedCustomerResponse response, QueryExpressionHelper query, List<SalesOpportunity> opportunities)
        {
            var opportunity = opportunities.FirstOrDefault(x => x.ExternalEventId.Equals(evt.SakId));

            if (opportunity != null)
            {
                Log.Debug("Check 1 Source => true");
                var isOpen = opportunity.StateCode == 0;
                UpdateSalesOpportunityForKreditt(evt, cust, opportunity.Id, response, query, isOpen);
                return true;
            }
            Log.Debug("Check 1 Source => false");
            return false;
        }

        private static bool StandardKredittOpen(SakHendelse evt, CustomerExtra cust, ProcessedCustomerResponse response, QueryExpressionHelper query, List<SalesOpportunity> opportunities)
        {
            var opportunity = opportunities.FirstOrDefault(x => x.StateCode == 0 &&
                                                      string.IsNullOrEmpty(x.App));

            if (opportunity != null)
            {
                Log.Debug("Check 2 Source => true");
                UpdateSalesOpportunityForKreditt(evt, cust, opportunity.Id, response, query, true);
                return true;
            }
            Log.Debug("Check 2 Source => false");
            return false;
        }

        #endregion

        private static SalesOpportunity GetFirstOpportunityWithChild(List<SalesOpportunity> opportunities, ProcessedCustomerResponse response, QueryExpressionHelper query)
        {
            foreach (var op in opportunities)
            {
                if (query.SalesOpportunityHasChild(op.Id, response)) return op;
            }

            return opportunities.FirstOrDefault();
        }
        private static void HandleSalesOpportunityChild(KundeHendelse evt, CustomerExtra cust, bool keepOpen, Guid parentOpportunityId, ProcessedCustomerResponse response, QueryExpressionHelper query)
        {
            CreateSalesOpportunityChild(evt, cust, keepOpen, parentOpportunityId, response, query);
        }

        #region SakHendelse

        private static void CreateSalesOpportunityRadgiverPluss(SakHendelse evt, CustomerExtra cust, bool keepOpen, bool lost, ProcessedCustomerResponse response, QueryExpressionHelper query)
        {
            var newEstValue = 0;
            var statuscode = ProcessHelper.GetOpportunityStatusCodeForRaadgiverpluss(cust.SakHendelseAttributes.Result);

            var ent = RequestBuilder.BuildCreateOpportunityRadgiverPluss(evt, cust, statuscode);

            ent.Id = query.CreateEntity(ent, response);

            //OpportunityProductManager.HandleOpportunityProduct(ent.Id, cust.Product, response, query, newEstValue);
            if (keepOpen)
            {
                InfoActivityManager.CreateInfoActivityInformingChange(evt, cust, ent.Id, response, query, true);
            }
            else
            {
                if(lost) query.CloseSalesOpportunityLost(ent.Id, evt.Tidspunkt, response);
                else query.CloseSalesOpportunityWon(ent.Id, newEstValue, evt.Tidspunkt, response);
            }
        }


        #endregion

        private static void CreateSalesOpportunity(KundeHendelse evt, CustomerExtra cust, bool keepOpen, ProcessedCustomerResponse response, QueryExpressionHelper query)
        {
           
            var newEstValue = (evt.Verdi - evt.TidligereVerdi);

            log.Add($"NewEstValue: {newEstValue}");

            if (newEstValue < 0)
            {
                log.Add("estimated value is less ");
                IncidentManager.CreateAndResolveIncident(evt, cust, response, query);
            }
            else
            {
                log.Add("estimated value is bigger ");
                var statuscode = ProcessHelper.GetOpportunityStatusCode(evt.Fase);
                log.Add("A1 " + statuscode);
                var ent = RequestBuilder.BuildCreateOpportunity(evt, cust, newEstValue, statuscode);
                //RequestBuilder.UpdateInterestedStatusOpportunity(ent);
                log.Add("A2 " );
                ent.Id = query.CreateEntity(ent, response);

                log.Add($"{Util.GetMethodName()} with keepOpen: '{keepOpen}'");

                
                OpportunityProductManager.HandleOpportunityProduct(ent.Id, cust.Product, response, query, newEstValue);
                log.Add($"{ent.Id}, {cust.DefaultTeamId}, {response.Success}, {newEstValue}");
                if (keepOpen) InfoActivityManager.CreateInfoActivityInformingChange(evt, ent.Id, cust.DefaultTeamId, response, query, true);
                else query.CloseSalesOpportunityWon(ent.Id, newEstValue, evt.Tidspunkt, response);
            }
        }

        private static Guid CreateSalesOpportunityRadgiverPluss(KundeHendelse evt, CustomerExtra cust, bool keepOpen, ProcessedCustomerResponse response, QueryExpressionHelper query)
        {
            var newEstValue = evt.Verdi - evt.TidligereVerdi;
            if (newEstValue < 0)
            {
                IncidentManager.CreateAndResolveIncident(evt, cust, response, query);
            }
            else
            {
                var statuscode = ProcessHelper.GetOpportunityStatusCode(evt.Fase);

                var ent = RequestBuilder.BuildCreateOpportunityRadgiverPluss(evt, cust, newEstValue, statuscode);

                ent.Id = query.CreateEntity(ent, response);

                OpportunityProductManager.HandleOpportunityProduct(ent.Id, cust.Product, response, query, newEstValue);
                if (keepOpen) InfoActivityManager.CreateInfoActivityInformingChange(evt, ent.Id, cust.DefaultTeamId, response, query, true);
                else query.CloseSalesOpportunityWon(ent.Id, newEstValue, evt.Tidspunkt, response);

                return ent.Id;
            }
            return Guid.Empty;
        }

        private static void CreateSalesOpportunityKreditt(SakHendelse evt, CustomerExtra cust, ProcessedCustomerResponse response, QueryExpressionHelper query)
        {
            var ent = RequestBuilder.BuildCreateOpportunityKreditt(evt, cust);

            //var res = false;
            //var val = (SakHendelseTypeEnum)Helper.GetEnumValueOf(evt.SakHendelsesType, typeof(SakHendelseTypeEnum));
            var val = EnumUtils.Parse<SakHendelseTypeEnum>(evt.SakHendelsesType);

            var res = val == SakHendelseTypeEnum.Ny_Sak_Opprettet ||
                      val == SakHendelseTypeEnum.Sak_Avsluttet;
            //val == SakHendelseTypeEnum.Statusendring;
            if (res)
            {
                RequestBuilder.UpdateOpportunityCustomer(cust, ent);
            }


            //switch ((SakHendelseTypeEnum)Helper.GetEnumValueOf(evt.SakHendelsesType, typeof(SakHendelseTypeEnum)))
            //{
            //    case SakHendelseTypeEnum.Ny_Sak_Opprettet:
            //    case SakHendelseTypeEnum.Sak_Avsluttet:
            //    case SakHendelseTypeEnum.Statusendring:
            //        //RequestBuilder.UpdateOpportunityCustomer(cust, ent);
            //        res = true;
            //        break;
            //    default:
            //        break;
            //}
            ent.Id = query.CreateEntity(ent, response);

            //if (keepOpen)
            //{
            //    var statuscode = ProcessHelper.GetOpportunityStatusCode(evt.SakStatus);
            //    RequestBuilder.UpdateOpportunityStatus(ent, statuscode);
            //}

            UpdateSalesOpportunityForKreditt(evt, cust, ent.Id, response, query, isNew: true);

        }

        private static void UpdateSalesOpportunityApp(KundeHendelse evt, CustomerExtra cust, SalesOpportunity opportunity, ProcessedCustomerResponse response, QueryExpressionHelper query)
        {
            var ent = new Entity(EntityName.opportunity.ToString())
            {
                Id = opportunity.Id,
                ["cap_app"] = evt.Applikasjon
            };

            query.UpdateEntity(ent, response);
        }

        //private static void ReopenAndUpdateSalesOpportunity(KundeHendelse evt, CustomerExtra cust, SalesOpportunity opportunity, ProcessedCustomerResponse response, QueryExpressionHelper query, Guid productId = new Guid())
        //{
        //    var newEstValue = evt.Verdi - evt.TidligereVerdi;

        //    // TODO check if this works while SO is closed
        //    OpportunityProductManager.HandleOpportunityProduct(opportunity.Id, cust.Product, response, query, newEstValue);

        //    if (newEstValue < 0)
        //    {
        //        IncidentManager.CreateAndResolveIncident(evt, cust, response, query);
        //    }
        //    else
        //    {
        //        query.ReopenSalesOpportunity(opportunity.Id, response);


        //        var ent = new Entity(EntityName.opportunity.ToString())
        //        {
        //            Id = opportunity.Id,
        //            ["cap_app"] = evt.Applikasjon
        //        };

        //        var success = query.UpdateEntity(ent, response);
        //        if (success) InfoActivityManager.CreateInfoActivityInformingChange(evt, ent.Id, cust.DefaultTeamId, response, query);


        //        if (opportunity.StateCode == (int)OpportunityStateCode.Won)
        //        {
        //            query.CloseSalesOpportunityWon(opportunity.Id, newEstValue, evt.Tidspunkt, response);
        //        }
        //        else if (opportunity.StateCode == (int)OpportunityStateCode.Lost)
        //        {
        //            query.CloseSalesOpportunityLost(opportunity.Id, evt.Tidspunkt, response);
        //        }
        //    }
        //}
        private static void UpdateSalesOpportunity(KundeHendelse evt, CustomerExtra cust, SalesOpportunity opportunity, bool keepOpen, ProcessedCustomerResponse response, QueryExpressionHelper query, Guid productId = new Guid())
        {
            if (opportunity.EventDate > evt.Tidspunkt)
            {
                Log.Warn($"Ignoring update of sales opportunity ({opportunity.Id}) due to old event timestamp. (op: {opportunity.EventDate} vs. evt: {evt.Tidspunkt})");
                // TODO : Consider to Create Info activity
                return;
            }

            var newEstValue = evt.Verdi - evt.TidligereVerdi;

            OpportunityProductManager.HandleOpportunityProduct(opportunity.Id, cust.Product, response, query, newEstValue);

            if (newEstValue < 0)
            {
                CloseSalesOpportunityLost(evt, opportunity, response, query);
                IncidentManager.CreateAndResolveIncident(evt, cust, response, query);
            }
            else
            {
                if (keepOpen)
                {
                    var statuscode = ProcessHelper.GetOpportunityStatusCode(evt.Fase);

                    var ent = RequestBuilder.BuildUpdateOpportunity(opportunity, evt, cust, newEstValue, productId, statuscode);

                    var success = query.UpdateEntity(ent, response);

                    if (success) InfoActivityManager.CreateInfoActivityInformingChange(evt, ent.Id, cust.DefaultTeamId, response, query);
                }
                else
                {
                    var ent = RequestBuilder.BuildUpdateOpportunityForClose(opportunity, evt, newEstValue, productId);

                    var success = query.UpdateEntity(ent, response);
                    if (success) query.CloseSalesOpportunityWon(opportunity.Id, newEstValue, evt.Tidspunkt, response);
                }
            }
        }
        private static void UpdateSalesOpportunityForRadgiverPluss(SakHendelse evt, CustomerExtra cust, SalesOpportunity opportunity, bool keepOpen, bool lost, ProcessedCustomerResponse response, QueryExpressionHelper query, Guid productId = new Guid())
        {
            var newEstValue = 0;
            var statuscode = ProcessHelper.GetOpportunityStatusCodeForRaadgiverpluss(cust.SakHendelseAttributes.Result);
            var ent = RequestBuilder.BuildUpdateOpportunityRadgiverPluss(opportunity, evt, cust, statuscode);
            var success = query.UpdateEntity(ent, response);

            if (keepOpen)
            {
                if (success) InfoActivityManager.CreateInfoActivityInformingChange(evt, cust, ent.Id, response, query);
            }
            else
            {
                if (lost) query.CloseSalesOpportunityLost(opportunity.Id, evt.Tidspunkt, response);
                else query.CloseSalesOpportunityWon(opportunity.Id, newEstValue, evt.Tidspunkt, response);
            }
        }

        private static void UpdateSalesOpportunityForRadgiverPluss(KundeHendelse evt, CustomerExtra cust, SalesOpportunity opportunity, bool keepOpen, ProcessedCustomerResponse response, QueryExpressionHelper query, Guid productId = new Guid())
        {
            var newEstValue = evt.Verdi - evt.TidligereVerdi;

            OpportunityProductManager.HandleOpportunityProduct(opportunity.Id, cust.Product, response, query, newEstValue);

            if (newEstValue < 0)
            {
                CloseSalesOpportunityLost(evt, opportunity, response, query);
                IncidentManager.CreateAndResolveIncident(evt, cust, response, query);
            }
            else
            {
                if (keepOpen)
                {
                    var statuscode = ProcessHelper.GetOpportunityStatusCode(evt.Fase);

                    var ent = RequestBuilder.BuildUpdateOpportunityRadgiverPluss(opportunity, evt, cust, newEstValue, productId, statuscode);

                    var success = query.UpdateEntity(ent, response);

                    if (success) InfoActivityManager.CreateInfoActivityInformingChange(evt, ent.Id, cust.DefaultTeamId, response, query);
                }
                else
                {
                    var ent = RequestBuilder.BuildUpdateOpportunityForClose(opportunity, evt, newEstValue, productId);

                    var success = query.UpdateEntity(ent, response);
                    if (success) query.CloseSalesOpportunityWon(opportunity.Id, newEstValue, evt.Tidspunkt, response);
                }
            }
        }

        private static void UpdateSalesOpportunityForKreditt(SakHendelse evt, CustomerExtra cust, Guid opportunityId, ProcessedCustomerResponse response, QueryExpressionHelper query, bool isOpen = true, bool isNew = false)
        {
            var ent = new Entity(EntityName.opportunity.ToString())
            {
                Id = opportunityId
            };

            var hendelseType = EnumUtils.Parse<SakHendelseTypeEnum>(evt.SakHendelsesType);
            //var hendelseType = (SakHendelseTypeEnum)Helper.GetEnumValueOf(evt.SakHendelsesType, typeof(SakHendelseTypeEnum));
            var statuscode = ProcessHelper.GetOpportunityStatusCode(evt.SakStatus);
            RequestBuilder.UpdateOpportunityStatus(ent, statuscode);

            if (!isOpen && hendelseType == SakHendelseTypeEnum.Ny_Sak_Opprettet)
            {
                query.ReopenSalesOpportunity(ent.Id, response);
                RequestBuilder.UpdateOpportunityForNySakOpprettet(evt, ent, cust);
            }
            else if (!isOpen)
            {
                return; //Nothing more to do
            }
            else if (hendelseType == SakHendelseTypeEnum.Ny_Sak_Opprettet && isNew)
            {
                RequestBuilder.UpdateOpportunityForNySakOpprettet(evt, ent, cust);
                //interessent rolle or something
            }
            else
            {
                switch (hendelseType)
                {
                    case SakHendelseTypeEnum.Statusendring: //TODO undefined
                        RequestBuilder.UpdateOpportunityTitleFormal(evt, ent);
                        break;
                    case SakHendelseTypeEnum.Ny_Interessent:
                        // Add/Update connection
                        // Update status??
                        break;
                    case SakHendelseTypeEnum.Slettet_Interresent:
                        // Remove connection
                        break;
                    case SakHendelseTypeEnum.Ny_Raadgiver:
                        RequestBuilder.UpdateCustomerOwner(cust, ent);
                        break;
                    case SakHendelseTypeEnum.Sak_Avsluttet:
                        switch (EnumUtils.Parse<HendelseStatusEnum>(evt.SakStatus))
                        {
                            case HendelseStatusEnum.Behov_Opphoert:
                            case HendelseStatusEnum.Avslatt:
                                //close as lost
                                return;
                            case HendelseStatusEnum.Ferdig_behandlet:
                                //close as won
                                return;
                            default:
                                //undefined sak status for event:  Sak_Avsluttet
                                return;
                        }
                    default:
                        break;
                }
            }

            RequestBuilder.UpdateOpportunityEventDate(evt, ent);
            var success = query.UpdateEntity(ent, response);
            //if (success) InfoActivityManager.CreateInfoActivityInformingChange(evt, ent.Id, cust.DefaultTeamId, response, query);
        }

        private static void UpdateAndCloseSalesOpportunityWon(KundeHendelse evt, CustomerExtra cust, SalesOpportunity opportunity, ProcessedCustomerResponse response, QueryExpressionHelper query, Guid productId = new Guid())
        {
            log.Add($"UpdateAndCloseSalesOpportunityWon");
            var newEstValue = evt.Verdi - evt.TidligereVerdi;
            log.Add($"UpdateAndCloseSalesOpportunityWon.newestValue: {newEstValue}");

            //cust.Product.Value = newEstValue; // TODO use this field instead of sending it with the method.
            OpportunityProductManager.HandleOpportunityProduct(opportunity.Id, cust.Product, response, query, newEstValue);

            if (newEstValue < 0)
            {
                CloseSalesOpportunityLost(evt, opportunity, response, query);
                IncidentManager.CreateAndResolveIncident(evt, cust, response, query);
            }
            else
            {
                var ent = RequestBuilder.BuildUpdateOpportunityForClose(opportunity, evt, newEstValue, productId);

                var success = query.UpdateEntity(ent, response);
                log.Add($"UpdateAndCloseSalesOpportunityWon.UpdateEntity: {success}");
                //if (success) InfoActivityManager.CreateInfoActivityInformingChange(evt, ent.Id, cust.DefaultTeamId, response, query);

                if (success) query.CloseSalesOpportunityWon(opportunity.Id, newEstValue, evt.Tidspunkt, response);
            }
        }
        private static void CloseSalesOpportunityLost(KundeHendelse evt, SalesOpportunity opportunity, ProcessedCustomerResponse response, QueryExpressionHelper query)
        {
            //var description = "Kunden har redusert omfanget av avtalen eller gjort endringer som ikke påvirker omfanget. Ny sak er opprettet.";
            var opportunityDescription = SalesOpportunityConstant.LostDescription;

            if (opportunity.Source == TerritoryCode.Crm)
            {
                opportunityDescription = $"{opportunity.Description}\n\n{SalesOpportunityConstant.LostDescription}";
            }

            query.UpdateEntity(new Entity(EntityName.opportunity.ToString())
            {
                Id = opportunity.Id,
                ["description"] = opportunityDescription
            }, response);

            query.CloseSalesOpportunityLost(opportunity.Id, evt.Tidspunkt, response);

        }


        private static void CreateSalesOpportunityChild(KundeHendelse evt, CustomerExtra cust, bool keepOpen, Guid parentOpportunityId, ProcessedCustomerResponse response, QueryExpressionHelper query)
        {
            var newEstValue = evt.Verdi - evt.TidligereVerdi;
            if (newEstValue < 0)
            {
                IncidentManager.CreateAndResolveIncident(evt, cust, response, query);
            }
            else
            {
                var statuscode = ProcessHelper.GetOpportunityStatusCode(evt.Fase);

                var ent = RequestBuilder.BuildCreateOpportunity(evt, cust, newEstValue, statuscode);
                RequestBuilder.UpdateOpportunityParent(ent, parentOpportunityId);

                SetOwnerForChildOpportunity(evt.Applikasjon, cust, ent);

                ent.Id = query.CreateEntity(ent, response);

                OpportunityProductManager.HandleOpportunityProduct(ent.Id, cust.Product, response, query, newEstValue);
                if (keepOpen) InfoActivityManager.CreateInfoActivityInformingChange(evt, ent.Id, cust.DefaultTeamId, response, query, true);
                else query.CloseSalesOpportunityWon(ent.Id, newEstValue, evt.Tidspunkt, response);
            }
        }

        private static void SetOwnerForChildOpportunity(string applikasjon, CustomerExtra cust, Entity ent)
        {
            if (IsApplication(applikasjon, KundehendelseAppName.tradex_source))
            {
                RequestBuilder.UpdateCustomerOwnerWithLoggedIn(cust, ent);
            }
        }


        private static bool IsApplication(string applikasjon, KundehendelseAppName appEnum)
        {
            return applikasjon.Equals(Initialize.KundehendelseAppNames[appEnum.ToString()], StringComparison.CurrentCultureIgnoreCase);
        }
        private static void SetParentOfOpportunity(Guid opportunityId, Guid parentOpportunityId, ProcessedCustomerResponse response, QueryExpressionHelper query)
        {
            var ent = new Entity(EntityName.opportunity.ToString()) { Id = opportunityId };
            RequestBuilder.UpdateOpportunityParent(ent, parentOpportunityId);

            query.UpdateEntity(ent, response);
        }

        #endregion

    }
}
