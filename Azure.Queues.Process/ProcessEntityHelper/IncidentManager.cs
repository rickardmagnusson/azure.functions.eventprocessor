﻿using System;
using System.Collections.Generic;
using Common;
using Common.Adapters;
using Common.Entities;
using Common.Enums;
using Common.Initializer;
using Common.Logging;
using Common.RequestBuilders;

namespace Azure.Queues.Process.ProcessEntityHelper
{
    public class IncidentManager
    {
        private static readonly MsmqLogger Log = MsmqLogger.CreateLogger("incident-manager");


        public static bool DeleteRelatedIncidents(Guid customerId, Guid subjectId, Guid vendorBankId, bool isPersonOnProductCompanyCard, string bankNum, ProcessedCustomerResponse response, QueryCrm query)
        {
            var casesToDelete = query.GetRelatedIncidents(customerId, subjectId, vendorBankId, isPersonOnProductCompanyCard, bankNum, response);
            if (response.FailedProcessing) return false;

            foreach (var caseToDelete in casesToDelete)
            {
                query.DeleteEntity(EntityName.incident, caseToDelete.Id, response);
            }
            if(casesToDelete.Count > 0) Log.Debug($"Related Cases ({casesToDelete.Count}) are deleted from card.");

            return response.Success;
        }


        public static void HandleIncident(KundeHendelse evt, CustomerExtra cust, ProcessedCustomerResponse response, QueryExpressionHelper query)
        {
            var opportunities = new List<SalesOpportunity>();
            if (!cust.IsNewCustomer) opportunities = query.GetSalesOpportunitiesByCustomerId(cust.CustomerId, response);
            if (response.FailedProcessing) return;

            OpportunityManager.SalesOpportunityZeroRevenueToLost(evt, cust, response, query, opportunities);

            //var opportunity = opportunities.FirstOrDefault(e => e.SubjectId == cust.SubjectId &&
            //                                                e.StateCode == 0 &&
            //                                                e.ActualRevenue == 0 &&
            //                                                //e.ProductId == cust.Product.Id &&
            //                                                e.App.Equals(evt.Applikasjon, StringComparison.CurrentCultureIgnoreCase));

            //if (opportunity != null)
            //{
            //    Log.Debug("Case check => true");
            //    CloseSalesOpportunityLost(evt, opportunity, response, query);
            //}

            CreateAndResolveIncident(evt, cust, response, query);
        }
        public static void CreateAndResolveIncident(KundeHendelse evt, CustomerExtra cust, ProcessedCustomerResponse response, QueryExpressionHelper query)
        {
            var incident = RequestBuilder.BuildCreateIncident(evt, cust);

            if (Helper.ApplicationIsFromWeb(evt.Applikasjon, Initialize.KundehendelseAppNames))
            {
                RequestBuilder.SetIncidentOriginToWeb(incident);
            }

            incident.Id = query.CreateEntity(incident, response);
            query.CloseIncidentResolved(incident, response);
        }
    }
}
