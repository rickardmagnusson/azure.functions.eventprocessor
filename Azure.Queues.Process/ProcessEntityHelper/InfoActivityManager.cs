﻿using System;
using Azure.Logging;
using Common;
using Common.Adapters;
using Common.Entities;
using Common.Enums;
using Common.RequestBuilders;

namespace Azure.Queues.Process.ProcessEntityHelper
{
    public class InfoActivityManager
    {
    
        private static SingletonLogger Log = SingletonLogger.Instance();

        public static bool HasRecentActivities(Guid customerId, EntityName entityName, QueryCrm query, bool productCompanyCard = false)
        {
            if (HasActivityLastYearOrFuturePlans(customerId, query)) return true;
            if (productCompanyCard && HasExternalCustomerOpportunityLastYear(customerId, entityName, query)) return true;

            return false;
        }

        private static bool HasExternalCustomerOpportunityLastYear(Guid customerId, EntityName entityName, QueryCrm query)
        {
            var isContact = entityName == EntityName.contact;

            if (query.HasExternalCustomerOpportunityLastGivenMonths(customerId, isContact, 12)) return true;

            return false;
        }


        private static bool HasActivityLastYearOrFuturePlans(Guid customerId, QueryCrm query)
        {
            if (query.HasActivityLastGivenMonthsOrFuturePlans(customerId, 12)) return true;
            if (query.HasOpportunityLastGivenMonths(customerId, 12)) return true;

            return false;
        }


        public static bool HasActivityLastSixMonths(Guid customerId, QueryCrm query)
        {
            if (query.GetLastGivenMonthsActivityCount(customerId, 6) > 0) return true;
            if (query.GetLastGivenMonthsOpportunityCount(customerId, 6) > 0) return true;
            return query.GetLastGivenMonthsCaseCount(customerId, 6) > 0;
        }

        #region SakHendelse

        public static void CreateInfoActivityInformingChange(SakHendelse evt, CustomerExtra cust, Guid opportunityId, ProcessedCustomerResponse response, QueryExpressionHelper query, bool createOfSo = false)
        {
            if (opportunityId == Guid.Empty) return;

            var ent = RequestBuilder.BuildInfoActivitySummaryOnSalesOpportunity(evt, cust, opportunityId, createOfSo);

            var entId = query.CreateEntity(ent, response);
            query.SetInfoActivityAsCompleted(entId, response);
        }


        public static void CreateUndefinedInfoActivity(SakHendelse evt, CustomerExtra cust, ProcessedCustomerResponse response, QueryExpressionHelper query)
        {
            var ent = RequestBuilder.BuildUndefinedInfoActivity(evt, cust);

            var entId = query.CreateEntity(ent, response);
            query.SetInfoActivityAsCompleted(entId, response);
        }

        #endregion


        public static void CreateInfoActivityInformingChange(KundeHendelse evt, Guid opportunityId, Guid defaultTeamId, ProcessedCustomerResponse response, QueryExpressionHelper query, bool createOfSo = false)
        {
            if (opportunityId == Guid.Empty) return;

            var ent = RequestBuilder.BuildInfoActivitySummaryOnSalesOpportunity(evt, opportunityId, defaultTeamId, createOfSo);

            var entId = query.CreateEntity(ent, response);
            query.SetInfoActivityAsCompleted(entId, response);
        }

        public static void CreateUndefinedInfoActivity(KundeHendelse evt, CustomerExtra cust, ProcessedCustomerResponse response, QueryExpressionHelper query)
        {
            var ent = RequestBuilder.BuildUndefinedInfoActivity(evt, cust);
            var entId = query.CreateEntity(ent, response);

            Log.Add($"CreateUndefinedInfoActivity: {entId} {response.ErrorMessage}");

            query.SetInfoActivityAsCompleted(entId, response);
        }


    }
}
