﻿using System;
using Common;
using Common.Adapters;
using Common.Entities;
using Common.Logging;
using Common.RequestBuilders;

namespace Azure.Queues.Process.ProcessEntityHelper
{
    public class DepartmentManager
    {
        
        public static void CreateOrUpdateDepartmentGeneric(string departmentName, string departmentNumber, CustomerExtra cust, ProcessedCustomerResponse response, QueryCrm query, MsmqLogger Log)
        {
            if (string.IsNullOrEmpty(departmentName) || string.IsNullOrEmpty(departmentNumber)) return;

            var departmentInfo = query.GetDepartmentInfoByNumberAndBank(departmentNumber, cust.BankId);
            cust.DepartmentId = departmentInfo.DepartmentId;

            if (cust.DepartmentId == Guid.Empty)
            {
                cust.DepartmentId = CreateDepartment(departmentName, departmentNumber, cust.DefaultTeamId, response, query);
                if (cust.DepartmentId == Guid.Empty) Log.Warn($"Failed to create department with number {departmentNumber}.");
            }
            else if (!departmentInfo.IsActive || !departmentInfo.DepartmentName.Equals(departmentName))
            {
                var success = UpdateDepartment(departmentName, cust.DepartmentId, response, query);
                if (!success) Log.Warn($"Failed to update department with id {cust.DepartmentId}.");
            }
        }



        public static void CreateOrUpdateDepartment(KundeRegisterEvent evt, CustomerExtra cust, ProcessedCustomerResponse response, QueryCrm query, MsmqLogger Log, bool onlyRetrieve = false)
        {
            if (string.IsNullOrEmpty(evt.DepartmentName) || string.IsNullOrEmpty(evt.DepartmentNumber)) return;

            var departmentInfo = query.GetDepartmentInfoByNumberAndBank(evt.DepartmentNumber, cust.BankId);
            cust.DepartmentId = departmentInfo.DepartmentId;

            if (!onlyRetrieve)
            {
                if (cust.DepartmentId == Guid.Empty)
                {
                    cust.DepartmentId = CreateDepartment(evt.DepartmentName, evt.DepartmentNumber, cust.DefaultTeamId, response, query);
                    if (cust.DepartmentId == Guid.Empty) Log.Warn($"Failed to create department with number {evt.DepartmentNumber}.");
                }
                else if (!departmentInfo.IsActive || !departmentInfo.DepartmentName.Equals(evt.DepartmentName))
                {
                    var success = UpdateDepartment(evt.DepartmentName, cust.DepartmentId, response, query);
                    if (!success) Log.Warn($"Failed to update department with id {cust.DepartmentId}.");
                }
            }
        }


        private static Guid CreateDepartment(string departmentName, string departmentNumber, Guid defaultTeamId, ProcessedCustomerResponse response, QueryCrm query)
        {
            var entity = RequestBuilder.CreateDepartment(departmentName, departmentNumber, defaultTeamId);
            return query.CreateEntity(entity, response);
        }
        private static bool UpdateDepartment(string departmentName, Guid depId, ProcessedCustomerResponse response, QueryCrm query)
        {
            var entity = RequestBuilder.UpdateDepartment(departmentName);
            entity.Id = depId;
            return query.UpdateEntity(entity, response);
        }


    }
}
