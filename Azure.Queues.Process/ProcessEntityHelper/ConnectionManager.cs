﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common;
using Common.Adapters;
using Common.Entities;
using Common.Enums;
using Common.Helpers;
using Common.Initializer;
using Common.Logging;
using Common.RequestBuilders;

namespace Azure.Queues.Process.ProcessEntityHelper
{
    public class ConnectionManager
    {
        //private static readonly MsmqLogger Log = MsmqLogger.CreateLogger("connection-manager");

        public static BankConnection GetBankConnection(CustomerExtra cust)
        {
            var bankConn = new BankConnection
            {
                CustomerId = cust.CustomerId,
                EntityName = cust.EntityName,
                OwnerId = cust.DefaultTeamId,
                BankConnectionId = cust.OppositeBankId,
            };

            GetConnectionRoleId(bankConn, cust.VendorRoleName, cust.HasCustomerRoleName);

            return bankConn;
        }


        public static BankConnection GetBankConnection(CustomerExtra cust, string vendorRoleName, string hasCustomerRoleName)
        {
            var bankConn = new BankConnection
            {
                CustomerId = cust.CustomerId,
                EntityName = cust.EntityName,
                OwnerId = cust.DefaultTeamId,
                BankConnectionId = cust.OppositeBankId,
            };

            GetConnectionRoleId(bankConn, vendorRoleName, hasCustomerRoleName);

            return bankConn;
        }


        public static List<Guid> AddConnection(BankConnection bankConn, CustomerExtra cust, ProcessedCustomerResponse response, QueryCrm query, MsmqLogger Log)
        {
            var activeConnectionsInfo = query.GetConnectionInfoFromBankConnections(bankConn.CustomerId, bankConn.VendorRoleId);
            var activeConnectionsBank = activeConnectionsInfo.Select(e => e.BankId).ToList();

            if (!activeConnectionsBank.Contains(cust.OppositeBankId))
            {
                var id = PerformCreateBankConnection(bankConn, response, query);
                activeConnectionsBank.Add(id);
            }

            if (!response.Success)
            {
                Log.Error($"An error occured at {Util.GetMethodName()} for customer id '{cust.CustomerId}'.");
            }

            return activeConnectionsBank;
        }


        public static List<Guid> RemoveConnection(CrmCustomer crmCustomerCard, KundeRegisterEvent evt, string vendorRoleName, Guid connectionBankId, ProcessedCustomerResponse response, QueryCrm query, MsmqLogger Log)
        {
            var connectionRoleId = GetConnectionRoleId(vendorRoleName);

            var activeConnectionsInfo = query.GetConnectionInfoFromBankConnections(crmCustomerCard.Id, connectionRoleId);
            var activeConnectionsBank = activeConnectionsInfo.Select(e => e.BankId).ToList();


            if (activeConnectionsBank.Contains(connectionBankId))
            {
                PerformDeleteConnection(activeConnectionsInfo.First(e => e.BankId == connectionBankId).ConnectionId, response, query);
                activeConnectionsBank.Remove(connectionBankId);
            }

            if (!response.Success)
            {
                Log.Error($"An error occured at {Util.GetMethodName()} for customer id '{crmCustomerCard.Id}'.");
            }

            return activeConnectionsBank;
        }


        private static Guid GetConnectionRoleId(string vendorRoleName)
        {
            var roleIdsByType = Initialize.ConnectionRoleByType;

            return roleIdsByType.ContainsKey(vendorRoleName) ? roleIdsByType[vendorRoleName] : Guid.Empty;
        }


        private static void GetConnectionRoleId(BankConnection bankConn, string vendorRoleName, string hasCustomerRoleName)
        {
            var roleIdsByType = Initialize.ConnectionRoleByType;

            bankConn.VendorRoleId = roleIdsByType.ContainsKey(vendorRoleName) ? roleIdsByType[vendorRoleName] : Guid.Empty;
            bankConn.HasCustomerRoleId = roleIdsByType.ContainsKey(hasCustomerRoleName) ? roleIdsByType[hasCustomerRoleName] : Guid.Empty;

            bankConn.Validated = bankConn.VendorRoleId != Guid.Empty && bankConn.HasCustomerRoleId != Guid.Empty;
        }


        private static Guid PerformCreateBankConnection(BankConnection conn, ProcessedCustomerResponse response, QueryCrm query)
        {
            var entity = RequestBuilder.BuildCreateConnectionEntity(conn);

            return query.CreateEntity(entity, response);
        }
        private static bool PerformDeleteConnection(Guid connectionId, ProcessedCustomerResponse response, QueryCrm query)
        {
            if (connectionId == Guid.Empty) return true;

            return query.DeleteEntity(EntityName.connection, connectionId, response);
        }


    }
}
