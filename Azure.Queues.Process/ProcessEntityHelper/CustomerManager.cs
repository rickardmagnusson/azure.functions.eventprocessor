﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common;
using Common.Adapters;
using Common.Entities;
using Common.Enums;
using Common.Helpers;
using Common.Logging;
using Common.RequestBuilders;
using Microsoft.Xrm.Sdk;

namespace Azure.Queues.Process.ProcessEntityHelper
{
    public class CustomerManager
    {
        
        public static bool ValidateShortCustomerEssentials(string custNo, string bankNo, ProcessedCustomerResponse response, MsmqLogger Log)
        {
            if (!string.IsNullOrEmpty(bankNo) &&
                !string.IsNullOrEmpty(custNo)) return true;

            LogHelper.Msg($"{Util.GetMethodName()} failed. The event is missing either customer number or bank number.", response, LogLevel.Warn, Log, true);
            return false;
        }


        public static bool ValidateCustomerEssentials(string custNo, string bankNo, string sourceId, ProcessedCustomerResponse response, MsmqLogger Log)
        {
            if (!string.IsNullOrEmpty(sourceId) && !string.IsNullOrEmpty(bankNo) &&
                !string.IsNullOrEmpty(custNo)) return true;

            LogHelper.Msg($"{Util.GetMethodName()} failed. The event is missing either sourceid, customer number or bank number.", response, LogLevel.Warn, Log, true);
            return false;
        }


        public static bool ValidateCustomerEssentials(KundeRegisterEvent evt, ProcessedCustomerResponse response, MsmqLogger Log)
        {
            if (!string.IsNullOrEmpty(evt.CustomerSourceId) && !string.IsNullOrEmpty(evt.DistributorBankNumber) &&
                !string.IsNullOrEmpty(evt.CustomerNumber)) return true;

            LogHelper.Msg($"{Util.GetMethodName()} failed. The event is missing either sourceid, customer number or bank number.", response, LogLevel.Warn, Log, true);
            return false;
        }


        public static bool ValidateBank(Guid bankId, string bankNumber, ProcessedCustomerResponse response, MsmqLogger Log)
        {
            if (bankId != Guid.Empty) return true;

            LogHelper.Msg($"{Util.GetMethodName()} failed. The BankNumber '{bankNumber}' is not valid (not found). The change is ignored.", response, LogLevel.Warn, Log);
            return false;
        }


        public static bool ValidateTimestamp(KundeRegisterEvent evt, CrmCustomer crmCustomerCard, CustomerExtra cust, MsmqLogger Log)
        {
            if (crmCustomerCard != null)
            {
                Log.Debug($"crm date: {crmCustomerCard.CapEventdate} __ vs __ evt date: {evt.CustomerEventSent}");

                if (crmCustomerCard.CapEventdate > evt.CustomerEventSent)
                {
                    Log.Warn($"A newer event has already been run. Skipping this change. Last datestamp on this customer: {crmCustomerCard.CapEventdate}");
                    return false;
                }
            }

            return true;
        }


        public static bool CanOverwriteCustomerCardOld(CrmCustomer crmCustomerCard, Source sourceOnEvent)
        {
            if (crmCustomerCard == null) return true;
            var sourceOnCard = crmCustomerCard.Source;

            if (sourceOnEvent == Source.Kerne) return true;

            if ((sourceOnEvent == Source.NiceSkade || sourceOnEvent == Source.NicePerson)
                && sourceOnCard != TerritoryCode.Kjerne) return true;

            if (sourceOnEvent == Source.Tradex
                && sourceOnCard != TerritoryCode.Kjerne
                && sourceOnCard != TerritoryCode.Nice) return true;

            if (sourceOnEvent == Source.BanQsoft
                && sourceOnCard != TerritoryCode.Kjerne
                && sourceOnCard != TerritoryCode.Nice
                && sourceOnCard != TerritoryCode.Tradex) return true;

            return false;
        }


        public static bool CanOverwriteCustomerCard(CrmCustomer crmCustomerCard, Source sourceOnEvent)
        {
            if (crmCustomerCard.Id == Guid.Empty || crmCustomerCard.StateCode == 1) return true;
            var sourceOnCard = crmCustomerCard.Source;

            var higherValuedSources = new List<TerritoryCode>();
            foreach (var item in SourceSpecific.PrioList)
            {
                if (item.Key == sourceOnEvent) break;
                higherValuedSources.Add(item.Value);
            }

            if (sourceOnEvent == Source.NicePerson) higherValuedSources.Remove(TerritoryCode.Nice);

            return !higherValuedSources.Contains(sourceOnCard);
        }


        public static CrmCustomer GetCustomerCardAndMergeDuplicates(KundeRegisterEvent evt, CustomerExtra cust, ProcessedCustomerResponse response, QueryCrm query, MsmqLogger Log)
        {
            var crmCustomersBySourceId = query.GetDetailedCrmCustomerBySourceId(evt.CustomerSourceId, cust, evt.Source, response);
            var crmCustomersByCustomerNumber = query.GetDetailedCrmCustomerByCustomerNumber(evt.CustomerNumber, cust, evt.Source, response);


            var customers = new List<CrmCustomer>();
            customers.AddRange(crmCustomersBySourceId);
            customers.AddRange(crmCustomersByCustomerNumber);
            if (customers.Count == 0 || !response.Success) return new CrmCustomer();

            customers = SortCustomerListByPriority(customers);
            cust.CustomerId = customers.First().Id;

            var masterCustomer = customers.First();
            MergeAndDeleteDuplicates(customers, masterCustomer, response, query, Log);

            return masterCustomer;
        }


        private static void MergeAndDeleteDuplicates(List<CrmCustomer> customers, CrmCustomer masterCustomer, ProcessedCustomerResponse response, QueryCrm query, MsmqLogger Log)
        {
            var masterId = masterCustomer.Id;

            var duplicateCustomers = customers.Where(e => e.Id != masterId).ToList();

            foreach (var duplicate in duplicateCustomers)
            {
                var sourceIdFieldsToUpdate = GetSourceIdFieldsToUpdate(masterCustomer, duplicate);

                if (!query.MergeEntity(masterId, duplicate.Id, masterCustomer.EntityName.ToString(), sourceIdFieldsToUpdate, response)) return;
                if (!query.DeleteEntity(duplicate.EntityName, duplicate.Id, response)) return;

                response.ErrorMessage = "One or more customer cards have been merged and deleted due to this event.";
                Log.Warn("Found duplicated. Merged a duplicate into a master card, and deleted the duplicate.");
            }
        }


        private static Entity GetSourceIdFieldsToUpdate(CrmCustomer masterCustomer, CrmCustomer duplicate)
        {
            var ent = new Entity(masterCustomer.EntityName.ToString());

            if (string.IsNullOrEmpty(masterCustomer.KerneSourceId) && !string.IsNullOrEmpty(duplicate.KerneSourceId))
                ent["cap_sourceid"] = duplicate.KerneSourceId;
            
            if (string.IsNullOrEmpty(masterCustomer.NiceSkadeSourceId) && !string.IsNullOrEmpty(duplicate.NiceSkadeSourceId))
                ent["cap_nicesourceid"] = duplicate.NiceSkadeSourceId;
            
            if (string.IsNullOrEmpty(masterCustomer.NicePersonSourceId) && !string.IsNullOrEmpty(duplicate.NicePersonSourceId))
                ent["cap_nice2sourceid"] = duplicate.NicePersonSourceId;
            
            if (string.IsNullOrEmpty(masterCustomer.BanQsoftSourceId) && !string.IsNullOrEmpty(duplicate.BanQsoftSourceId))
                ent["cap_banqsoftsourceid"] = duplicate.BanQsoftSourceId;
            
            if (string.IsNullOrEmpty(masterCustomer.TradexSourceId) && !string.IsNullOrEmpty(duplicate.TradexSourceId))
                ent["cap_tradexsourceid"] = duplicate.TradexSourceId;

            return ent;
        }


        public static string GetSourceIdFromOtherCustomerCard(List<Guid> banks, KundeRegisterEvent evt, EntityName entityName, QueryCrm query)
        {
            var customerNumber = evt.CustomerNumber;
            var source = evt.Source;
            var currentSourceId = evt.CustomerSourceId;

            foreach (var bankIdAdditionalBank in banks)
            {
                var sourceIdFromOtherCustomerCard = query.GetSourceIdByBankAndCustomerNumber(customerNumber, bankIdAdditionalBank, entityName, Helper.GetSourceIdName(source));
                if (!string.IsNullOrEmpty(sourceIdFromOtherCustomerCard) && !sourceIdFromOtherCustomerCard.Equals(currentSourceId))
                {
                    return sourceIdFromOtherCustomerCard;
                }
            }
            return string.Empty;
        }


        public static bool UpdateCustomerSourceId(CrmCustomer crmCustomerCard, Source source, string sourceId, ProcessedCustomerResponse response, QueryCrm query, MsmqLogger Log, bool hendelse = false)
        {
            if (string.IsNullOrEmpty(sourceId)) return true;
            if (crmCustomerCard.SourceId.Equals(sourceId)) return true;
            if (hendelse && !string.IsNullOrEmpty(crmCustomerCard.SourceId)) return true; // TODO, decide to use this for kundehendelse

            var entity = new Entity(crmCustomerCard.EntityName.ToString()) { Id = crmCustomerCard.Id };
            RequestBuilder.UpdateCustomerSourceId(source, sourceId, entity);

            query.UpdateEntity(entity, response);

            if (!response.Success)
            {
                Log.Error($"An error occured at {Util.GetMethodName()} for customerId '{entity.Id}' updating sourceid '{sourceId}'.");
            }

            return response.Success;
        }


        public static bool RemoveCustomerSourceId(CrmCustomer crmCustomerCard, KundeRegisterEvent evt, ProcessedCustomerResponse response, QueryCrm query, MsmqLogger Log)
        {
            if (string.IsNullOrEmpty(crmCustomerCard.SourceId)) return true;

            var entity = new Entity(crmCustomerCard.EntityName.ToString()) { Id = crmCustomerCard.Id };
            RequestBuilder.RemoveCustomerSourceId(evt, entity);

            query.UpdateEntity(entity, response);

            if (!response.Success)
            {
                Log.Error($"An error occured at {Util.GetMethodName()} for customerId '{entity.Id}'.");
            }

            return response.Success;
        }


        public static bool RemoveCustomerSourceIdOfDeleteRequest(CrmCustomer crmCustomerCard, KundeRegisterEvent evt, ProcessedCustomerResponse response, QueryCrm query, MsmqLogger Log)
        {
            if (string.IsNullOrEmpty(crmCustomerCard.SourceId)) return true;
            if(IsDifferentPersonCustomerRelationshipAsSourceId(crmCustomerCard, evt)) return true;

            var entity = new Entity(crmCustomerCard.EntityName.ToString()) { Id = crmCustomerCard.Id };
            RequestBuilder.RemoveCustomerSourceId(evt.Source, entity);

            query.UpdateEntity(entity, response);

            if (!response.Success)
            {
                Log.Error($"An error occured at {Util.GetMethodName()} for customerId '{entity.Id}'.");
            }

            return response.Success;
        }


        private static bool IsDifferentPersonCustomerRelationshipAsSourceId(CrmCustomer crmCustomerCard, KundeRegisterEvent evt)
        {
            return evt.Source == Source.NicePerson &&
                   !string.IsNullOrEmpty(evt.CustomerSourceId) &&
                   !string.IsNullOrEmpty(crmCustomerCard.SourceId) &&
                   !evt.CustomerSourceId.Equals(crmCustomerCard.SourceId);
        }


        public static bool CreateOrUpdateCustomer(KundeRegisterEvent evt, CustomerExtra cust, ProcessedCustomerResponse response, QueryCrm query, MsmqLogger Log, bool productCompanyCustomerCard = false)
        {
            var directCustomer = evt.OwnerBankNumber.Equals(evt.DistributorBankNumber);
            if (directCustomer || !productCompanyCustomerCard)
            {
                cust.CustomerCardSystemuserOwnerId = query.GetUserIdByUsernameAndBankId(evt.OwnerUsername, cust.BankId);
                if (cust.CustomerCardSystemuserOwnerId == Guid.Empty && !string.IsNullOrEmpty(evt.OwnerUsername)) Log.Debug($"Username given '{evt.OwnerUsername}' was not found within the businessunit.");
            }
            DepartmentManager.CreateOrUpdateDepartment(evt, cust, response, query, Log);

            var entity = new Entity(cust.EntityName.ToString()) { Id = cust.CustomerId };
            RequestBuilder.UpdateCustomerEntity(evt, cust, entity, Log);

            cust.CustomerId = query.CreateOrUpdateEntity(entity, response);

            if (!response.Success)
            {
                Log.Error($"An error occured at {Util.GetMethodName()} for customer number '{evt.CustomerNumber}' for bank '{evt.DistributorBankNumber}'.");
            }

            return response.Success;
        }

        public static bool DeactivateCustomer(CrmCustomer customer, ProcessedCustomerResponse response, QueryCrm query, MsmqLogger Log)
        {
            var entity = RequestBuilder.BuildDeactivateEntity(customer.EntityName, customer.Id);
            query.UpdateEntity(entity, response);

            if (!response.Success)
            {
                Log.Error($"An error occured while trying to deactivate customer with customer id '{customer.Id}'.");
            }

            return response.Success;
        }


        public static bool SetCustomerToPotential(CrmCustomer customer, string sourceId, ProcessedCustomerResponse response, QueryCrm query, MsmqLogger Log)
        {
            if (customer.StateCode == 1)
            {
                LogHelper.LogDeleteOutcomes("Avoiding altering customer card due to crm card being inactive. Customer card has recent activity.", sourceId, Log);
                return true;
            }

            var entity = new Entity(customer.EntityName.ToString()) {Id = customer.Id};

            RequestBuilder.SetCustomerAsPotentialCustomer(entity);
            query.UpdateEntity(entity, response);

            if (!response.Success)
            {
                Log.Error($"An error occured while trying to set customer to potential customer with customer id '{customer.Id}'.");
            }
            else LogHelper.LogDeleteOutcomes("Customer has been set to potential in CRM, due to not having other customer relationships, but having recent activity on the card.", sourceId, Log);

            return response.Success;
        }


        public static bool DeleteCustomer(CrmCustomer customer, string sourceId, ProcessedCustomerResponse response, QueryCrm query, MsmqLogger Log)
        {
            query.DeleteEntity(customer.EntityName, customer.Id, response);

            if (!response.Success)
            {
                Log.Error($"An error occured while trying to delete customer with customer id '{customer.Id}'.");
            }
            else LogHelper.LogDeleteOutcomes("Customer card is deleted from CRM, due to not having other customer relationships and no recent activity on the card.", sourceId, Log);

            return response.Success;
        }


        #region Updating customer card with alternate source

        public static bool UpdateCustomerCardWithAlternateSource(KundeRegisterEvent evt, CrmCustomer crmCustomerCard, CustomerExtra cust, Source oldSource, ProcessedCustomerResponse response, QueryCrm query, MsmqLogger Log, bool productCompanyCard = false)
        {
            if (evt == null) return true;
            //if (!IsDifferentPersonCustomerRelationshipAsSourceId(crmCustomerCard, evt)) return true;

            Log.Debug($"Updating alternate customer card with source: '{evt.Source}' & sourceid: '{evt.CustomerSourceId}'");

            cust.EntityName = crmCustomerCard.EntityName;
            var includeSetOwner = cust.BankId == crmCustomerCard.BankId;
            if (includeSetOwner)
            {
                cust.CustomerCardSystemuserOwnerId = query.GetUserIdByUsernameAndBankId(evt.OwnerUsername, cust.BankId);
                if (cust.CustomerCardSystemuserOwnerId == Guid.Empty && !string.IsNullOrEmpty(evt.OwnerUsername)) Log.Debug($"Username given '{evt.OwnerUsername}' was not found within the businessunit.");
            }
            else
            {
                Log.Debug("includeSetOwner is FALSE");
            }

            DepartmentManager.CreateOrUpdateDepartment(evt, cust, response, query, Log, true);

            var entity = new Entity(cust.EntityName.ToString()) { Id = crmCustomerCard.Id };
            RequestBuilder.RemoveCustomerSourceId(oldSource, entity);
            RequestBuilder.UpdateCustomerEntity(evt, cust, entity, Log);

            cust.CustomerId = query.CreateOrUpdateEntity(entity, response);

            if (!response.Success)
            {
                Log.Error($"An error occured at {Util.GetMethodName()} for customer number '{evt.CustomerNumber}' for bank '{evt.DistributorBankNumber}'.");
            }

            return response.Success;
        }



        #endregion



        #region Private Methods

        private static List<CrmCustomer> SortCustomerListByPriority(List<CrmCustomer> customers)
        {
            return customers.OrderBy(e => e.StateCode)
                            .ThenByDescending(e => e.Source == TerritoryCode.Kjerne)
                            .ThenByDescending(e => e.Source == TerritoryCode.Nice)
                            .ThenByDescending(e => e.Source == TerritoryCode.BanQsoft)
                            .ThenByDescending(e => e.Source == TerritoryCode.Tradex)
                            .ThenByDescending(e => !string.IsNullOrEmpty(e.SourceId)).ToList();
        }

        #endregion
    }
}
