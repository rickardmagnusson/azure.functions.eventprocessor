﻿using System;
using Azure.Queues.Process;
using Common;
using Common.Adapters;
using Common.Entities;
using Common.Enums;
using Common.Helpers;
using Common.Logging;
using Common.Models;
using Azure.Queues.Process.ProcessEntityHelper;
using Azure.Queues.Process.ProcessHelpers;
using System.Text;

namespace Azure.Queues.Process.Kunderegister
{
    public class KundeRegisterManager
    {
        private static readonly MsmqLogger Log = MsmqLogger.CreateLogger("kunderegister-manager");
        static StringBuilder Logger;

        public static void IgnoringDelete(KundeRegisterEvent evt, ProcessedCustomerResponse response, QueryCrm query)
        {
            try
            {
                LogHelper.Msg($"Delete Customer request from source: '{evt.Source}'. Ignoring delete request due to waiting for business rules. Putting event in FailedMessages.", response, LogLevel.Warn, Log, true);
            }
            catch (Exception ex)
            {
                LogHelper.Error(Util.GetMethodName(), ex, response, Log);
            }
        }


        public static void PerformCustomerInsert(KundeRegisterEvent evt, ProcessedCustomerResponse response, QueryCrm query)
        {
            try
            {
                Log.Debug($"Starting {Util.GetMethodName()} for bank customer card.");
                var cust = ValidateAndPrepare(evt, response, query);
                if (!response.Success) return;

                var crmCustomerCard = CustomerManager.GetCustomerCardAndMergeDuplicates(evt, cust, response, query, Log);
                if (!response.Success) return;


                var timestampValidated = true;
                if (CustomerManager.CanOverwriteCustomerCard(crmCustomerCard, evt.Source))
                {
                    Log.Debug($"Inside {Util.GetMethodName()}. Source card: {crmCustomerCard.Source} :: Source evt: {evt.Source}");

                    var sameSource = SourceSpecific.PrioList.ContainsKey(evt.Source) && SourceSpecific.PrioList[evt.Source] == crmCustomerCard.Source;
                    if (sameSource) timestampValidated = CustomerManager.ValidateTimestamp(evt, crmCustomerCard, cust, Log);

                    if (!sameSource || timestampValidated)
                    {
                        if (!CustomerManager.CreateOrUpdateCustomer(evt, cust, response, query, Log)) return;
                    }
                }
                else
                {
                    Log.Debug($"Inside Update SourceId. CRM card: {crmCustomerCard.Id} :: Source evt: {evt.Source}");
                    if (!CustomerManager.UpdateCustomerSourceId(crmCustomerCard, evt.Source, evt.CustomerSourceId, response, query, Log)) return;
                }


                if (!timestampValidated) response.ErrorMessage = "Completed, but skipped customer card update due to timestamp being older than last update in CRM.";
            }
            catch (Exception ex)
            {
                LogHelper.Error(Util.GetMethodName(), ex, response, Log);
            }
        }


        public static void PerformCustomerDelete(KundeRegisterEvent evt, ProcessedCustomerResponse response, QueryCrm query, DataReader etlDb)
        {
            try
            {
                LogHelper.LogDeleteOutcomes($"Customer relationship for '{evt.CustomerNumber}' starting deletion under bank {evt.DistributorBankNumber} for source {evt.Source}.", evt.CustomerSourceId, Log);

                var cust = ValidateAndPrepare(evt, response, query);
                if (!response.Success) return;

                var crmCustomerCard = CustomerManager.GetCustomerCardAndMergeDuplicates(evt, cust, response, query, Log);
                if (crmCustomerCard.Id == Guid.Empty) LogHelper.Msg($"{Util.GetMethodName()}. Customer card not found within the bank. Event Ignored.", response, LogLevel.Warn, Log, true);
                if (!response.Success) return;


                if (!CustomerManager.RemoveCustomerSourceIdOfDeleteRequest(crmCustomerCard, evt, response, query, Log)) return;
                Log.Debug("SourceId is removed from card.");

                if (CustomerManager.CanOverwriteCustomerCard(crmCustomerCard, evt.Source))
                {
                    if (!HandleDeletionForCustomerCard(evt, response, query, etlDb, crmCustomerCard)) return;
                }
                else LogHelper.LogDeleteOutcomes($"Avoiding altering customer card due to card having a higher prioritized source. card source: {crmCustomerCard.Source}", evt.CustomerSourceId, Log);


                LogHelper.LogDeleteOutcomes($"Customer relationship for '{evt.CustomerNumber}' finished deletion under bank {evt.DistributorBankNumber} for source {evt.Source}.", evt.CustomerSourceId, Log);
            }
            catch (Exception ex)
            {
                LogHelper.Error(Util.GetMethodName(), ex, response, Log);
            }
        }


        public static void PerformVendorCustomerInsert(KundeRegisterEvent evt, ProcessedCustomerResponse response, QueryCrm query, bool productCompanyCustomerCard = false)
        {
            try
            {
                var bu = productCompanyCustomerCard ? "product company" : "bank";
                Log.Debug($"Starting {Util.GetMethodName()} for {bu} customer card.");
                var cust = ValidateAndPrepare(evt, response, query, productCompanyCustomerCard);
                if (!response.Success) return;


                var crmCustomerCard = CustomerManager.GetCustomerCardAndMergeDuplicates(evt, cust, response, query, Log);
                if (!response.Success) return;


                var timestampValidated = true;
                if (CustomerManager.CanOverwriteCustomerCard(crmCustomerCard, evt.Source))
                {
                    Log.Debug($"Inside {nameof(PerformVendorCustomerInsert)}. Source card: {crmCustomerCard.Source} :: Source evt: {evt.Source}");

                    var sameSource = SourceSpecific.PrioList.ContainsKey(evt.Source) && SourceSpecific.PrioList[evt.Source] == crmCustomerCard.Source;
                    if (sameSource) timestampValidated = CustomerManager.ValidateTimestamp(evt, crmCustomerCard, cust, Log);
                    if (!sameSource || timestampValidated)
                    {
                        if (!CustomerManager.CreateOrUpdateCustomer(evt, cust, response, query, Log, productCompanyCustomerCard)) return;
                    }
                }
                else
                {
                    Log.Debug($"Inside Update SourceId. CRM card: {crmCustomerCard.Id} :: Source evt: {evt.Source}");
                    if (!CustomerManager.UpdateCustomerSourceId(crmCustomerCard, evt.Source, evt.CustomerSourceId, response, query, Log)) return;
                }


                var connectionToAdd = ConnectionManager.GetBankConnection(cust);
                ConnectionManager.AddConnection(connectionToAdd, cust, response, query, Log);
                if (!response.Success) return;


                if (!timestampValidated) response.ErrorMessage = "Completed, but skipped a customer card update due to timestamp being older than last update in CRM.";
            }
            catch (Exception ex)
            {
                LogHelper.Error(Util.GetMethodName(), ex, response, Log);
            }
        }


        public static void PerformVendorCustomerDelete(KundeRegisterEvent evt, ProcessedCustomerResponse response, QueryCrm query, DataReader etlDb, bool productCompanyCustomerCard = false)
        {
            try
            {
                var bankNum = productCompanyCustomerCard ? "product company" : $"bank {evt.DistributorBankNumber}";
                LogHelper.LogDeleteOutcomes($"Customer relationship for '{evt.CustomerNumber}' starting deletion under {bankNum} for source {evt.Source}.", evt.CustomerSourceId, Log);

                var cust = ValidateAndPrepare(evt, response, query, productCompanyCustomerCard);
                if (!response.Success) return;


                var crmCustomerCard = CustomerManager.GetCustomerCardAndMergeDuplicates(evt, cust, response, query, Log);
                if (crmCustomerCard.Id == Guid.Empty) LogHelper.Msg($"{Util.GetMethodName()}. Customer card not found within the {bankNum}. Event Ignored.", response, LogLevel.Warn, Log, true);
                if (!response.Success) return;


                var activeConnectionsBank = ConnectionManager.RemoveConnection(crmCustomerCard, evt, cust.VendorRoleName, cust.OppositeBankId, response, query, Log);
                if (!response.Success) return;
                Log.Debug("Connection is removed from card.");

                if (!ProcessHelper.HandleDeleteAndUnshareRelatedRecords(evt, crmCustomerCard, productCompanyCustomerCard, response, query)) return;

                if (!CustomerManager.RemoveCustomerSourceIdOfDeleteRequest(crmCustomerCard, evt, response, query, Log)) return;
                Log.Debug("SourceId is removed from card.");


                if (CustomerManager.CanOverwriteCustomerCard(crmCustomerCard, evt.Source))
                {
                    if (!HandleDeletionForCustomerCard(evt, response, query, etlDb, crmCustomerCard, productCompanyCustomerCard)) return;
                }
                else LogHelper.LogDeleteOutcomes($"Avoiding altering customer card due to card having a higher prioritized source. card source: {crmCustomerCard.Source}", evt.CustomerSourceId, Log);


                LogHelper.LogDeleteOutcomes($"Customer relationship for '{evt.CustomerNumber}' finished deletion under {bankNum} for source {evt.Source}.", evt.CustomerSourceId, Log);
            }
            catch (Exception ex)
            {
                LogHelper.Error(Util.GetMethodName(), ex, response, Log);
            }
        }


        #region Private Methods

        private static bool HandleDeletionForCustomerCard(KundeRegisterEvent evt, ProcessedCustomerResponse response, QueryCrm query, DataReader etlDb, CrmCustomer crmCustomerCard, bool productCompanyCustomerCard = false)
        {
            var alternateCustomerCard = EtlDatabaseHelper.GetAlternateCustomerCard(evt, response, etlDb, productCompanyCustomerCard);
            if (alternateCustomerCard != null)
            {
                if (!UpdateAlternateCustomerCard(evt, response, query, alternateCustomerCard, crmCustomerCard, productCompanyCustomerCard)) return false;

                LogHelper.LogDeleteOutcomes($"Updating the crm customer card from {crmCustomerCard.Source} :: {crmCustomerCard.MainSourceId} :: (isactive={crmCustomerCard.IsActive})  to =>  {alternateCustomerCard.Source} :: {alternateCustomerCard.SourceId}", evt.CustomerSourceId, Log);
            }
            else
            {
                if (!DeleteOrSetToPotential(evt, crmCustomerCard, response, query)) return false;
            }

            return response.Success;
        }

        private static bool UpdateAlternateCustomerCard(KundeRegisterEvent evt, ProcessedCustomerResponse response, QueryCrm query, MsmqCompletedMessages altCustCard, CrmCustomer crmCustomerCard, bool productCompanyCustomerCard = false)
        {
            var eventtype = altCustCard.EventType;
            KundeRegisterEvent alternateCustomerCard = null;
            Log.Debug($"Found alt source of eventtype '{altCustCard.EventType}'");

            switch (eventtype)
            {
                case (int)EventMessageType.KundeRegisterEvent:
                {
                    alternateCustomerCard = SerializeHelper.DeserializeEventMessageContent<KundeRegisterEvent>(altCustCard.MessageContent);
                    break;
                }

                case (int)EventMessageType.NiceCustomerEvent:
                {
                    var altCard = SerializeHelper.DeserializeEventMessageContent<CustomerEvent>(altCustCard.MessageContent);
                    alternateCustomerCard = Mapper.MapOldToNewKundeRegisterEvent(altCard);
                    break;
                }
                case (int)EventMessageType.EventStore_EventMaster:
                {
                    var altCard = SerializeHelper.DeserializeEventMessageContent<EventMaster>(altCustCard.MessageContent);
                    alternateCustomerCard = Mapper.MapOldToNewKundeRegisterEvent(altCard);
                    break;
                }
                case (int)EventMessageType.DataVarehus_TemporaryEvent:
                {
                    var altCard = SerializeHelper.DeserializeEventMessageContent<DataWarehouseTempEvent>(altCustCard.MessageContent);
                    alternateCustomerCard = Mapper.MapOldToNewKundeRegisterEvent(altCard);
                    break;
                }
            }

            

            var alternateCust = ValidateAndPrepare(alternateCustomerCard, response, query, productCompanyCustomerCard);
            if (!response.Success) return true;

            var success = CustomerManager.UpdateCustomerCardWithAlternateSource(alternateCustomerCard, crmCustomerCard, alternateCust, evt.Source, response, query, Log, productCompanyCustomerCard);

            return response.Success;
        }

        private static bool DeleteOrSetToPotential(KundeRegisterEvent evt, CrmCustomer crmCustomerCard, ProcessedCustomerResponse response, QueryCrm query)
        {
            if (!ProcessHelper.HasSameSource(crmCustomerCard.Source, evt.Source))
            {
                LogHelper.LogDeleteOutcomes($"Avoiding altering customer card due to crm card having a different source. Card source: {crmCustomerCard.Source}", evt.CustomerSourceId, Log);
                //Log.Info($"Avoiding altering customer card due to crm card having a different source. Card source: {crmCustomerCard.Source}");
                return true;
            }

            var hasRecentActivity = InfoActivityManager.HasRecentActivities(crmCustomerCard.Id, crmCustomerCard.EntityName, query);

            Log.Debug("Gonna delete or set to potential.");
            return hasRecentActivity
                ? CustomerManager.SetCustomerToPotential(crmCustomerCard, evt.CustomerSourceId, response, query, Log)
                : CustomerManager.DeleteCustomer(crmCustomerCard, evt.CustomerSourceId, response, query, Log);
        }


        private static CustomerExtra ValidateAndPrepare(KundeRegisterEvent evt, ProcessedCustomerResponse response, QueryCrm query, bool productCompanyCard = false)
        {
            CustomerExtra cust = null;
            if (evt == null) return cust;

            switch (evt.Source)
            {
                case Source.Kerne:
                    cust = KerneCustomerManager.ValidateAndPrepare(evt, response, query);
                    break;
                case Source.NiceSkade:
                case Source.NicePerson:
                    cust = NiceCustomerManager.ValidateAndPrepare(evt, productCompanyCard, response, query);
                    break;
                case Source.BanQsoft:
                    cust = BanqsoftCustomerManager.ValidateAndPrepare(evt, response, query);
                    break;
                case Source.Tradex:
                    cust = TradexCustomerManager.ValidateAndPrepare(evt, productCompanyCard, response, query);
                    break;
            }

            return cust;
        }

        #endregion


    }
}
