﻿using System;
using Common;
using Common.Adapters;
using Common.Constants;
using Common.Entities;
using Common.Enums;
using Common.Helpers;
using Common.Initializer;
using Common.Logging;
using Azure.Queues.Process.ProcessEntityHelper;

namespace Azure.Queues.Process.Kunderegister
{
    public class TradexCustomerManager
    {
        private static readonly MsmqLogger Log = MsmqLogger.CreateLogger("tradex-manager");


        #region Public Methods


        public static void InsertCustomer(KundeRegisterEvent evt, ProcessedCustomerResponse response, QueryCrm query)
        {
            if (!CustomerManager.ValidateCustomerEssentials(evt, response, Log)) return;
            if (!GetEkfBankNumber(evt, response)) return;

            var directCustomer = evt.OwnerBankNumber.Equals(evt.DistributorBankNumber);
            if (!directCustomer)
            {
                // Update bank customer card
                KundeRegisterManager.PerformVendorCustomerInsert(evt, response, query);
            }

            // Update product company customer card
            KundeRegisterManager.PerformVendorCustomerInsert(evt, response, query, true);
        }
        public static void DeleteCustomer(KundeRegisterEvent evt, ProcessedCustomerResponse response, QueryCrm query, DataReader etlDb)
        {
            if (!CustomerManager.ValidateCustomerEssentials(evt, response, Log)) return;
            if (!GetEkfBankNumber(evt, response)) return;

            var directCustomer = evt.OwnerBankNumber.Equals(evt.DistributorBankNumber);
            if (!directCustomer)
            {
                // Remove bank customer card
                KundeRegisterManager.PerformVendorCustomerDelete(evt, response, query, etlDb);
            }

            // Remove product company customer card
            KundeRegisterManager.PerformVendorCustomerDelete(evt, response, query, etlDb, true);
        }

        public static CustomerExtra ValidateAndPrepare(KundeRegisterEvent evt, bool productCompanyCard, ProcessedCustomerResponse response, QueryCrm query)
        {
            var notValidatedResponse = $"The {nameof(KundeRegisterEvent)} request is not validated for custNum: '{evt.CustomerNumber}' & bank: '{evt.DistributorBankNumber}'";

            if (!CustomerManager.ValidateCustomerEssentials(evt, response, Log)) return null;
            if (!GetEkfBankNumber(evt, response)) return null;

            var connectionToBankNo = productCompanyCard ? evt.DistributorBankNumber : evt.OwnerBankNumber;
            var customerCardBankNo = productCompanyCard ? evt.OwnerBankNumber : evt.DistributorBankNumber;
            var isDirectCustomer = evt.DistributorBankNumber.Equals(evt.OwnerBankNumber);

            var bankId = query.GetBankIdByBankNo(customerCardBankNo);
            Log.Debug($"Validating bank num: '{customerCardBankNo}'");
            if (!CustomerManager.ValidateBank(bankId, customerCardBankNo, response, Log)) return null;

            var cust = new CustomerExtra
            {
                BankId = bankId,
                EntityName = Helper.GetEntityName(evt),
                DefaultTeamId = query.GetDefaultTeamIdByBankId(bankId)
            };

            Log.Debug("Validating entityname");
            if (cust.EntityName == EntityName.Undefined)
            {
                LogHelper.Msg($"{notValidatedResponse}. EntityName is undefined.", response, LogLevel.Error, Log);
                return null;
            }

            if (isDirectCustomer)
            {
                cust.OppositeBankId = bankId;
            }
            else
            {
                var oppositeBankId = query.GetBankIdByBankNo(connectionToBankNo);
                if (!CustomerManager.ValidateBank(oppositeBankId, connectionToBankNo, response, Log)) return null;
                cust.OppositeBankId = oppositeBankId;
            }

            cust.VendorRoleName = VendorConstant.TradexRoleName;
            cust.HasCustomerRoleName = VendorConstant.HasCustomerRoleName;

            return cust;
        }


        #endregion

        #region Private Methods


        private static bool GetEkfBankNumber(KundeRegisterEvent evt, ProcessedCustomerResponse response)
        {
            const string name = "Kapitalforvaltning";
            var bankNoByName = Initialize.EtlConfigBankNos;

            evt.OwnerBankNumber = bankNoByName.ContainsKey(name) ? bankNoByName[name] : string.Empty;

            if (string.IsNullOrEmpty(evt.OwnerBankNumber))
            {
                LogHelper.Msg($"An error at {Util.GetMethodName()}. Check insurance type: '{evt.InsuranceType}' and etl_config for BankNo", response, LogLevel.Error, Log);
                return false;
            }

            return true;
        }

        #endregion
    }
}
