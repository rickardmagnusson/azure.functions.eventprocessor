﻿using System;
using Common;
using Common.Adapters;
using Common.Entities;
using Common.Enums;
using Common.Helpers;
using Common.Logging;
using Azure.Queues.Process.ProcessEntityHelper;
using System.Text;

namespace Azure.Queues.Process.Kunderegister
{
    class KerneCustomerManager
    {
        private static readonly MsmqLogger Log = MsmqLogger.CreateLogger("kerne-customer-manager");
        static StringBuilder Logger;
        #region Public Methods

        public static void InsertCustomer(KundeRegisterEvent evt, ProcessedCustomerResponse response, QueryCrm query, StringBuilder logger)
        {
            Logger = logger;
            try
            {
                //Log.Debug($"Starting {Util.GetMethodName()} for bank customer card.");
                //var cust = ValidateAndPrepare(evt, response, query);
                //if (!response.Success) return;

                KundeRegisterManager.PerformCustomerInsert(evt, response, query);
            }
            catch (Exception ex)
            {
                LogHelper.Error(Util.GetMethodName(), ex, response, Log);
            }
        }
        public static void DeleteCustomer(KundeRegisterEvent evt, ProcessedCustomerResponse response, QueryCrm query, DataReader etlDb, StringBuilder logger)
        {
            Logger = logger;
            try
            {
                //Log.Debug($"Starting {Util.GetMethodName()} for bank customer card.");
                //var cust = ValidateAndPrepare(evt, response, query);
                //if (!response.Success) return;

                KundeRegisterManager.PerformCustomerDelete(evt, response, query, etlDb);
            }
            catch (Exception ex)
            {
                LogHelper.Error(Util.GetMethodName(), ex, response, Log);
            }
        }

        #endregion

        #region Private  Methods
        public static CustomerExtra ValidateAndPrepare(KundeRegisterEvent evt, ProcessedCustomerResponse response, QueryCrm query)
        {
            var notValidatedResponse = $"The {nameof(KundeRegisterEvent)} request is not validated for custNum: '{evt.CustomerNumber}' & bank: '{evt.DistributorBankNumber}'";

            if (!CustomerManager.ValidateCustomerEssentials(evt, response, Log)) return null;
            if (evt.CustomerSourceId.Equals("empty")) evt.CustomerSourceId = "";

            var bankId = query.GetBankIdByBankNo(evt.DistributorBankNumber);
            if (!CustomerManager.ValidateBank(bankId, evt.DistributorBankNumber, response, Log)) return null;

            var cust = new CustomerExtra
            {
                BankId = bankId,
                EntityName = Helper.GetEntityName(evt),
                DefaultTeamId = query.GetDefaultTeamIdByBankId(bankId)
            };

            if (cust.EntityName == EntityName.Undefined)
            {
                LogHelper.Msg($"{notValidatedResponse}. EntityName is undefined. Check source: '{evt.Source}'", response, LogLevel.Error, Log);
                return null;
            }

            if (string.IsNullOrEmpty(evt.OwnerBankNumber)) evt.OwnerBankNumber = evt.DistributorBankNumber;

            return cust;
        }

        #endregion

    }
}
