﻿
using System.Collections.Generic;
using System.Linq;
using Common.Adapters;
using Common.Entities;
using Common.Enums;
using Common.Initializer;
using Common.Models;
using Azure.Queues.Process.ProcessHelpers;
using System.Text;

namespace Azure.Queues.Process.Kunderegister
{
    public class KundeRegisterProcess
    {
        static StringBuilder Logger;

        public static void ProcessMessages(List<MsmqMessageInbox> items, DataReader dataReader, DataWriter dataWriter, QueryCrm query)
        {
            Logger = dataWriter.Logger;

            var validSources = new List<string>
            {
                $"{Source.Kerne}",
                $"{Source.NiceSkade}",
                $"{Source.NicePerson}",
                $"{Source.Tradex}",
                $"{Source.BanQsoft}"
            };

            var kunderegisterMessages = items.Where(m => validSources.Contains(m.Source)).ToList();

            if (kunderegisterMessages.Count > 0)
            {
                var processedEventResponses = ProcessEvents(kunderegisterMessages, dataReader, query);
                EtlDatabaseHelper.UpdateMessagesInDatabase(processedEventResponses, dataWriter);
            }

            var oddCases = items.Where(m => !validSources.Contains(m.Source)).ToList();

            EtlDatabaseHelper.SetMessagesToIgnore(oddCases, "Source is not ready to be processed yet.", dataWriter);
        }


        #region Private Methods

        private static List<ProcessedCustomerResponse> ProcessEvents(List<MsmqMessageInbox> items, DataReader dataReader, QueryCrm query)
        {
            var result = new List<ProcessedCustomerResponse>();
            var bankSpecificEmails = query.GetAllCommonEmailsFromBusinessUnits();
            Initialize.UpdateVendorConfigValues(query);

            //Hook in here some where here with the new businessrules (KundeRegisterV2).
            //Only take the ones with new statuses, and remove the others from the current list.

            foreach (var item in items)
            {
                var response = new ProcessedCustomerResponse { MsmqInboxId = item.Id, RetryCount = item.RetryCount, Success = true };
                var evt = SerializeHelper.DeserializeEventMessageContent<KundeRegisterEvent>(item.MessageContent);

                if (evt.CustomerInformation != null)
                {
                    if (CustomRules.IsBankSpecificEmail(bankSpecificEmails, evt.CustomerInformation.Email1)) evt.CustomerInformation.Email1 = string.Empty;
                    if (CustomRules.IsBankSpecificEmail(bankSpecificEmails, evt.CustomerInformation.Email2)) evt.CustomerInformation.Email2 = string.Empty;
                }

                Logger.AppendLine($"ProcessEvents: {evt.Source}");

                switch (evt.Source)
                {
                    case Source.Kerne:
                        ProcessKerneCustomer(evt, response, dataReader, query);
                        break;
                    case Source.NiceSkade:
                    case Source.NicePerson:
                        ProcessNiceCustomer(evt, response, dataReader, query);
                        break;
                    case Source.Tradex:
                        ProcessTradexCustomer(evt, response, dataReader, query);
                        break;
                    case Source.BanQsoft:
                        ProcessBanqsoftCustomer(evt, response, dataReader, query);
                        break;
                    default:
                        response.ErrorMessage = $"Unknown Source. The Source '{evt.Source}' for the {nameof(KundeRegisterEvent)} is unsupported.";
                        response.IsIgnored = true;
                        break;
                }

                result.Add(response);
            }

            return result;
        }


        private static void ProcessKerneCustomer(KundeRegisterEvent evt, ProcessedCustomerResponse response, DataReader dataReader, QueryCrm query)
        {

            switch (evt.EventType)
            {
                case EventType.Insert:
                    Logger.AppendLine($"Try to insert customer...");
                    KerneCustomerManager.InsertCustomer(evt, response, query, Logger);
                    break;
                case EventType.Delete:
                    Logger.AppendLine($"Try to delete customer...");
                    KerneCustomerManager.DeleteCustomer(evt, response, query, dataReader, Logger);
                    break;
                default:
                    Logger.AppendLine($"Unknown EventType. The EventType '{evt.EventType}' for the TradexCustomerEvent is unsupported.");
                    response.ErrorMessage = $"Unknown EventType. The EventType '{evt.EventType}' for the TradexCustomerEvent is unsupported.";
                    response.IsIgnored = true;
                    break;
            }
        }


        private static void ProcessNiceCustomer(KundeRegisterEvent evt, ProcessedCustomerResponse response, DataReader dataReader, QueryCrm query)
        {
            switch (evt.EventType)
            {
                case EventType.Insert:
                    NiceCustomerManager.InsertCustomer(evt, response, query);
                    break;
                case EventType.Delete:
                    NiceCustomerManager.DeleteCustomer(evt, response, query, dataReader);
                    break;
                default:
                    response.ErrorMessage = $"Unknown EventType. The EventType '{evt.EventType}' for the TradexCustomerEvent is unsupported.";
                    response.IsIgnored = true;
                    break;
            }
        }


        private static void ProcessTradexCustomer(KundeRegisterEvent evt, ProcessedCustomerResponse response, DataReader dataReader, QueryCrm query)
        {
            switch (evt.EventType)
            {
                case EventType.Insert:
                    TradexCustomerManager.InsertCustomer(evt, response, query);
                    break;
                case EventType.Delete:
                    TradexCustomerManager.DeleteCustomer(evt, response, query, dataReader);
                    break;
                default:
                    response.ErrorMessage = $"Unknown EventType. The EventType '{evt.EventType}' for the TradexCustomerEvent is unsupported.";
                    response.IsIgnored = true;
                    break;
            }
        }


        private static void ProcessBanqsoftCustomer(KundeRegisterEvent evt, ProcessedCustomerResponse response, DataReader dataReader, QueryCrm query)
        {
            switch (evt.EventType)
            {
                case EventType.Insert:
                    BanqsoftCustomerManager.InsertCustomer(evt, response, query);
                    break;
                case EventType.Delete:
                    BanqsoftCustomerManager.DeleteCustomer(evt, response, query, dataReader);
                    break;
                default:
                    response.ErrorMessage = $"Unknown EventType. The EventType '{evt.EventType}' for the TradexCustomerEvent is unsupported.";
                    response.IsIgnored = true;
                    break;
            }
        }

        #endregion
    }
}
