﻿using System;
using Common;
using Common.Adapters;
using Common.Constants;
using Common.Entities;
using Common.Enums;
using Common.Helpers;
using Common.Initializer;
using Common.Logging;
using Azure.Queues.Process.ProcessEntityHelper;

namespace Azure.Queues.Process.Kunderegister
{
    public class NiceCustomerManager
    {
        private static readonly MsmqLogger Log = MsmqLogger.CreateLogger("nice-customer-manager");


        #region Public Methods

        public static void InsertCustomer(KundeRegisterEvent evt, ProcessedCustomerResponse response, QueryCrm query)
        {
            if (!CustomerManager.ValidateCustomerEssentials(evt, response, Log)) return;
            if (!GetEfBankNumber(evt, response)) return;

            var directCustomer = evt.OwnerBankNumber.Equals(evt.DistributorBankNumber);
            if (!directCustomer)
            {
                // Update bank customer card
                KundeRegisterManager.PerformVendorCustomerInsert(evt, response, query);
            }

            // Update product company customer card
            KundeRegisterManager.PerformVendorCustomerInsert(evt, response, query, true);
        }


        public static void DeleteCustomer(KundeRegisterEvent evt, ProcessedCustomerResponse response, QueryCrm query, DataReader etlDb)
        {
            if (!CustomerManager.ValidateCustomerEssentials(evt, response, Log)) return;
            if (!GetEfBankNumber(evt, response)) return;

            var directCustomer = evt.OwnerBankNumber.Equals(evt.DistributorBankNumber);
            if (!directCustomer)
            {
                // Remove bank customer card
                KundeRegisterManager.PerformVendorCustomerDelete(evt, response, query, etlDb);
            }

            // Remove product company customer card
            KundeRegisterManager.PerformVendorCustomerDelete(evt, response, query, etlDb, true);
        }


        public static void InsertCustomerOld(KundeRegisterEvent evt, ProcessedCustomerResponse response, QueryCrm query)
        {
            if (!CustomerManager.ValidateCustomerEssentials(evt, response, Log)) return;
            if (!GetEfBankNumber(evt, response)) return;

            var efDirectCustomer = evt.OwnerBankNumber.Equals(evt.DistributorBankNumber);
            if (!efDirectCustomer)
            {
                // Update bank customer card
                PerformInsertCustomer(evt, response, query);
            }

            // Update product company customer card
            PerformInsertCustomer(evt, response, query, true);
        }


        public static void DeleteCustomerOld(KundeRegisterEvent evt, ProcessedCustomerResponse response, QueryCrm query)
        {
            if (!CustomerManager.ValidateCustomerEssentials(evt, response, Log)) return;
            if (!GetEfBankNumber(evt, response)) return;

            var efDirectCustomer = evt.OwnerBankNumber.Equals(evt.DistributorBankNumber);
            if (!efDirectCustomer)
            {
                // Remove bank customer card
                PerformDeleteCustomer(evt, response, query);
            }

            // Remove product company customer card
            PerformDeleteCustomer(evt, response, query, true);
        }

        #endregion

        #region Private Methods

        private static void PerformInsertCustomer(KundeRegisterEvent evt, ProcessedCustomerResponse response, QueryCrm query, bool productCompanyCustomerCard = false)
        {
            try
            {
                Log.Debug($"Starting {Util.GetMethodName()} for {(productCompanyCustomerCard ? "product company" : "bank")} customer card.");
                var cust = ValidateAndPrepare(evt, productCompanyCustomerCard, response, query);
                if (!response.Success) return;


                var crmCustomerCard = CustomerManager.GetCustomerCardAndMergeDuplicates(evt, cust, response, query, Log);
                if (!response.Success) return;


                var timestampValidated = true;
                if (CustomerManager.CanOverwriteCustomerCard(crmCustomerCard, evt.Source))
                {
                    Log.Debug($"Inside {nameof(PerformInsertCustomer)}. Source card: {crmCustomerCard.Source} :: Source evt: {evt.Source}");

                    timestampValidated = CustomerManager.ValidateTimestamp(evt, crmCustomerCard, cust, Log);
                    if (timestampValidated)
                    {
                        if (!CustomerManager.CreateOrUpdateCustomer(evt, cust, response, query, Log, productCompanyCustomerCard)) return;
                    }
                }
                else
                {
                    Log.Debug($"Inside Update SourceId. CRM card: {crmCustomerCard.Id} :: Source evt: {evt.Source}");
                    if (!CustomerManager.UpdateCustomerSourceId(crmCustomerCard, evt.Source, evt.CustomerSourceId, response, query, Log)) return;
                }


                var connectionToAdd = ConnectionManager.GetBankConnection(cust);
                ConnectionManager.AddConnection(connectionToAdd, cust, response, query, Log);
                if (!response.Success) return;


                if (!timestampValidated) response.ErrorMessage = "Completed, but skipped a customer card update due to timestamp being older than last update in CRM.";
            }
            catch (Exception ex)
            {
                LogHelper.Error(Util.GetMethodName(), ex, response, Log);
            }
        }
        private static void PerformDeleteCustomer(KundeRegisterEvent evt, ProcessedCustomerResponse response, QueryCrm query, bool productCompanyCustomerCard = false)
        {
            try
            {
                Log.Debug($"Starting {Util.GetMethodName()} for {(productCompanyCustomerCard ? "product company" : "bank")} customer card.");
                var cust = ValidateAndPrepare(evt, productCompanyCustomerCard, response, query);
                if (!response.Success) return;


                var crmCustomerCard = CustomerManager.GetCustomerCardAndMergeDuplicates(evt, cust, response, query, Log);
                #region return no customer found
                if (crmCustomerCard.Id == Guid.Empty)
                {
                    response.ErrorMessage = "No customer card found. The customer is already deleted from CRM.";
                    response.Success = true;
                    return;
                }
                #endregion


                var activeConnectionsBank = ConnectionManager.RemoveConnection(crmCustomerCard, evt, cust.VendorRoleName, cust.OppositeBankId, response, query, Log);
                if (!response.Success) return;
                var noActiveConnections = activeConnectionsBank.Count == 0;


                if (CustomerManager.CanOverwriteCustomerCard(crmCustomerCard, evt.Source) && noActiveConnections)
                {
                    var deletingEfCustomer = !InfoActivityManager.HasActivityLastSixMonths(crmCustomerCard.Id, query);

                    var success = deletingEfCustomer
                        ? CustomerManager.DeleteCustomer(crmCustomerCard, evt.CustomerSourceId, response, query, Log) 
                        : CustomerManager.DeactivateCustomer(crmCustomerCard, response, query, Log);

                    if (!success) return;
                }
                else if (crmCustomerCard.SourceId.Equals(evt.CustomerSourceId))
                {
                    if (noActiveConnections)
                    {
                        if(!CustomerManager.RemoveCustomerSourceId(crmCustomerCard, evt, response, query, Log)) return;
                    }
                    else
                    {
                        var sourceIdOtherBank = CustomerManager.GetSourceIdFromOtherCustomerCard(activeConnectionsBank, evt, cust.EntityName, query);

                        if (!string.IsNullOrEmpty(sourceIdOtherBank))
                        {
                            evt.CustomerSourceId = sourceIdOtherBank;
                            if (!CustomerManager.UpdateCustomerSourceId(crmCustomerCard, evt.Source, evt.CustomerSourceId, response, query, Log)) return;
                        }
                        else
                        {
                            if(!CustomerManager.RemoveCustomerSourceId(crmCustomerCard, evt, response, query, Log)) return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.Error(Util.GetMethodName(), ex, response, Log);
            }
        }




        public static CustomerExtra ValidateAndPrepare(KundeRegisterEvent evt, bool productCompanyCard, ProcessedCustomerResponse response, QueryCrm query)
        {
            var notValidatedResponse = $"The {nameof(KundeRegisterEvent)} request is not validated for custNum: '{evt.CustomerNumber}' & bank: '{evt.DistributorBankNumber}'";

            if (!CustomerManager.ValidateCustomerEssentials(evt, response, Log)) return null;
            if (!GetEfBankNumber(evt, response)) return null;

            var connectionToBankNo = productCompanyCard ? evt.DistributorBankNumber : evt.OwnerBankNumber;
            var customerCardBankNo = productCompanyCard ? evt.OwnerBankNumber : evt.DistributorBankNumber;
            var isDirectCustomer = evt.DistributorBankNumber.Equals(evt.OwnerBankNumber);

            var bankId = query.GetBankIdByBankNo(customerCardBankNo);
            if (!CustomerManager.ValidateBank(bankId, customerCardBankNo, response, Log)) return null;

            var cust = new CustomerExtra
            {
                BankId = bankId,
                EntityName = Helper.GetEntityName(evt),
                DefaultTeamId = query.GetDefaultTeamIdByBankId(bankId)
            };

            if (cust.EntityName == EntityName.Undefined)
            {
                LogHelper.Msg($"{notValidatedResponse}. EntityName is undefined.", response, LogLevel.Error, Log);
                return null;
            }
            if (evt.InsuranceType == InsuranceType.Undefined)
            {
                LogHelper.Msg($"{notValidatedResponse}. InsuranceType is undefined.", response, LogLevel.Error, Log);
                return null;
            }

            if (isDirectCustomer)
            {
                cust.OppositeBankId = bankId;
            }
            else
            {
                var oppositeBankId = query.GetBankIdByBankNo(connectionToBankNo);
                if (!CustomerManager.ValidateBank(oppositeBankId, connectionToBankNo, response, Log)) return null;
                cust.OppositeBankId = oppositeBankId;
            }

            cust.VendorRoleName = evt.Source == Source.NiceSkade
                ? VendorConstant.SkadeRoleName
                : VendorConstant.PersonRoleName;

            cust.HasCustomerRoleName = VendorConstant.HasCustomerRoleName;

            return cust;
        }

        public static CustomerExtra ValidateAndPrepareAlternate(CustomerEvent evt, bool productCompanyCard, ProcessedCustomerResponse response, QueryCrm query)
        {
            var notValidatedResponse = $"{Util.GetMethodName()}. The {nameof(KundeRegisterEvent)} request is not validated for custNum: '{evt.CustomerNumber}' & bank: '{evt.BankNumber}'";

            if (!CustomerManager.ValidateCustomerEssentials(evt.CustomerNumber, evt.BankNumber, evt.CustomerSourceId, response, Log)) return null;

            if (evt.InsuranceType == InsuranceType.Undefined)
            {
                if (evt.Source == CustomerRegisterSource.NiceSkade) evt.InsuranceType = InsuranceType.Skade;
                else if (evt.Source == CustomerRegisterSource.NicePerson) evt.InsuranceType = InsuranceType.Person;
                else return null;
            }

            var owningBankNumber = GetEfBankNumber(evt, response);

            var customerCardBankNo = productCompanyCard ? owningBankNumber : evt.BankNumber;

            var bankId = query.GetBankIdByBankNo(customerCardBankNo);
            if (!CustomerManager.ValidateBank(bankId, customerCardBankNo, response, Log)) return null;

            var cust = new CustomerExtra
            {
                BankId = bankId,
                //EntityName = Helper.GetEntityNameNice(evt.CustomerNumber, evt.NiceCustomerType),
                DefaultTeamId = query.GetDefaultTeamIdByBankId(bankId)
            };

            if (cust.EntityName == EntityName.Undefined)
            {
                LogHelper.Msg($"{notValidatedResponse}. EntityName is undefined.", response, LogLevel.Error, Log);
                return null;
            }

            return cust;
        }


        private static bool GetEfBankNumber(KundeRegisterEvent evt, ProcessedCustomerResponse response)
        {
            var bankNoByName = Initialize.EtlConfigBankNos;
            
            var keyName = evt.InsuranceType == InsuranceType.Skade
                ? $"{InsuranceType.Skade}forsikring"
                : $"{InsuranceType.Person}forsikring";

            bankNoByName.TryGetValue(keyName, out var owningBankNumber);
            evt.OwnerBankNumber = owningBankNumber;

            if (string.IsNullOrEmpty(evt.OwnerBankNumber))
            {
                LogHelper.Msg($"An error at {Util.GetMethodName()}. Check insurance type: '{evt.InsuranceType}' and etl_config for BankNo", response, LogLevel.Error, Log);
                return false;
            }

            return true;
        }
        
        private static string GetEfBankNumber(CustomerEvent evt, ProcessedCustomerResponse response)
        {
            var bankNoByName = Initialize.EtlConfigBankNos;

            var keyName = evt.InsuranceType == InsuranceType.Skade
                            ? $"{InsuranceType.Skade}forsikring"
                            : $"{InsuranceType.Person}forsikring";

            bankNoByName.TryGetValue(keyName, out var owningBankNumber);


            if (string.IsNullOrEmpty(owningBankNumber))
            {
                LogHelper.Msg($"An error at {Util.GetMethodName()}. Check insurance type: '{evt.InsuranceType}' and etl_config for BankNo", response, LogLevel.Error, Log);
            }

            return owningBankNumber;
        }

        #endregion

    }
}
