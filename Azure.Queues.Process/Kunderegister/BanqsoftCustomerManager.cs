﻿using System;
using Common;
using Common.Adapters;
using Common.Entities;
using Common.Enums;
using Common.Helpers;
using Common.Logging;
using Azure.Queues.Process.ProcessEntityHelper;

namespace Azure.Queues.Process.Kunderegister
{
    class BanqsoftCustomerManager
    {
        private static readonly MsmqLogger Log = MsmqLogger.CreateLogger("banqsoft-customer-manager");

        #region Public Methods

        public static void InsertCustomer(KundeRegisterEvent evt, ProcessedCustomerResponse response, QueryCrm query)
        {
            try
            {
                //Log.Debug($"Starting {Util.GetMethodName()} for bank customer card.");
                var cust = ValidateAndPrepare(evt, response, query);
                //if (!response.Success) return;

                KundeRegisterManager.PerformCustomerInsert(evt, response, query);
            }
            catch (Exception ex)
            {
                LogHelper.Error(Util.GetMethodName(), ex, response, Log);
            }
        }
        public static void DeleteCustomer(KundeRegisterEvent evt, ProcessedCustomerResponse response, QueryCrm query, DataReader etlDb)
        {
            try
            {
                //Log.Debug($"Starting {Util.GetMethodName()} for bank customer card.");
                //var cust = ValidateAndPrepare(evt, response, query);
                //if (!response.Success) return;

                //KundeRegisterManager.IgnoringDelete(evt, response, query);
                KundeRegisterManager.PerformCustomerDelete(evt, response, query, etlDb);
            }
            catch (Exception ex)
            {
                LogHelper.Error(Util.GetMethodName(), ex, response, Log);
            }
        }

        #endregion

        #region Private  Methods
        public static CustomerExtra ValidateAndPrepare(KundeRegisterEvent evt, ProcessedCustomerResponse response, QueryCrm query)
        {
            var notValidatedResponse = $"The {nameof(KundeRegisterEvent)} request is not validated for custNum: '{evt.CustomerNumber}' & bank: '{evt.DistributorBankNumber}'";
            var ekbBankNumber = "9811";

            if (!CustomerManager.ValidateCustomerEssentials(evt, response, Log)) return null;

            var bankId = query.GetBankIdByBankNo(evt.DistributorBankNumber);
            if (!CustomerManager.ValidateBank(bankId, evt.DistributorBankNumber, response, Log)) return null;

            var cust = new CustomerExtra
            {
                BankId = bankId,
                EntityName = Helper.GetEntityName(evt),
                DefaultTeamId = query.GetDefaultTeamIdByBankId(bankId)
            };

            if (cust.EntityName == EntityName.Undefined)
            {
                LogHelper.Msg($"{notValidatedResponse}. EntityName is undefined. Check source: '{evt.Source}'", response, LogLevel.Error, Log);
                return null;
            }

            if (string.IsNullOrEmpty(evt.OwnerBankNumber)) evt.OwnerBankNumber = ekbBankNumber;

            if (!evt.DistributorBankNumber.Equals(ekbBankNumber))
            {
                LogHelper.Msg($"{notValidatedResponse}. Expected DistributorBankNumber '{ekbBankNumber}'. Event has '{evt.DistributorBankNumber}'. Ignoring event.", response, LogLevel.Error, Log, true);
                return null;
            }


            return cust;
        }
        #endregion

    }
}
