﻿using System;
using Common;
using Common.Adapters;
using Common.Entities;
using Common.Enums;
using Common.Helpers;
using Common.Initializer;
using Common.Logging;
using Azure.Queues.Process.ProcessHelpers;

namespace Azure.Queues.Process.Sakhendelse
{
    public class SakHendelseManager
    {
        #region Private Members

        private static readonly MsmqLogger Log = MsmqLogger.CreateLogger("sakhendelse-manager");

        #endregion

        #region Public Methods

        public static void HandleSakHendelseEvent(SakHendelse evt, ProcessedCustomerResponse response, QueryExpressionHelper query)
        {
            try
            {
                var cust = ValidateAndPrepare(evt, response, query);
                if (cust == null) return;


                Log.Debug("Done with ValidateAndPrepare.");
                
                if(evt.Interessent != null)
                {
                    var crmCustomerCard = ProcessHelper.GetCustomerCard(evt, cust, response, query);
                    if (response.FailedProcessing) return;

                    Log.Debug("Done with GetCustomerCard.");

                    ProcessHelper.GetOwnerIfExist(crmCustomerCard, cust);

                    Log.Debug("Done with GetOwnerIfExist.");

                    if (evt.Interessent.OffentligId != null && cust.CustomerId == Guid.Empty)
                    {
                        ProcessHelper.CreatePotentialCustomer(evt, cust, response, query);
                        if (response.FailedProcessing) return;
                    }
                    else ProcessHelper.UpdateCustomerSourceId(crmCustomerCard, evt.FagSystem, evt.FagsystemKundenummer, response, query, true);

                    Log.Debug("Done with CreatePotentialCustomer.");
                }


                var role = EnumUtils.Parse<SakHendelseRolle>(evt.Interessent?.Rolle);
                if(role != SakHendelseRolle.Hoved)
                {
                    cust.ContributerCustomerId = cust.CustomerId;
                    cust.CustomerId = Guid.Empty;
                }


                // TODO Support create / update of product through Sakhendelse
                //var success = ProductManager.UpsertProduct(evt, cust, response, query, Log);
                //#region return failure
                //if (!success)
                //{
                //    response.ErrorMessage = "Error during CreateOrUpdateProduct.";
                //    return;
                //}
                //#endregion

                //Log.Debug("Done with CreateOrUpdateProduct.");

                
                if (evt.Prosessomraade == ProcessArea.Sparing && Helper.ApplicationIsFromRaadgiverPluss(evt.Applikasjon, Initialize.KundehendelseAppNames))
                {
                    ProcessHelper.CreateOutcomeForSparingAndRadgiverPluss(evt, cust, response, query, Log);
                    if (response.FailedProcessing) return;
                }
                else if (evt.Prosessomraade == ProcessArea.Kreditt)
                {
                    ProcessHelper.CreateOutcomeForKreditt(evt, cust, response, query, Log);
                    if (response.FailedProcessing) return;
                }
                else
                {
                    Helper.SetLogAndResponse($"Skipping any outcome of the 'kundehendelse' due to process area {evt.Prosessomraade} or {evt.Applikasjon} is not yet supported.", response, LogLevel.Warn, Log, true);
                    return;
                }

                Log.Debug("Done with CreateOutcome.");


                response.Success = true;
            }
            catch (Exception ex)
            {
                Helper.SetLogAndResponse($"Error while processing {System.Reflection.MethodBase.GetCurrentMethod().Name}. Error message: {ManagerHelper.GetExceptionMessage(ex)}", response, LogLevel.Error, Log);
            }
        }

        #endregion

        #region Private  Methods
        private static CustomerExtra ValidateAndPrepare(SakHendelse evt, ProcessedCustomerResponse response, QueryExpressionHelper query)
        {
            //TODO fjern FELLES_SPRING_APPLICATION / Rådgiver+ spesifik tilpasning 
            if (evt.Applikasjon.Equals("FELLES_SPRING_APPLICATION"))
            {
                evt.Prosessomraade = ProcessArea.Sparing;
                evt.FagSystem = "RAADGIVER_PLUSS";
                if(evt.Interessent != null)
                {
                    evt.Interessent.Rolle = "Hoved";
                }
            }

            var bankId = query.GetBankIdByBankNo(evt.Bankregnr);
            if (!ProcessHelper.ValidateBank(bankId, evt.Bankregnr, response, Log))
            {
                response.ErrorMessage = "Sakhendelse not valid. Can't find 'Brukersted' in Crm.";
                return null;
            }

            if(string.IsNullOrEmpty(evt.SakId))
            {
                Helper.SetLogAndResponse("Sakhendelse not valid. Required field 'SakId' is null or empty.", response, LogLevel.Warn, Log);
                return null;
            }

            var cust = new CustomerExtra
            {
                BankId = bankId,
                EntityName = Helper.GetEntityName(evt),
                LoggedInUserOwnerId = query.GetUserIdByUsernameAndBankId(evt.RaadgiverId, bankId),
                DefaultTeamId = query.GetDefaultTeamIdByBankId(bankId),
                SubjectId = query.GetSubjectIdByTitle(Helper.MapSubject(evt.Prosessomraade, evt.FagSystem)),
                PriceListId = query.GetPriceListIdByTitle(Helper.MapSubject(evt.Prosessomraade, evt.FagSystem)),
                VendorBankNumber = Helper.GetVendorBankNumber(evt.FagSystem),
                Product = query.GetProductByCode(evt.ProduktKode),
                SakHendelseAttributes = Helper.MapSakhendelseAttributes(evt)
            };


            //if (cust.EntityName == EntityName.Undefined)
            //{
            //    Helper.SetLogAndResponse("Sakhendelse not valid. Can't find a valid EntityName from 'KundeId'.", response, LogLevel.Warn, Log, true);
            //    return null;
            //}
            if (cust.SubjectId == Guid.Empty)
            {
                Helper.SetLogAndResponse("Sakhendelse not valid. Can't find 'ProsessOmraade' in Crm.", response, LogLevel.Warn, Log);
                return null;
            }
            if (cust.PriceListId == Guid.Empty)
            {
                Helper.SetLogAndResponse("Sakhendelse not valid. Can't find 'Price List' (ProsessOmraade) in Crm.", response, LogLevel.Warn, Log);
                return null;
            }
            // TODO Active this when we start receiving product (or if we will always receive it)
            //if (string.IsNullOrEmpty(evt.ProduktKode))
            //{
            //    Helper.SetLogAndResponse("Sakhendelse not valid. 'ProduktKode' is missing.", response, LogLevel.Warn, Log);
            //    return null;
            //}
            if (cust.Product.Id != Guid.Empty && !cust.Product.Active)
            {
                Helper.SetLogAndResponse("Sakhendelse not valid. Product is inactive in Crm.", response, LogLevel.Warn, Log);
                return null;
            }
            if (string.IsNullOrEmpty(cust.VendorBankNumber))
            {
                Helper.SetLogAndResponse("Sakhendelse not valid. Can't find 'VendorBankNumber' given 'FagSystem'.", response, LogLevel.Warn, Log);
                return null;
            }


            if (!Helper.ValidateKundehendelseAppNames(Initialize.KundehendelseAppNames))
            {
                Helper.SetLogAndResponse("Sakhendelse not valid. KundehendelseAppNames in 'ETL_Config' is missing one of the Enum defined AppNames in this code.", response, LogLevel.Warn, Log);
                return null;
            }
            

            if (!(
                    (evt.Prosessomraade == ProcessArea.Sparing && Helper.ApplicationIsFromRaadgiverPluss(evt.Applikasjon, Initialize.KundehendelseAppNames)) ||
                    //(evt.ProsessOmraade == ProcessArea.Kreditt && Helper.ApplicationIsFromSource(evt.Applikasjon, Initialize.KundehendelseAppNames)) ||
                    false
                ))
            {
                Helper.SetLogAndResponse("Sakhendelse not valid. 'ProsessOmraade' and 'Applikasjon' is not within the defined values.", response, LogLevel.Warn, Log);
                return null;
            }


            return cust;
        }
        #endregion

    }
}
