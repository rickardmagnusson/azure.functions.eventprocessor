﻿using System.Collections.Generic;
using Azure.Queues.Process;
using Azure.Queues.Process.ProcessHelpers;
using Azure.Queues.Process.Sakhendelse;
using Common.Adapters;
using Common.Entities;
using Common.Initializer;
using Common.Models;
using Crm.Services.Extensions;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

namespace ProcessManager.Sakhendelse
{
    public class SakHendelseProcess
    {
        public static void ProcessMessages(List<MsmqMessageInbox> items, DataWriter dataWriter, QueryCrm query)
        {

            //IOrganizationService serviceProvider = OrganizationServiceHelper.GetIOrganizationService();

            //IOrganizationServiceFactory service = (IOrganizationServiceFactory)serviceProvider;
            //var User = service.CreateOrganizationService("systemuser", userId, new ColumnSet("fullname"));


            EtlDatabaseHelper.SetMessagesToIgnore(items, "SakHendelse is not ready to be processed yet.", dataWriter);
        }



        #region Private Methods

        private static List<ProcessedCustomerResponse> ProcessEvents(List<MsmqMessageInbox> items, QueryCrm queryToUse)
        {
            var result = new List<ProcessedCustomerResponse>();

            using (var query = new QueryExpressionHelper())
            {
                var validBanksToRun = Initialize.GetBankToRunForSakHendelse();
                Initialize.LoadProductCodes();
                Initialize.LoadAppNames();


                foreach (var item in items)
                {
                    var response = new ProcessedCustomerResponse { MsmqInboxId = item.Id, RetryCount = item.RetryCount };
                    var evt = SerializeHelper.DeserializeEventMessageContent<SakHendelse>(item.MessageContent);

                    if (validBanksToRun.Contains(evt.Bankregnr))
                    {
                        SakHendelseManager.HandleSakHendelseEvent(evt, response, query);
                    }
                    else
                    {
                        response.ErrorMessage = $"Skipping SakHendelse for bank '{evt.Bankregnr}.'";
                        response.IsIgnored = true;
                    }

                    result.Add(response);
                }
            }
            return result;
        }

        #endregion
    }
}
