﻿
using System.Collections.Generic;
using System.Text;
using Azure.Queues.Process;

namespace Common
{
    /// <summary>
    /// ??
    /// </summary>
    public class InitializeHelper
    {   
        /// <summary>
        /// Get a dictionary of configuration values.
        /// </summary>
        /// <param name="category">The category to get the configuration from.</param>
        /// <returns>Return a Dictionary from the current category selected.</returns>
        public static Dictionary<string, string> LoadFromDb(string category)
        {
            return ConfigurationExtensions.GetConfigurationFor(category);
        }


        /// <summary>
        /// Ugly as h..l
        /// </summary>
        /// <param name="etlCrmSetting"></param>
        /// <returns></returns>
        public static string SetServiceUrl(Dictionary<string, string> etlCrmSetting)
        {
            var serviceUrl = "https://SERVERNAME/CRMORG/XRMServices/2011/Organization.svc";

            var serverName = etlCrmSetting["serverName"];
            var crmOrg = etlCrmSetting["crmOrg"];

            serviceUrl = serviceUrl.Replace("SERVERNAME", serverName);
            serviceUrl = serviceUrl.Replace("CRMORG", crmOrg);
            return serviceUrl;
        }
        

        /// <summary>
        /// ??
        /// </summary>
        /// <param name="etlConfig"></param>
        public static void IntializeEtlVariables(Dictionary<string, string> etlConfig)
        {
            Notify.smtpServer = etlConfig.ContainsKey("smtpServer") ? etlConfig["smtpServer"] : "smtp.eika.no";
            Notify.toAddress = etlConfig.ContainsKey("toAddress") ? etlConfig["toAddress"].Split(';') : new string[] { "drift_crm@eika.no" };
            Notify.AllowSendEmail = etlConfig.ContainsKey("AllowSendEmail") && bool.Parse(etlConfig["AllowSendEmail"]);
        }
    }
}
