﻿#define DEBUG

using System;
using System.Text;
using Common.Enums;
using Common.Models;
using Newtonsoft.Json.Linq;
using Azure.Functions.Helpers;
using Azure.Logging;
using Azure.Functions.Extensions.Converters;
using Common.Entities;
using Newtonsoft.Json;


namespace Azure.Queues.Process.Helpers
{
    /// <summary>
    /// Main class for handle events.
    /// It transform events into a MsmqMessageInbox item, that also will be insterted as xml.
    /// </summary>
    public static class EventHandler
    {
        static SingletonLogger Log = SingletonLogger.Instance();

        /// <summary>
        /// Parse the incomming Json to an InboxMessage.
        /// Internally converts the json to an Xml
        /// dependent of the json sent.
        /// </summary>
        /// <param name="json">The string of Json sent</param>
        /// <param name="log">Logging</param>
        /// <returns>A InboxMessage</returns>
        public static MsmqMessageInbox CreateEvent(this string json, StringBuilder log, EventMessageType eventType)
        {
            MsmqMessageInbox inboxMessage = null;

            try { 
                var o = JObject.Parse(json);
                var typeConverter = new TypeConverter();

                switch (eventType)
                {
                    case EventMessageType.KundeRegisterEvent:

                        var kundeRegister = KundeRegisterConverter.ToKundeRegisterEvent(o.ToString());
                        inboxMessage = new MsmqMessageInbox(
                            kundeRegister.DistributorBankNumber,
                            kundeRegister.CustomerNumber, 
                            typeConverter.CreateXml(kundeRegister), 
                            eventType, (int)kundeRegister.EventType, 
                            kundeRegister.CustomerSourceId, 
                            kundeRegister.Source.ToString());
#if DEBUG
                        LogItem.Add($"{inboxMessage.BankId}, {inboxMessage.CustomerNumber}, {inboxMessage.EventType}, {inboxMessage.SourceId}, {inboxMessage.Source}");
#endif
                        break;

                    case EventMessageType.KundeHendelseEvent:

                        var kundeHendelse = KundeHendelseConverter.ToKundeHendelse(json);
                        inboxMessage = new MsmqMessageInbox(
                            kundeHendelse.Brukersted, 
                            kundeHendelse.KundeId, 
                            typeConverter.CreateXml(kundeHendelse), 
                            eventType, 1, 
                            kundeHendelse.FagsystemKundenummer, 
                            kundeHendelse.FagSystem);
#if DEBUG
                        LogItem.Add($"{inboxMessage.BankId}, {inboxMessage.CustomerNumber}, {inboxMessage.EventType}, {inboxMessage.SourceId}, {inboxMessage.Source}");
#endif
                        break;

                    case EventMessageType.SakHendelseEvent:

                        var sakHendelse = JsonConvert.DeserializeObject<Azure.Functions.Extensions.Converters.SakHendelseEvent.SakHendelse>(o.ToString());
                        inboxMessage = new MsmqMessageInbox(
                            sakHendelse.Brukersted.Value,
                            sakHendelse.Interessent != null ? sakHendelse.Interessent.OffentligId : string.Empty, 
                            typeConverter.CreateXml(sakHendelse), 
                            eventType, 1, 
                            sakHendelse.FagsystemKundenummer, 
                            sakHendelse.Fagsystem.Name);
#if DEBUG
                        LogItem.Add($"{inboxMessage.BankId}, {inboxMessage.CustomerNumber}, {inboxMessage.EventType}, {inboxMessage.SourceId}, {inboxMessage.Source}");
#endif
                        break;
                       
                 }
            }
            catch (Exception ex)
            {
                Log.AppendLine($"Failed to create InboxMessage: {ex} {eventType} {json}");
            }
            return inboxMessage;
        }


        /// <summary>
        /// Get a subject from Json object
        /// </summary>
        /// <param name="obj"></param>
        /// <returns>string Subject</returns>
        public static string GetSubject(this JObject obj)
        {
            var subject = JObject.Parse(obj.ToString());
            return Convert.ToString(subject["subject"]);
        }
    }
}
