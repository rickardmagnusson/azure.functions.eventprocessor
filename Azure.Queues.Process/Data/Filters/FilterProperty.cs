﻿using System;
using FluentNHibernate.Automapping;
using FluentNHibernate.Conventions;
using FluentNHibernate.Conventions.AcceptanceCriteria;
using FluentNHibernate.Conventions.Inspections;
using FluentNHibernate.Conventions.Instances;

namespace Azure.Queues.Process.Data.Filters
{
    
    /// <summary>
    /// Filter for Ignore a property
    /// </summary>
    public class IgnoreProperty : Attribute { }


    /// <summary>
    /// Extension to ignore attributes
    /// </summary>
    public static class FluentIgnore
    {

        /// <summary>
        /// Ignore a single property.
        /// Property marked with this attributes will no be persisted to table.
        /// </summary>
        /// <param name="p">IPropertyIgnorer</param>
        /// <param name="propertyType">The type to ignore.</param>
        /// <returns>The property to ignore.</returns>
        public static IPropertyIgnorer IgnoreProperty(this IPropertyIgnorer p, Type propertyType)
        {
            return p.IgnoreProperties(x => x.MemberInfo.GetCustomAttributes(propertyType, false).Length > 0);
        }
    }


    /// <summary>
    /// Adds length value to string fields in database(TEXT)
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class MaxLength : System.Attribute
    {
        public int Length = 0;
        public MaxLength(int len)
        {
            Length = len;
        }
    }


    /// <summary>
    /// Fluent dont understand Stringlength? Bug?, Anyway, this helper does the job!!
    /// </summary>
    public class StringColumnLengthConvention : IPropertyConvention, IPropertyConventionAcceptance
    {
        /// <summary>
        /// Accept criteria
        /// </summary>
        /// <param name="criteria"></param>
        public void Accept(IAcceptanceCriteria<IPropertyInspector> criteria)
        {
            criteria.Expect(x => x.Type == typeof(string)).Expect(x => x.Length == 0);
        }

        /// <summary>
        /// Apply instance
        /// </summary>
        /// <param name="instance"></param>
        public void Apply(IPropertyInstance instance)
        {
            instance.Length(10000);
        }
    }


}
