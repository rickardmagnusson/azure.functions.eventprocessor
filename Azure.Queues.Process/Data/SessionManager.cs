﻿using System;
using System.Linq;
using System.Text;
using FluentNHibernate.Automapping;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Tool.hbm2ddl;
using System.Reflection;
using NHibernate.Dialect;
using NHibernate.Driver;
using System.IO;
using Common.Models;
using Azure.Queues.Process.Data.Filters;

namespace Azure.Queues.Process.Data
{
    /// <summary>
    /// Datamanagement class. Read and writes to database.
    /// </summary>
    public class SessionManager
    {
        private static StringBuilder Logger;
        private static SessionManager _instance;


        private SessionManager() { }


        private static SessionManager Instance()
        {
            if (_instance == null)
                _instance = new SessionManager();
            return _instance;
        }


        /// <summary>
        /// Get Current ISessionFactory
        /// </summary>
        private static ISessionFactory CurrentFactory
        {
            get
            {
                return Instance().CreateConfig();
            }
        }


        /// <summary>
        /// Opens the connection to the database.
        /// </summary>
        /// <param name="logger">Open with a log</param>
        /// <returns></returns>
        public static ISession Open(StringBuilder logger)
        {
                Logger = logger;
                return CurrentFactory.OpenSession();
        }



        /// <summary>
        /// Opens the connection to the database.
        /// </summary>
        /// <returns></returns>
        public static ISession Open()
        {
            Logger = new StringBuilder();
            return CurrentFactory.OpenSession();
        }


        /// <summary>
        /// Creates the ISessionFactory configuration.
        /// </summary>
        /// <returns>ISessionFactory of current configuation.</returns>
        private ISessionFactory CreateConfig()
        {
            return FluentConfiguration
                .BuildSessionFactory();
        }


        /// <summary>
        /// NHibernate FluentConfiguration
        /// </summary>
        private FluentConfiguration FluentConfiguration
        {
            get
            {
                try {
                    //Works in Azure but not local. Maybe change it...
                    var conn = Environment.GetEnvironmentVariable("crm-sqldb-connectionstring-secret", EnvironmentVariableTarget.Process);
                    var dlls = AppDomain.CurrentDomain.GetAssemblies();

                    return Fluently
                               .Configure()
                               .Database(MsSqlConfiguration.MsSql2012
                               .ShowSql()
                               .Dialect<MsSqlAzure2008Dialect>()
                               .Driver<SqlClientDriver>()
                               .ConnectionString(conn))
                               .ExposeConfiguration(cfg => new SchemaExport(cfg).Execute(true, false, false))
                               .ExposeConfiguration(cfg => new SchemaUpdate(cfg).Execute(false, true))
                               .Mappings(MapAssemblies);
                }
                catch (Exception ex)
                {
                    if (ex is ReflectionTypeLoadException)
                    {
                        var typeLoadException = ex as ReflectionTypeLoadException;
                        var loaderExceptions = typeLoadException.LoaderExceptions;
                        Logger.AppendLine($"Error:ReflectionTypeLoadException: {loaderExceptions}");

                        foreach (Exception exSub in loaderExceptions)
                        {
                            Logger.AppendLine(exSub.Message);
                            var exFileNotFound = exSub as FileNotFoundException;
                            if (exFileNotFound != null)
                            {
                                if (!string.IsNullOrEmpty(exFileNotFound.FusionLog))
                                {
                                    Logger.AppendLine("Fusion Log:");
                                    Logger.AppendLine(exFileNotFound.FusionLog);
                                }
                            }
                            Logger.AppendLine("");
                        }
                    }

                    Logger.AppendLine($"Error:FluentConfiguration: {ex} {ex.InnerException}"); 
                }
                return null;
            }
        }


        /// <summary>
        /// Add assembly mappings to configuration.
        /// </summary>
        /// <param name="fmc"></param>
        internal static void MapAssemblies(MappingConfiguration fmc)
        {
            fmc.AutoMappings.Add(AutoMap.Assembly(typeof(MsmqMessageInbox).Assembly)
                .OverrideAll(p => {
                    p.IgnoreProperty(typeof(IgnoreProperty));
            }).Conventions.Add<StringColumnLengthConvention>()
            .Where(IsEntity));
        }


        /// <summary>
        /// The Entity to look for.
        /// If class inherit the Entity class, it will be reflected in database.
        /// </summary>
        /// <param name="t">The type to compare</param>
        /// <returns>If type is Entity</returns>
        private static bool IsEntity(Type t)
        {
            return typeof(Common.Entities.IEntity).IsAssignableFrom(t);
        }
    }
}
