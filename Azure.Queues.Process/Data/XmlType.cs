﻿using NHibernate.Engine;
using NHibernate.SqlTypes;
using NHibernate.Type;
using System;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Xml.Serialization;

namespace Azure.Queues.Process.Data
{

    /// <summary>
    /// nHibernate Helper, to create a max length of column.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    [Serializable]
    public class XmlType<T> : MutableType
    {

        /// <summary>
        /// 
        /// </summary>
        public XmlType()
            : base(new XmlSqlType())
        {
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sqlType"></param>
        public XmlType(SqlType sqlType)
            : base(sqlType)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public override string Name
        {
            get { return "XmlOfT"; }
        }

        /// <summary>
        /// 
        /// </summary>
        public override System.Type ReturnedClass
        {
            get { return typeof(T); }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="value"></param>
        /// <param name="index"></param>
        /// <param name="session"></param>
        public override void Set(DbCommand cmd, object value, int index, ISessionImplementor session)
        {
            cmd.Parameters[index].Value = XmlUtils.ConvertToXml(value);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="rs"></param>
        /// <param name="index"></param>
        /// <param name="session"></param>
        /// <returns></returns>
        public override object Get(DbDataReader rs, int index, ISessionImplementor session)
        {
            string xmlString = Convert.ToString(rs.GetValue(index));
            return FromStringValue(xmlString);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="rs"></param>
        /// <param name="name"></param>
        /// <param name="session"></param>
        /// <returns></returns>
        public override object Get(DbDataReader rs, string name, ISessionImplementor session)
        {
            string xmlString = Convert.ToString(rs[name]);
            return FromStringValue(xmlString);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        public override string ToString(object val)
        {
            return val == null ? null : XmlUtils.ConvertToXml(val);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="xml"></param>
        /// <returns></returns>
        public override object FromStringValue(string xml)
        {
            if (xml != null)
            {
                return XmlUtils.FromXml<T>(xml);
            }
            return null;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public override object DeepCopyNotNull(object value)
        {
            var original = (T)value;
            var copy = XmlUtil.FromXml<T>(XmlUtils.ConvertToXml(original));
            return copy;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public override bool IsEqual(object x, object y)
        {
            if (x == null && y == null)
            {
                return true;
            }
            if (x == null || y == null)
            {
                return false;
            }
            return XmlUtils.ConvertToXml(x) == XmlUtils.ConvertToXml(y);
        }


    }

 
    /// <summary>
    /// XmlUtils
    /// </summary>
    public static class XmlUtils
    {
        /// <summary>
        /// Converts and object to Xml
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public static string ConvertToXml(object item)
        {
            XmlSerializer xmlser = new XmlSerializer(item.GetType());
            using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
            {
                xmlser.Serialize(ms, item);
                UTF8Encoding textconverter = new UTF8Encoding();
                return textconverter.GetString(ms.ToArray());
            }
        }

        /// <summary>
        /// Converts a string to an object
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="xml"></param>
        /// <returns></returns>
        public static T FromXml<T>(string xml)
        {
            XmlSerializer xmlser = new XmlSerializer(typeof(T));
            using (System.IO.StringReader sr = new System.IO.StringReader(xml))
            {
                return (T)xmlser.Deserialize(sr);
            }
        }

    }

}
