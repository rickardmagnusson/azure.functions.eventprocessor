﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using Azure.Queues.Process.Data;
using Common.Enums;
using Common.Entities;
using Common.Helpers;
using Common.Models;

namespace Azure.Queues.Process
{
    /// <summary>
    /// 
    /// </summary>
    public class DataReader
    {
        private static StringBuilder Logger; 

        private DataReader(){}


        /// <summary>
        /// Creates a new instance of DataReader
        /// </summary>
        /// <param name="logger">StringBuilder log, the log to append logitems</param>
        public DataReader(StringBuilder logger)
        {
            Logger = logger;
        }


        /// <summary>
        /// Get the next range of MsmqMessageInbox items
        /// </summary>
        /// <param name="batchSize">The batchsise to get</param>
        /// <param name="type">EventType to process</param>
        /// <returns></returns>
        public List<MsmqMessageInbox> GetNextRange(int batchSize, EventMessageType type)
        {
            var typeAsInt = (int)type;

            using (var db = SessionManager.Open(Logger)) {
                return db.All<MsmqMessageInbox>().Where(m => m.Status == (int)MessageInboxStatus.Received && m.EventType == typeAsInt).OrderBy(m => m.Id).Take(batchSize).ToList();
            }
        }


        /// <summary>
        /// Get the next range of MsmqMessageInbox items
        /// </summary>
        /// <returns>A list of MsmqMessageInbox items</returns>
        public List<MsmqMessageInbox> GetNextRange(int batchSize)
        {
            //Need some new arrangement here..
            using (var db = SessionManager.Open(Logger))
            {
                var liveEventsReady = db.All<MsmqMessageInbox>().Where(m => m.Status == (int)MessageInboxStatus.Received && m.EventType == (int)EventMessageType.NiceCustomerEvent)
                    .OrderBy(m => m.Received).Take(batchSize).ToList();

                var remainingSizeToTake = batchSize - liveEventsReady.Count();
                if (remainingSizeToTake > 0)
                {
                    var remaining = db.All<MsmqMessageInbox>().Where(m => m.Status == (int)MessageInboxStatus.Received && m.EventType != (int)EventMessageType.NiceCustomerEvent) 
                        .OrderBy(m => m.Received).Take(remainingSizeToTake).ToList();
                    liveEventsReady.AddRange(remaining);
                }
                return liveEventsReady;

                //Need some new arragement here..
                /* return db.QueryOver<MsmqMessageInbox>()
                    .OrderBy(o => o.Id)
                    .Desc.Take(batchSize)
                    .List()
                    .ToList();
                */
            }
        }


        public MsmqCompletedMessages GetAlternativeCustomerCard(KundeRegisterEvent evt, List<Source> alternateSources, bool productCompanyCustomerCard)
        {
            var custNum = evt.CustomerNumber;
            var bankNum = evt.DistributorBankNumber;

            if (string.IsNullOrEmpty(custNum) || string.IsNullOrEmpty(bankNum))
            {
                Logger.AppendLine($"{Util.GetMethodName()}. Either custNum '{custNum}' or bankNum '{bankNum}' is not set.");
                return null;
            }

            var customerEvents = productCompanyCustomerCard
                ? GetAlternativeProductCompanyCustomerCards(custNum, evt.Source.ToString())
                : GetAlternativeCustomerCards(custNum, bankNum);

            Logger.AppendLine($"alternateSources count: {alternateSources.Count}");

            if (productCompanyCustomerCard)
            {
                var toCheck = customerEvents.Where(e => !e.SourceId.Equals(evt.CustomerSourceId)).OrderByDescending(x => x.Received).ToList();
                var bySourceId = toCheck.GroupBy(e => e.SourceId).ToDictionary(e => e.Key, f => f.ToList());
                
                foreach (var customerRelationship in bySourceId)
                {
                    toCheck = customerRelationship.Value.OrderByDescending(x => x.Received).ToList();

                    var possible = toCheck.FirstOrDefault();

                    if (possible != null && possible.ActionType == (int)EventType.Insert)
                    {
                        return possible;
                    }
                } 
            }
            else
            {
                foreach (var source in alternateSources)
                {
                    Logger.AppendLine($"__source : '{source}'");

                    var toCheck = customerEvents.Where(e => e.Source.Equals($"{source}")).ToList();
                    if (source == Source.Kerne) toCheck.AddRange(customerEvents.Where(e => e.Source.Equals("Sdc")).ToList());
                    toCheck = toCheck.OrderByDescending(x => x.Received).ToList();

                    var possible = toCheck.FirstOrDefault();

                    if (possible != null && (possible.ActionType == (int)EventType.Insert || (possible.EventType == (int) EventMessageType.EventStore_EventMaster && possible.ActionType == (int)eventTypeType.CREATE)))
                    {
                        return possible;
                    }
                } 
            }

            return null;
        }


        private List<MsmqCompletedMessages> GetAlternativeCustomerCards(string custNum, string bankNum)
        {
            var result = new List<MsmqCompletedMessages>();

            if (string.IsNullOrEmpty(custNum) || string.IsNullOrEmpty(bankNum))
            {
                Logger.AppendLine($"{Util.GetMethodName()}. Either custNum '{custNum}' or bankNum '{bankNum}' is not set.");
                return result;
            }

            var eventtypes = new List<int>
            {
                (int) EventMessageType.DataVarehus_TemporaryEvent,
                (int) EventMessageType.NiceCustomerEvent,
                (int) EventMessageType.KundeRegisterEvent
            };

            using (var db = SessionManager.Open(Logger))
            {
                result = db.All<MsmqCompletedMessages>()
                         .Where(m => eventtypes.Contains(m.EventType) && 
                          m.CustomerNumber.Equals(custNum) && 
                          m.BankId.Equals(bankNum))
                          .ToList();

                var eventStoreInsertMessages = db.All<MsmqCompletedMessages>().Where(m => m.EventType == (int)EventMessageType.EventStore_EventMaster && 
                                                               (m.ActionType == 0 || m.ActionType == 1) &&
                                                               m.CustomerNumber.Equals(custNum) && m.BankId.Equals(bankNum)).ToList();

                result.AddRange(eventStoreInsertMessages);
                return result;
            }
        }
        


        private List<MsmqCompletedMessages> GetAlternativeProductCompanyCustomerCards(string custNum, string source)
        {
            var result = new List<MsmqCompletedMessages>();

            if (string.IsNullOrEmpty(custNum) || string.IsNullOrEmpty(source))
            {
                Logger.AppendLine($"{Util.GetMethodName()}. Either custNum '{custNum}' or source '{source}' is not set.");
                return result;
            }

            var eventtypes = new List<int>
            {
                (int) EventMessageType.NiceCustomerEvent,
                (int) EventMessageType.KundeRegisterEvent
            };

            using (var db = SessionManager.Open(Logger))
            {
                return db.All<MsmqCompletedMessages>().Where(m => eventtypes.Contains(m.EventType) && m.CustomerNumber.Equals(custNum) && m.Source.Equals(source)).ToList();
            }
        }
    }
}
