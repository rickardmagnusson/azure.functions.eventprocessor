﻿using Azure.Queues.Process.Data;
using Common.Models;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Azure.Queues.Process
{
    /// <summary>
    /// Contains methods to read and create configuration dictionary's
    /// </summary>
    public class ConfigurationExtensions
    {
        private static StringBuilder Logger;
        private static ConfigurationExtensions _instance;
        private static Dictionary<string, Dictionary<string, string>> configuration;
     
        private ConfigurationExtensions() { }


        /// <summary>
        /// Singleton implementation of ConfigurationExtensions.
        /// Loads all configurations into a dictionary.
        /// </summary>
        /// <returns>Instance of ConfigurationExtensions</returns>
        private static ConfigurationExtensions GetInstance()
        {
            if (_instance == null)
            {
                Logger = new StringBuilder();
                _instance = new ConfigurationExtensions();
                _instance.LoadConfiguration();
            }
            return _instance;
        }


        /// <summary>
        /// Get a dictionary of configuration values.
        /// </summary>
        /// <param name="category">The category to get the configuration from.</param>
        /// <returns>Return a Dictionary from the current category selected.</returns>
        public static Dictionary<string, string> GetConfigurationFor(string category)
        {
            GetInstance();
            var conf = new Dictionary<string, string>();
            configuration.TryGetValue(category, out conf);
            return conf;
        }


        /// <summary>
        /// Reads out all configurations and put each into a dictionary.
        /// </summary>
        private void LoadConfiguration()
        {
            configuration = SessionManager.Open(Logger).All<ETL_Config>()
                .Where(c => c.IsActive == true)
                .ToList()
                .OfType<ETL_Config>()
                .GroupBy(r => r.Category)
                .ToDictionary(group => group.Key,
                    group => group.ToDictionary(d => d.Property,
                                                d => d.Value));
        }
    }
}
