﻿using Azure.Queues.Process.Data;
using Common.Enums;
using Common.Models;
using System;
using System.Linq;
using System.Text;


namespace Azure.Queues.Process
{

    /// <summary>
    /// Contains logic for handling InboxMessages
    /// </summary>
    public class DataWriter
    {

        public StringBuilder Logger;

        private DataWriter(){}


        /// <summary>
        /// Create a new instance of DataWriter
        /// </summary>
        /// <param name="logger"></param>
        public DataWriter(StringBuilder logger)
        {
            Logger = logger;
        }


        /// <summary>
        /// Insert a new Message to inbox
        /// </summary>
        /// <param name="message"></param>
        /// <param name="Log"></param>
        public void InsertMessage(MsmqMessageInbox message)
        {
            using (var db = SessionManager.Open(Logger))
            {
                try
                {
                    db.WithTransaction(s=> s.Save(message), Logger);
                }
                catch (Exception ex)
                {
                    Logger.AppendLine($"Failed during insert: {ex.Message}");
                }
            }
        }


        /// <summary>
        /// Set status for th current message
        /// </summary>
        /// <param name="id"></param>
        /// <param name="updatedStatus"></param>
        /// <param name="errorMessage"></param>
        /// <param name="operation"></param>
        /// <returns></returns>
        public bool SetStatusForMessage(long id, MessageInboxStatus updatedStatus, string errorMessage, EventOperation operation = EventOperation.Import)
        {
            using (var db = SessionManager.Open(Logger))
            {
                var result = false;

                using (var transaction = db.BeginTransaction())
                {
                    try
                    {
                        switch (operation)
                        {
                            case EventOperation.Import:
                                result = UpdateInboxMessage(id, updatedStatus, errorMessage);
                                break;
                            case EventOperation.Retry:
                                result = UpdateRetryMessage(id, updatedStatus);
                                break;
                        }
                        transaction.Commit();
                        db.Flush();
                        return result;
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            }
        }


        /// <summary>
        /// If something goes wrong dont delete the message just update the Retrycount
        /// </summary>
        /// <param name="id"></param>
        /// <param name="updatedStatus"></param>
        /// <returns></returns>
        private bool UpdateRetryMessage(long id, MessageInboxStatus updatedStatus)
        {
            var db = SessionManager.Open(Logger);

            var failedEntry = db.Find<MsmqFailedMessages>(m => m.Id == id).SingleOrDefault();

            if (failedEntry != null)
            {
                failedEntry.LastUpdated = DateTime.Now;
                failedEntry.Status = (int)updatedStatus;

                if (updatedStatus == MessageInboxStatus.Failed)
                {
                    failedEntry.RetryCount++;
                }
                else
                {
                    db.SaveOrUpdate(failedEntry.ToCompletedMessage());
                    db.All<MsmqCompletedMessages>().Add(failedEntry.ToCompletedMessage());
                    db.Delete<MsmqFailedMessages>(failedEntry);
                }

                db.Flush();
               return true;
            }
            return false;
        }


        /// <summary>
        /// Update the status for a Message. 
        /// Contains logic for Retry, insert and delete.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="updatedStatus"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        private bool UpdateInboxMessage(long id, MessageInboxStatus updatedStatus, string errorMessage)
        {
            var db = SessionManager.Open(Logger);
            var entry = db.All<MsmqMessageInbox>().SingleOrDefault(m => m.Id == id);
            var keepInInbox = false;

            if (entry != null)
            {
                entry.LastUpdated = DateTime.Now;
                entry.Status = (int)updatedStatus;
                entry.ErrorMessage = errorMessage;

                if (updatedStatus == MessageInboxStatus.Failed)
                {
                    entry.RetryCount++;
                    if (entry.RetryCount >= 5) {
                        db.Save<MsmqFailedMessages>(entry.ToFailedMessage());
                    }
                    else {
                        entry.Status = (int)MessageInboxStatus.Received;
                        keepInInbox = true;
                    }
                }
                else if (updatedStatus == MessageInboxStatus.Ignored) {
                    db.Save<MsmqFailedMessages>(entry.ToFailedMessage());
                }
                else {
                    db.Save<MsmqCompletedMessages>(entry.ToCompletedMessage());
                }

                if(!keepInInbox)
                    db.Delete<MsmqMessageInbox>(entry);

                db.Flush();

                return true;
            }

            return false;
        }
    }
}
