﻿using System;
using System.Text;
using Azure.Queues.Process.Helpers;
using Azure.Queues.Process.Data;
using Common.Models;
using Common.Enums;
using System.Collections.Generic;
using Common.Entities;
using Newtonsoft.Json.Linq;
using System.Linq;

namespace Azure.Queues.Process
{

    /// <summary>
    /// Contains logic for processing InboxMessages
    /// </summary>
    public class Process
    {

        /// <summary>
        /// Check if there's more items to process in InboxMessages table.
        /// </summary>EventProcessorFunc
        /// <returns>True if InboxMessages has items</returns>
        public static bool HasNext(StringBuilder log)
        {
            var session = SessionManager.Open(log);
            if (session == null){
                throw new Exception($"SessionManager is not ready: {session.IsOpen}");
            }

            var inbox = session.Rows<MsmqMessageInbox>();

            log.AppendLine($"Current count in inbox: {inbox.ToString()}");
            return inbox > 0;
        }


        /// <summary>
        /// Inserts a batch from a json string. 
        /// </summary>
        /// <param name="json">string json as a JArray</param>
        /// <param name="type">EventMessageType type</param>
        /// <param name="log">StringBuilder log</param>
        /// <returns>Log string</returns>
        public static string InsertBatch(string json, EventMessageType type, StringBuilder log)
        {
            var objlist = new List<JToken>();
            var token = JToken.Parse(json);

            if (token is JArray)
                objlist = token.Children().ToList();
            else
                objlist.Add(token);

            objlist.ForEach(o => {
                log.AppendLine(Insert(o.ToString(), EventMessageType.BankKundeRegisterEvent, log));
            });
           
            return log.ToString();
        }


        /// <summary>
        /// Insert a new InboxMessage
        /// </summary>
        /// <param name="item">The items(Json) string</param>
        /// <param name="type">Type of items to insert (EventType)</param>
        /// <param name="log">Logger</param>
        /// <returns>Logger string</returns>
        public static string Insert(string item, EventMessageType type, StringBuilder log)
        {
            var inboxMessage = item.CreateEvent(log, type);
            var session = SessionManager.Open(log);

            if (inboxMessage != null)
            {
                log.AppendLine($"Trying to add new {type}");

                session.WithTransaction(s =>
                {
                    s.Save(inboxMessage);
                    s.Flush();
                    log.AppendLine($"Insterted row: {inboxMessage.Id}");
                }, log);
            }

            return log.ToString();
        }
    }
}
