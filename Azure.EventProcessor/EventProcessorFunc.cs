
#define DEBUG //Turn on (override)debugger, remove in production

using Microsoft.Azure.WebJobs;
using Newtonsoft.Json.Linq;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using System.Text;
using System;
using Microsoft.ServiceBus.Messaging;
using Azure.Queues.Process;
using Azure.Queues.Process.EventGrid;
using Microsoft.Azure.WebJobs.Extensions.EventGrid;
using Azure.Queues.Process.Helpers;
using ProcessManager;
using Common.Enums;
using Azure.Logging;
using Microsoft.ApplicationInsights.Extensibility;
using Microsoft.ApplicationInsights;

namespace EventProcessor
{
    /// <summary>
    /// Functions for processing InboxMessages
    /// </summary>
    public static class EventProcessorFunc
    {
        // ****************************************************   EDIT   ****************************************************************

        private const string KundeRegisterSubscription = "crm";
        private const string KundeRegisterTopic = "kunderegister";

        private const string BankKundeRegisterSubscription = "crm";
        private const string BankKundeRegisterTopic = "bankkunderegister";


        private const string KundeHendelseSubscription = "crm";
        private const string KundeHendelseTopic = "kundehendelse";

        private const string SaksHendelseSubscription = "crm";
        private const string SaksHendelseTopic = "sakhendelse";

        // ************************************************ NO EDIT BELOW ***************************************************************


        //Debugger
        private static string key = TelemetryConfiguration.Active.InstrumentationKey = Environment.GetEnvironmentVariable("APPINSIGHTS_INSTRUMENTATIONKEY", EnvironmentVariableTarget.Process);
        private static TelemetryClient telemetry = new TelemetryClient() { InstrumentationKey = key };
        private static SingletonLogger Log = SingletonLogger.Instance();
        private const string ServicebusConnectionstring = "crm-bus-eventprocessor-endpoint-secret";   //KeyVault


        /// <summary>
        /// Processing InboxMessages
        /// </summary>
        /// <param name="eventGridEvent"></param>
        /// <param name="starter"></param>
        /// <param name="log"></param>
        /// <returns></returns>
        [Singleton(Mode = SingletonMode.Function)]
        [FunctionName(nameof(EventProcessor))]
        public static Task EventProcessor([EventGridTrigger]JObject eventGridEvent,
            [OrchestrationClient] DurableOrchestrationClient starter,
            ILogger log)
        {
            var Logger = new StringBuilder();
            Log.Add("EventProcessor Started");

            var message = eventGridEvent.GetSubject();
            Logger.AppendLine($"Subject received: {message}");

            var SessionID = new Guid();
            Task.Run(async () => {
                await Common.Initializer.Initialize.InitSettings();
            });

            var handler = ExternalEventHandler.GetInstance(SessionID);
            Logger.AppendLine("Starting RunProcessBatch");
            var result = handler.RunProcessBatch();
#if DEBUG
            Logger.Append($"RunBatch result: {result.Result}");
            log.LogInformation($"Log: {Logger.ToString()}");
#endif        
            if (Process.HasNext(Logger))
            {
                Logger.Append("Request restart success.");
#if DEBUG
                Logger.Append($"Log: {Logger.ToString()}");
                log.LogInformation(Logger.ToString());
#endif
                Log.Add("RequestRestart");
                return starter.StartNewAsync("ReRun", null); 
            }
            else {
                return Task.Run(()=> {
                    Log.Add("Done process batch.");
                    Logger.Append("Done process batch.");
                });
            }
        }


        /// <summary>
        /// Trigger to rerun EventProcessor
        /// </summary>
        /// <param name="context"></param>
        /// <param name="log"></param>
        /// <returns></returns>
        [Singleton(Mode = SingletonMode.Listener)]
        [FunctionName(nameof(ReRun))]
        public static async Task ReRun(
           [OrchestrationTrigger] DurableOrchestrationContext context, ILogger log)
        {
            log.LogInformation("Rerun started");
            var result = await EventGridSender.Send("Restart");
            log.LogInformation($"{result.StatusCode}");
            log.LogInformation("Rerun ended");
        }


        /// <summary>
        ///  Process a Servicebus message of type KundeRegister
        /// </summary>
        /// <param name="queueItem"></param>
        /// <param name="log"></param>
        [Singleton(Mode = SingletonMode.Function)]
        [FunctionName(nameof(KundeRegister))]
        public static void KundeRegister([ServiceBusTrigger(KundeRegisterTopic, KundeRegisterSubscription, AccessRights.Listen, Connection = ServicebusConnectionstring)]string queueItem, ILogger log)
        {
            log.LogInformation("Kunderegister started");
            var logger = new StringBuilder();
 #if DEBUG
            log.LogInformation($"{queueItem}");
#endif
            var loginfo = Process.InsertBatch(queueItem, EventMessageType.KundeRegisterEvent, logger);
            var result = EventGridSender.Send("Kunderegister");
#if DEBUG
            log.LogInformation(loginfo);
#endif
            log.LogInformation("Kunderegister ended");
        }


        /// <summary>
        ///  Process a Servicebus message of type BankKundeRegister
        /// </summary>
        /// <param name="queueItem"></param>
        /// <param name="log"></param>
        [Singleton(Mode = SingletonMode.Function)]
        [FunctionName(nameof(BankKundeRegister))]
        public static void BankKundeRegister([ServiceBusTrigger(BankKundeRegisterTopic, BankKundeRegisterSubscription, AccessRights.Listen, Connection = ServicebusConnectionstring)]string queueItem, ILogger log)
        {
            log.LogInformation("BankKundeRegister started");
            var logger = new StringBuilder();
#if DEBUG
            log.LogInformation($"{queueItem}");
#endif
            var loginfo = Process.InsertBatch(queueItem, EventMessageType.BankKundeRegisterEvent, logger);
            var result = EventGridSender.Send("BankKunderegister");
#if DEBUG
            log.LogInformation(loginfo);
#endif
            log.LogInformation("BankKundeRegister ended");
        }


        /// <summary>
        /// Process a Servicebus message of type KundeHendelse
        /// </summary>
        /// <param name="queueItem"></param>
        /// <param name="log"></param>
        [Singleton(Mode = SingletonMode.Function)]
        [FunctionName(nameof(KundeHendelse))]
        public static void KundeHendelse([ServiceBusTrigger(KundeHendelseTopic, KundeHendelseSubscription, AccessRights.Listen, Connection = ServicebusConnectionstring)]string queueItem, ILogger log)
        {
            log.LogInformation("KundeHendelse started");
            var logger = new StringBuilder();
#if DEBUG
            log.LogInformation($"{queueItem}");
#endif
            var loginfo = Process.Insert(queueItem, EventMessageType.KundeHendelseEvent, logger);
            var result = EventGridSender.Send("Kundehendelse");
#if DEBUG
            log.LogInformation(loginfo);
#endif
            log.LogInformation("KundeHendelse ended");
        }


        /// <summary>
        /// Process a Servicebus message of type SaksHendelse
        /// </summary>
        /// <param name="queueItem"></param>
        /// <param name="log"></param>
        [Singleton(Mode = SingletonMode.Function)]
        [FunctionName(nameof(Sakshendelse))]
        public static void Sakshendelse([ServiceBusTrigger(SaksHendelseTopic, SaksHendelseSubscription, AccessRights.Listen, Connection = ServicebusConnectionstring)]string queueItem, ILogger log)
        {
            log.LogInformation("Sakshendelse started");
            var logger = new StringBuilder();
#if DEBUG
            log.LogInformation($"{queueItem}");
#endif
            var loginfo = Process.Insert(queueItem, EventMessageType.SakHendelseEvent, logger);
            var result = EventGridSender.Send("Sakshendelse");
#if DEBUG
            log.LogInformation($"{loginfo}");
#endif
            log.LogInformation("Sakshendelse ended");
        }
    }
}
