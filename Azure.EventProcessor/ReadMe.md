﻿# Azure.Functions.EventProcessorQueue

#### Overview
	Contains Azure functions that handle the flow of events, communication with 
	Servicebus, EventGrid and CRM. 

## Project status 

| Project | Status | Description |
|---------|--------|-------------|
| Eventprocessor func | In progress | Configuration in Azure
| RuleEngine (test)  | In progress | Configure rules and tests 
| Azure.Queues.Process  | In progress | Need test of rules  
| Azure.Functions.Helpers  | In progress | Need all EventTypes added
| Azure.Logging  | Done | Need some improvements to separate each queue
| Crm.Services.Extensions  | Done | Support both WebApi and SDK
| EventManager  | Done | But, needs to be tested with SakHendelse (Json sent)
| Common  | Done | But, needs more cleanup

 

	