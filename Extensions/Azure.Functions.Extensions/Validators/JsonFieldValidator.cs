﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Azure.Functions.Helpers
{
    /// <summary>
    /// Contains functions for validating Json fields against a model properties.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class JsonFieldsValidator<T> where T : class
    {
        private static string _temp;
        private readonly Dictionary<string, JValue> _fields;
        public IEnumerable<KeyValuePair<string, JValue>> GetAllFields() => _fields;


        /// <summary>
        /// Validate json fields with model properties. 
        /// </summary>
        /// <param name="token">A token to validate</param>
        /// <param name="json">A json document</param>
        public JsonFieldsValidator(JToken token, string json)
        {
            _temp = JsonConvert.DeserializeObject(json).ToString();
            _fields = new Dictionary<string, JValue>();
            CollectFields(token);
        }


        /// <summary>
        /// Returns the validated JSON 
        /// </summary>
        /// <returns>Valid Json string</returns>
        public override string ToString()
        {
            return _temp;
        }


        #region private methods

        private void CollectFields(JToken jToken)
        {
            switch (jToken.Type)
            {
                case JTokenType.Object:
                    foreach (var child in jToken.Children<JProperty>())
                    {
                        var propertyName = Validate(typeof(T), child.Name);
                        SetValue(child.Name, propertyName);
                        CollectFields(child);
                    }
                    break;
                case JTokenType.Array:
                    foreach (var child in jToken.Children())
                        CollectFields(child);
                    break;
                case JTokenType.Property:
                    CollectFields(((JProperty)jToken).Value);
                    break;
                default:
                    _fields.Add(jToken.Path, (JValue)jToken);
                    break;
            }
        }


        /// <summary>
        /// Compares the values in json with the properties in the class.
        /// </summary>
        /// <param name="t"></param>
        /// <param name="propName"></param>
        /// <returns></returns>
        internal static string Validate(Type t, string propertyName)
        {
            var props = t.GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (var type in props)
            {
                if (!IsPrimitiveType(type.PropertyType))
                {
                    if (type.GetType().IsClass)
                        foreach (var a in props)
                            propertyName = Validate(type.PropertyType, propertyName);
                }
                if (propertyName.Equals(type.Name, StringComparison.InvariantCultureIgnoreCase))
                    return type.Name;
            }
            return propertyName;
        }


        //Internal helper: Dont list properties if its a simple type. Only validate real classes.
        internal static bool IsPrimitiveType(Type type)
        {
            return
                type.IsPrimitive ||
                new Type[] {
                typeof(String),
                typeof(Decimal),
                typeof(DateTime),
                typeof(DateTimeOffset),
                typeof(TimeSpan),
                typeof(Guid)
                }.Contains(type) ||
                Convert.GetTypeCode(type) != TypeCode.Object ||
                (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>) && IsPrimitiveType(type.GetGenericArguments()[0]));
        }


        /// <summary>
        /// Replace the key with the key from the property.
        /// </summary>
        /// <param name="key">The key found in json</param>
        /// <param name="newKey">The key to change</param>
        internal static void SetValue(string oldKey, string newKey)
        {
            if (_temp.IndexOf(oldKey) > 0)
                _temp = _temp.Replace($"\"{oldKey}\":", $"\"{newKey}\":");
        }

        #endregion
    }
}
