﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Azure.Functions.Extensions.Converters
{
  
    /// <summary>
    /// Moved into Common project
    /// </summary>
    [Obsolete("Moved into Common project")]
    public class JsonConvertAttribute : Attribute
    {
        public string In{ get; set; }
    }
}
