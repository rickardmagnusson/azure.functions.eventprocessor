﻿using System.Collections.Generic;

namespace Azure.Functions.Helpers.Converters.Kunderegister.Bank
{
    public class Adresse
    {
        public IList<string> adresselinjer { get; set; }
        public Postnummer postnummer { get; set; }
        public string land { get; set; }
    }
}
