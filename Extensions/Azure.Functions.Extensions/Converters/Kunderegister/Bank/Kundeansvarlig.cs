﻿namespace Azure.Functions.Helpers.Converters.Kunderegister.Bank
{
    public class Kundeansvarlig
    {
        public string brukernavn { get; set; }
        public string navn { get; set; }
    }
}
