﻿using System;

namespace Azure.Functions.Helpers.Converters.Kunderegister.Bank
{
    public class KundeOppdatering
    {
        public enum Type { OPPDATERT, KUNDENUMMERBYTTE, SLETTET }

        public int kunderegisterMeldingsnummer { get; set; }
        public DateTime tidspunkt { get; set; }
        public Type type { get; set; }
        public KundeId kundeId { get; set; }
        public Kunde kunde { get; set; }
        public KundeId nyKundeId { get; set; }
    }
}
