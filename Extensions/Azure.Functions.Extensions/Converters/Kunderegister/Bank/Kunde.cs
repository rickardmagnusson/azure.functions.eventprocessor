﻿using Common.Enums;

namespace Azure.Functions.Helpers.Converters.Kunderegister.Bank
{
    public class Kunde
    {
        public KundeId kundeId { get; set; }
        public Kundestatus kundestatus { get; set; }
        public Privatkunde privatkunde { get; set; }
        public Organisasjon organisasjon { get; set; }
        public Epost epost { get; set; }
        public Telefonnummer telefonnummer { get; set; }
        public Telefonnummer mobilnummer { get; set; }
        public Adresse hovedadresse { get; set; }
        public Adresse postadresse { get; set; }
        public Kundeansvarlig kundeansvarlig { get; set; }

        public string GetFornavn()
        {
            return privatkunde?.fornavn;
        }

        public string GetEtternavn()
        {
            return privatkunde?.etternavn ?? organisasjon?.navn;
        }

        // TODO Kundetype kan gjerne sendes med direkte i kunde. Sjekk med CRM-team at det settes riktig
        public CustomerType GetCustomerType()
        {
            return privatkunde != null ? CustomerType.Privat : CustomerType.Naering;
        }
    }
}
