﻿using System;
using System.Collections.Generic;
using Common.Entities;
using Newtonsoft.Json;

namespace Azure.Functions.Helpers.Converters.Kunderegister.Bank
{
    public static class BankkundeConverter
    {
        public static IEnumerable<KundeRegisterEvent> ToKunderegisterEvents(string json)
        {
            var kundeOppdateringer = JsonConvert.DeserializeObject<IEnumerable<KundeOppdatering>>(json);
            var kunderegisterEvents = new List<KundeRegisterEvent>();
            foreach (var kundeOppdatering in kundeOppdateringer)
                kunderegisterEvents.Add(ToKunderegisterEvent(kundeOppdatering));

            return kunderegisterEvents;
        }

        private static KundeRegisterEvent ToKunderegisterEvent(KundeOppdatering kundeOppdatering)
        {
            switch (kundeOppdatering.type)
            {
                case KundeOppdatering.Type.OPPDATERT:
                    return new KundeRegisterEvent()
                    {
                        Source = Common.Enums.Source.Kerne,
                        //CustomerSourceId = NA (brukes av forsikring og EKF, identifisert med offentlig id for bank)
                        EventType = Common.Enums.EventType.Insert,
                        EventId = kundeOppdatering.kunderegisterMeldingsnummer.ToString(),
                        OwnerBankNumber = kundeOppdatering.kundeId.bank.value,
                        DistributorBankNumber = null, // Ikke akutelt for bank. Skal det sette samme som owner bank?
                        CustomerNumber = kundeOppdatering.kundeId.kundeValue,
                        CustomerEventSent = kundeOppdatering.tidspunkt,
                        //CreatedOn = NA
                        //LastUpdated = NA
                        CustomerInformation = new Common.Helpers.CustomerInfo
                        {
                            FirstName = kundeOppdatering.kunde.GetFornavn(),
                            LastName = kundeOppdatering.kunde.GetEtternavn(),
                            Phone1 = kundeOppdatering.kunde.telefonnummer?.value,
                            Phone2 = kundeOppdatering.kunde.mobilnummer?.value,
                            Email1 = kundeOppdatering.kunde.epost?.value,
                            Address1 = GetAddresss(kundeOppdatering.kunde.hovedadresse),
                            Address2 = GetAddresss(kundeOppdatering.kunde.hovedadresse),
                            //ShieldedCustomer = NA 
                            Status = kundeOppdatering.kunde.kundestatus.value
                        },
                        OwnerUsername = kundeOppdatering.kunde.kundeansvarlig?.brukernavn,
                        OwnerName = kundeOppdatering.kunde.kundeansvarlig?.navn,
                        DepartmentName = null,
                        DepartmentNumber = null,
                        CustomerType = kundeOppdatering.kunde.GetCustomerType(),
                        //InsuranceType = NA
                    };

                case KundeOppdatering.Type.SLETTET:
                    return new KundeRegisterEvent()
                    {
                        Source = Common.Enums.Source.Kerne,
                        EventType = Common.Enums.EventType.Delete,
                        EventId = kundeOppdatering.kunderegisterMeldingsnummer.ToString(),
                        OwnerBankNumber = kundeOppdatering.kundeId.bank.value,
                        DistributorBankNumber = null, 
                        CustomerNumber = kundeOppdatering.kundeId.kundeValue,
                        CustomerEventSent = kundeOppdatering.tidspunkt,
                    };

                case KundeOppdatering.Type.KUNDENUMMERBYTTE:
                    return new KundeRegisterEvent()
                    {
                        Source = Common.Enums.Source.Kerne,
                        EventType = Common.Enums.EventType.Key,
                        EventId = kundeOppdatering.kunderegisterMeldingsnummer.ToString(),
                        OwnerBankNumber = kundeOppdatering.kundeId.bank.value,
                        DistributorBankNumber = null,
                        CustomerNumber = kundeOppdatering.kundeId.kundeValue,
                        CustomerEventSent = kundeOppdatering.tidspunkt,
                    };

                default:
                    throw new Exception("Unknown type for kunderegister.bank.KundeOpppdatering");
            }
        }

        private static Common.Entities.Address GetAddresss(Adresse adresse)
        {
            if (adresse == null)
                return null;

            return new Address
            {
                AddressLine1 = adresse.adresselinjer.Count > 0 ? adresse.adresselinjer[0] : null,
                AddressLine2 = adresse.adresselinjer.Count > 1 ? adresse.adresselinjer[1] : null,
                AddressLine3 = adresse.adresselinjer.Count > 2 ? adresse.adresselinjer[2] : null,
                AddressLine4 = adresse.adresselinjer.Count > 3 ? adresse.adresselinjer[3] : null,
                AddressLine5 = adresse.adresselinjer.Count > 4 ? adresse.adresselinjer[4] : null,
                PostalCode = adresse.postnummer?.value,
                CountryCode = adresse.land,
            };
        }
    }
}
