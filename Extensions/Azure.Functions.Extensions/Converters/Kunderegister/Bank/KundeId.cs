﻿namespace Azure.Functions.Helpers.Converters.Kunderegister.Bank
{
    public class KundeId
    {
        public string kundeValue { get; set; }
        public Bank bank { get; set; }
    }
}
