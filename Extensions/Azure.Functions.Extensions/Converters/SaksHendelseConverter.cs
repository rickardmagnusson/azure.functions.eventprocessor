﻿using Azure.Functions.Helpers;
using System.Linq;
using Azure.Functions.Extensions.Converters.SakHendelseEvent;
using System;

namespace Azure.Functions.Extensions.Converters
{
   
    /// <summary>
    /// For now this class isn't used. 
    /// In future classes will be added to Deserialize the SakHendelse class
    /// </summary>
    public class SakHendelseConverter
    {
        /// <summary>
        /// Not used yet
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        public static Common.Entities.SakHendelse ToSakHendelse(string json)
        {
            return new TypeConverter().ToObject<SakHendelse>(json).Cast<Common.Entities.SakHendelse>();
        }


        /// <summary>
        /// Log entries
        /// </summary>
        /// <param name="sakHendelse"></param>
        /// <returns></returns>
        public static string ToLog(Common.Entities.SakHendelse sakHendelse)
        {
            //Here I'm missing values in json: SourceId
            return $"CreateEvent: {sakHendelse.FagSystem}, {(sakHendelse.Interessent != null ? sakHendelse.Interessent.OffentligId : string.Empty)}, {nameof(Common.Entities.SakHendelse)}, {sakHendelse.FagSystem}, {sakHendelse.FagSystem}"; 
        }
    }
}
