﻿using System;

namespace Azure.Functions.Extensions.Converters.SakHendelseEvent
{
    /// <summary>
    /// Wrapper for parsing events
    /// </summary>
    public class SakHendelse
    {
        public Guid? Uuid { get; set; }
        public App App { get; set; }
        public Fagsystem Fagsystem { get; set; }
        public Channel Channel { get; set; }
        public Bankregnr Brukersted { get; set; }
        public UserId RaadgiverId { get; set; }
        public string RaadgiverNavn { get; set; }
        public long? SakId { get; set; }
        public Guid? SakUuid { get; set; }
        public SakHendelseType SakHendelseType { get; set; }
        public object SakOppdatering { get; set; }
        public string SakStatus { get; set; }
        public string SakFase { get; set; }
        public string SakType { get; set; }
        public Interessent Interessent { get; set; }
        public DateTimeOffset? Tidspunkt { get; set; }
        public object Resultat { get; set; }
        public ApplikasjonVersjon ApplikasjonVersjon { get; set; }
        public Attributter Attributter { get; set; }
        public string FagsystemKundenummer { get; set; } //This is needed in future. It's not provided in Json sent. (SourceId)
    }

    public class Bankregnr
    {
        public string Value { get; set; }
    }

    public class SakHendelseType
    {
        public string Name { get; set; }
    }

    public class UserId
    {
        public string RaadgiverId { get; set; }
    }

    public class Fagsystem
    {
        public string Name { get; set; }
    }

    public class App
    {
        public string Prosessomraade { get; set; }
        public int InternId { get; set; }
        public string Navn { get; set; }
    }

    public class ApplikasjonVersjon
    {
        public string Versjon { get; set; }
    }

    public class Attributter
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }

    public class Brukersted
    {
        public string Value { get; set; }
    }

    public class Channel
    {
        public string Name { get; set; }
    }

    public class Interessent
    {
        public Brukersted KundeId { get; set; }
        public string Fornavn { get; set; }
        public string Etternavn { get; set; }
        public string Rolle { get; set; }

        public string OffentligId { get; set; }
    }

}
