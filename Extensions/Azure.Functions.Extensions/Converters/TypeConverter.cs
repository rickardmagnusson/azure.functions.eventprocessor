﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Azure.Functions.Helpers
{
    /// <summary>
    /// Contains method to convert type/strings to objects
    /// </summary>
    public class TypeConverter
    {
        /// <summary>
        /// Get the parsed xml.
        /// </summary>
        public string Xml { get; private set; }


        /// <summary>
        /// Creates an object of type T from a Json string.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="json">Json string</param>
        /// <returns>T from json</returns>
        public T ToObject<T>(string json) where T : class, new()
        {
            var jsonToken = JToken.Parse(json);
            var result = new JsonFieldsValidator<T>(jsonToken, json);
            string xml = SerializeJsonToXml<T>(result.ToString());
            var serializer = new XmlSerializer(typeof(T));
            var stringReader = new StringReader(xml);
            this.Xml = xml;

            return (T)serializer.Deserialize(stringReader);
        }


        /// <summary>
        /// Serialize an object to an Xml string
        /// </summary>
        /// <typeparam name="T">Object to extract xml from</typeparam>
        /// <param name="t">Object Type</param>
        /// <returns>Xml string</returns>
        public string CreateXml<T>(T t)
        {
            return SerializeToValidXml<T>(SerializeObject<T>(t));
        }


        #region Serializer


        /// <summary>
        /// Serialize an object 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="toSerialize"></param>
        /// <returns></returns>
        public static string SerializeObject<T>(T toSerialize)
        {
            var xmlSerializer = new XmlSerializer(toSerialize.GetType());
            var settings = new XmlWriterSettings
            {
                Encoding = new UnicodeEncoding(false, false),
                Indent = true,
                OmitXmlDeclaration = true
            };

            using (var textWriter = new StringWriter())
            using (var xmlWriter = XmlWriter.Create(textWriter, settings))
            {
                xmlSerializer.Serialize(textWriter, toSerialize);
                return textWriter.ToString();
            }
        }


        /// <summary>
        /// Creates a XmlDocument(as string) from a json string.
        /// </summary>
        /// <typeparam name="T">The type to convert into</typeparam>
        /// <param name="json">The json</param>
        /// <returns>Xml string</returns>
        public string SerializeJsonToXml<T>(string json) where T : class
        {
            var typeName = typeof(T).Name;
            var o = JObject.Parse(json);
            var so = JsonConvert.SerializeObject(o);
            var xml = JsonConvert.DeserializeXmlNode(so, typeName).OuterXml;
            this.Xml = xml;

            return SerializeToValidXml<T>(xml);
        }


        /// <summary>
        /// A function that will validate the xml created and also add Xml declaration.
        /// </summary>
        /// <typeparam name="T">A model of type T</typeparam>
        /// <param name="xml">The xml string</param>
        /// <returns>A valid xml document as string</returns>
        public string SerializeToValidXml<T>(string xml)
        {
            var serializer = new XmlSerializer(typeof(T));
            var settings = new XmlWriterSettings
            {
                Encoding = new UnicodeEncoding(false, false),
                Indent = true,
                OmitXmlDeclaration = true
            };

            this.Xml = xml;
            var stringReader = new StringReader(xml);
            var nobj = serializer.Deserialize(stringReader);

            using (var textWriter = new StringWriter())
            {
                using (var xmlWriter = XmlWriter.Create(textWriter, settings))
                {
                    serializer.Serialize(xmlWriter, nobj);
                }
                return textWriter.ToString();
            }
        }

        #endregion
    }
}
