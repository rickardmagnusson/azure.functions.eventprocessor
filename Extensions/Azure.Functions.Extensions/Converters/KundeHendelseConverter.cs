﻿using Azure.Functions.Helpers;
using Common.Entities;



namespace Azure.Functions.Extensions.Converters
{
    public class KundeHendelseConverter
    {
        public static KundeHendelse ToKundeHendelse(string json)
        { 
            return CastExtensions.ConvertTo<KundeHendelse>(json);
        }
    }
}
