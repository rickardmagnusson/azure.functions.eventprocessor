﻿using Common.Entities;


namespace Azure.Functions.Extensions.Converters
{
    /// <summary>
    /// Wrapper for KundeRegister to convert an object to KundeRegisterEvent
    /// </summary>
    public class KundeRegisterConverter
    {
        /// <summary>
        /// Convert a json to KundeRegisterEvent
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        public static KundeRegisterEvent ToKundeRegisterEvent(string json)
        {
            //Going to add a list of event to processed
            return new KundeRegisterEvent();
        }
    }
}
