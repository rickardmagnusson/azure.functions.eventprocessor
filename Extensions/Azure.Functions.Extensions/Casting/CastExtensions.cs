﻿using Azure.Functions.Extensions.Converters;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Azure.Functions.Helpers
{
    /// <summary>
    /// Contains Converter / Cast helper methods.
    /// </summary>
    public static class CastExtensions
    {
        /// <summary>
        /// Cast between equal objects.
        /// Copys data from one class to another that has the same properties.
        /// </summary>
        /// <typeparam name="T">Type to cast into</typeparam>
        /// <param name="myobj">Object with data</param>
        /// <returns>An object casted to object T</returns>
        public static T Cast<T>(this object myobj)
        {
            Type objectType = myobj.GetType();
            Type target = typeof(T);
            var x = Activator.CreateInstance(target, false);
            var z = from source in objectType.GetMembers().ToList()
                    where source.MemberType == MemberTypes.Property
                    select source;

            var d = from source in target.GetMembers().ToList()
                    where source.MemberType == MemberTypes.Property
                    select source;

            List<MemberInfo> members = d.Where(memberInfo => d.Select(c => c.Name)
               .ToList().Contains(memberInfo.Name)).ToList();

            PropertyInfo propertyInfo;
            object value;
            foreach (var memberInfo in members)
            {
                var newProp = myobj.GetType().GetProperty(memberInfo.Name).Attributes;
                propertyInfo = typeof(T).GetProperty(memberInfo.Name);
                value = myobj.GetType().GetProperty(memberInfo.Name).GetValue(myobj, null);

                propertyInfo.SetValue(x, value, null);
            }
            return (T)x;
        }


        /// <summary>
        /// Cast a JObject to class T
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="myobj"></param>
        /// <returns></returns>
        public static T Cast<T>(this JObject myobj)
        {
            return JsonConvert.DeserializeObject<T>(myobj.ToString());
        }


        /// <summary>
        /// Get all properties from a class with an attibutes marked with JsonConvert.
        /// Dependent on JsonConvert <see cref="Azure.Functions.Extensions.Converters.JsonConvertAttribute"/>
        /// It will read the In value from the attibute.
        /// </summary>
        /// <param name="t">Object Type</param>
        /// <returns>A dictionary of properties with the In values a value</returns>
        private static Dictionary<string, string> GetProperties<T>()
        {
            var _dict = new Dictionary<string, string>();
            var props = typeof(T).GetProperties();

            foreach (PropertyInfo prop in props)
            {
                object[] attrs = prop.GetCustomAttributes(true);

                foreach (object attrib in attrs)
                {
                    var attr = attrib as Common.JsonConvertAttribute;
                    if (attr != null)
                    {
                        string propName = prop.Name;
                        string inVal = attr.In;
                        _dict.Add(propName, inVal);
                    }
                }
            }

            return _dict;
        }


        /// <summary>
        /// Convert a json string to type T and flattens the object into the class.
        /// This method will only work if the properties has JsonConvert <see cref="Azure.Functions.Extensions.Converters.JsonConvertAttribute"/> attribute.
        /// </summary>
        /// <typeparam name="T">Type of object</typeparam>
        /// <param name="json"></param>
        /// <returns>An object of type T</returns>
        public static T ConvertTo<T>(string json)
        {
            var dict = GetProperties<T>();
            var kh = JObject.Parse(json);
            var newClass = new JObject();

            foreach (var k in dict.Keys)
            {
                if (dict[k].Contains(','))
                {
                    foreach (string a in dict[k].Split(','))
                    {
                        JToken token = null;
                        try
                        {
                            token = kh.SelectToken(a);
                            if (token != null)
                                newClass.Add(k, token);
                        }
                        catch { }
                    }
                }
                else
                {
                    newClass.Add(k, kh.SelectToken(dict[k]));
                }
            }

            return Cast<T>(newClass);
        }
    }
}
