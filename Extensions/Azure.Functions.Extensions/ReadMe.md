﻿# Azure.Functions.Helpers

#### Overview
	Contains functions for converting, parsing and validate events.

### Casting
    Contains cast functions


### Converters
    Contains converter for Kunde event like "Sak", "Kunde" och "KundeRegisterEvent"

### Validator
    Contains validator for Json when validatate Json data.