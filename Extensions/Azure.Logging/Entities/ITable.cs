﻿
namespace Azure.Logging
{
    /// <summary>
    /// Table entity
    /// Just a dummy to let nHibernate create the table
    /// </summary>
    public interface ITable : IEntity
    {
    }
}
