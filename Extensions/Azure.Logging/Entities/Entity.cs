﻿using FluentNHibernate.Mapping;


namespace Azure.Logging
{
    /// <summary>
    /// Main entity for nHibernate automatation
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class Entity<T> : ClassMap<T> where T : class, IEntity
    {
    }
}
