﻿using FluentNHibernate.Automapping;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using FluentNHibernate.Mapping;
using NHibernate;
using NHibernate.Dialect;
using NHibernate.Driver;
using NHibernate.Tool.hbm2ddl;
using System;
using System.IO;
using System.Reflection;
using System.Text;

namespace Azure.Logging
{
    /// <summary>
    /// Sessionmanager
    /// </summary>
    public class SessionManager
    {
        /// <summary>
        /// Opens the connection to the database.
        /// </summary>
        /// <param name="logger"></param>
        /// <returns></returns>
        public static ISession Open(StringBuilder logger)
        {
            Logger = logger;
            return CurrentFactory.OpenSession();
        }


        /// <summary>
        /// Opens the connection to the database.
        /// </summary>
        /// <param name="logger"></param>
        /// <returns></returns>
        public static ISession Open()
        {
            Logger = new StringBuilder();
            return CurrentFactory.OpenSession();
        }


        #region private methods


        private static StringBuilder Logger;
        private static SessionManager _instance;

        private SessionManager() { }


        private static SessionManager Instance()
        {
            if (_instance == null)
                _instance = new SessionManager();
            return _instance;
        }


        /// <summary>
        /// Get Current ISessionFactory
        /// </summary>
        private static ISessionFactory CurrentFactory
        {
            get
            {
                return Instance().CreateConfig();
            }
        }


        /// <summary>
        /// Creates the ISessionFactory configuration.
        /// </summary>
        /// <returns>ISessionFactory of current configuation.</returns>
        private ISessionFactory CreateConfig()
        {
            return FluentConfiguration
                .BuildSessionFactory();
        }


        /// <summary>
        /// NHibernate FluentConfiguration
        /// </summary>
        private FluentConfiguration FluentConfiguration
        {
            get
            {
                try
                {
                    //Only works in Azure
                    var conn = Environment.GetEnvironmentVariable("crm-sqldb-connectionstring-secret", EnvironmentVariableTarget.Process);
                    var dlls = AppDomain.CurrentDomain.GetAssemblies();

                    return Fluently
                               .Configure()
                               .Database(MsSqlConfiguration.MsSql2012
                               .ShowSql()
                               .Dialect<MsSqlAzure2008Dialect>()
                               .Driver<SqlClientDriver>()
                               .ConnectionString(conn))
                               .ExposeConfiguration(cfg => new SchemaExport(cfg).Execute(true, false, false))
                               .ExposeConfiguration(cfg => new SchemaUpdate(cfg).Execute(false, true))
                               .Mappings(MapAssemblies);
                }
                catch (Exception ex)
                {
                    if (ex is ReflectionTypeLoadException)
                    {
                        var typeLoadException = ex as ReflectionTypeLoadException;
                        var loaderExceptions = typeLoadException.LoaderExceptions;
                        Logger.AppendLine($"Error:ReflectionTypeLoadException: {loaderExceptions}");

                        foreach (Exception exSub in loaderExceptions)
                        {
                            Logger.AppendLine(exSub.Message);
                            var exFileNotFound = exSub as FileNotFoundException;
                            if (exFileNotFound != null)
                            {
                                if (!string.IsNullOrEmpty(exFileNotFound.FusionLog))
                                {
                                    Logger.AppendLine("Fusion Log:");
                                    Logger.AppendLine(exFileNotFound.FusionLog);
                                }
                            }
                            Logger.AppendLine("");
                        }
                    }

                    Logger.AppendLine($"Error:FluentConfiguration: {ex} {ex.InnerException}");
                }
                return null;
            }
        }


        /// <summary>
        /// Map assemblies
        /// </summary>
        /// <param name="fmc"></param>
        internal static void MapAssemblies(MappingConfiguration fmc)
        {
            fmc.AutoMappings.Add(AutoMap.Assembly(typeof(SingletonLogger).Assembly)
            .Where(IsEntity));
        }


        /// <summary>
        /// The Entity to look for
        /// </summary>
        /// <param name="t">The type to compare</param>
        /// <returns>If type is Entity</returns>
        private static bool IsEntity(Type t)
        {
            return typeof(IEntity).IsAssignableFrom(t);
        }

        #endregion
    }
}
