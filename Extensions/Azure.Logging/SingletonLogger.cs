﻿using System;
using System.Text;

namespace Azure.Logging
{
    /// <summary>
    /// Default instance of Logging
    /// Implemented as Singleton
    /// </summary>
    public class SingletonLogger
    {
        private static SingletonLogger _instance;
        private static StringBuilder _logger;

        private SingletonLogger() { }


        /// <summary>
        /// Instance of SingletonLogger
        /// </summary>
        /// <returns></returns>
        public static SingletonLogger Instance()
        {
            if (_instance == null)
            {
                _logger = new StringBuilder();
                _instance = new SingletonLogger();
            }
            return _instance;
        }


       /// <summary>
       /// Add a message to log
       /// </summary>
       /// <param name="message"></param>
        public void Add(string message)
        {
            Log(message, "", LevelType.Info);
        }


        /// <summary>
        /// In production we only shows error created from code in log
        /// </summary>
        /// <param name="message"></param>
        public void Debug(string message)
        {
            Log(message, "", LevelType.Debug);
        } 


        /// <summary>
        /// Add to log
        /// </summary>
        /// <param name="message"></param>
        public void AppendLine(string message)
        {
            Log(message, "", LevelType.Info);
        }


        //In production we only shows error created
        public void Error(string message)
        {
            Log(message, "", LevelType.Error);
        }


        private void Log(string message, string category, LevelType levelType = LevelType.Undefined)
        {
            var log = new LogItem
            {
                Category = "Default",
                Message = message,
                LevelType = levelType,
                Created = DateTime.Now
            };

            Instance().Create(log);
            _logger.AppendLine(message);
        }

       
        private void Create(LogItem item)
        {
            try { 
                var session = SessionManager.Open();
                session.Save(item);
                session.Flush();
            }catch
            {

            }
        }
    }

    /// <summary>
    /// Base class for Entity
    /// </summary>
    public class LogItem : ITable, IEntity
    {
        public virtual int Id { get; set; }

        public virtual string Category { get; set; }

        public virtual string Message { get; set; }

        public virtual LevelType LevelType { get; set; }

        public virtual DateTime Created { get; set; } = DateTime.Now;
    }

    /// <summary>
    /// Loglevel
    /// </summary>
    public enum LevelType
    {
        /// <summary>
        /// Not defined
        /// </summary>
        Undefined,
        /// <summary>
        /// Information
        /// </summary>
        Info,
        /// <summary>
        /// Error
        /// </summary>
        Error,
        /// <summary>
        /// Debug 
        /// </summary>
        Debug
    }

}
