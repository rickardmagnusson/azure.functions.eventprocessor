﻿#define DEBUG

using Azure.Logging;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;


namespace Crm.Services.Extensions
{
    /// <summary>
    /// Contains various methods to simplify work against CRM web api.
    /// </summary>
    public partial class OrganizationServiceHelper
    {
        private static readonly string SdkVersion = "v9.1";
        private static readonly SingletonLogger Log = SingletonLogger.Instance();

        /// <summary>
        /// Connect to CRM Web Api 
        /// </summary>
        /// <returns>A connected HttpClient</returns>
        public static HttpClient WebApi()
        {
            try { 
                var _accessToken = TokenHelper.GetToken();
                var client = new HttpClient
                {
                    BaseAddress = new Uri($"{ConfigurationManager.AppSettings["crm-app-resource"]}/api/data/{SdkVersion}/")
                };

                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Authorization", String.Format("Bearer {0}", _accessToken));
                client.DefaultRequestHeaders.Add("OData-MaxVersion", "4.0");
                client.DefaultRequestHeaders.Add("OData-Version", "4.0");
                return client;
            }
            catch (Exception ex){
                //For some reason the connection failed. Add to log.
                Log.Add($"{ex}");
            }

            return null;
        }


        /// <summary>
        /// Retreive an entity from a oData query
        /// </summary>
        /// <typeparam name="T">Type T</typeparam>
        /// <param name="query">oData query</param>
        /// <returns>A list of item T</returns>
        public static async Task<List<T>> ToEntity<T>(string query)
        {
            var client = WebApi();
            var response = await client.GetAsync(client.BaseAddress + query);
#if DEBUG
            Log.Add(query);
#endif
            if (response.IsSuccessStatusCode)
            {
                var data = await response.Content.ReadAsStringAsync();
                var token = JToken.Parse(data).SelectToken("value");
#if DEBUG
                Log.Add($"{token}");
#endif
                if (token is JArray)
                    return token.ToObject<List<T>>();
                else if (token is JObject)
                    return new List<T>() { token.ToObject<T>() };
            }
            else
            {
                Log.Add($"Error in query: {response.ReasonPhrase}");
            }
            //FailSafe return, something went totally wrong
            return new List<T>();
        }


        /// <summary>
        /// Retreive an entity from a oData query from the type specified
        /// </summary>
        /// <typeparam name="T">Type T</typeparam>
        /// <param name="query">oData query</param>
        /// <param name="oftype">string of type to get</param>
        /// <returns>A list of item T</returns>
        public static async Task<List<T>> ToEntity<T>(string query, string oftype)
        {
            var client = WebApi();
            var response = await client.GetAsync(client.BaseAddress + query);
            Console.WriteLine(response.StatusCode + "\n" + query + "\n");

            if (response.IsSuccessStatusCode)
            {
                var data = await response.Content.ReadAsStringAsync();
                var token = JToken.Parse(data).SelectToken(oftype);
#if DEBUG
                Log.Add($"{token}");
#endif
                if (token is JArray)
                    return token.ToObject<List<T>>();
                else if (token is JObject)
                    return new List<T>() { token.ToObject<T>() };
            }
            else
            {
                Log.Add($"Error in query: {response.ReasonPhrase}");
            }
            //FailSafe return, something went totally wrong
            return new List<T>();
        }


        /// <summary>
        /// Create entity
        /// </summary>
        /// <param name="jobject"></param>
        /// <param name="entityType">EntityType, for .eg contact</param>
        /// <returns></returns>
        public static async Task<bool> Put(JObject jobject, string entityType)
        {
            var request = new HttpRequestMessage(HttpMethod.Post, entityType);
            request.Content = new StringContent(jobject.ToString(), Encoding.UTF8, "application/json");
            var createResponse = await WebApi().SendAsync(request);

            if (createResponse.IsSuccessStatusCode)
                return true;
            return false;
        }


        /// <summary>
        /// Edit/Update entity
        /// </summary>
        /// <param name="jobject"></param>
        /// <param name="entityType">EntityType, for .eg contact</param>
        /// <returns></returns>
        public static async Task<bool> Edit(JObject jobject, string entityType, string id)
        {
            var request = new HttpRequestMessage(HttpMethod.Post, entityType);
            request.Content = new StringContent(jobject.ToString(), Encoding.UTF8, "application/json");
            var createResponse = await WebApi().SendAsync(request);

            if (createResponse.IsSuccessStatusCode)
                return true;
            return false;
        }


        /// <summary>
        /// Delete entity
        /// </summary>
        /// <param name="JObject"></param>
        /// <param name="entityType">EntityType, for .eg contact</param>
        /// <returns></returns>
        public static async Task<bool> Delete(string entityType, string id)
        {
            var request = new HttpRequestMessage(HttpMethod.Post, entityType);
            request.Content = new StringContent(entityType, Encoding.UTF8, "application/json");
            var createResponse = await WebApi().SendAsync(request);

            if (createResponse.IsSuccessStatusCode)
                return true;
            return false;
        }
    }
}
