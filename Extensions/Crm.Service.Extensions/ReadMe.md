﻿# Crm.Services.Extensions

#### Overview
	Contains functions to communicate with CRM.
	Create connection via Xrm (SDK) or Web Api. Reads configuration from AppSettings.
	
	WebApi: return a HttpClient connected to Crm via WebApi.

	OrganizationServiceHelper:
		Returns a IOrganizationService instance.

	Built in Tokenhelper which read applicationsettings:

    Example of App configuration:
	"CrmAppKeySecret": "key",
    "ida:ClientId": "8a5249d1-3a0b-43c8-b9ba-989c7ffc9d9d",
    "ida:ResourceToCRM": "https://eikacrmutv.api.crm4.dynamics.com",
    "ida:AADInstance": "https://login.microsoftonline.com/",
    "ida:TenantId": "9489b80b-0945-4e63-82b4-35f627f53d17",