﻿using Microsoft.IdentityModel.Clients.ActiveDirectory;
using System.Configuration;
using System.Threading.Tasks;

namespace Crm.Services.Extensions
{
    /// <summary>
    /// Helper to create a connection to AAD
    /// Contains one method that will create a string Token.
    /// </summary>
    public class TokenHelper
    {

        /// <summary>
        /// Aquire a Token
        /// </summary>
        /// <returns>The Token created</returns>
        public static string GetToken()
        {
            var token = GetInstance().CreateToken();
            return token.Result.AccessToken;
        }


        #region Private Methods

        static TokenHelper _instance;
        private TokenHelper() { }


        /// <summary>
        /// Creates a new instance of TokenHelper
        /// </summary>
        /// <returns></returns>
        internal static TokenHelper GetInstance()
        {
            if (_instance == null)
                _instance = new TokenHelper();

            return _instance;
        }


        /// <summary>
        /// Internally creates a Request for a Token.
        /// </summary>
        /// <returns>AuthenticationResult of the provided credentials in <see cref="AppSettingsSection"/>AppSettings</returns>
        private async Task<AuthenticationResult> CreateToken()
        {
            string clientId = ConfigurationManager.AppSettings["crm-app-clientid"];
            string clientSecret = ConfigurationManager.AppSettings["crm-app-key-secret"];
            string resource = ConfigurationManager.AppSettings["crm-app-resource"];
            string aadInstance = ConfigurationManager.AppSettings["crm-app-aadinstance"]; ;
            string tenantId = ConfigurationManager.AppSettings["crm-app-tenantid"];
            string authority = aadInstance + tenantId;
            AuthenticationResult _token = null;

            try
            {
                await Task.Run(() =>
                {
                    var authContext = new AuthenticationContext(authority);
                    var credential = new ClientCredential(clientId, clientSecret);
                    _token = authContext.AcquireTokenAsync(resource, credential).Result;
                });
            }
            catch
            {
                return null;
            }

            return _token;
        }

        #endregion
    }
}
