﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.WebServiceClient;
using System;
using System.Configuration;

namespace Crm.Services.Extensions
{
    /// <summary>
    /// Contains helpers for connecting to CRM
    /// Only contain one function, the connection to CRM thru IOrganizationService.
    /// </summary>
    public partial class OrganizationServiceHelper
    {

        /// <summary>
        /// Creates an IOrganizationService connection.
        /// </summary>
        /// <returns>IOrganizationService. Return null if no instance was made (Check the application settings if so).</returns>
        public static IOrganizationService GetIOrganizationService()
        {
            return GetInstance().CreateCrmOrganizationService();
        }


        #region Private Methods

        private static OrganizationServiceHelper _instance;


        private OrganizationServiceHelper() { }


        private static OrganizationServiceHelper GetInstance()
        {
            if (_instance == null)
                _instance = new OrganizationServiceHelper();
            return _instance;
        }


        private IOrganizationService CreateCrmOrganizationService()
        {
            try
            {
                var _accessToken = TokenHelper.GetToken();
                var _crmclient = new OrganizationWebProxyClient(new Uri(CreateOrganizationUri()), false);
                _crmclient.HeaderToken = _accessToken;
                return _crmclient;
            }
            catch
            {
                return null;
            }
        }


        private string CreateOrganizationUri()
        {
            string _crmUrl = ConfigurationManager.AppSettings["resourcetocrm"];
            string _tempcrmServiceUrl = "XRMServices/2011/Organization.svc";
            string _sdk_version = "9.0";
            string _resultUrl = $"{_crmUrl}/{_tempcrmServiceUrl}/web?SdkClientVersion={_sdk_version}";

            return _resultUrl;
        }

        #endregion
    }
}
