﻿
using Common.Enums;
using System;


namespace Azure.Servicebus.Sender
{
    public class KundeRegisterEventLoader
    {
        public static void LoadKundeRegisterEvent(KundeRegisterWrapper khw)
        {
            //Load all possible values from enums

            foreach (InsuranceType pa in (InsuranceType[])Enum.GetValues(typeof(InsuranceType)))
                khw.InsuranceType.Add(pa);

            foreach (EventType pa in (EventType[])Enum.GetValues(typeof(EventType)))
                khw.EventType.Add(pa);

            foreach (CustomerType pa in (CustomerType[])Enum.GetValues(typeof(CustomerType)))
                khw.CustomerType.Add(pa);

            foreach (Source pa in (Source[])Enum.GetValues(typeof(Source)))
                khw.Source.Add(pa);

        }
    }
}
