﻿using Common.Entities;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Azure.Servicebus.Sender
{
    public class KundeHendelseLoader
    {
        public static void LoadKundeHendelse(KundeHendelseWrapper khw)
        {
            //Load all possible values from enums

            foreach (ProcessArea pa in (ProcessArea[])Enum.GetValues(typeof(ProcessArea)))
                khw.ProcessArea.Add(pa);

            foreach (KundeHendelsePhase p in (KundeHendelsePhase[])Enum.GetValues(typeof(KundeHendelsePhase)))
                khw.Fase.Add(p);

            foreach (Fagsystem p in (Fagsystem[])Enum.GetValues(typeof(Fagsystem)))
                khw.Fagsystem.Add(p);

            foreach (HendelseAction p in (HendelseAction[])Enum.GetValues(typeof(HendelseAction)))
                khw.Handling.Add(p);

            foreach (HendelseTransaction p in (HendelseTransaction[])Enum.GetValues(typeof(HendelseTransaction)))
                khw.Transaction.Add(p);

            foreach (KundeHendelseChannel p in (KundeHendelseChannel[])Enum.GetValues(typeof(KundeHendelseChannel)))
                khw.KundeHendelseChannel.Add(p);

            foreach (Applikasjon p in (Applikasjon[])Enum.GetValues(typeof(Applikasjon)))
                khw.Applikasjon.Add(p); 
        }
    }
}
