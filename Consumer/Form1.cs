﻿using Common.Entities;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Crm.Services.Extensions;
using Microsoft.Xrm.Sdk;
using System.Configuration;
using Newtonsoft.Json;
using Common.Helpers;
using System.IO;
using Microsoft.ServiceBus.Messaging;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using System.Diagnostics;
using System.Threading;
using System.Dynamic;
using System.Reflection;

namespace Azure.Servicebus.Sender
{
    public partial class Form1 : Form
    {
        private readonly SynchronizationContext synchronizationContext;
        private KundeHendelseWrapper khw = new KundeHendelseWrapper();
        private KundeRegisterWrapper krw = new KundeRegisterWrapper();
        private SaksHendelseWrapper shw = new SaksHendelseWrapper();
        private List<Product> Products = new List<Product>();
        private List<Team> Teams = new List<Team>();
        private IOrganizationService service;
        private string _crmUrl;


        public Form1()
        {
            InitializeComponent();
            synchronizationContext = SynchronizationContext.Current;

            try
            {
                Log("Connecting to CRM..."); // Just not to be shown...
                service = OrganizationServiceHelper.GetIOrganizationService();
                _crmUrl = ConfigurationManager.AppSettings["ida:ResourceToCRM"];
                lblConnection.Text = "Connected";
                Log($"Connected to CRM:\nUrl: {_crmUrl}");
            }
            catch
            {
               Log("Check your appsettings(app.config). Applicationsettings for CRM is probably wrong.");
            }
        }


        //Load all combinations
        private void LoadComponentsData()
        {
            EventTypes et = new EventTypes();
            
            et.Events.AddRange(new List<EventMessageType> { EventMessageType.Undefined, EventMessageType.KundeRegisterEvent, EventMessageType.KundeHendelseEvent, EventMessageType.SakHendelseEvent });
            AddToCombo<EventMessageType>(cmbEventMessageTypes, et.Events);
            cmbEventMessageTypes.Text = "Velg eventtype";
            txtKundeId.Text = "10131983576";
            txtBrukersted.Text = "3290";
            txtFirstName.Text = "Jackie";
            txtLastName.Text = "Chanses";

            var crm = new CrmLogics(service);
            crm.GetProducts(cmbProducts, Products);
            crm.GetTeams(cmbTeams, Teams);
            KundeHendelseLoader.LoadKundeHendelse(khw);
            KundeRegisterEventLoader.LoadKundeRegisterEvent(krw);
        }


        private void AddToCombo<T>(ComboBox cb, List<T> items)
        {
            cb.Items.Clear();
            try {
                foreach (var item in items) { 
                    Debug.WriteLine(item);
                    cb.Items.Add(item);
                }
            }
            catch { }
            //cb.Items.RemoveAt(0);
        }


        private void Form1_Load(object sender, EventArgs e)
        {
            LoadComponentsData();
        }


        private void cmbEventMessageTypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            var sel = ((ComboBox)sender).SelectedItem;
            EventMessageType type = (EventMessageType)Enum.Parse(typeof(EventMessageType), sel.ToString());
            clearAll();

            switch (type)
            {
                case EventMessageType.KundeRegisterEvent:
                    tabControl1.SelectTab(1);
                    AddToCombo(cmbEventType, krw.EventType);
                    AddToCombo(cmbCustomerType, krw.CustomerType);
                    AddToCombo(cmbSource, krw.Source);
                    AddToCombo(cmbInsuranceType, krw.InsuranceType);
                    Log("KundeRegisterEvent");
                    break;
                case EventMessageType.KundeHendelseEvent:
                    tabControl1.SelectTab(0);
                    Log("KundeHendelseEvent");
                    AddToCombo(cmbProcessArea, khw.ProcessArea);
                    cmbProcessArea.Text = "Velg ProcessArea";
                    break;
                case EventMessageType.SakHendelseEvent:
                    tabControl1.SelectTab(2);
                    Log("SakHendelseEvent");
                    break;
               default:
                    button1.Enabled = false;
                    break;
            }

            button1.Enabled = true;
        }


        private void clearAll()
        {
            cmbApplikasjon.Items.Clear();
            cmbApplikasjon.ResetText();
            cmbFagsystem.Items.Clear();
            cmbFagsystem.ResetText();
            cmbKundeHendelsePhase.Items.Clear();
            cmbKundeHendelsePhase.ResetText();
        }


        public void Log(string message)
        {
            var logged = lblLog.Text;
            lblLog.Text = Environment.NewLine + message + logged;
        }


        private void button1_Click(object sender, EventArgs e)
        {

            if (cmbEventMessageTypes.SelectedIndex == -1) {
                MessageBox.Show("Velg eventtype");
                cmbEventMessageTypes.Focus();
               return;
            }

            if (cmbTeams.SelectedIndex == -1)
            {
                MessageBox.Show("Velg businessunit (Brukersted)");
                cmbTeams.Focus();
                return;
            }

            lblLog.MaximumSize = new Size(500, 0);

            var sel = cmbEventMessageTypes.SelectedItem;
            EventMessageType Topic = (EventMessageType)Enum.Parse(typeof(EventMessageType), sel.ToString());
            EventMessageType evt = Helpers.FromTopic(Topic.ToString());

            dynamic data = null;

            switch (Topic)
            {
                case EventMessageType.KundeRegisterEvent:
                    data = CreateRegisterEvent();
                    ActionManager.AddToAction(EventMessageType.KundeRegisterEvent);
                    break;
                case EventMessageType.KundeHendelseEvent:
                    data = CreateHendelseEvent();
                    ActionManager.AddToAction(EventMessageType.KundeHendelseEvent);
                    break;
                case EventMessageType.SakHendelseEvent:
                    ActionManager.AddToAction(EventMessageType.SakHendelseEvent);
                    data = CreateSakEvent();
                    break;
            }

            if (data == null)
                return;

            string log = string.Empty;
            data = JsonConvert.SerializeObject(data);
            string jsonFormatted = JValue.Parse(data).ToString(Formatting.Indented);
            txtCopy.Text = jsonFormatted;

            var message = new
            {
                Id = Guid.NewGuid(),
                Message = data.Replace("\\",""),
                EventType = evt,
                Type = Topic,
                Created = DateTime.UtcNow
            };

            Sender(data.Replace("\\", ""), Topic.ToTopic());
            
        }


        private static TopicClient topicClient;

        private void Sender(object item, string topic)
        {
            var json = JsonConvert.SerializeObject(item);
            //string jsonFormatted = JValue.Parse(json).ToString(Formatting.Indented); //Messy code

            JObject o = JObject.Parse(json);
            string id = (string)o["Id"];
            Log($"-----> {id}");
            Log($"Sending message with topic '{topic}'");

            try
            {
                //Allow us to continue with application when sending.
                var uiScheduler = TaskScheduler.FromCurrentSynchronizationContext();
                var sendTask = Task.Factory.StartNew(async () => 
                {
                    var _serviceBusConn = ConfigurationManager.AppSettings["SbEndpoint"];
                    topicClient = TopicClient.CreateFromConnectionString(_serviceBusConn, topic.ToLower());
                    BrokeredMessage msg = null;
                    msg = new BrokeredMessage(new MemoryStream(Encoding.UTF8.GetBytes(json)));
                    msg.MessageId = id;
                    msg.SessionId = Guid.NewGuid().ToString();
                    await topicClient.SendAsync(msg);
                }); 

                sendTask.ContinueWith(x =>
                {
                    //txtCopy.Text = jsonFormatted; //Messy, dont show it
                    Log("Message succesfully sent. Happy day!!!");
                    txtAnnet.Text = "";
                    btnCrmOpen.Enabled = true;
                    ActionManager.ResetEvent();

                }, uiScheduler
                 );  
            }
            catch (Exception ex)
            {
                Log($"Failed sending message: ex: {ex}");
            }
        }



        private void cmbProcessArea_SelectedIndexChanged(object sender, EventArgs e)
        {
            cmbKundeHendelsePhase.Items.Clear();
            AddToCombo(cmbKundeHendelsePhase, khw.Fase);
            cmbKundeHendelsePhase.Text = "Velg fase";
            Log("Added Fase");

            cmbFagsystem.Items.Clear();
            AddToCombo(cmbFagsystem, khw.Fagsystem);
            cmbFagsystem.Text = "Velg fagsystem";
            Log("Added Fagsystem");

            cmbHandling.Items.Clear();
            AddToCombo(cmbHandling, khw.Handling);
            cmbHandling.Text = "Velg handling";
            Log("Added handling");

            cmbTransaction.Items.Clear();
            AddToCombo(cmbTransaction, khw.Transaction);
            cmbTransaction.Text = "Velg transactionstype";
            Log("Added transactionstype");

            cmbChannel.Items.Clear();
            AddToCombo(cmbChannel, khw.KundeHendelseChannel);
            cmbChannel.Text = "Velg channel";
            Log("Added channel");

            cmbApplikasjon.Items.Clear();
            AddToCombo(cmbApplikasjon, khw.Applikasjon);
            cmbApplikasjon.Text = "Velg Applikasjon";
            Log("Added Velg Applikasjon");

           
        }




        //Events
        private object CreateRegisterEvent()
        {
            //if (cmbEventType.SelectedItem == null)
            //{
            //    MessageBox.Show("Velg eventype");
            //    cmbEventType.Focus();
            //    return null;
            //}

            //var customerSourceId = GetSourceIdByBankAndCustomerNumber();

            //new CrmLogics(service).GetHighestNumber();

            var message = new
            {
                EventType = cmbEventType.SelectedItem.ToString(),
                //EventId = "1",
                CustomerEventSent = CreateDate(),
                LastUpdated = SetZeroDateTime(),
                CreatedOn = SetZeroDateTime(),
                CustomerType = cmbCustomerType.SelectedItem.ToString(),
                Source = cmbSource.SelectedItem.ToString(),
                CustomerNumber = txtKundeId.Text,
                DistributorBankNumber = txtBrukersted.Text,
                OwnerBankNumber = "0501", // Or Parent? 0501
                CustomerSourceId = txtCustomerSourceId.Text,
                OwnerName = "",
                OwnerUsername = "",
                //DepartmentNumber = "",
                //DepartmentName = "",
                CustomerInformation = new CustomerInfo {
                    FirstName = string.IsNullOrEmpty(txtFirstName.Text) ? "FirstName" : txtFirstName.Text,
                    LastName = string.IsNullOrEmpty(txtLastName.Text) ? "LastName" : txtLastName.Text,
                    Email1 = "someEmail1@domain.com",
                    Email2 = "someEmail2@domain.com",
                    Phone1 = "48091498",
                    Phone2 = "48091498",
                    IndustrialCode = "IndustrialCode",
                    IndustrialName = "IndustrialName",
                    SectorCode = "SectorCode",
                    SectorName = "SectorName",
                    Segment = "Segment",
                    ShieldedCustomer = false,
                    Status = "1",
                    Address1 = new Address
                    { 
                        AddressLine1 = "AddressLine1",
                        AddressLine2 = "AddressLine2",
                        AddressLine3 = "AddressLine3",
                        AddressLine4 = "AddressLine4",
                        AddressLine5 = "AddressLine5",
                        City= "Oslo",
                        Country= "Country",
                        CountryCode="NO",
                        County="Norway",
                        PostalCode="0952",
                        State ="Oppland",
                        Type = "Type"
                    },
                    Address2 = new Address
                    {
                        AddressLine1 = "AddressLine1",
                        AddressLine2 = "AddressLine2",
                        AddressLine3 = "AddressLine3",
                        AddressLine4 = "AddressLine4",
                        AddressLine5 = "AddressLine5",
                        City = "Oslo",
                        Country = "Country",
                        CountryCode = "NO",
                        County = "Norway",
                        PostalCode = "0952",
                        State = "Oppland",
                        Type = "Type"
                    },
                    Address3 = new Address
                    {
                        AddressLine1 = "AddressLine1",
                        AddressLine2 = "AddressLine2",
                        AddressLine3 = "AddressLine3",
                        AddressLine4 = "AddressLine4",
                        AddressLine5 = "AddressLine5",
                        City = "Oslo",
                        Country = "Country",
                        CountryCode = "NO",
                        County = "Norway",
                        PostalCode = "0952",
                        State = "Oppland",
                        Type = "Type"
                    }
                },
                TimeOfDeath = SetZeroDateTime(),
                InsuranceType = cmbInsuranceType.SelectedItem.ToString()
            };

            return message;
        }


        private DateTime SetZeroDateTime()
        {
            return DateTime.MinValue.ToUniversalTime();
        }

        private DateTime CreateDate()
        {
            return DateTime.UtcNow;
        }
        

        private object CreateHendelseEvent()
        {
            var dynamicObject = new ExpandoObject() as IDictionary<string, Object>;

            dynamicObject.Add("Uuid", Guid.NewGuid().ToString());

            if (cmbApplikasjon.SelectedItem != null)
                dynamicObject.Add("Applikasjon", cmbApplikasjon.SelectedItem.ToString());

            if(cmbFagsystem.SelectedItem != null)
                dynamicObject.Add("FagSystem", cmbFagsystem.SelectedItem.ToString());

            if (cmbKundeHendelsePhase.SelectedItem != null)
                dynamicObject.Add("Fase", cmbKundeHendelsePhase.SelectedItem.ToString());

            if (!string.IsNullOrEmpty(txtKundeId.Text))
                dynamicObject.Add("KundeId", txtKundeId.Text);

            if (!string.IsNullOrEmpty(txtBrukersted.Text))
                dynamicObject.Add("Brukersted", txtBrukersted.Text);

            if (!string.IsNullOrEmpty(txtKundeId.Text))
                dynamicObject.Add("FagsystemKundenummer", txtKundeId.Text);

            if (cmbHandling.SelectedItem != null)
                dynamicObject.Add("Handling", cmbHandling.SelectedItem.ToString());

            if (cmbChannel.SelectedItem != null)
                dynamicObject.Add("Channel", cmbChannel.SelectedItem.ToString());

            if (cmbProcessArea.SelectedItem != null)
                dynamicObject.Add("ProsessOmraade", cmbProcessArea.SelectedItem.ToString());

            if (cmbTransaction.SelectedItem != null)
                dynamicObject.Add("TransaksjonsType", cmbTransaction.SelectedItem.ToString());

            if (!string.IsNullOrEmpty(txtVerdi.Text))
                dynamicObject.Add("Verdi", txtVerdi.Text);

            if (!string.IsNullOrEmpty(txtTidligereVerdi.Text))
                dynamicObject.Add("TidligereVerdi", txtTidligereVerdi.Text);

            if (!string.IsNullOrEmpty(txtProduktId.Text))
                dynamicObject.Add("ProduktKode", txtProduktId.Text);

            if (!string.IsNullOrEmpty(txtProduktNavn.Text))
                dynamicObject.Add("ProduktNavn", txtProduktNavn.Text);

            if (!string.IsNullOrEmpty(txtAnnet.Text))
                dynamicObject.Add("Annet", txtAnnet.Text);

            dynamicObject.Add("Tidspunkt", DateTime.UtcNow);

            return dynamicObject;
        }


        private SakHendelse CreateSakEvent()
        {
            var message = new SakHendelse
            {
                Uuid = Guid.NewGuid().ToString(),
                Applikasjon = (string)cmbApplikasjon.SelectedItem.ToString(),
                FagSystem = (string)cmbFagsystem.SelectedItem.ToString(),

            };
            return message;
        }

        private void cmbProducts_SelectedIndexChanged(object sender, EventArgs e)
        {
            var sel = cmbProducts.SelectedItem;
            var part = sel.ToString().Split(',')[0].TrimStart();
            var val = Products.Where(k=> k.Key==part).FirstOrDefault();

            txtProduktId.Text = val.Key;
            txtProduktNavn.Text = val.Value;
        }


        private void cmbTeams_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            var sel = cmbTeams.SelectedItem;
            var val = Teams.Where(k => k.Value == sel.ToString()).FirstOrDefault();
            var owner = val.OwnerBankNumber;
            lblOwner.Text = $"Parent: {owner}";
            txtBrukersted.Text = val.Key;
            lblbankSelected.Text = val.Key;
        }


        private bool visible = true;

        private void label2_Click(object sender, EventArgs e)
        {
            if (visible) {
                visible = false;
                txtCopy.Hide();
            }
            else{
                visible = true;
                txtCopy.Show();
            }
        }


        private void label19_Click(object sender, EventArgs e)
        {
            txtKundeId.Text = "";
            var ssn = Extensions.GenSSN();
            lblKundeId.Text = $"KundeId({ssn.Gender})";
            txtFirstName.Text = ssn.FirstName;
            txtLastName.Text = ssn.LastName;
            txtKundeId.Text = ssn.Number;
            Log($"Generated: {ssn.FirstName} {ssn.LastName} {ssn.Number}");
            txtCustomerSourceId.Text = Extensions.GenNumber();
            ActionManager.IsNewCustomer = true;
            btnCrmOpen.Enabled = false;
        }

        
        private void button2_Click(object sender, EventArgs e)
        {
            var ssn = txtKundeId.Text;
            if (string.IsNullOrEmpty(ssn))
                return;

            var uid = new CrmLogics(service).QueryCrmContact(ssn);
            if(uid==null)
            {
                MessageBox.Show($"The user with SSN '{ssn}'  doens't exist yet. Try again in a while.");
                return;
            }

            var contactPageViewUrl = $"/main.aspx?etc={(int)ETC.Contact}&extraqs=&histKey=&id={uid}&newWindow=true&pagetype=entityrecord#138135821";

            Process.Start($"{_crmUrl}{contactPageViewUrl}");
        }

        private void txtKundeId_TextChanged(object sender, EventArgs e)
        {
            btnCrmOpen.Enabled = true;
           
        }

        private void idaappKeyToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void devToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var form = new SettingsForm("dev");
            form.Show();
        }

        private void qAToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var form = new SettingsForm("qa");
            form.Show();
        }

        private void devToolStripMenuItem1_Click(object sender, EventArgs e)
        {
           
            var item = ((ToolStripMenuItem)sender);
            item.CheckOnClick = true;

        }

        private void qAToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            var item = ((ToolStripMenuItem)sender);
            item.CheckOnClick = true;
        }
    }


    public enum ETC
    {
        Acccount = 1,
        Contact = 2
    }



    /// <summary>
    /// Class to remember states in application.
    /// </summary>
    public static class ActionManager
    {

        public static EventMessageType Current { get; private set; }
        public static bool IsNewCustomer { get; set; }

        public static void AddToAction(EventMessageType eventType)
        {
            Current = eventType;
        }

        public static void ResetEvent()
        {
            Current = EventMessageType.Undefined;
            IsNewCustomer = false;
        }
    }
}


