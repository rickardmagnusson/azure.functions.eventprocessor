﻿namespace Azure.Servicebus.Sender
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.cmbEventMessageTypes = new System.Windows.Forms.ComboBox();
            this.lblConnection = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblLog = new System.Windows.Forms.Label();
            this.txtKundeId = new System.Windows.Forms.TextBox();
            this.lblKundeId = new System.Windows.Forms.Label();
            this.txtProduktNavn = new System.Windows.Forms.TextBox();
            this.txtProduktId = new System.Windows.Forms.TextBox();
            this.lblProduktNavn = new System.Windows.Forms.Label();
            this.lblProduktId = new System.Windows.Forms.Label();
            this.cmbProducts = new System.Windows.Forms.ComboBox();
            this.txtCopy = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtAnnet = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtBrukersted = new System.Windows.Forms.TextBox();
            this.lblTidligereVerdi = new System.Windows.Forms.Label();
            this.lblVerdi = new System.Windows.Forms.Label();
            this.txtTidligereVerdi = new System.Windows.Forms.TextBox();
            this.txtVerdi = new System.Windows.Forms.TextBox();
            this.cmbProcessArea = new System.Windows.Forms.ComboBox();
            this.cmbChannel = new System.Windows.Forms.ComboBox();
            this.cmbApplikasjon = new System.Windows.Forms.ComboBox();
            this.cmbKundeHendelsePhase = new System.Windows.Forms.ComboBox();
            this.cmbTransaction = new System.Windows.Forms.ComboBox();
            this.cmbFagsystem = new System.Windows.Forms.ComboBox();
            this.cmbHandling = new System.Windows.Forms.ComboBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cmbSource = new System.Windows.Forms.ComboBox();
            this.cmbCustomerType = new System.Windows.Forms.ComboBox();
            this.cmbEventType = new System.Windows.Forms.ComboBox();
            this.cmbInsuranceType = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.label8 = new System.Windows.Forms.Label();
            this.txtFirstName = new System.Windows.Forms.TextBox();
            this.txtLastName = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.cmbTeams = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.lblbankSelected = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label19 = new System.Windows.Forms.Label();
            this.btnCrmOpen = new System.Windows.Forms.Button();
            this.lblOwner = new System.Windows.Forms.Label();
            this.txtCustomerSourceId = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.idaappKeyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.devToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.qAToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.environmentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.devToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.qAToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // cmbEventMessageTypes
            // 
            this.cmbEventMessageTypes.FormattingEnabled = true;
            this.cmbEventMessageTypes.Location = new System.Drawing.Point(42, 103);
            this.cmbEventMessageTypes.Name = "cmbEventMessageTypes";
            this.cmbEventMessageTypes.Size = new System.Drawing.Size(328, 21);
            this.cmbEventMessageTypes.TabIndex = 0;
            this.cmbEventMessageTypes.SelectedIndexChanged += new System.EventHandler(this.cmbEventMessageTypes_SelectedIndexChanged);
            // 
            // lblConnection
            // 
            this.lblConnection.AutoSize = true;
            this.lblConnection.Location = new System.Drawing.Point(742, 63);
            this.lblConnection.Name = "lblConnection";
            this.lblConnection.Size = new System.Drawing.Size(86, 13);
            this.lblConnection.TabIndex = 4;
            this.lblConnection.Text = "ConnectionState";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(973, 606);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(140, 23);
            this.button1.TabIndex = 5;
            this.button1.Text = "Send event";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(113, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(151, 24);
            this.label1.TabIndex = 6;
            this.label1.Text = "Event Manager";
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.lblLog);
            this.panel1.Location = new System.Drawing.Point(742, 100);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(370, 222);
            this.panel1.TabIndex = 7;
            // 
            // lblLog
            // 
            this.lblLog.AutoSize = true;
            this.lblLog.Location = new System.Drawing.Point(21, 19);
            this.lblLog.Name = "lblLog";
            this.lblLog.Size = new System.Drawing.Size(0, 13);
            this.lblLog.TabIndex = 0;
            // 
            // txtKundeId
            // 
            this.txtKundeId.Location = new System.Drawing.Point(406, 229);
            this.txtKundeId.Name = "txtKundeId";
            this.txtKundeId.Size = new System.Drawing.Size(170, 20);
            this.txtKundeId.TabIndex = 8;
            this.txtKundeId.TextChanged += new System.EventHandler(this.txtKundeId_TextChanged);
            // 
            // lblKundeId
            // 
            this.lblKundeId.AutoSize = true;
            this.lblKundeId.Location = new System.Drawing.Point(403, 212);
            this.lblKundeId.Name = "lblKundeId";
            this.lblKundeId.Size = new System.Drawing.Size(49, 13);
            this.lblKundeId.TabIndex = 9;
            this.lblKundeId.Text = "Kundeid ";
            // 
            // txtProduktNavn
            // 
            this.txtProduktNavn.Location = new System.Drawing.Point(406, 274);
            this.txtProduktNavn.Name = "txtProduktNavn";
            this.txtProduktNavn.Size = new System.Drawing.Size(299, 20);
            this.txtProduktNavn.TabIndex = 13;
            // 
            // txtProduktId
            // 
            this.txtProduktId.Location = new System.Drawing.Point(406, 317);
            this.txtProduktId.Name = "txtProduktId";
            this.txtProduktId.Size = new System.Drawing.Size(299, 20);
            this.txtProduktId.TabIndex = 14;
            // 
            // lblProduktNavn
            // 
            this.lblProduktNavn.AutoSize = true;
            this.lblProduktNavn.Location = new System.Drawing.Point(403, 258);
            this.lblProduktNavn.Name = "lblProduktNavn";
            this.lblProduktNavn.Size = new System.Drawing.Size(70, 13);
            this.lblProduktNavn.TabIndex = 15;
            this.lblProduktNavn.Text = "ProduktNavn";
            // 
            // lblProduktId
            // 
            this.lblProduktId.AutoSize = true;
            this.lblProduktId.Location = new System.Drawing.Point(403, 301);
            this.lblProduktId.Name = "lblProduktId";
            this.lblProduktId.Size = new System.Drawing.Size(52, 13);
            this.lblProduktId.TabIndex = 16;
            this.lblProduktId.Text = "Produktid";
            // 
            // cmbProducts
            // 
            this.cmbProducts.FormattingEnabled = true;
            this.cmbProducts.Location = new System.Drawing.Point(406, 370);
            this.cmbProducts.Name = "cmbProducts";
            this.cmbProducts.Size = new System.Drawing.Size(299, 21);
            this.cmbProducts.Sorted = true;
            this.cmbProducts.TabIndex = 17;
            this.cmbProducts.SelectedIndexChanged += new System.EventHandler(this.cmbProducts_SelectedIndexChanged);
            // 
            // txtCopy
            // 
            this.txtCopy.Location = new System.Drawing.Point(742, 360);
            this.txtCopy.Multiline = true;
            this.txtCopy.Name = "txtCopy";
            this.txtCopy.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtCopy.Size = new System.Drawing.Size(370, 240);
            this.txtCopy.TabIndex = 18;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(742, 345);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 19;
            this.label2.Text = "JSON";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(42, 141);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(332, 463);
            this.tabControl1.TabIndex = 20;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.label18);
            this.tabPage1.Controls.Add(this.label17);
            this.tabPage1.Controls.Add(this.label16);
            this.tabPage1.Controls.Add(this.label15);
            this.tabPage1.Controls.Add(this.label14);
            this.tabPage1.Controls.Add(this.label13);
            this.tabPage1.Controls.Add(this.label12);
            this.tabPage1.Controls.Add(this.label11);
            this.tabPage1.Controls.Add(this.txtAnnet);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.txtBrukersted);
            this.tabPage1.Controls.Add(this.lblTidligereVerdi);
            this.tabPage1.Controls.Add(this.lblVerdi);
            this.tabPage1.Controls.Add(this.txtTidligereVerdi);
            this.tabPage1.Controls.Add(this.txtVerdi);
            this.tabPage1.Controls.Add(this.cmbProcessArea);
            this.tabPage1.Controls.Add(this.cmbChannel);
            this.tabPage1.Controls.Add(this.cmbApplikasjon);
            this.tabPage1.Controls.Add(this.cmbKundeHendelsePhase);
            this.tabPage1.Controls.Add(this.cmbTransaction);
            this.tabPage1.Controls.Add(this.cmbFagsystem);
            this.tabPage1.Controls.Add(this.cmbHandling);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(324, 437);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Kundehendelse";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(23, 204);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(49, 13);
            this.label18.TabIndex = 34;
            this.label18.Text = "Handling";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(166, 162);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(30, 13);
            this.label17.TabIndex = 33;
            this.label17.Text = "Fase";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(23, 162);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(57, 13);
            this.label16.TabIndex = 32;
            this.label16.Text = "Fagsystem";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(166, 118);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(46, 13);
            this.label15.TabIndex = 31;
            this.label15.Text = "Channel";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(23, 118);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(63, 13);
            this.label14.TabIndex = 30;
            this.label14.Text = "Transaction";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(23, 74);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(61, 13);
            this.label13.TabIndex = 29;
            this.label13.Text = "Applikasjon";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(23, 28);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(80, 13);
            this.label12.TabIndex = 28;
            this.label12.Text = "Processomrade";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(23, 315);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(35, 13);
            this.label11.TabIndex = 27;
            this.label11.Text = "Annet";
            // 
            // txtAnnet
            // 
            this.txtAnnet.Location = new System.Drawing.Point(23, 337);
            this.txtAnnet.Multiline = true;
            this.txtAnnet.Name = "txtAnnet";
            this.txtAnnet.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtAnnet.Size = new System.Drawing.Size(283, 47);
            this.txtAnnet.TabIndex = 26;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(201, 256);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 13);
            this.label3.TabIndex = 25;
            this.label3.Text = "Brukersted";
            // 
            // txtBrukersted
            // 
            this.txtBrukersted.Location = new System.Drawing.Point(201, 275);
            this.txtBrukersted.Name = "txtBrukersted";
            this.txtBrukersted.Size = new System.Drawing.Size(100, 20);
            this.txtBrukersted.TabIndex = 24;
            // 
            // lblTidligereVerdi
            // 
            this.lblTidligereVerdi.AutoSize = true;
            this.lblTidligereVerdi.Location = new System.Drawing.Point(23, 271);
            this.lblTidligereVerdi.Name = "lblTidligereVerdi";
            this.lblTidligereVerdi.Size = new System.Drawing.Size(73, 13);
            this.lblTidligereVerdi.TabIndex = 23;
            this.lblTidligereVerdi.Text = "Tidligere verdi";
            // 
            // lblVerdi
            // 
            this.lblVerdi.AutoSize = true;
            this.lblVerdi.Location = new System.Drawing.Point(25, 251);
            this.lblVerdi.Name = "lblVerdi";
            this.lblVerdi.Size = new System.Drawing.Size(31, 13);
            this.lblVerdi.TabIndex = 22;
            this.lblVerdi.Text = "Verdi";
            // 
            // txtTidligereVerdi
            // 
            this.txtTidligereVerdi.Location = new System.Drawing.Point(103, 275);
            this.txtTidligereVerdi.Name = "txtTidligereVerdi";
            this.txtTidligereVerdi.Size = new System.Drawing.Size(82, 20);
            this.txtTidligereVerdi.TabIndex = 21;
            // 
            // txtVerdi
            // 
            this.txtVerdi.Location = new System.Drawing.Point(103, 249);
            this.txtVerdi.Name = "txtVerdi";
            this.txtVerdi.Size = new System.Drawing.Size(82, 20);
            this.txtVerdi.TabIndex = 20;
            // 
            // cmbProcessArea
            // 
            this.cmbProcessArea.FormattingEnabled = true;
            this.cmbProcessArea.Location = new System.Drawing.Point(23, 46);
            this.cmbProcessArea.Name = "cmbProcessArea";
            this.cmbProcessArea.Size = new System.Drawing.Size(279, 21);
            this.cmbProcessArea.TabIndex = 19;
            this.cmbProcessArea.SelectedIndexChanged += new System.EventHandler(this.cmbProcessArea_SelectedIndexChanged);
            // 
            // cmbChannel
            // 
            this.cmbChannel.FormattingEnabled = true;
            this.cmbChannel.Location = new System.Drawing.Point(166, 134);
            this.cmbChannel.Name = "cmbChannel";
            this.cmbChannel.Size = new System.Drawing.Size(135, 21);
            this.cmbChannel.TabIndex = 18;
            // 
            // cmbApplikasjon
            // 
            this.cmbApplikasjon.FormattingEnabled = true;
            this.cmbApplikasjon.Location = new System.Drawing.Point(23, 90);
            this.cmbApplikasjon.Name = "cmbApplikasjon";
            this.cmbApplikasjon.Size = new System.Drawing.Size(278, 21);
            this.cmbApplikasjon.TabIndex = 13;
            // 
            // cmbKundeHendelsePhase
            // 
            this.cmbKundeHendelsePhase.FormattingEnabled = true;
            this.cmbKundeHendelsePhase.Location = new System.Drawing.Point(166, 178);
            this.cmbKundeHendelsePhase.Name = "cmbKundeHendelsePhase";
            this.cmbKundeHendelsePhase.Size = new System.Drawing.Size(135, 21);
            this.cmbKundeHendelsePhase.TabIndex = 14;
            // 
            // cmbTransaction
            // 
            this.cmbTransaction.FormattingEnabled = true;
            this.cmbTransaction.Location = new System.Drawing.Point(23, 134);
            this.cmbTransaction.Name = "cmbTransaction";
            this.cmbTransaction.Size = new System.Drawing.Size(138, 21);
            this.cmbTransaction.TabIndex = 17;
            // 
            // cmbFagsystem
            // 
            this.cmbFagsystem.FormattingEnabled = true;
            this.cmbFagsystem.Location = new System.Drawing.Point(23, 178);
            this.cmbFagsystem.Name = "cmbFagsystem";
            this.cmbFagsystem.Size = new System.Drawing.Size(139, 21);
            this.cmbFagsystem.TabIndex = 15;
            // 
            // cmbHandling
            // 
            this.cmbHandling.FormattingEnabled = true;
            this.cmbHandling.Location = new System.Drawing.Point(23, 219);
            this.cmbHandling.Name = "cmbHandling";
            this.cmbHandling.Size = new System.Drawing.Size(278, 21);
            this.cmbHandling.TabIndex = 16;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.label7);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Controls.Add(this.cmbSource);
            this.tabPage2.Controls.Add(this.cmbCustomerType);
            this.tabPage2.Controls.Add(this.cmbEventType);
            this.tabPage2.Controls.Add(this.cmbInsuranceType);
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(324, 437);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "KundeRegister";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(17, 175);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(41, 13);
            this.label7.TabIndex = 8;
            this.label7.Text = "Source";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(17, 124);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(75, 13);
            this.label6.TabIndex = 7;
            this.label6.Text = "CustomerType";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(17, 76);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "EventType";
            // 
            // cmbSource
            // 
            this.cmbSource.FormattingEnabled = true;
            this.cmbSource.Location = new System.Drawing.Point(18, 191);
            this.cmbSource.Name = "cmbSource";
            this.cmbSource.Size = new System.Drawing.Size(285, 21);
            this.cmbSource.TabIndex = 5;
            // 
            // cmbCustomerType
            // 
            this.cmbCustomerType.FormattingEnabled = true;
            this.cmbCustomerType.Location = new System.Drawing.Point(18, 141);
            this.cmbCustomerType.Name = "cmbCustomerType";
            this.cmbCustomerType.Size = new System.Drawing.Size(285, 21);
            this.cmbCustomerType.TabIndex = 4;
            // 
            // cmbEventType
            // 
            this.cmbEventType.FormattingEnabled = true;
            this.cmbEventType.Location = new System.Drawing.Point(18, 93);
            this.cmbEventType.Name = "cmbEventType";
            this.cmbEventType.Size = new System.Drawing.Size(285, 21);
            this.cmbEventType.TabIndex = 3;
            // 
            // cmbInsuranceType
            // 
            this.cmbInsuranceType.FormattingEnabled = true;
            this.cmbInsuranceType.Location = new System.Drawing.Point(18, 43);
            this.cmbInsuranceType.Name = "cmbInsuranceType";
            this.cmbInsuranceType.Size = new System.Drawing.Size(285, 21);
            this.cmbInsuranceType.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(17, 26);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(78, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "InsuranceType";
            // 
            // tabPage3
            // 
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(324, 437);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Sakhendelse";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(404, 164);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(33, 13);
            this.label8.TabIndex = 21;
            this.label8.Text = "Navn";
            // 
            // txtFirstName
            // 
            this.txtFirstName.Location = new System.Drawing.Point(406, 180);
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.Size = new System.Drawing.Size(100, 20);
            this.txtFirstName.TabIndex = 22;
            // 
            // txtLastName
            // 
            this.txtLastName.Location = new System.Drawing.Point(528, 180);
            this.txtLastName.Name = "txtLastName";
            this.txtLastName.Size = new System.Drawing.Size(160, 20);
            this.txtLastName.TabIndex = 23;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(404, 354);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(53, 13);
            this.label9.TabIndex = 24;
            this.label9.Text = "Produkter";
            // 
            // cmbTeams
            // 
            this.cmbTeams.FormattingEnabled = true;
            this.cmbTeams.Location = new System.Drawing.Point(408, 421);
            this.cmbTeams.Name = "cmbTeams";
            this.cmbTeams.Size = new System.Drawing.Size(297, 21);
            this.cmbTeams.Sorted = true;
            this.cmbTeams.TabIndex = 25;
            this.cmbTeams.SelectedIndexChanged += new System.EventHandler(this.cmbTeams_SelectedIndexChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(406, 405);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(71, 13);
            this.label10.TabIndex = 26;
            this.label10.Text = "Businessunits";
            // 
            // lblbankSelected
            // 
            this.lblbankSelected.AutoSize = true;
            this.lblbankSelected.Location = new System.Drawing.Point(407, 449);
            this.lblbankSelected.Name = "lblbankSelected";
            this.lblbankSelected.Size = new System.Drawing.Size(76, 13);
            this.lblbankSelected.TabIndex = 27;
            this.lblbankSelected.Text = "Selected bank";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(42, 26);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(65, 55);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 28;
            this.pictureBox1.TabStop = false;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(583, 232);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(51, 13);
            this.label19.TabIndex = 29;
            this.label19.Text = "Generate";
            this.label19.Click += new System.EventHandler(this.label19_Click);
            // 
            // btnCrmOpen
            // 
            this.btnCrmOpen.Location = new System.Drawing.Point(1011, 63);
            this.btnCrmOpen.Name = "btnCrmOpen";
            this.btnCrmOpen.Size = new System.Drawing.Size(100, 23);
            this.btnCrmOpen.TabIndex = 30;
            this.btnCrmOpen.Text = "Open in CRM";
            this.btnCrmOpen.UseVisualStyleBackColor = true;
            this.btnCrmOpen.Click += new System.EventHandler(this.button2_Click);
            // 
            // lblOwner
            // 
            this.lblOwner.AutoSize = true;
            this.lblOwner.Location = new System.Drawing.Point(640, 449);
            this.lblOwner.Name = "lblOwner";
            this.lblOwner.Size = new System.Drawing.Size(38, 13);
            this.lblOwner.TabIndex = 31;
            this.lblOwner.Text = "Owner";
            // 
            // txtCustomerSourceId
            // 
            this.txtCustomerSourceId.Location = new System.Drawing.Point(409, 492);
            this.txtCustomerSourceId.Name = "txtCustomerSourceId";
            this.txtCustomerSourceId.Size = new System.Drawing.Size(296, 20);
            this.txtCustomerSourceId.TabIndex = 32;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(410, 473);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(94, 13);
            this.label20.TabIndex = 33;
            this.label20.Text = "CustomerSourceId";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.settingsToolStripMenuItem,
            this.environmentToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1158, 24);
            this.menuStrip1.TabIndex = 34;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.idaappKeyToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.settingsToolStripMenuItem.Text = "File";
            // 
            // idaappKeyToolStripMenuItem
            // 
            this.idaappKeyToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.devToolStripMenuItem,
            this.qAToolStripMenuItem});
            this.idaappKeyToolStripMenuItem.Name = "idaappKeyToolStripMenuItem";
            this.idaappKeyToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.idaappKeyToolStripMenuItem.Text = "Settings";
            this.idaappKeyToolStripMenuItem.Click += new System.EventHandler(this.idaappKeyToolStripMenuItem_Click);
            // 
            // devToolStripMenuItem
            // 
            this.devToolStripMenuItem.Name = "devToolStripMenuItem";
            this.devToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.devToolStripMenuItem.Text = "Dev";
            this.devToolStripMenuItem.Click += new System.EventHandler(this.devToolStripMenuItem_Click);
            // 
            // qAToolStripMenuItem
            // 
            this.qAToolStripMenuItem.Name = "qAToolStripMenuItem";
            this.qAToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.qAToolStripMenuItem.Text = "QA";
            this.qAToolStripMenuItem.Click += new System.EventHandler(this.qAToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // environmentToolStripMenuItem
            // 
            this.environmentToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.devToolStripMenuItem1,
            this.qAToolStripMenuItem1});
            this.environmentToolStripMenuItem.Name = "environmentToolStripMenuItem";
            this.environmentToolStripMenuItem.Size = new System.Drawing.Size(87, 20);
            this.environmentToolStripMenuItem.Text = "Environment";
            // 
            // devToolStripMenuItem1
            // 
            this.devToolStripMenuItem1.Name = "devToolStripMenuItem1";
            this.devToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.devToolStripMenuItem1.Text = "Dev";
            this.devToolStripMenuItem1.Click += new System.EventHandler(this.devToolStripMenuItem1_Click);
            // 
            // qAToolStripMenuItem1
            // 
            this.qAToolStripMenuItem1.Name = "qAToolStripMenuItem1";
            this.qAToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.qAToolStripMenuItem1.Text = "QA";
            this.qAToolStripMenuItem1.Click += new System.EventHandler(this.qAToolStripMenuItem1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1158, 652);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.txtCustomerSourceId);
            this.Controls.Add(this.lblOwner);
            this.Controls.Add(this.btnCrmOpen);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.lblbankSelected);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.cmbTeams);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtLastName);
            this.Controls.Add(this.txtFirstName);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtCopy);
            this.Controls.Add(this.cmbProducts);
            this.Controls.Add(this.lblConnection);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lblProduktId);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.lblProduktNavn);
            this.Controls.Add(this.cmbEventMessageTypes);
            this.Controls.Add(this.lblKundeId);
            this.Controls.Add(this.txtProduktId);
            this.Controls.Add(this.txtKundeId);
            this.Controls.Add(this.txtProduktNavn);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Event Manager";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbEventMessageTypes;
        private System.Windows.Forms.Label lblConnection;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblLog;
        private System.Windows.Forms.TextBox txtKundeId;
        private System.Windows.Forms.Label lblKundeId;
        private System.Windows.Forms.TextBox txtProduktNavn;
        private System.Windows.Forms.TextBox txtProduktId;
        private System.Windows.Forms.Label lblProduktNavn;
        private System.Windows.Forms.Label lblProduktId;
        private System.Windows.Forms.ComboBox cmbProducts;
        private System.Windows.Forms.TextBox txtCopy;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.ComboBox cmbChannel;
        private System.Windows.Forms.ComboBox cmbTransaction;
        private System.Windows.Forms.ComboBox cmbHandling;
        private System.Windows.Forms.ComboBox cmbFagsystem;
        private System.Windows.Forms.ComboBox cmbKundeHendelsePhase;
        private System.Windows.Forms.ComboBox cmbApplikasjon;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.ComboBox cmbProcessArea;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtBrukersted;
        private System.Windows.Forms.Label lblTidligereVerdi;
        private System.Windows.Forms.Label lblVerdi;
        private System.Windows.Forms.TextBox txtTidligereVerdi;
        private System.Windows.Forms.TextBox txtVerdi;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbInsuranceType;
        private System.Windows.Forms.ComboBox cmbEventType;
        private System.Windows.Forms.ComboBox cmbCustomerType;
        private System.Windows.Forms.ComboBox cmbSource;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtFirstName;
        private System.Windows.Forms.TextBox txtLastName;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cmbTeams;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lblbankSelected;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtAnnet;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Button btnCrmOpen;
        private System.Windows.Forms.Label lblOwner;
        private System.Windows.Forms.TextBox txtCustomerSourceId;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem idaappKeyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem devToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem qAToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem environmentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem devToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem qAToolStripMenuItem1;
    }
}

