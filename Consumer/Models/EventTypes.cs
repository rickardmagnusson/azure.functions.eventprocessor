﻿using Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Azure.Servicebus.Sender
{
    public class EventTypes
    {
        public List<EventMessageType> Events { get; set; } = new List<EventMessageType>();
    }

}
