﻿
namespace Azure.Servicebus.Sender
{
    public class Product
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }


    public class Team
    {
        public string Key { get; set; }
        public string Value { get; set; }

        public string OwnerBankNumber { get; set; }

        public string Tradexsourceid { get; set; }
    }

   
    public class SourceMap
    {
        public string fullname { get; set; }
        public string cap_nicesourceid { get; set; }

        public string cap_nice2sourceid { get; set; }
        public string cap_tradexsourceid { get; set; }

        public string cap_banqsoftsourceid { get; set; }
    }
}
