﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Azure.Servicebus.Sender
{
    public enum Applikasjon
    {
        KREDITTBANK_SALGSPANTLAAN_MS_WS,
        KREDITTBANK_SMALAN_MS_WS,
        FORSIKRING_SKADE_KJOEP_DOMENE,  // (Forsikring - Web)
        SPARING_ADVENT_TRADEX_WS,       // (Sparing - Web)
        DAGLIGBANK_BLI_KUNDE_DOMENE
    }
}
