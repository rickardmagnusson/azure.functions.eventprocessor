﻿using Common.Entities;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Azure.Servicebus.Sender
{
    public class KundeRegisterWrapper
    {
        public List<InsuranceType> InsuranceType { get; set; } = new List<InsuranceType>();

        public List<EventType> EventType { get; set; } = new List<EventType>();

        public List<CustomerType> CustomerType { get; set; } = new List<CustomerType>();

        public List<Source> Source { get; set; } = new List<Source>();

    }
}
