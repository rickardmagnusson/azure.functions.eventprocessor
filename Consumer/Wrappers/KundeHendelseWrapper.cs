﻿using Common.Entities;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Azure.Servicebus.Sender
{
    public class KundeHendelseWrapper
    {
        public List<ProcessArea> ProcessArea { get; set; } = new List<ProcessArea>();
        public List<KundeHendelsePhase> Fase { get; set; } = new List<KundeHendelsePhase>();

        public List<Fagsystem> Fagsystem { get; set; } = new List<Fagsystem>();

        public List<HendelseAction> Handling { get; set; } = new List<HendelseAction>();

        public List<HendelseTransaction> Transaction { get; set; } = new List<HendelseTransaction>();

        public List<SalesOpportunity> SalesOpportunity { get; set; } = new List<SalesOpportunity>();

        public List<KundeHendelseChannel> KundeHendelseChannel { get; set; } = new List<KundeHendelseChannel>();

        public List<Applikasjon> Applikasjon { get; set; } = new List<Applikasjon>();


    }
}
