﻿using Common.Enums;
using Crm.Services.Extensions;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Azure.Servicebus.Sender
{
    public class CrmLogics
    {

        private IOrganizationService _service;
        private EntityCollection _teams;
        private EntityCollection _products;


        public CrmLogics(IOrganizationService service)
        {
            _service = service;
        }

        private void CheckConnection()
        {
            try
            {
                var whoRequest = new WhoAmIRequest();
                var whoResponse = (WhoAmIResponse)_service.Execute(whoRequest);
                if (whoResponse == null)
                    _service = OrganizationServiceHelper.GetIOrganizationService();
            }
            catch
            {
                _service = OrganizationServiceHelper.GetIOrganizationService();
            }
        }


        public string GetSourceIdByBankAndCustomerNumber(string customerNumber, Guid bankId, EntityName entityName, string sourceIdName)
        {
            var result = string.Empty;
            var isContact = entityName == EntityName.contact;

            var qe = new QueryExpression
            {
                EntityName = entityName.ToString(),
                ColumnSet = new ColumnSet(sourceIdName)
            };

            var fe = new FilterExpression(LogicalOperator.And);
            fe.AddCondition(new ConditionExpression("statecode", ConditionOperator.Equal, 0));
            fe.AddCondition(new ConditionExpression("owningbusinessunit", ConditionOperator.Equal, bankId));
            fe.AddCondition(new ConditionExpression(isContact ? "governmentid" : "accountnumber", ConditionOperator.Equal, customerNumber));
            qe.Criteria = fe;

            try
            {
                var collection = _service.RetrieveMultiple(qe);
                if (collection.Entities.Count > 0)
                {
                    foreach (var entity in collection.Entities)
                    {
                        var sourceId = entity.Contains(sourceIdName) ? entity[sourceIdName].ToString() : string.Empty;

                        if (!string.IsNullOrEmpty(sourceId))
                        {
                            return sourceId;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.Write(ex);
            }
            return result;
        }



        //public void ExitsInsource()
        //{
        //    CheckConnection();
        //    List<SourceMap> Sources = new List<SourceMap>(); 
        //    var query = new QueryExpression("contact");
        //    var columnNames = new[] { "fullname", "cap_nicesourceid", "cap_tradexsourceid", "cap_nice2sourceid", "cap_banqsoftsourceid" };
        //    query.ColumnSet = new ColumnSet(true);
           
     
        //    _teams = _service.RetrieveMultiple(query);

        //    foreach (var item in _teams.Entities)
        //    {
        //        if (item.Contains("fullname")) {

        //            var tradexsourceid = item.Attributes["cap_tradexsourceid"];
        //            var nicesourceid = item.Attributes["cap_nicesourceid"];
        //            var nice2sourceid = item.Attributes["cap_nice2sourceid"];
        //            var banqsoftsourceid = item.Attributes["cap_banqsoftsourceid"];

        //            var i = new SourceMap
        //            {
        //                cap_nicesourceid = nicesourceid == null ? "": nicesourceid.ToString(),
        //                cap_tradexsourceid = tradexsourceid == null ? "" : tradexsourceid.ToString(),
        //                cap_nice2sourceid = nice2sourceid == null ? "" : nice2sourceid.ToString(),
        //                cap_banqsoftsourceid = banqsoftsourceid == null ? "" : banqsoftsourceid.ToString()

        //            };
        //        }
        //    }
        //}



        public void GetTeams(ComboBox cmbTeams, List<Team> teams)
        {
            CheckConnection();

            cmbTeams.Items.Clear();
            var query = new QueryExpression("businessunit");
            var columnNames = new[] { "name", "divisionname", "isdisabled", "parentbusinessunitid" };
            query.ColumnSet = new ColumnSet(columnNames);
            query.Criteria.AddCondition("isdisabled", ConditionOperator.Equal, false);

            _teams = _service.RetrieveMultiple(query);


            foreach (var item in _teams.Entities)
            {
                if (item.Contains("divisionname"))
                {

                
                    var dept = item.Attributes["divisionname"];
                    var val = item.Attributes["name"];
                    var i = new Team
                    {
                        OwnerBankNumber = "0501",
                        Key = dept.ToString(),
                        Value = val.ToString()
                    };
                    teams.Add(i);
                    cmbTeams.Items.Add(i.Value);
                }
            }
        }



        public void GetProducts(ComboBox cmbProducts, List<Product> products)
        {
            CheckConnection();

            cmbProducts.Items.Clear();
            var query = new QueryExpression("product");
            var columnNames = new[] { "name", "productnumber", "statecode" };
            query.ColumnSet = new ColumnSet(columnNames);
            query.Criteria.AddCondition("statecode", ConditionOperator.Equal, 0);

            _products = _service.RetrieveMultiple(query);

            foreach (var item in _products.Entities)
            {
                var i = new Product
                {
                    Key = (string)item.Attributes["productnumber"],
                    Value = (string)item.Attributes["name"],
                };

                products.Add(i);
                cmbProducts.Items.Add(i.Key + ", " + i.Value);
            }
        }


        public string QueryCrmContact(string ssn)
        {
            CheckConnection();

            EntityCollection ec = GetEntityCollection(
                "contact",
                "governmentid", 
                 ssn,
                 new ColumnSet("governmentid"));

            if (ec.Entities.Any()) { 
                var contactid = ec.Entities[0];
                var guid = contactid.Id;
                Console.WriteLine($"Guid -->{guid}");

                return guid.ToString();
                }
            return null;
        }


        /// <summary>
        /// Generic function for making calls to CRM
        /// </summary>
        /// <param name="entityName"></param>
        /// <param name="attributeName"></param>
        /// <param name="attributeValue"></param>
        /// <param name="cols"></param>
        /// <returns></returns>
        public EntityCollection GetEntityCollection(string entityName, string attributeName, string attributeValue, ColumnSet cols)
        {
            CheckConnection();

            QueryExpression query = new QueryExpression
            {
                EntityName = entityName,
                ColumnSet = cols,
                Criteria = new FilterExpression
                {
                    Conditions =
                    {
                    new ConditionExpression
                    {
                        AttributeName = attributeName,
                        Operator = ConditionOperator.Equal,
                        Values = { attributeValue }
                    }
                    }
                }
            };
            return _service.RetrieveMultiple(query);
        }
    }
}
