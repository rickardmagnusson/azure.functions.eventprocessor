﻿using Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Azure.Servicebus.Sender
{
    public static class Helpers
    {


        public static EventMessageType FromTopic(this string topic)
        {
            topic = topic.ToLower();

            switch (topic)
            {
                case "kunderegister":
                    return EventMessageType.KundeRegisterEvent;
                case "kundehendelse":
                    return EventMessageType.KundeHendelseEvent;
                case "sakshendelse":
                    return EventMessageType.KundeHendelseEvent;
                default:
                    return EventMessageType.Undefined;
            }
        }


        public static string ToTopic(this EventMessageType type)
        {

            switch (type)
            {
                case EventMessageType.KundeRegisterEvent:
                    return "KundeRegister";
                case EventMessageType.KundeHendelseEvent:
                    return "KundeHendelse";
                case EventMessageType.SakHendelseEvent:
                    return "Sakendelse";
                default:
                    return "";
            }
        }

    }
}
