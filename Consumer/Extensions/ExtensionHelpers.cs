﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Azure.Servicebus.Sender
{
    public static partial class Extensions
    {
        public static string GenNumber()
        {
            var r = new Random();
            var nmbr = r.Next(1, 11000);
            return nmbr.ToString();
        }
    }
}
