﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Azure.Servicebus.Sender
{
    /// <summary>
    /// Simple form settings
    /// </summary>
    public partial class SettingsForm : Form
    {
        string environment = "";
        public SettingsForm(string env)
        {
            environment = env;
            InitializeComponent();
        }

        private void SettingsForm_Load(object sender, EventArgs e)
        {
            var settings = new Settings();
            var list = new[] { "AppKey", "ClientId", "ResourceToCRM", "AADInstance", "TenantId" };

            int left = 40;
            int txtLeft = 200;
            int top = 50;
            foreach(var s in list)
            {
                var lbl = new Label();
                lbl.Text = s;

                var txtBox = new TextBox();
                txtBox.Name = s;
                txtBox.Text = settings.Read(environment, s);

                lbl.Top = top;
                lbl.Left = left;
                txtBox.Top = top;
                txtBox.Left = txtLeft;
                txtBox.Width = 300;

                top += 30;

                this.Controls.Add(lbl);
                this.Controls.Add(txtBox);
                txtBox.TextChanged += TxtBox_TextChanged;
            }
        }

        private void TxtBox_TextChanged(object sender, EventArgs e)
        {
            var txt = (TextBox)sender;
            var key = txt.Name;
            var val = txt.Text;
            Console.WriteLine(key + ":" + val);
            new Settings().Write(key, val, environment);
        }
    }
}
