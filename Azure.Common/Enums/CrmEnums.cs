﻿namespace Common.Enums
{
    public enum CapChannel
    {
        Bankkontor = 0,
        Mobilbank = 2,
        Web = 4,
        Nettbank = 809020000,
        Ukjent = 809020001
    }


    public enum TerritoryCode
    {
        Default = 0,
        Crm = 1,
        Kjerne = 809020000,
        BliKunde = 809020001,
        Nice = 809020002,
        Dsf = 809020003,
        Migrering = 809020004,
        Tradex = 809020005,
        BanQsoft = 809020006,
        RaagiverPluss = 809020007,

    }

    public enum OpportunityStateCode
    {
        Open = 0,
        Won = 1,
        Lost = 2
    }
    public enum OpportunityStatusCode
    {
        NotFound = 0,
        Kontakt = 1,
        Interessert = 2,
        Losningsforslag = 809020000,
        Interesse = 809020003,
        InteresseForProdukt = 809020004,
        PrisOgProdukt = 809020005,
        Tilbud = 809020006,
        FraLaanesoknad = 809020007,
        Ny = 809020008,
        UnderArbeid = 809020009,
        TilBeslutning = 809020010,
        Innstilt = 809020011,
        Besluttet = 809020012,
        UnderProduksjon = 809020013,
        TilSignering = 809020014,
        Signert = 809020015,
        KlarForDepotkontrollOgUtbetaling = 809020016,
        UnderDepotKontrollOgUtbetaling = 809020017,
        UnderDepotkontrollUtbetalt = 809020018,

        //Lost
        SlettetIFagsystem = 809020001,
        BehovOpphoert = 809020019,
        Avslatt = 809020020,

        //Won
        Won = 3,
    }

    public enum CaseTypeCode
    {
        Undefined = 0,
        EndringOgAdministrasjon = 809020002,
        Avslutning = 809020003
    }

    public enum EntityName
    {
        Undefined = 0,
        account = 1,
        contact = 2,
        cap_department = 3,
        team = 4,
        systemuser = 5,
        businessunit = 6,
        connection = 7,
        connectionrole = 8,
        activitypointer = 9,
        opportunity = 10,
        incident = 11,
        subject = 12,
        product = 13,
        uom = 14,
        uomschedule = 15,
        cap_info_activity = 16,
        incidentresolution = 17,
        opportunityproduct = 18,
        pricelevel = 19,
        productpricelevel = 20,
        opportunityclose = 21,
        cap_opportunity_contact = 22,
        cap_opportunity_account = 23,
    }


    public enum CustomerTypeCode
    {
        Undefined = 0,
        Customer = 1,
        PotentialCustomer = 2,
        Other = 3
    }

    public enum CapActivityType
    {
        Default = 0,
        Kundehendelse = 809020000,
        Systemhendelse = 809020001
    }



    public enum ActivityTypeCode
    {
        Undefined = 0,
        cap_info_activity = 10011, // 10009 in prod
        appointment = 4201,
        email = 4202,
        fax = 4204,
        incidentresolution = 4206,
        letter = 4207,
        opportunityclose = 4208,
        orderclose = 4209,
        phonecall = 4210,
        quoteclose = 4211,
        task = 4212,
        serviceappointment = 4214,
        recurringappointmentmaster = 4251,
        campaignresponse = 4401,
        campaignactivity = 4402,
        bulkoperation = 4406
    }

    public enum ActivityParticipationCode
    {
        Undefined = 0,
        ToRecipient = 2,
        Regarding = 8
    }
}
