﻿namespace Common.Enums
{
    public enum EventMessageType
    {
        Undefined = 0,
        EventStore_EventMaster = 1,
        DataVarehus_TemporaryEvent = 2,
        NiceCustomerEvent = 3,
        KundeRegisterEvent = 4,
        KundeHendelseEvent = 5,
        SakHendelseEvent = 6,
        BankKundeRegisterEvent = 7
    }
}
