﻿namespace Common.Enums
{
    public enum EventType
    {
        Undefined = 0,
        Insert = 1,
        Delta = 2,
        Delete = 3,
        Key = 4,
        AKTIV_RELASJON,             //#1 :Rulestep
        AKTIV,                      //#1
        AVVENTER_SJEKK,             //#1
        OFFENTLIGID_IKKE_I_KERNE,   //#2
        KERNEID_IKKE_GYLDIG,        //#3
        IKKE_FUNNET_AKTIV_I_KERNE,  //#4
        BANKREGNR_IKKE_EIKABANK,    //IGNORE
        FIKTIVID_IKKE_I_KERNE,      //IGNORE
    }
}
