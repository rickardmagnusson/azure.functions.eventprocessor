﻿namespace Common.Enums
{
    public enum CustomerType
    {
        Undefined = 0,
        Privat = 1,
        Landbruk = 2,
        Naering = 3
    }
}
