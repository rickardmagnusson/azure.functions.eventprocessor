﻿namespace Common.Enums
{


    public enum HendelseSource
    {
        //Undefined
        NotFound = 0,

        //Kundehendelse
        KERNE,
        NICE_SKADE,
        NICE_PERSON,
        TRADEX,
        BANQSOFT
    }


    //Fase- og sakstatus
    public enum HendelseStatusEnum
    {
        //Undefined
        NotFound = 0,

        //Kundehendelse
        Soknad,
        Interessert,
        Interesse,
        Interesse_For_Produkt,
        Interesse_Pris_Og_Produkt,
        Salg,
        Endret,
        Annet,

        //Sakhendelse
        Ny,
        Fra_lanesoknad,
        Under_arbeid,
        Til_beslutning,
        Innstilt,
        Besluttet,
        Under_produksjon,
        Til_signering,
        Signert,
        Klar_for_depotkontroll_og_utbetaling,
        Under_depotkontroll_og_utbetaling,
        Under_depotkontroll_utbetalt,

        //Lost
        Behov_Opphoert,
        Avslatt,
        Avsluttet,

        //Won
        Ferdig_behandlet,
    }


    public enum HendelseAction
    {
        //Undefined
        NotFound = 0,

        //Kundehendelse
        
        Utvidet,
        Redusert,
        Annet,
        //Ny //Shouldnt this be added?
    }

    public enum HendelseTransaction
    {
        //Undefined
        NotFound = 0,

        //Kundehendelse
        Salg,
        Spareavtale,
        Engangsinnskudd,
        Uttak,
        Annet,
    }

   

    public enum SakHendelseTypeEnum
    {
        NotFound = 0,

        Ny_Sak_Opprettet,
        Sak_Avsluttet,
        Statusendring,
        Ny_Interessent,
        Slettet_Interresent,
        Ny_Raadgiver,
        Sak_Oppdatert,
        Ukjent
    }

    public enum SakHendelseRolle
    {
        NotFound = 0,

        Hoved,
        Hovedlantaker,
        Medlantaker,
        Kausjonist,
        Annet
    }

}
