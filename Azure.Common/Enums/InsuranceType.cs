﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Common.Enums
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum InsuranceType
    {
        Undefined = 0,
        Skade = 1,
        Person = 2
    }
}
