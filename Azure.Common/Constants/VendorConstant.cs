﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Constants
{
    public class VendorConstant
    {
        public const string SkadeRoleName = "SkadeRoleId";
        public const string PersonRoleName = "PersonRoleId";

        public const string TradexRoleName = "TradexRoleId";

        public const string HasCustomerRoleName = "HasCustomerRoleId";
    }
}
