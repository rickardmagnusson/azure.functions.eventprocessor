﻿namespace Common.Constants
{
    public class SalesOpportunityConstant
    {
        public const string LostDescription = "Kunden har redusert omfanget av avtalen eller gjort endringer som ikke påvirker omfanget. Ny sak er opprettet.";
        public const string WonDescription = "Salgsmuligheten er lukket via kundehendelse.";
    }
}
