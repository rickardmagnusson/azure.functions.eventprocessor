﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Enums;
using Microsoft.Xrm.Sdk;

namespace Common.RequestBuilders
{
    public partial class RequestBuilder
    {
        public static Entity CreateOpportunityProductsEntity(Guid opportunityId, ProductInfo product, decimal priceperunit, decimal quantity = 1)
        {
            return new Entity(EntityName.opportunityproduct.ToString())
            {
                ["opportunityid"] = new EntityReference(EntityName.opportunity.ToString(), opportunityId),
                ["productid"] = new EntityReference(EntityName.product.ToString(), product.Id),
                ["uomid"] = new EntityReference(EntityName.uom.ToString(), product.PrimaryUomId),
                ["ispriceoverridden"] = true,
                ["priceperunit"] = new Money(priceperunit),
                ["baseamount"] = new Money(priceperunit * quantity),
                ["extendedamount"] = new Money(priceperunit * quantity),
                ["quantity"] = quantity
            };
        }
        public static Entity UpdateOpportunityProductsEntity(Guid opportunityProductId, decimal priceperunit, decimal quantity = 1)
        {
            Console.WriteLine($"new verdi: '{priceperunit}' :: op id: '{opportunityProductId}'");

            return new Entity(EntityName.opportunityproduct.ToString())
            {
                Id = opportunityProductId,
                ["priceperunit"] = new Money(priceperunit),
                ["baseamount"] = new Money(priceperunit * quantity),
                ["extendedamount"] = new Money(priceperunit * quantity),
                ["quantity"] = quantity
            };
        }

    }
}
