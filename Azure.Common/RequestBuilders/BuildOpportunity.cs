﻿using System;
using Azure.Logging;
using Common.Entities;
using Common.Enums;
using Microsoft.Xrm.Sdk;

namespace Common.RequestBuilders
{
    public partial class RequestBuilder
    {
        private static SingletonLogger log = SingletonLogger.Instance();

        public static Entity BuildCreateOpportunity(KundeHendelse evt, CustomerExtra cust, long estimatedValue, OptionSetValue statuscode)
        {
            //log.AppendLine($"BuildCreateOpportunity: {cust.EntityName.ToString()}, " +
            //    $"cap_subjectid: {cust.SubjectId}, " +
            //    $"cap_channel: {cust.PriceListId}, " +
            //    $"Channel: {Helper.GetChannel(evt.Channel).Value}, " +
            //    $"cap_source: {Helper.GetTerritoryCode(evt.FagSystem).Value}," +
            //    $"cap_app {evt.Applikasjon}, " +
            //    $"ownerid: {Helper.SetOwner(cust)}," +
            //    $"Value: {estimatedValue}," +
            //    $"Tid: {evt.Tidspunkt}");

            //BuildCreateOpportunity: contact, 
            //cap_subjectid: 8bad6427-df29-e911-a855-000d3a2a0765, 
            //cap_channel: ad650c4f-c27e-e811-80e5-90e2bad851b5, 
            //Channel: 2, 
            //cap_source: 809020005,
            //cap_app SPARING_ADVENT_TRADEX_WS, 
            //ownerid: ,
            //Value: 900,Tid: 2/2/2019 8:03:51 PM

            string s = "Start BuildCreateOpportunity:";

            var ent = new Entity(EntityName.opportunity.ToString())
            {
                ["customerid"] = new EntityReference(cust.EntityName.ToString(), cust.CustomerId),
                ["cap_subjectid"] = new EntityReference("subject", cust.SubjectId),
                ["pricelevelid"] = new EntityReference("pricelevel", cust.PriceListId),
                ["cap_channel"] = Helper.GetChannel(evt.Channel),
                ["cap_source"] = Helper.GetTerritoryCode(evt.FagSystem),
                ["cap_initialapp"] = evt.Applikasjon,
                ["cap_app"] = evt.Applikasjon,
                ["ownerid"] = Helper.SetOwner(cust),
                ["estimatedvalue"] = new Money(estimatedValue),
                ["cap_eventdate"] = evt.Tidspunkt
            };

            

            foreach (var v in ent.KeyAttributes)
            {
                s += v.Key + " : " + ent[v.Key] + ", ";
            }
            log.Add(s);

            UpdateOpportunityName(ent, evt, cust);
            UpdateOpportunityDescription(ent, evt);

            // TODO decide to use default statuscode or not set it like this
            if (statuscode.Value != (int)OpportunityStatusCode.NotFound) { 
                ent["statuscode"] = statuscode;
                log.Add($"Statuscode equal{statuscode}.");
            }

            return ent;
        }
        public static Entity BuildCreateOpportunityRadgiverPluss(KundeHendelse evt, CustomerExtra cust, long estimatedValue, OptionSetValue statuscode)
        {
            log.AppendLine("BuildCreateOpportunityRadgiverPluss");

            var ent = new Entity(EntityName.opportunity.ToString())
            {
                ["estimatedvalue"] = new Money(estimatedValue),
                ["cap_app"] = evt.Applikasjon,
                ["cap_initialapp"] = evt.Applikasjon,
                ["cap_eventdate"] = evt.Tidspunkt,
                ["ownerid"] = Helper.SetOwner(cust),
                ["cap_radgivermeetingstatus"] = "todo", // TODO bytt til riktig felt (nytt felt 'møtestatus')
                ["name"] = "todo", //TODO nytt målsettingsfelt
                ["cap_radgiverportfolio"] = "todo", // TOOD nytt portefølje felt
                ["cap_radgiverinvestmentadvice"] = true, // TOOD nytt avkall felt
                ["cap_radgiverresolution"] = "todo", // TOOD nytt felt områdningstid
                ["description"] = "todo kommentar", // TOOD nytt felt kommentar
                ["cap_externaleventid"] = "todo id", // TOOD nytt felt saksid

                ["customerid"] = new EntityReference(cust.EntityName.ToString(), cust.CustomerId),
                ["cap_subjectid"] = new EntityReference("subject", cust.SubjectId),
                ["pricelevelid"] = new EntityReference("pricelevel", cust.PriceListId),

                ["cap_source"] = Helper.GetTerritoryCode(evt.FagSystem),
                ["cap_channel"] = Helper.GetChannel(evt.Channel),  // TODO shall we set a default channel when its from 'source/r+'
            };

            // TODO decide to use default statuscode or not set it like this
            if (statuscode.Value != (int)OpportunityStatusCode.NotFound) ent["statuscode"] = statuscode;

            return ent;
        }
        public static Entity BuildCreateOpportunityKreditt(SakHendelse evt, CustomerExtra cust, bool customer = false)
        {
            log.AppendLine("BuildCreateOpportunityKreditt");

            var ent = new Entity(EntityName.opportunity.ToString())
            {
                //["cap_subjectid"] = new EntityReference("subject", cust.SubjectId),
                //["pricelevelid"] = new EntityReference("pricelevel", cust.PriceListId),

                ["name"] = "", // TODO find the field for the title
                //["description"] = Helper.GetDescription(evt),
                ["cap_channel"] = Helper.GetChannel(evt.Channel),
                ["cap_source"] = Helper.GetTerritoryCode(evt.FagSystem),
                ["cap_initialapp"] = evt.Applikasjon,
                ["cap_app"] = evt.Applikasjon,
                ["ownerid"] = Helper.SetOwner(cust),
                ["cap_externaleventid"] = "", // TODO 'sak_id'

                //mangler et felt for 'Formål'
            };


            //if(cust.CustomerId != Guid.Empty) 
            if (customer && cust.CustomerId != Guid.Empty) ent["customerid"] = new EntityReference(cust.EntityName.ToString(), cust.CustomerId);

            return ent;
        }


        public static Entity BuildCreateOpportunityClose(Guid opportunityId, DateTime timestamp, decimal value, string description)
        {
            log.AppendLine($"BuildCreateOpportunityClose '{EntityName.opportunityclose.ToString()}' with values: {opportunityId}, {timestamp}, {value}, {description}");
   

            var opportunityClose = new Entity(EntityName.opportunityclose.ToString())
            {
                ["opportunityid"] = new EntityReference("opportunity", opportunityId),
                ["actualend"] = timestamp,
                ["actualrevenue"] = new Money(value),
                ["description"] = description
            };

            return opportunityClose;
        }



        #region SakHendelse

        public static Entity BuildCreateOpportunityRadgiverPluss(SakHendelse evt, CustomerExtra cust, OptionSetValue statuscode)
        {
            var ent = new Entity(EntityName.opportunity.ToString())
            {
                //["estimatedvalue"] = new Money(estimatedValue),
                ["cap_app"] = evt.Applikasjon,
                ["cap_initialapp"] = evt.Applikasjon,
                ["cap_eventdate"] = evt.Tidspunkt,
                ["ownerid"] = Helper.SetOwner(cust), 
                ["cap_radgivermeetingstatus"] = evt.SakStatus,
                ["name"] = "Sparesamtale", //TODO nytt målsettingsfelt
                ["cap_radgiverportfolio"] = cust.SakHendelseAttributes.ChosenPortefolio, 
                //["cap_radgiverinvestmentadvice"] = true, 
                //["cap_radgiverresolution"] = "todo", // Not filled for now
                ["description"] = cust.SakHendelseAttributes.ResultComment, 
                ["cap_externaleventid"] = evt.SakId,

                ["customerid"] = new EntityReference(cust.EntityName.ToString(), cust.CustomerId),
                ["cap_subjectid"] = new EntityReference("subject", cust.SubjectId),
                ["pricelevelid"] = new EntityReference("pricelevel", cust.PriceListId),

                ["cap_source"] = Helper.GetTerritoryCode(evt.FagSystem), 
                ["cap_channel"] = Helper.GetChannel(evt.Channel),  // TODO shall we set a default channel when its from 'source/r+'
            };

            // TODO decide to use default statuscode or not set it like this
            if (statuscode.Value != (int)OpportunityStatusCode.NotFound) ent["statuscode"] = statuscode;

            return ent;
        }

        public static Entity BuildUpdateOpportunityRadgiverPluss(SalesOpportunity so, SakHendelse evt, CustomerExtra cust, OptionSetValue statuscode)
        {

            var ent = new Entity(EntityName.opportunity.ToString())
            {
                Id = so.Id,
                ["cap_eventdate"] = evt.Tidspunkt,
                ["cap_app"] = evt.Applikasjon,
                //["ownerid"] = Helper.SetOwner(cust), //keep as it is
                ["cap_radgivermeetingstatus"] = evt.SakStatus,
                ["cap_radgiverportfolio"] = cust.SakHendelseAttributes.ChosenPortefolio, 
                //["cap_radgiverinvestmentadvice"] = true, // TOOD nytt avkall felt
                //["cap_radgiverresolution"] = "todo", // TOOD nytt felt områdningstid
                ["description"] = cust.SakHendelseAttributes.ResultComment, 
                ["cap_externaleventid"] = evt.SakId,
                ["cap_channel"] = Helper.GetChannel(evt.Channel),  // TODO shall we set a default channel when its from 'source/r+'
            };

            if (statuscode.Value != (int)OpportunityStatusCode.NotFound) ent["statuscode"] = statuscode;

            return ent;
        }
        #endregion

        #region SakHendelse
        public static Entity BuildUpdateOpportunityForClose(SalesOpportunity so, SakHendelse evt, long estimatedValue)
        {
            var ent = new Entity(EntityName.opportunity.ToString())
            {
                Id = so.Id,
                ["estimatedvalue"] = new Money(estimatedValue),
                ["cap_eventdate"] = evt.Tidspunkt,
                ["cap_app"] = evt.Applikasjon,
            };

            return ent;
        }
        #endregion


        public static Entity BuildUpdateOpportunity(SalesOpportunity so, KundeHendelse evt, CustomerExtra cust, long estimatedValue, Guid productId, OptionSetValue statuscode)
        {
            log.AppendLine("BuildUpdateOpportunity");

            var ent = new Entity(EntityName.opportunity.ToString())
            {
                Id = so.Id,
                ["estimatedvalue"] = new Money(estimatedValue),
                ["cap_eventdate"] = evt.Tidspunkt,
                ["cap_app"] = evt.Applikasjon,
            };

            UpdateOpportunityName(ent, evt, cust);
            UpdateOpportunityDescription(ent, evt);

            if (statuscode.Value != (int)OpportunityStatusCode.NotFound) ent["statuscode"] = statuscode;
            //if (productId != Guid.Empty) ent["cap_product"] = new EntityReference("product", productId); // won't be as a single field anymore

            return ent;
        }
        public static Entity BuildUpdateOpportunityForClose(SalesOpportunity so, KundeHendelse evt, long estimatedValue, Guid productId)
        {
            log.AppendLine("BuildUpdateOpportunityForClose");

            var ent = new Entity(EntityName.opportunity.ToString())
            {
                Id = so.Id,
                ["estimatedvalue"] = new Money(estimatedValue),
                ["cap_eventdate"] = evt.Tidspunkt,
                ["cap_app"] = evt.Applikasjon,
            };

            return ent;
        }
        public static Entity BuildUpdateOpportunityRadgiverPluss(SalesOpportunity so, KundeHendelse evt, CustomerExtra cust, long estimatedValue, Guid productId, OptionSetValue statuscode)
        {
            log.AppendLine("BuildUpdateOpportunityRadgiverPluss");

            var ent = new Entity(EntityName.opportunity.ToString())
            {
                Id = so.Id,
                ["cap_eventdate"] = evt.Tidspunkt,
                ["cap_app"] = evt.Applikasjon,
                ["ownerid"] = Helper.SetOwner(cust),
                ["estimatedvalue"] = new Money(estimatedValue),
                ["cap_radgivermeetingstatus"] = "todo", // TODO bytt til riktig felt (nytt felt 'møtestatus')
                ["name"] = "todo", //TODO nytt målsettingsfelt
                ["cap_radgiverportfolio"] = "todo", // TOOD nytt portefølje felt
                ["cap_radgiverinvestmentadvice"] = true, // TOOD nytt avkall felt
                ["cap_radgiverresolution"] = "todo", // TOOD nytt felt områdningstid
                ["description"] = "todo kommentar", // TOOD nytt felt kommentar
                ["cap_externaleventid"] = "todo id", // TOOD nytt felt saksid
            };

            if (statuscode.Value != (int)OpportunityStatusCode.NotFound) ent["statuscode"] = statuscode;

            return ent;
        }
        


        public static void UpdateOpportunityName(Entity ent, KundeHendelse evt, CustomerExtra cust)
        {
            log.AppendLine("UpdateOpportunityName");
            var name = Helper.GetTitleForKundehendelse(evt.Handling, evt.TransaksjonsType, cust.Product.Name, evt.FagSystem);
            ent["name"] = name;
            log.AppendLine($"GetTitleForKundehendelse returns: {name}");
        }


        public static void UpdateOpportunityDescription(Entity ent, KundeHendelse evt)
        {
            log.AppendLine($"UpdateOpportunityDescription: {Helper.GetDescription(evt)}");

            ent["description"] = Helper.GetDescription(evt);
        }

        public static void UpdateOpportunityStatus(Entity ent, OptionSetValue status)
        {
            ent["statuscode"] = status;
        }

        public static void UpdateOpportunityForNySakOpprettet(SakHendelse evt, Entity opportunity, CustomerExtra cust)
        {
            log.AppendLine("UpdateOpportunityForNySakOpprettet");
            //opportunity["statuscode"] = status;
            opportunity["cap_app"] = evt.Applikasjon;
            opportunity["ownerid"] = Helper.SetOwner(cust);
            opportunity["cap_externaleventid"] = evt.SakId;
            //opportunity["name"] = ; // TODO
            //opportunity["formal"] // TODO
        }
        public static void UpdateOpportunityTitleFormal(SakHendelse evt, Entity opportunity)
        {
            //["name"] = ; // TODO
            //["formal"] // TODO
        }
        public static void UpdateOpportunityEventDate(SakHendelse evt, Entity opp)
        {
            opp["cap_eventdate"] = evt.Tidspunkt;
        }
        public static void UpdateOpportunityParent(Entity ent, Guid parentId)
        {
            ent["cap_opportunityid"] = new EntityReference(EntityName.opportunity.ToString(), parentId);
        }
        public static void UpdateOpportunityCustomer(CustomerExtra cust, Entity opportunity)
        {
            opportunity["customerid"] = new EntityReference(cust.EntityName.ToString(), cust.CustomerId);
        }
    }
}
