﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Entities;
using Common.Enums;
using Microsoft.Xrm.Sdk;

namespace Common.RequestBuilders
{
    public partial class RequestBuilder
    {
        #region SakHendelse
        public static Entity BuildUndefinedInfoActivity(SakHendelse evt, CustomerExtra cust)
        {
            return new Entity(EntityName.cap_info_activity.ToString())
            {
                ["cap_type"] = new OptionSetValue((int)CapActivityType.Kundehendelse),
                ["subject"] = "Udefinert kundehendelse",
                ["description"] = "Hendelse på kunde har et udefinert utfall.\n\n" +
                                  $"Produktnavn: {evt.ProduktNavn}\n" +
                                  $"Fagsystem: {evt.FagSystem}\n" +
                                  $"Prosessområde: {evt.Prosessomraade}\n" +
                                  $"Resultat: {cust.SakHendelseAttributes.Result}\n" +
                                  //$"Handling: {evt.Handling}\n" +
                                  $"Kanal: {evt.Channel}",
                                  //$"Verdi: {evt.Verdi}\n" +
                                  //$"Tidligere verdi: {evt.TidligereVerdi}",
                ["regardingobjectid"] = new EntityReference(cust.EntityName.ToString(), cust.CustomerId),
                ["ownerid"] = new EntityReference(EntityName.team.ToString(), cust.DefaultTeamId),
                ["cap_channel"] = Helper.GetChannel(evt.Channel),
                ["cap_source"] = Helper.GetTerritoryCode(evt.FagSystem),
                ["cap_app"] = evt.Applikasjon,
                ["cap_eventdate"] = evt.Tidspunkt
                //["scheduledend"] = request.DueDate.Value,
                //["createdon"] = request.CreatedDate
            };
        }
        public static Entity BuildInfoActivitySummaryOnSalesOpportunity(SakHendelse evt, CustomerExtra cust, Guid opportunityId, bool create = false)
        {
            var subject = create ? "Kundehendelse - Ny salgsmulighet" : "Kundehendelse - Oppdatering av salgsmulighet";

            return new Entity(EntityName.cap_info_activity.ToString())
            {
                ["cap_type"] = new OptionSetValue((int)CapActivityType.Kundehendelse),
                ["subject"] = subject,
                ["regardingobjectid"] = new EntityReference(EntityName.opportunity.ToString(), opportunityId),
                ["description"] = cust.SakHendelseAttributes.ResultComment,
                ["ownerid"] = new EntityReference(EntityName.team.ToString(), cust.DefaultTeamId),
                ["cap_channel"] = Helper.GetChannel(evt.Channel),
                ["cap_source"] = Helper.GetTerritoryCode(evt.FagSystem),
                ["cap_app"] = evt.Applikasjon,
                ["cap_eventdate"] = evt.Tidspunkt
            };
        }
        #endregion

        public static Entity BuildUndefinedInfoActivity(KundeHendelse evt, CustomerExtra cust)
        {
            return new Entity(EntityName.cap_info_activity.ToString())
            {
                ["cap_type"] = new OptionSetValue((int)CapActivityType.Kundehendelse),
                ["subject"] = "Udefinert kundehendelse",
                ["description"] = "Hendelse på kunde har et udefinert utfall.\n\n" +
                                  $"Produktnavn: {evt.ProduktNavn}\n" +
                                  $"Fagsystem: {evt.FagSystem}\n" +
                                  $"Prosessområde: {evt.ProsessOmraade}\n" +
                                  $"Fase: {evt.Fase}\n" +
                                  $"Handling: {evt.Handling}\n" +
                                  $"Kanal: {evt.Channel}\n" +
                                  $"Verdi: {evt.Verdi}\n" +
                                  $"Tidligere verdi: {evt.TidligereVerdi}",
                ["regardingobjectid"] = new EntityReference(cust.EntityName.ToString(), cust.CustomerId),
                ["ownerid"] = new EntityReference(EntityName.team.ToString(), cust.DefaultTeamId),
                ["cap_channel"] = Helper.GetChannel(evt.Channel),
                ["cap_source"] = Helper.GetTerritoryCode(evt.FagSystem),
                ["cap_app"] = evt.Applikasjon,
                ["cap_eventdate"] = evt.Tidspunkt
                //["scheduledend"] = request.DueDate.Value,
                //["createdon"] = request.CreatedDate
            };
        }
        public static Entity BuildInfoActivitySummaryOnSalesOpportunity(KundeHendelse evt, Guid opportunityId, Guid defaultTeamId, bool create = false)
        {
            var newEstValue = evt.Verdi - evt.TidligereVerdi;
            if (newEstValue < 0) newEstValue = 0;
            var subject = create ? "Kundehendelse - Ny salgsmulighet" : "Kundehendelse - Oppdatering av salgsmulighet";
            log.Add($"BuildInfoActivitySummaryOnSalesOpportunity: {subject}");

            return new Entity(EntityName.cap_info_activity.ToString())
            {
                ["cap_type"] = new OptionSetValue((int)CapActivityType.Kundehendelse),
                ["subject"] = subject,
                ["regardingobjectid"] = new EntityReference(EntityName.opportunity.ToString(), opportunityId),
                ["description"] = $"Ny estimert verdi: {newEstValue}\n" +
                                  $"Produkt: {evt.ProduktNavn}",
                ["ownerid"] = new EntityReference(EntityName.team.ToString(), defaultTeamId),
                ["cap_channel"] = Helper.GetChannel(evt.Channel),
                ["cap_source"] = Helper.GetTerritoryCode(evt.FagSystem),
                ["cap_app"] = evt.Applikasjon,
                ["cap_eventdate"] = evt.Tidspunkt
            };
        }
    }
}
