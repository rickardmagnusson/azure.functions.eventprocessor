﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Entities;
using Common.Enums;
using Microsoft.Xrm.Sdk;

namespace Common.RequestBuilders
{
    public partial class RequestBuilder
    {

        public static Entity BuildCreateProduct(KundeHendelse evt, CustomerExtra cust, Guid parentId, Guid unitGroupId, Guid primaryUnitId)
        {
            return new Entity(EntityName.product.ToString())
            {
                ["name"] = cust.Product.Name,
                ["productnumber"] = cust.Product.Number,
                ["productstructure"] = new OptionSetValue(1),
                ["producttypecode"] = new OptionSetValue(809020000),
                ["parentproductid"] = new EntityReference(EntityName.product.ToString(), parentId),
                ["quantitydecimal"] = 2,
                ["cap_vendorid"] = new EntityReference(EntityName.businessunit.ToString(), cust.VendorBankId), // TODO: leave empty for banks. Otherwise, select product company
                ["subjectid"] = new EntityReference(EntityName.businessunit.ToString(), cust.SubjectId),
                //["pricelevelid"] = new EntityReference(EntityName.pricelevel.ToString(), cust.PriceListId),

                ["defaultuomscheduleid"] = new EntityReference(EntityName.uomschedule.ToString(), unitGroupId),
                ["defaultuomid"] = new EntityReference(EntityName.uom.ToString(), primaryUnitId),
            };
        }
        public static Entity BuildCreateProductPriceLevel(KundeHendelse evt, CustomerExtra cust, Guid productId, Guid unitGroupId, Guid primaryUnitId)
        {
            return new Entity(EntityName.productpricelevel.ToString())
            {
                ["pricelevelid"] = new EntityReference(EntityName.pricelevel.ToString(), cust.PriceListId),
                ["productid"] = new EntityReference(EntityName.product.ToString(), productId),
                ["uomid"] = new EntityReference(EntityName.uom.ToString(), primaryUnitId),
                ["amount"] = new Money(0),
            };
        }
        public static Entity BuildUpdateProduct(Guid productId, string name)
        {
            return new Entity(EntityName.product.ToString())
            {
                Id = productId,
                ["name"] = Helper.CapitalizeFirstLetter(name)
            };
        }


    }
}
