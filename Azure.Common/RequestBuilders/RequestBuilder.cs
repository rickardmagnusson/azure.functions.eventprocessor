﻿using System;
using System.Linq;
using Common.Constants;
using Common.Entities;
using Common.Enums;
using Common.Logging;
using Microsoft.Xrm.Sdk;

namespace Common.RequestBuilders
{
    public partial class RequestBuilder
    {

        //private static readonly MsmqLogger Log = MsmqLogger.CreateLogger("request-builder");

        // TODO consider to split RequestBuilder into several classes

        #region KundeRegister V1

        public static void UpdateCustomerEntity(CustomerEvent request, CustomerExtra cust, Entity ent, MsmqLogger Log)
        {
            var isContact = cust.EntityName == EntityName.contact;

            UpdateCustomerNumber(request.CustomerNumber, ent, isContact);

            UpdateCustomerName(request, ent, isContact);
            UpdateCustomerPhones(request, ent, isContact);
            UpdateCustomerEmails(request, ent);
            UpdateCustomerAddresses(request, ent);

            UpdateCustomerSourceId(request, ent);
            UpdateCustomerOwner(cust, ent);

            UpdateCustomerDepartment(cust, ent);

            ActivateEntity(ent);

            UpdateTerritoryCode(request, ent);
            UpdateTypeToCustomer(ent);
            ent["cap_eventdate"] = request.Timestamp;

        }


        public static void UpdateCustomerAddresses(CustomerEvent request, Entity ent)
        {
            if (request.CustomerInformation.Addresses?.Count > 0)
            {
                var adr = request.CustomerInformation.Addresses.First();

                if ((request.Source == CustomerRegisterSource.NicePerson || request.Source == CustomerRegisterSource.NiceSkade) && !string.IsNullOrEmpty(adr.AddressLine1) && !string.IsNullOrEmpty(adr.AddressLine2))
                {
                    ent.SetValueReflect("address1_line1", adr.AddressLine2);
                    ent.SetValueReflect("address1_line2", adr.AddressLine1);
                }
                else
                {
                    ent.SetValueReflect("address1_line1", adr.AddressLine1);
                    ent.SetValueReflect("address1_line2", adr.AddressLine2);
                }

                ent.SetValueReflect("address1_line3", adr.AddressLine3);
                ent.SetValueReflect("address1_city", adr.City);
                ent.SetValueReflect("address1_postalcode", adr.PostalCode);
                ent.SetValueReflect("address1_stateorprovince", adr.State);
                ent.SetValueReflect("address1_country", adr.Country);
            }
            else 
            {
                ent["address1_line1"] = string.Empty;
                ent["address1_line2"] = string.Empty;
                ent["address1_line3"] = string.Empty;
                ent["address1_city"] = string.Empty;
                ent["address1_postalcode"] = string.Empty;
                ent["address1_stateorprovince"] = string.Empty;
                ent["address1_country"] = string.Empty;
            }

            // TODO use countrycode or just country?
        }
        public static void UpdateCustomerEmails(CustomerEvent request, Entity ent)
        {
            ent.SetValueReflect("emailaddress1", request.CustomerInformation.Email1);
            //ent.SetValueReflectOrDelta("emailaddress2", request.CustomerInformation.Email2); // TODO, figure out if we can set this (should be empty untill we get data here)
        }
        public static void UpdateCustomerPhones(CustomerEvent request, Entity ent, bool isContact)
        {
            if (isContact)
            {
                ent.SetValueReflect("mobilephone", request.CustomerInformation.Phone1);
                ent.SetValueReflect("telephone1", request.CustomerInformation.Phone2);
                ent.SetValueReflect("telephone2", request.CustomerInformation.Phone3);
            }
            else
            {
                ent.SetValueReflect("telephone1", request.CustomerInformation.Phone1);
                ent.SetValueReflect("telephone2", request.CustomerInformation.Phone2);
                ent.SetValueReflect("telephone3", request.CustomerInformation.Phone3);
            }
        }
        public static void UpdateCustomerNumber(string customerNumber, Entity ent, bool isContact)
        {
            if (isContact)
            {
                ent.SetValueIfPresent("governmentid", customerNumber);
                if (customerNumber.Length == 11)
                {
                    ent["gendercode"] = customerNumber.ElementAt(8) % 2 == 0
                                            ? new OptionSetValue(2)  // female
                                            : new OptionSetValue(1); // male

                    var dateOfBirth = Helper.CalculateDoB(customerNumber);
                    if (dateOfBirth != null) ent["birthdate"] = dateOfBirth;
                }
            }
            else
            {
                ent.SetValueIfPresent("accountnumber", customerNumber);
            }
        }
        public static void UpdateCustomerName(CustomerEvent request, Entity ent, bool isContact)
        {
            if (isContact)
            {
                ent.SetValueReflect("firstname", Helper.GiveContactNameCrmStandard(request.CustomerInformation.FirstName));
                ent.SetValueIfPresent("lastname", Helper.GiveContactNameCrmStandard(request.CustomerInformation.LastName));
            }
            else
            {
                ent.SetValueIfPresent("name", Helper.GiveAccountNameCrmStandard(request.CustomerInformation.FirstName, request.CustomerInformation.LastName));
            }
        }
        public static void UpdateCustomerSourceId(CustomerEvent request, Entity ent)
        {
            ent.SetValueIfPresent(Helper.GetSourceIdName(request.Source), request.CustomerSourceId);
        }
        public static void UpdateCustomerOwner(CustomerExtra cust, Entity ent)
        {
            ent["ownerid"] = Helper.SetOwner(cust);
        }
        public static void UpdateCustomerDepartment(CustomerExtra cust, Entity ent)
        {
            if (cust.DepartmentId != Guid.Empty)
            {
                ent["cap_departmentid"] = new EntityReference(EntityName.cap_department.ToString(), cust.DepartmentId);
            }
        }
        public static void UpdateTerritoryCode(CustomerEvent request, Entity ent)
        {
            var territoryCode = Helper.GetTerritoryCode(request.Source);
            if (territoryCode.Value != 0) ent["territorycode"] = territoryCode;
        }
        public static void UpdateTypeToCustomer(Entity ent)
        {
            ent["customertypecode"] = new OptionSetValue(1);
        }

        #endregion

        #region KundeRegister V2

        public static void UpdateCustomerEntity(KundeRegisterEvent request, CustomerExtra cust, Entity ent, MsmqLogger Log)
        {
            var isContact = cust.EntityName == EntityName.contact;

            UpdateCustomerNumber(request, ent, isContact);

            UpdateCustomerName(request, ent, isContact);
            UpdateCustomerPhones(request, ent, isContact);
            UpdateCustomerEmails(request, ent);
            UpdateCustomerAddresses(request, ent);

            UpdateCustomerSourceId(request, ent);
            UpdateCustomerOwner(cust, ent);

            UpdateCustomerDepartment(cust, ent);

            ActivateEntity(ent);

            UpdateTerritoryCode(request, ent);
            UpdateTypeToCustomer(ent);
            ent["cap_eventdate"] = request.CustomerEventSent;

        }


        public static void UpdateCustomerAddresses(KundeRegisterEvent request, Entity ent)
        {
            if (request.CustomerInformation.Address1 != null)
            {
                var adr = request.CustomerInformation.Address1;

                if ((request.Source == Source.NicePerson || request.Source == Source.NiceSkade) && !string.IsNullOrEmpty(adr.AddressLine1) && !string.IsNullOrEmpty(adr.AddressLine2))
                {
                    ent.SetValueReflect("address1_line1", adr.AddressLine2);
                    ent.SetValueReflect("address1_line2", adr.AddressLine1);
                }
                else
                {
                    ent.SetValueReflect("address1_line1", adr.AddressLine1);
                    ent.SetValueReflect("address1_line2", adr.AddressLine2);
                }

                ent.SetValueReflect("address1_line3", adr.AddressLine3);
                ent.SetValueReflect("address1_city", adr.City);
                ent.SetValueReflect("address1_postalcode", adr.PostalCode);
                ent.SetValueReflect("address1_stateorprovince", adr.State);
                ent.SetValueReflect("address1_country", adr.Country);
            }
            else
            {
                ent["address1_line1"] = string.Empty;
                ent["address1_line2"] = string.Empty;
                ent["address1_line3"] = string.Empty;
                ent["address1_city"] = string.Empty;
                ent["address1_postalcode"] = string.Empty;
                ent["address1_stateorprovince"] = string.Empty;
                ent["address1_country"] = string.Empty;
            }

            // TODO use countrycode or just country?
        }
        public static void UpdateCustomerEmails(KundeRegisterEvent request, Entity ent)
        {
            ent.SetValueReflect("emailaddress1", request.CustomerInformation.Email1);
            //ent.SetValueReflectOrDelta("emailaddress2", request.CustomerInformation.Email2); // TODO, figure out if we can set this (should be empty untill we get data here)
        }
        public static void UpdateCustomerPhones(KundeRegisterEvent request, Entity ent, bool isContact)
        {
            if (isContact)
            {
                ent.SetValueReflect("mobilephone", request.CustomerInformation.Phone1);
                ent.SetValueReflect("telephone1", request.CustomerInformation.Phone2);
                //ent.SetValueReflect("telephone2", request.CustomerInformation.Phone3);
            }
            else
            {
                ent.SetValueReflect("telephone1", request.CustomerInformation.Phone1);
                ent.SetValueReflect("telephone2", request.CustomerInformation.Phone2);
                //ent.SetValueReflect("telephone3", request.CustomerInformation.Phone3);
            }
        }
        public static void UpdateCustomerNumber(KundeRegisterEvent request, Entity ent, bool isContact)
        {

            if (isContact)
            {
                ent.SetValueIfPresent("governmentid", request.CustomerNumber);
                if (request.CustomerNumber.Length == 11)
                {
                    ent["gendercode"] = request.CustomerNumber.ElementAt(8) % 2 == 0
                                            ? new OptionSetValue(2)  // female
                                            : new OptionSetValue(1); // male

                    var dateOfBirth = Helper.CalculateDoB(request.CustomerNumber);
                    if (dateOfBirth != null) ent["birthdate"] = dateOfBirth;
                }
            }
            else
            {
                ent.SetValueIfPresent("accountnumber", request.CustomerNumber);
            }
        }
        public static void UpdateCustomerName(KundeRegisterEvent request, Entity ent, bool isContact)
        {
            if (isContact)
            {
                ent.SetValueReflect("firstname", Helper.GiveContactNameCrmStandard(request.CustomerInformation.FirstName));
                ent.SetValueIfPresent("lastname", Helper.GiveContactNameCrmStandard(request.CustomerInformation.LastName));
            }
            else
            {
                ent.SetValueIfPresent("name", Helper.GiveAccountNameCrmStandard(request.CustomerInformation.FirstName, request.CustomerInformation.LastName));
            }
        }
        public static void UpdateCustomerSourceId(KundeRegisterEvent request, Entity ent)
        {
            ent.SetValueIfPresent(Helper.GetSourceIdName(request.Source), request.CustomerSourceId);
        }
        public static void RemoveCustomerSourceId(KundeRegisterEvent request, Entity ent)
        {
            ent.SetValueReflect(Helper.GetSourceIdName(request.Source), string.Empty);
        }
        public static void RemoveCustomerSourceId(Source source, Entity ent)
        {
            ent.SetValueReflect(Helper.GetSourceIdName(source), string.Empty);
        }
        public static void UpdateTerritoryCode(KundeRegisterEvent request, Entity ent)
        {
            var territoryCode = Helper.GetTerritoryCode(request.Source);
            if (territoryCode.Value != 0) ent["territorycode"] = territoryCode;
        }

        #endregion










        public static Entity BuildUpdateCustomerSourceId(EntityName entityName, Guid id, bool isSkadeInsurance, string sourceId)
        {
            var field = isSkadeInsurance ? "cap_nicesourceid" : "cap_nice2sourceid";

            return new Entity(entityName.ToString())
            {
                Id = id,
                [field] = sourceId
            };
        }
        public static Entity BuildRemoveCustomerSourceId(EntityName entityName, Guid id, bool isSkadeInsurance)
        {
            var field = isSkadeInsurance ? "cap_nicesourceid" : "cap_nice2sourceid";

            return new Entity(entityName.ToString())
            {
                Id = id,
                [field] = string.Empty
            };
        }
        public static Entity BuildDeactivateEntity(EntityName entityName, Guid id)
        {
            return new Entity(entityName.ToString())
            {
                Id = id,
                ["statecode"] = new OptionSetValue(1),
                ["statuscode"] = new OptionSetValue(2),
            };
        }
        public static Entity BuildActivateEntity(string entityName, Guid id)
        {
            return new Entity(entityName)
            {
                Id = id,
                ["statecode"] = new OptionSetValue(0),
                ["statuscode"] = new OptionSetValue(1),
            };
        }


        public static void UpdateCustomerOwnerWithLoggedIn(CustomerExtra cust, Entity ent)
        {
            ent["ownerid"] = Helper.SetOwnerWithLoggedInUser(cust);
        }

        public static void UpdateTerritoryCode(string source, Entity ent)
        {
            var territoryCode = Helper.GetTerritoryCode(source);
            if (territoryCode.Value != 0) ent["territorycode"] = territoryCode;
        }
        public static void SetCustomerAsPotentialCustomer(Entity ent)
        {
            ent["customertypecode"] = new OptionSetValue(2);
        }
        public static void UpdateCustomerNameForPotentialKundeHendelse(ProcessArea processArea ,Entity ent, bool isContact)
        {
            var first = "Automatisk Generert";
            var last = "Kundehendelse";
            //var last = $"Kundehendelse ({processArea})";

            if (isContact)
            {
                ent.SetValueReflect("firstname", first);
                ent.SetValueIfPresent("lastname", last);
            }
            else
            {
                ent.SetValueIfPresent("name", $"{first} {last}");
            }
        }
        public static void UpdateCustomerSourceId(string source, string sourceId, Entity ent)
        {
            ent.SetValueIfPresent(Helper.GetSourceIdName(source), sourceId);
        }
        public static void UpdateCustomerSourceId(Source source, string sourceId, Entity ent)
        {
            ent.SetValueIfPresent(Helper.GetSourceIdName(source), sourceId);
        }


        public static void ActivateEntity(Entity ent)
        {
            ent["statecode"] = new OptionSetValue(0);
            ent["statuscode"] = new OptionSetValue(1);
        }
        public static void DeactivateEntity(Entity ent)
        {
            ent["statecode"] = new OptionSetValue(1);
            ent["statuscode"] = new OptionSetValue(2);
        }
        //public static void SetOwnerForCustomer(Entity ent, Guid ownerId, bool isUser)
        //{
        //    if (ownerId == Guid.Empty) return;

        //    ent["ownerid"] = isUser 
        //                    ? new EntityReference(CrmConstants.SystemUserEntityName, ownerId) 
        //                    : new EntityReference(CrmConstants.TeamEntityName, ownerId);
        //}


        public static Entity BuildPotentialCustomerForKundeHendelse(KundeHendelse evt, CustomerExtra cust)
        {
            var isContact = cust.EntityName == EntityName.contact;
            var ent = new Entity(cust.EntityName.ToString());

            UpdateCustomerNameForPotentialKundeHendelse(evt.ProsessOmraade, ent, isContact);
            UpdateCustomerNumber(evt.KundeId, ent, isContact);
            UpdateCustomerOwner(cust, ent);

            UpdateTerritoryCode(evt.FagSystem, ent);
            SetCustomerAsPotentialCustomer(ent);
            UpdateCustomerSourceId(evt.FagSystem, evt.FagsystemKundenummer, ent);

            ent["cap_eventdate"] = evt.Tidspunkt;

            return ent;
        }


        public static Entity BuildPotentialCustomerForKundeHendelse(SakHendelse evt, CustomerExtra cust)
        {
            var isContact = cust.EntityName == EntityName.contact;
            var ent = new Entity(cust.EntityName.ToString());

            UpdateCustomerNameForPotentialKundeHendelse(evt.Prosessomraade, ent, isContact);
            UpdateCustomerNumber(evt.Interessent.OffentligId, ent, isContact);
            UpdateCustomerOwner(cust, ent);

            UpdateTerritoryCode(evt.FagSystem, ent);
            SetCustomerAsPotentialCustomer(ent);
            UpdateCustomerSourceId(evt.FagSystem, evt.FagsystemKundenummer, ent);

            ent["cap_eventdate"] = evt.Tidspunkt;

            return ent;
        }

    }
}
