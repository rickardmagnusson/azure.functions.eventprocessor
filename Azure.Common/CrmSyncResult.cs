﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Enums;

namespace Common
{
    public class CrmSyncResult
    {
        public Guid RequestId { get; set; }
        public MessageInboxStatus ResultStatus { get; set; }
        public string ErrorMessage { get; set; }
    }
}
