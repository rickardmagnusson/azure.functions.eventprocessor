﻿using System;

namespace Common
{
  
    public class JsonConvertAttribute : Attribute
    {
        public string In{ get; set; }
    }
}
