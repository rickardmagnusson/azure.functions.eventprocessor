﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Text;

using Common.Logging;

using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Tooling.Connector;

namespace Common.Adapters
{


    /// <summary>
    /// This is needed to be rewritten totally(Medium effort)
    /// </summary>

    public class ManagerHelper
    {
        public static string GetExceptionMessage(Exception ex)
        {
            var strBuilder = new StringBuilder(ex.Message);

            if (ex.InnerException != null)
            {
                strBuilder.Append("Inner exception: ");
                strBuilder.Append(ex.InnerException.Message);
            }

            return strBuilder.ToString();
        }

        [Obsolete("This using an old version of IOrganizationService")]
        public static OrganizationServiceProxy GetCrmOrganizationServiceProxy(Uri crmOrganizationUri)
        {
            Uri homeRealmUri = null;
            var credentials = new ClientCredentials();

            credentials.Windows.ClientCredential = System.Net.CredentialCache.DefaultNetworkCredentials;

            var orgServiceProxy = new OrganizationServiceProxy(crmOrganizationUri, homeRealmUri, null, null);

            return orgServiceProxy;
        }
    }
}
