﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Text;
using Common.Constants;
using Common.Entities;
using Microsoft.Xrm.Sdk;
using Common.Logging;
using Common.Enums;

namespace Common.Adapters
{
    public class CrmDbAdapter
    {
        internal string CrmDbConnString;
        private static readonly string logPrefix = "Eika::CRM::DataLoad::CrmDBAdapter::";
        private MsmqLogger Log;

        public CrmDbAdapter(string sqlConnstring, MsmqLogger log)
        {
            CrmDbConnString = sqlConnstring;
            Log = log;
        }
        public Guid GetBusinessUnitByBankNo(string bankNo)
        {
            if (string.IsNullOrEmpty(bankNo))
                return Guid.Empty;

            const string commandText = "SELECT BusinessUnitId FROM dbo.Businessunit WHERE divisionname = @bankNo";

            using (var connection = new SqlConnection(CrmDbConnString))
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandType = System.Data.CommandType.Text;
                    command.CommandText = commandText;
                    command.Parameters.Add(new SqlParameter("@bankNo", bankNo));

                    command.Connection.Open();
                    var temp = command.ExecuteScalar();
                    command.Connection.Close();
                    if (temp != null)
                    {
                        var businessUnitId = temp.ToString();
                        Log.Debug(string.Format(CultureInfo.InvariantCulture, "{0}::GetBusinessUnitByBankNo '{1}' is found for bankNo '{2}'", logPrefix, businessUnitId, bankNo));
                        return new Guid(businessUnitId);
                    }
                    Log.Warn(string.Format(CultureInfo.InvariantCulture, "{0}::GetBusinessUnitByBankNo failed to find guid for bankNo '{1}'", logPrefix, bankNo));
                    return Guid.Empty;
                }
            }
        }
        
        public Guid GetDefaultTeamIdByBankNo(string bankNo)
        {
            var bankId = GetBusinessUnitByBankNo(bankNo);
            return GetDefaultTeamIdByBusinessUnitId(bankId);
        }

        public Guid GetDefaultTeamIdByBusinessUnitId(Guid businessUnitId)
        {
            if (businessUnitId == Guid.Empty)
                return Guid.Empty;

            const string commandText = "SELECT TeamId FROM dbo.Team WHERE BusinessUnitId = @BusinessUnitId and IsDefault=1";

            using (var connection = new SqlConnection(CrmDbConnString))
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandType = System.Data.CommandType.Text;
                    command.CommandText = commandText;
                    command.Parameters.Add(new SqlParameter("@BusinessUnitId", businessUnitId.ToString()));

                    command.Connection.Open();
                    var temp = command.ExecuteScalar();
                    command.Connection.Close();
                    if (temp != null)
                    {
                        var defaultTeamId = temp.ToString();
                        Log.Debug(string.Format(CultureInfo.InvariantCulture, "{0}::GetDefaultTeamIdByBusinessUnitId '{1}' is found for BusinessUnitId '{2}'", logPrefix, defaultTeamId, businessUnitId));
                        return new Guid(defaultTeamId);
                    }
                    Log.Warn(string.Format(CultureInfo.InvariantCulture, "{0}::GetDefaultTeamIdByBusinessUnitId failed to find team's guid for BusinessUnitId '{1}'", logPrefix, businessUnitId));
                    return Guid.Empty;
                }
            }
        }

        public List<DataWarehouseLite> GetCrmCustomerByCustomerNumber(string customerNumber, Guid bankGuid, bool isContact)
        {
            var result = new List<DataWarehouseLite>();
            var sb = new StringBuilder();

            #region Query

            sb.Append(isContact 
                    ? "select contactid id, governmentid customernumber, firstname, lastname, mobilephone"
                    : "select accountid id, accountnumber customernumber, name, telephone3");
            sb.Append(@", cap_eventid
                        , cap_sourceid
                        , dbo.fn_UTCToTzSpecificLocalTime(cap_eventdate,-60,-60,0,3,5,2,0,0,0,0,0,0,10,5,3,0,0,0,0) cap_eventdate
                        , telephone1
                        , telephone2
                        , fax
                        , emailaddress1
                        , address1_addresstypecode
                        , address1_line1
                        , address1_line2
                        , address1_line3
                        , address1_postalcode
                        , address1_city
                        , address1_country
                        , statecode
                        , customertypecode
                        , ownerid ");
            sb.Append(isContact 
                    ? "from contact where OwningBusinessUnit = @OwnerBU and governmentid = @CustomerNumber "
                    : "from account where OwningBusinessUnit = @OwnerBU and accountnumber = @CustomerNumber ");
            sb.Append("order by statecode asc ");

            #endregion

            using (var connection = new SqlConnection(CrmDbConnString))
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandType = System.Data.CommandType.Text;
                    command.CommandText = sb.ToString();
                    command.Parameters.Add(new SqlParameter("@CustomerNumber", customerNumber));
                    command.Parameters.Add(new SqlParameter("@OwnerBU", bankGuid.ToString()));

                    command.Connection.Open();
                    using (var dataReader = command.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            var record = new DataWarehouseLite();
                            #region Mapping

                            record.Entity = isContact ? EntityName.contact : EntityName.account;
                            record.Id = (Guid)dataReader["id"];
                            record.CustomerNumber = dataReader["customernumber"] != DBNull.Value ? dataReader["customernumber"].ToString() : string.Empty;
                            record.CapEventdate = dataReader["cap_eventdate"] != DBNull.Value ? (DateTime)dataReader["cap_eventdate"] : DateTime.MinValue;
                            if (isContact)
                            {
                                record.FirstName = dataReader["firstname"] != DBNull.Value ? dataReader["firstname"].ToString() : string.Empty;
                                record.LastName = dataReader["lastname"] != DBNull.Value ? dataReader["lastname"].ToString() : string.Empty;
                                record.TelephoneHome = dataReader["telephone1"] != DBNull.Value ? dataReader["telephone1"].ToString() : string.Empty;
                                record.TelephoneWork = dataReader["telephone2"] != DBNull.Value ? dataReader["telephone2"].ToString() : string.Empty;
                                record.TelephoneMobile = dataReader["mobilephone"] != DBNull.Value ? dataReader["mobilephone"].ToString() : string.Empty;
                                record.EmailPrivate = dataReader["emailaddress1"] != DBNull.Value ? dataReader["emailaddress1"].ToString() : string.Empty;
                            }
                            else
                            {
                                record.CompanyName = dataReader["name"] != DBNull.Value ? dataReader["name"].ToString() : string.Empty;
                                record.TelephoneWork = dataReader["telephone1"] != DBNull.Value ? dataReader["telephone1"].ToString() : string.Empty;
                                record.TelephoneHome = dataReader["telephone2"] != DBNull.Value ? dataReader["telephone2"].ToString() : string.Empty;
                                record.TelephoneMobile = dataReader["telephone3"] != DBNull.Value ? dataReader["telephone3"].ToString() : string.Empty;
                                record.EmailWork = dataReader["emailaddress1"] != DBNull.Value ? dataReader["emailaddress1"].ToString() : string.Empty;
                            }
                            
                            var adr1 = dataReader["address1_line1"] != DBNull.Value ? dataReader["address1_line1"].ToString() : string.Empty;
                            var adr2 = dataReader["address1_line2"] != DBNull.Value ? dataReader["address1_line2"].ToString() : string.Empty;
                            var adr3 = dataReader["address1_line3"] != DBNull.Value ? dataReader["address1_line3"].ToString() : string.Empty;
                            record.StreetName = $"{adr1}{adr2}{adr3}";

                            record.PostalCode = dataReader["address1_postalcode"] != DBNull.Value ? dataReader["address1_postalcode"].ToString() : string.Empty;
                            record.City = dataReader["address1_city"] != DBNull.Value ? dataReader["address1_city"].ToString() : string.Empty;
                            record.AddressCountryCode = dataReader["address1_country"] != DBNull.Value ? dataReader["address1_country"].ToString() : string.Empty;
                            record.StateCode = dataReader["statecode"] != DBNull.Value ? (int)dataReader["statecode"] : -1;
                            var sourceIdString = dataReader["cap_sourceid"] != DBNull.Value ? dataReader["cap_sourceid"].ToString() : "-1";
                            long sourceId = 0;
                            long.TryParse(sourceIdString, out sourceId);
                            record.SourceId = sourceId;
                            record.Type = dataReader["customertypecode"] != DBNull.Value ? (CustomerTypeCode)dataReader["customertypecode"] : CustomerTypeCode.Undefined;
                            record.OwnerId = new Guid(dataReader["ownerid"].ToString());

                            #endregion
                            result.Add(record);
                        }
                    }
                }
            }

            return result;
        }
        public List<DataWarehouseLite> GetCrmCustomerBySourceId(long sourceId, Guid bankGuid, bool isContact)
        {
            var result = new List<DataWarehouseLite>();
            if (sourceId == 0) return result;
            var sb = new StringBuilder();

            #region Query

            sb.Append(isContact
                    ? "select contactid id, governmentid customernumber, firstname, lastname, mobilephone"
                    : "select accountid id, accountnumber customernumber, name, telephone3");
            sb.Append(@", cap_eventid
                        , dbo.fn_UTCToTzSpecificLocalTime(cap_eventdate,-60,-60,0,3,5,2,0,0,0,0,0,0,10,5,3,0,0,0,0) cap_eventdate
                        , telephone1
                        , telephone2
                        , fax
                        , emailaddress1
                        , address1_addresstypecode
                        , address1_line1
                        , address1_line2
                        , address1_line3
                        , address1_postalcode
                        , address1_city
                        , address1_country
                        , statecode
                        , ownerid ");
            sb.Append(isContact
                    ? "from contact where OwningBusinessUnit = @OwnerBU and cap_sourceid = @SourceId "
                    : "from account where OwningBusinessUnit = @OwnerBU and cap_sourceid = @SourceId ");
            sb.Append("order by statecode asc ");

            #endregion

            using (var connection = new SqlConnection(CrmDbConnString))
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandType = System.Data.CommandType.Text;
                    command.CommandText = sb.ToString();
                    command.Parameters.Add(new SqlParameter("@SourceId", sourceId.ToString()));
                    command.Parameters.Add(new SqlParameter("@OwnerBU", bankGuid.ToString()));

                    command.Connection.Open();
                    using (var dataReader = command.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            var record = new DataWarehouseLite();
                            #region Mapping

                            record.Entity = isContact ? EntityName.contact : EntityName.account;
                            record.Id = (Guid)dataReader["id"];
                            record.CustomerNumber = dataReader["customernumber"] != DBNull.Value ? dataReader["customernumber"].ToString() : string.Empty;
                            record.CapEventdate = dataReader["cap_eventdate"] != DBNull.Value ? (DateTime)dataReader["cap_eventdate"] : DateTime.MinValue;
                            if (isContact)
                            {
                                record.FirstName = dataReader["firstname"] != DBNull.Value ? dataReader["firstname"].ToString() : string.Empty;
                                record.LastName = dataReader["lastname"] != DBNull.Value ? dataReader["lastname"].ToString() : string.Empty;
                                record.TelephoneHome = dataReader["telephone1"] != DBNull.Value ? dataReader["telephone1"].ToString() : string.Empty;
                                record.TelephoneWork = dataReader["telephone2"] != DBNull.Value ? dataReader["telephone2"].ToString() : string.Empty;
                                record.TelephoneMobile = dataReader["mobilephone"] != DBNull.Value ? dataReader["mobilephone"].ToString() : string.Empty;
                                record.EmailPrivate = dataReader["emailaddress1"] != DBNull.Value ? dataReader["emailaddress1"].ToString() : string.Empty;
                            }
                            else
                            {
                                record.CompanyName = dataReader["name"] != DBNull.Value ? dataReader["name"].ToString() : string.Empty;
                                record.TelephoneWork = dataReader["telephone1"] != DBNull.Value ? dataReader["telephone1"].ToString() : string.Empty;
                                record.TelephoneHome = dataReader["telephone2"] != DBNull.Value ? dataReader["telephone2"].ToString() : string.Empty;
                                record.TelephoneMobile = dataReader["telephone3"] != DBNull.Value ? dataReader["telephone3"].ToString() : string.Empty;
                                record.EmailWork = dataReader["emailaddress1"] != DBNull.Value ? dataReader["emailaddress1"].ToString() : string.Empty;
                            }

                            var adr1 = dataReader["address1_line1"] != DBNull.Value ? dataReader["address1_line1"].ToString() : string.Empty;
                            var adr2 = dataReader["address1_line2"] != DBNull.Value ? dataReader["address1_line2"].ToString() : string.Empty;
                            var adr3 = dataReader["address1_line3"] != DBNull.Value ? dataReader["address1_line3"].ToString() : string.Empty;
                            record.StreetName = $"{adr1}{adr2}{adr3}";

                            record.PostalCode = dataReader["address1_postalcode"] != DBNull.Value ? dataReader["address1_postalcode"].ToString() : string.Empty;
                            record.City = dataReader["address1_city"] != DBNull.Value ? dataReader["address1_city"].ToString() : string.Empty;
                            record.AddressCountryCode = dataReader["address1_country"] != DBNull.Value ? dataReader["address1_country"].ToString() : string.Empty;
                            record.StateCode = dataReader["statecode"] != DBNull.Value ? (int)dataReader["statecode"] : -1;
                            record.SourceId = sourceId;
                            record.OwnerId = new Guid(dataReader["ownerid"].ToString());

                            #endregion
                            result.Add(record);
                        }
                    }
                }
            }

            return result;
        }

        public List<CustomerLite> GetCrmContacts(string[] customerNos, string bankNo)
        {
            var result = new List<CustomerLite>();
            var businessUnitId = GetBusinessUnitByBankNo(bankNo);
            using (var connection = new SqlConnection(CrmDbConnString))
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandType = System.Data.CommandType.Text;
                    command.CommandText = @"select contactid
                                                , cap_eventid
                                                , governmentid
                                                , dbo.fn_UTCToTzSpecificLocalTime(cap_eventdate,-60,-60,0,3,5,2,0,0,0,0,0,0,10,5,3,0,0,0,0) cap_eventdate
                                                , firstname
                                                , lastname
                                                , telephone1
                                                , telephone2
                                                , mobilephone
                                                , fax
                                                , emailaddress1
                                                , address1_addresstypecode
                                                , address1_line1
                                                , address1_line2
                                                , address1_line3
                                                , address1_postalcode
                                                , address1_city
                                                , address1_country
                                                , statecode
                                                , ownerid
                                                , owneridtype
                                                , owneridname
                                            from contact
                                            where OwningBusinessUnit = '{1}' and governmentid in('{0}')
                                            order by statecode asc";
                    command.CommandText = string.Format(command.CommandText, string.Join("', '", customerNos), businessUnitId);
                    command.Parameters.AddWithValue("@ownerBU", businessUnitId.ToString());

                    command.Connection.Open();
                    using (var dataReader = command.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            var currRecord = new CustomerLite();
                            currRecord.entity = EntityName.contact;
                            currRecord.customerGuid = (Guid)dataReader["contactid"];
                            currRecord.cap_eventid = dataReader["cap_eventid"] != DBNull.Value ? dataReader["cap_eventid"].ToString() : string.Empty;
                            currRecord.governmentid = dataReader["governmentid"] != DBNull.Value ? dataReader["governmentid"].ToString() : string.Empty;
                            currRecord.cap_eventdate = dataReader["cap_eventdate"] != DBNull.Value ? (DateTime)dataReader["cap_eventdate"] : DateTime.MinValue;
                            currRecord.firstname = dataReader["firstname"] != DBNull.Value ? dataReader["firstname"].ToString() : string.Empty;
                            currRecord.lastname = dataReader["lastname"] != DBNull.Value ? dataReader["lastname"].ToString() : string.Empty;
                            currRecord.homephone = dataReader["telephone1"] != DBNull.Value ? dataReader["telephone1"].ToString() : string.Empty;
                            currRecord.workphone = dataReader["telephone2"] != DBNull.Value ? dataReader["telephone2"].ToString() : string.Empty;
                            currRecord.mobilephone = dataReader["mobilephone"] != DBNull.Value ? dataReader["mobilephone"].ToString() : string.Empty;
                            currRecord.fax = dataReader["fax"] != DBNull.Value ? dataReader["fax"].ToString() : string.Empty;
                            currRecord.emailaddress1 = dataReader["emailaddress1"] != DBNull.Value ? dataReader["emailaddress1"].ToString() : string.Empty;
                            currRecord.address1_line1 = dataReader["address1_line1"] != DBNull.Value ? dataReader["address1_line1"].ToString() : string.Empty;
                            currRecord.address1_line2 = dataReader["address1_line2"] != DBNull.Value ? dataReader["address1_line2"].ToString() : string.Empty;
                            currRecord.address1_line3 = dataReader["address1_line3"] != DBNull.Value ? dataReader["address1_line3"].ToString() : string.Empty;
                            currRecord.address1_postalcode = dataReader["address1_postalcode"] != DBNull.Value ? dataReader["address1_postalcode"].ToString() : string.Empty;
                            currRecord.address1_city = dataReader["address1_city"] != DBNull.Value ? dataReader["address1_city"].ToString() : string.Empty;
                            currRecord.address1_country = dataReader["address1_country"] != DBNull.Value ? dataReader["address1_country"].ToString() : string.Empty;
                            currRecord.statecode = dataReader["statecode"] != DBNull.Value ? (int)dataReader["statecode"] : 0;
                            var ownerid = new EntityReference();
                            ownerid.Id = new Guid(dataReader["ownerid"].ToString());
                            ownerid.LogicalName = (int)dataReader["owneridtype"] == CrmConstants.TeamEntityTypeCode ? CrmConstants.TeamEntityName : CrmConstants.SystemUserEntityName;
                            ownerid.Name = dataReader["owneridname"].ToString();
                            currRecord.ownerid = ownerid;
                            result.Add(currRecord);
                        }
                    }
                }
            }
            if (result.Count > 0)
                Log.Debug(string.Format(CultureInfo.InvariantCulture, "{0}::GetCrmContacts Succeeded for Bank '{1}' and found {2} matching contacts out of {3}", logPrefix, bankNo, result.Count, customerNos.Length));
            else
                Log.Debug(string.Format(CultureInfo.InvariantCulture, "{0}::GetCrmContacts failed for Bank '{1}' and custList of {2} customers and no match found", logPrefix, bankNo, customerNos.Length));

            return result;
        }

        public List<CustomerLite> GetCrmAccounts(string[] customerNos, string bankNo)
        {
            var result = new List<CustomerLite>();
            var businessUnitId = GetBusinessUnitByBankNo(bankNo);
            using (var connection = new SqlConnection(CrmDbConnString))
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandType = System.Data.CommandType.Text;
                    command.CommandText = @"select accountid
                                            , cap_eventid
                                            , accountnumber
                                            , dbo.fn_UTCToTzSpecificLocalTime(cap_eventdate,-60,-60,0,3,5,2,0,0,0,0,0,0,10,5,3,0,0,0,0) cap_eventdate
                                            , Name
                                            , telephone1
                                            , telephone2
                                            , Telephone3 MobilePhone
                                            , fax
                                            , emailaddress1
                                            , address1_addresstypecode
                                            , address1_line1
                                            , address1_line2
                                            , address1_line3
                                            , address1_postalcode
                                            , address1_city
                                            , address1_country
                                            , statecode
                                            , ownerid
                                            , owneridtype
                                            , owneridname
                                            from Account
                                            where OwningBusinessUnit = '{1}' and accountnumber in('{0}')
                                            order by statecode asc";
                    command.CommandText = string.Format(command.CommandText, string.Join("', '", customerNos), businessUnitId);
                    command.Parameters.AddWithValue("@ownerBU", businessUnitId.ToString());

                    command.Connection.Open();
                    using (var dataReader = command.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            var currRecord = new CustomerLite();
                            currRecord.entity = EntityName.account;
                            currRecord.customerGuid = (Guid)dataReader["accountid"];
                            currRecord.cap_eventid = dataReader["cap_eventid"] != DBNull.Value ? dataReader["cap_eventid"].ToString() : string.Empty;
                            currRecord.governmentid = dataReader["accountnumber"] != DBNull.Value ? dataReader["accountnumber"].ToString() : string.Empty;
                            currRecord.cap_eventdate = dataReader["cap_eventdate"] != DBNull.Value ? (DateTime)dataReader["cap_eventdate"] : DateTime.MinValue;
                            currRecord.lastname = dataReader["name"] != DBNull.Value ? dataReader["name"].ToString() : string.Empty;
                            currRecord.workphone = dataReader["telephone1"] != DBNull.Value ? dataReader["telephone1"].ToString() : string.Empty;
                            currRecord.homephone = dataReader["telephone2"] != DBNull.Value ? dataReader["telephone2"].ToString() : string.Empty;
                            currRecord.mobilephone = dataReader["mobilephone"] != DBNull.Value ? dataReader["mobilephone"].ToString() : string.Empty;
                            currRecord.fax = dataReader["fax"] != DBNull.Value ? dataReader["fax"].ToString() : string.Empty;
                            currRecord.emailaddress1 = dataReader["emailaddress1"] != DBNull.Value ? dataReader["emailaddress1"].ToString() : string.Empty;
                            currRecord.address1_line1 = dataReader["address1_line1"] != DBNull.Value ? dataReader["address1_line1"].ToString() : string.Empty;
                            currRecord.address1_line2 = dataReader["address1_line2"] != DBNull.Value ? dataReader["address1_line2"].ToString() : string.Empty;
                            currRecord.address1_line3 = dataReader["address1_line3"] != DBNull.Value ? dataReader["address1_line3"].ToString() : string.Empty;
                            currRecord.address1_postalcode = dataReader["address1_postalcode"] != DBNull.Value ? dataReader["address1_postalcode"].ToString() : string.Empty;
                            currRecord.address1_city = dataReader["address1_city"] != DBNull.Value ? dataReader["address1_city"].ToString() : string.Empty;
                            currRecord.address1_country = dataReader["address1_country"] != DBNull.Value ? dataReader["address1_country"].ToString() : string.Empty;
                            currRecord.statecode = dataReader["statecode"] != DBNull.Value ? (int)dataReader["statecode"] : 0;
                            var ownerid = new EntityReference();
                            ownerid.Id = new Guid(dataReader["ownerid"].ToString());
                            ownerid.LogicalName = (int)dataReader["owneridtype"] == CrmConstants.TeamEntityTypeCode ? CrmConstants.TeamEntityName : CrmConstants.SystemUserEntityName;
                            ownerid.Name = dataReader["owneridname"].ToString();
                            currRecord.ownerid = ownerid;
                            result.Add(currRecord);
                        }
                    }
                }
            }
            if (result.Count > 0)
                Log.Debug(string.Format(CultureInfo.InvariantCulture, "{0}::GetCrmAccounts Succeeded for Bank '{1}' and found {2} matching accounts out of {3}", logPrefix, bankNo, result.Count, customerNos.Length));
            else
                Log.Debug(string.Format(CultureInfo.InvariantCulture, "{0}::GetCrmAccounts failed for Bank '{1}' and custList of {2} customers and no match found", logPrefix, bankNo, customerNos.Length));

            return result;
        }

        public Guid GetAccountOrContactIdByBank(string customerNumber, string bankGuid, string entityName = "not_set")
        {
            if (entityName.Equals("not_set")) entityName = customerNumber.Length == 11 ? "contact" : "account";

            var commandText = entityName.ToLower().Equals("contact")
                ? "SELECT contactid FROM Contact WHERE OwningBusinessUnit = @BankGuid AND governmentid = @CustomerNumber"
                : "SELECT accountid FROM Account WHERE OwningBusinessUnit = @BankGuid AND accountnumber = @CustomerNumber";


            using (var connection = new SqlConnection(CrmDbConnString))
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandType = System.Data.CommandType.Text;
                    command.CommandText = commandText;
                    command.Parameters.Add(new SqlParameter("@BankGuid", bankGuid));
                    command.Parameters.Add(new SqlParameter("@CustomerNumber", customerNumber));

                    command.Connection.Open();
                    var temp = command.ExecuteScalar();
                    command.Connection.Close();
                    if (temp != null)
                    {
                        var customerid = temp.ToString();
                        Log.Debug(string.Format(CultureInfo.InvariantCulture, "{0}::GetAccountOrContactId '{1}' is found for customer '{2}' with bank '{3}'", logPrefix, customerid, customerNumber, bankGuid));
                        return new Guid(customerid);
                    }
                    Log.Debug(string.Format(CultureInfo.InvariantCulture, "{0}::GetAccountOrContactId failed to find guid for customer '{1}' with bank '{2}'", logPrefix, customerNumber, bankGuid));
                    return Guid.Empty;
                }
            }
        }
        
        public bool HasDepartmentByGuid(Guid id, EntityName entity)
        {
            var commandText = entity == EntityName.contact
                ? "SELECT cap_departmentid FROM Contact WHERE contactid = @Id"
                : "SELECT cap_departmentid FROM Account WHERE accountid = @Id";

            using (var connection = new SqlConnection(CrmDbConnString))
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandType = System.Data.CommandType.Text;
                    command.CommandText = commandText;
                    command.Parameters.Add(new SqlParameter("@Id", id));

                    command.Connection.Open();
                    var temp = command.ExecuteScalar();
                    command.Connection.Close();
                    var dep = temp?.ToString();
                    return !string.IsNullOrEmpty(dep);
                }
            }
        }
        
        public bool IsActiveByGuid(string entityName, Guid customerGuid)
        {
            if (customerGuid == Guid.Empty)
                throw new Exception("The customerGuid provided should not be empty at this point.");
            
            var commandText = entityName.Equals(EntityName.contact.ToString())
                ? "SELECT statecode FROM Contact WHERE contactid = @CustomerGuid"
                : "SELECT statecode FROM Account WHERE accountid = @CustomerGuid";

            using (var connection = new SqlConnection(CrmDbConnString))
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandType = System.Data.CommandType.Text;
                    command.CommandText = commandText;
                    command.Parameters.Add(new SqlParameter("@CustomerGuid", customerGuid.ToString()));

                    command.Connection.Open();
                    var temp = command.ExecuteScalar();
                    command.Connection.Close();
                    if (temp != null)
                    {
                        var statecode = (int)temp;
                        Log.Debug($"{logPrefix}::IsActiveByGuid '{statecode}' is found for customerId '{customerGuid}'");
                        return statecode == 0;
                    }
                }
            }
            Log.Debug($"{logPrefix}::IsActiveByGuid failed to find state for customer with guid '{customerGuid}'");
            throw new Exception($"IsActiveByGuid did not find a statecode for customer with guid '{customerGuid}'");
        }
        public DepartmentLite GetDepartmentByDepartmentNumber(string departmentNumber, string bankGuid, bool active = true)
        {
            var result = new DepartmentLite { CrmId = Guid.Empty };

            if (string.IsNullOrEmpty(departmentNumber))
                return result;

            const string commandText = @"select cap_departmentid, cap_name, ownerid 
                                         from cap_department 
                                         where cap_departmentnumber = @DepartmentNumber 
                                         and owningbusinessunit = @BankGuid
                                         and statecode = @Statecode";

            using (var connection = new SqlConnection(CrmDbConnString))
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandType = System.Data.CommandType.Text;
                    command.CommandText = commandText;
                    command.Parameters.AddWithValue("@DepartmentNumber", departmentNumber);
                    command.Parameters.AddWithValue("@BankGuid", bankGuid);
                    command.Parameters.AddWithValue("@Statecode", active ? "0" :  "1");

                    command.Connection.Open();

                    using (var dataReader = command.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            result.CrmId = (Guid) dataReader["cap_departmentid"];
                            result.CapName = dataReader["cap_name"] != DBNull.Value ? dataReader["cap_name"].ToString() : string.Empty;
                            result.OwnerId = (Guid)dataReader["ownerid"];
                            result.CapDepartmentNumber = departmentNumber;
                        }
                    }
                    command.Connection.Close();
                }
            }
            if (result.CrmId != Guid.Empty)
                Log.Debug($"{logPrefix}::GetDepartmentByDepartmentNumber Succeeded for DepartmentNumber '{departmentNumber}'.");
            else
                Log.Debug($"{logPrefix}::GetDepartmentByDepartmentNumber failed for DepartmentNumber '{departmentNumber}'. Found no match.");

            return result;
        }
        public Guid GetUserIdByUsername(string username, Guid bankid)
        {
            if(string.IsNullOrEmpty(username))
                return Guid.Empty;

            const string commandText = "SELECT systemuserid FROM dbo.systemuser WHERE domainname LIKE @Username and businessunitid = @BankId";

            using (var connection = new SqlConnection(CrmDbConnString))
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandType = System.Data.CommandType.Text;
                    command.CommandText = commandText;
                    command.Parameters.Add(new SqlParameter("@Username", $"%{username}"));
                    command.Parameters.Add(new SqlParameter("@BankId", $"{bankid}"));

                    command.Connection.Open();
                    var temp = command.ExecuteScalar();
                    command.Connection.Close();
                    if (temp != null)
                    {
                        var id = temp.ToString();
                        Log.Debug($"{logPrefix}::GetUserIdByUsername '{id}' is found for username '{username}'");
                        return new Guid(id);
                    }
                    Log.Debug($"{logPrefix}::GetUserIdByUsername failed to find guid for username '{username}'");
                    return Guid.Empty;
                }
            }
        }
        

    }
}
