﻿using System;
using System.Collections.Generic;
using System.Linq;

using Common.Entities;
using Common.Enums;
using Common.Helpers;
using Common.Logging;

using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Tooling.Connector;
using Crm.Services.Extensions;

namespace Common.Adapters
{
    public class QueryCrm
    {
        #region Private Methods
        private readonly IOrganizationService _service;
        private readonly string _connectionString;
        private readonly Guid _callerId;

        //private readonly ILog Log = LogManager.GetLogger("query-crm");
        private readonly MsmqLogger Log = MsmqLogger.CreateLogger("query-crm");
        #endregion

        #region Constructor
        public QueryCrm(string connectionString, Guid callerId)
        {
            _connectionString = connectionString;
            _callerId = callerId;

            _service = OrganizationServiceHelper.GetIOrganizationService();

            Log.Debug(_service == null ? "NOT CONNECTED XRM." : "Connected xrm!");
        }

        public QueryCrm(Dictionary<string, string> config)
        {
            _service = OrganizationServiceHelper.GetIOrganizationService();

            var isConnectedToCrm = _service != null;

            if(!isConnectedToCrm) Log.Fatal($"Not connected to CRM.");
            else Log.Debug("Connected to CRM!");
        }

        #endregion

        public bool IsConnected()
        {
            return _service != null;
        }


        public Guid GetBankIdByBankNo(string bankNo)
        {
            var qe = new QueryExpression("businessunit");

            var fe = new FilterExpression();
            fe.AddCondition(new ConditionExpression("divisionname", ConditionOperator.Equal, bankNo));
            qe.Criteria = fe;

            try
            {
                var coll = _service.RetrieveMultiple(qe);

                if (coll.Entities.Count > 0)
                    return coll.Entities[0].Id;
            }
            catch (Exception ex)
            {
                LogHelper.Error(Util.GetMethodName(), ex, Log);
            }

            return Guid.Empty;
        }
        public Guid GetDefaultTeamIdByBankId(Guid bankId)
        {
            var qe = new QueryExpression
            {
                EntityName = "team"
            };

            var fe = new FilterExpression(LogicalOperator.And);
            fe.AddCondition(new ConditionExpression("businessunitid", ConditionOperator.Equal, bankId));
            fe.AddCondition(new ConditionExpression("isdefault", ConditionOperator.Equal, "1"));
            qe.Criteria = fe;
            qe.PageInfo.ReturnTotalRecordCount = true;
            try
            {
                var collection = _service.RetrieveMultiple(qe);

                if (collection.Entities.Count > 0)
                    return collection.Entities[0].Id;
            }
            catch (Exception ex)
            {
                LogHelper.Error(Util.GetMethodName(), ex, Log);
            }

            return Guid.Empty;
        }
        public string GetBankName(string bankNo)
        {
            try
            {
                var qe = new QueryExpression("businessunit")
                {
                    ColumnSet = new ColumnSet("name", "divisionname")
                };

                var fe = new FilterExpression();
                fe.AddCondition(new ConditionExpression("divisionname", ConditionOperator.Equal, bankNo));
                qe.Criteria = fe;

                var coll = _service.RetrieveMultiple(qe);

                foreach (var ent in coll.Entities)
                {
                    return ent.Contains("name") ? ent["name"].ToString() : string.Empty;
                }
            }
            catch (Exception ex)
            {
                LogHelper.Error(Util.GetMethodName(), ex, Log);
            }
            return string.Empty;
        }



        public Entity GetCustomer(Guid customerId, string entity)
        {
            try
            {
                return _service.Retrieve(entity, customerId, new ColumnSet(true));
            }
            catch (Exception ex)
            {
                LogHelper.Error(Util.GetMethodName(), ex, Log);
                return null;
            }
        }
        public Guid GetCustomerId(string customerNo, string bankNo)
        {
            var bankId = GetBankIdByBankNo(bankNo);
            if (bankId == Guid.Empty) return Guid.Empty;

            var qe = new QueryExpression
            {
                EntityName = "contact"
            };

            var fe = new FilterExpression(LogicalOperator.And);
            fe.AddCondition(new ConditionExpression("owningbusinessunit", ConditionOperator.Equal, bankId));
            fe.AddCondition(new ConditionExpression("governmentid", ConditionOperator.Equal, customerNo));
            fe.AddCondition(new ConditionExpression("statecode", ConditionOperator.Equal, 0));
            qe.Criteria = fe;

            try
            {
                var collection = _service.RetrieveMultiple(qe);
                if (collection.Entities.Count > 0)
                {
                    return collection.Entities[0].Id;
                }
            }
            catch (Exception ex)
            {
                LogHelper.Error(Util.GetMethodName(), ex, Log);
            }
            return Guid.Empty;
        }


        public Dictionary<string, Guid> GetConnectionRoleIds(Dictionary<string, string> connectionRoleNames)
        {
            var result = new Dictionary<string, Guid>();
            var qe = new QueryExpression
            {
                EntityName = EntityName.connectionrole.ToString(),
                ColumnSet = new ColumnSet("connectionroleid", "name")
            };

            var fe = new FilterExpression();
            fe.AddCondition(new ConditionExpression("name", ConditionOperator.In, connectionRoleNames.Values.ToList()));
            qe.Criteria = fe;
            qe.PageInfo.ReturnTotalRecordCount = true;
            try
            {
                var collection = _service.RetrieveMultiple(qe);
                if (collection.Entities.Count > 0)
                {
                    foreach (var ent in collection.Entities)
                    {
                        var name = ent["name"].ToString();

                        foreach (var item in connectionRoleNames)
                        {
                            if (name.Equals(item.Value))
                            {
                                result.Add($"{item.Key}Id", ent.Id);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.Error(Util.GetMethodName(), ex, Log);
            }
            return result;
        }
        public List<string> GetAllCommonEmailsFromBusinessUnits()
        {
            var result = new List<string>();
            const string field = "emailaddress";

            var qe = new QueryExpression(EntityName.businessunit.ToString())
            {
                ColumnSet = new ColumnSet(field)
            };

            var fe = new FilterExpression();
            fe.AddCondition(new ConditionExpression("emailaddress", ConditionOperator.NotNull));
            qe.Criteria = fe;

            try
            {
                var coll = _service.RetrieveMultiple(qe);

                var email = string.Empty;
                foreach (var ent in coll.Entities)
                {
                    if (ent.Contains(field))
                    {
                        email = ent[field].ToString().ToLower();
                        if (!result.Contains(email)) result.Add(email);
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.Error(Util.GetMethodName(), ex, Log);
            }

            return result;
        }


        public List<CrmCustomer> GetDetailedCrmCustomerBySourceId(string sourceId, CustomerExtra cust, Source source, ProcessedCustomerResponse response)
        {
            var result = new List<CrmCustomer>();
            if (string.IsNullOrEmpty(sourceId)) return result;

            #region Query

            var isContact = cust.EntityName == EntityName.contact;
            var qe = new QueryExpression
            {
                EntityName = cust.EntityName.ToString(),
                ColumnSet = new ColumnSet(true)
            };

            var fe = new FilterExpression(LogicalOperator.And);
            fe.AddCondition(new ConditionExpression("owningbusinessunit", ConditionOperator.Equal, cust.BankId));
            fe.AddCondition(new ConditionExpression(Helper.GetSourceIdName(source), ConditionOperator.Equal, sourceId));
            qe.Criteria = fe;
            qe.AddOrder("statecode", OrderType.Ascending);
            qe.PageInfo.ReturnTotalRecordCount = true;
            #endregion

            try
            {
                var collection = _service.RetrieveMultiple(qe);

                if (collection.Entities.Count > 0)
                {
                    foreach (var ent in collection.Entities)
                    {
                        var crmCustomer = Mapper.CrmCustomer(ent, cust, isContact);
                        crmCustomer.SourceId = sourceId;
                        crmCustomer.MainSourceId = Helper.GetCardMainSourceId(crmCustomer);
                        crmCustomer.IsActive = crmCustomer.StateCode == 0;

                        result.Add(crmCustomer);
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.Error(Util.GetMethodName(), ex, response, Log);
            }

            return result;
        }
        public List<CrmCustomer> GetDetailedCrmCustomerByCustomerNumber(string customerNumber, CustomerExtra cust, Source source, ProcessedCustomerResponse response)
        {
            var result = new List<CrmCustomer>();

            #region Query
            var isContact = cust.EntityName == EntityName.contact;
            var qe = new QueryExpression
            {
                EntityName = cust.EntityName.ToString(),
                ColumnSet = new ColumnSet(true)
            };


            var fe = new FilterExpression(LogicalOperator.And);
            fe.AddCondition(new ConditionExpression("owningbusinessunit", ConditionOperator.Equal, cust.BankId));
            fe.AddCondition(new ConditionExpression(isContact ? "governmentid" : "accountnumber", ConditionOperator.Equal, customerNumber));
            qe.Criteria = fe;
            qe.AddOrder("statecode", OrderType.Ascending);
            qe.PageInfo.ReturnTotalRecordCount = true;
            #endregion

            try
            {
                var collection = _service.RetrieveMultiple(qe);

                if (collection.Entities.Count > 0)
                {
                    foreach (var ent in collection.Entities)
                    {
                        var crmCustomer = Mapper.CrmCustomer(ent, cust, isContact);
                        crmCustomer.SourceId = Helper.GetStrIfValue(ent, Helper.GetSourceIdName(source));
                        crmCustomer.MainSourceId = Helper.GetCardMainSourceId(crmCustomer);
                        crmCustomer.IsActive = crmCustomer.StateCode == 0;

                        result.Add(crmCustomer);
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.Error(Util.GetMethodName(), ex, response, Log);
            }

            return result;
        }



        public Guid GetUserIdByUsernameAndBankId(string username, Guid bankId)
        {
            if (string.IsNullOrEmpty(username)) return Guid.Empty;

            var qe = new QueryExpression
            {
                EntityName = EntityName.systemuser.ToString()
            };

            var fe = new FilterExpression(LogicalOperator.And);
            fe.AddCondition(new ConditionExpression("businessunitid", ConditionOperator.Equal, bankId));
            fe.AddCondition(new ConditionExpression("domainname", ConditionOperator.EndsWith, $@"\{username}"));
            qe.Criteria = fe;
            qe.PageInfo.ReturnTotalRecordCount = true;
            try
            {
                var collection = _service.RetrieveMultiple(qe);

                if (collection.Entities.Count == 1)
                {
                    var entity = collection.Entities[0];
                    return entity.Id;
                }
                if (collection.Entities.Count > 1)
                {
                    Log.Warn($"Multiple hits on username: '{username}' with bankId: '{bankId}'. Forced to set the bank as owner");
                }
            }
            catch (Exception ex)
            {
                LogHelper.Error(Util.GetMethodName(), ex, Log);
            }

            return Guid.Empty;
        }
        public DepartmentInfo GetDepartmentInfoByNumberAndBank(string departmentNumber, Guid bankId)
        {
            if (string.IsNullOrEmpty(departmentNumber))
                return new DepartmentInfo();

            var result = new DepartmentInfo();

            var qe = new QueryExpression
            {
                EntityName = EntityName.cap_department.ToString(),
                ColumnSet = new ColumnSet("statecode", "cap_name")
            };

            var fe = new FilterExpression(LogicalOperator.And);
            fe.AddCondition(new ConditionExpression("owningbusinessunit", ConditionOperator.Equal, bankId));
            fe.AddCondition(new ConditionExpression("cap_departmentnumber", ConditionOperator.Equal, departmentNumber));
            qe.Criteria = fe;
            qe.PageInfo.ReturnTotalRecordCount = true;
            try
            {
                var collection = _service.RetrieveMultiple(qe);
                if (collection.Entities.Count > 0)
                {
                    var ent = collection.Entities[0];

                    result.DepartmentId = ent.Id;
                    result.IsActive = ((OptionSetValue)ent["statecode"]).Value == 0;
                    result.DepartmentName = ent["cap_name"].ToString();
                }
            }
            catch (Exception ex)
            {
                LogHelper.Error(Util.GetMethodName(), ex, Log);
            }

            return result;
        }

        public List<ConnectionInfo> GetConnectionInfoFromBankConnections(Guid customerId, Guid productCompanyRole)
        {
            var result = new List<ConnectionInfo>();
            var qe = new QueryExpression
            {
                EntityName = EntityName.connection.ToString(),
                ColumnSet = new ColumnSet("record1id")
            };

            var fe = new FilterExpression(LogicalOperator.And);
            fe.AddCondition(new ConditionExpression("record2id", ConditionOperator.Equal, customerId));
            fe.AddCondition(new ConditionExpression("record2roleid", ConditionOperator.Equal, productCompanyRole));
            qe.Criteria = fe;
            qe.PageInfo.ReturnTotalRecordCount = true;
            try
            {
                var collection = _service.RetrieveMultiple(qe);
                if (collection.Entities.Count > 0)
                {
                    foreach (var entity in collection.Entities)
                    {
                        result.Add(new ConnectionInfo
                        {
                            ConnectionId = entity.Id,
                            BankId = ((EntityReference)entity["record1id"]).Id
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.Error(Util.GetMethodName(), ex, Log);
            }
            return result;
        }
        public string GetSourceIdByBankAndCustomerNumber(string customerNumber, Guid bankId, EntityName entityName, string sourceIdName)
        {
            var result = string.Empty;
            var isContact = entityName == EntityName.contact;

            var qe = new QueryExpression
            {
                EntityName = entityName.ToString(),
                ColumnSet = new ColumnSet(sourceIdName)
            };

            var fe = new FilterExpression(LogicalOperator.And);
            fe.AddCondition(new ConditionExpression("statecode", ConditionOperator.Equal, 0));
            fe.AddCondition(new ConditionExpression("owningbusinessunit", ConditionOperator.Equal, bankId));
            fe.AddCondition(new ConditionExpression(isContact ? "governmentid" : "accountnumber", ConditionOperator.Equal, customerNumber));
            qe.Criteria = fe;

            try
            {
                var collection = _service.RetrieveMultiple(qe);
                if (collection.Entities.Count > 0)
                {
                    foreach (var entity in collection.Entities)
                    {
                        var sourceId = entity.Contains(sourceIdName) ? entity[sourceIdName].ToString() : string.Empty;

                        if (!string.IsNullOrEmpty(sourceId))
                        {
                            return sourceId;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.Error(Util.GetMethodName(), ex, Log);
            }
            return result;
        }

        public int GetLastGivenMonthsActivityCount(Guid id, int monthsAgo)
        {
            var result = 0;
            if (monthsAgo <= 0)
            {
                Log.Error($"{Util.GetMethodName()}. Bad input '{monthsAgo}'. Expecting int larger than 0.");
                return result;
            }
            var qe = new QueryExpression
            {
                EntityName = EntityName.activitypointer.ToString(),
                ColumnSet = new ColumnSet("createdon")
            };

            var fe = new FilterExpression();
            fe.AddCondition(new ConditionExpression("regardingobjectid", ConditionOperator.Equal, id));
            qe.Criteria = fe;
            try
            {
                var collection = _service.RetrieveMultiple(qe);

                if (collection.Entities.Count > 0)
                {
                    foreach (var ent in collection.Entities)
                    {
                        var createdOn = (DateTime)ent["createdon"];
                        if (createdOn < DateTime.UtcNow && createdOn > DateTime.UtcNow.AddMonths(-monthsAgo))
                        {
                            result++;
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                LogHelper.Error(Util.GetMethodName(), ex, Log);
            }
            return result;
        }
        public int GetLastGivenMonthsOpportunityCount(Guid id, int monthsAgo)
        {
            var result = 0;
            if (monthsAgo <= 0)
            {
                Log.Error($"{Util.GetMethodName()}. Bad input '{monthsAgo}'. Expecting int larger than 0.");
                return result;
            }
            var qe = new QueryExpression
            {
                EntityName = EntityName.opportunity.ToString(),
                ColumnSet = new ColumnSet("createdon")
            };

            var fe = new FilterExpression();
            fe.AddCondition(new ConditionExpression("customerid", ConditionOperator.Equal, id));
            qe.Criteria = fe;
            try
            {
                var collection = _service.RetrieveMultiple(qe);

                if (collection.Entities.Count > 0)
                {
                    foreach (var ent in collection.Entities)
                    {
                        var createdOn = (DateTime)ent["createdon"];
                        if (createdOn < DateTime.UtcNow && createdOn > DateTime.UtcNow.AddMonths(-monthsAgo))
                        {
                            result++;
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                LogHelper.Error(Util.GetMethodName(), ex, Log);
            }
            return result;
        }
        public int GetLastGivenMonthsCaseCount(Guid id, int monthsAgo)
        {
            var result = 0;
            if (monthsAgo <= 0)
            {
                Log.Error($"{Util.GetMethodName()}. Bad input '{monthsAgo}'. Expecting int larger than 0.");
                return result;
            }
            var qe = new QueryExpression
            {
                EntityName = EntityName.incident.ToString(),
                ColumnSet = new ColumnSet("createdon")
            };

            var fe = new FilterExpression(LogicalOperator.Or);
            fe.AddCondition(new ConditionExpression("customerid", ConditionOperator.Equal, id));
            fe.AddCondition(new ConditionExpression("cap_externalcustomerid", ConditionOperator.Equal, id));
            qe.Criteria = fe;
            try
            {
                var collection = _service.RetrieveMultiple(qe);

                if (collection.Entities.Count > 0)
                {
                    foreach (var ent in collection.Entities)
                    {
                        var createdOn = (DateTime)ent["createdon"];
                        if (createdOn < DateTime.UtcNow && createdOn > DateTime.UtcNow.AddMonths(-monthsAgo))
                        {
                            result++;
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                LogHelper.Error(Util.GetMethodName(), ex, Log);
            }
            return result;
        }

        public bool HasActivityLastGivenMonthsOrFuturePlans(Guid id, int monthsAgo)
        {
            if (monthsAgo <= 0)
            {
                Log.Error($"{Util.GetMethodName()}. Bad input '{monthsAgo}'. Expecting int larger than 0.");
                return false;
            }
            var qe = new QueryExpression
            {
                EntityName = EntityName.activitypointer.ToString(),
                ColumnSet = new ColumnSet("scheduledstart", "createdon")
            };

            var fe = new FilterExpression();
            fe.AddCondition(new ConditionExpression("regardingobjectid", ConditionOperator.Equal, id));
            qe.Criteria = fe;
            try
            {
                var collection = _service.RetrieveMultiple(qe);

                foreach (var ent in collection.Entities)
                {
                    var dueDate = (DateTime)ent["scheduledstart"];
                    if (dueDate >= DateTime.UtcNow) return true;

                    var createdOn = (DateTime)ent["createdon"];
                    if (createdOn > DateTime.UtcNow.AddMonths(-monthsAgo)) return true;
                }
            }
            catch (Exception ex)
            {
                LogHelper.Error(Util.GetMethodName(), ex, Log);
            }
            return false;
        }
        public bool HasOpportunityLastGivenMonths(Guid id, int monthsAgo)
        {
            if (monthsAgo <= 0)
            {
                Log.Error($"{Util.GetMethodName()}. Bad input '{monthsAgo}'. Expecting int larger than 0.");
                return false;
            }
            var qe = new QueryExpression
            {
                EntityName = EntityName.opportunity.ToString(),
                ColumnSet = new ColumnSet("createdon")
            };
            
            var fe = new FilterExpression(LogicalOperator.Or);
            fe.AddCondition(new ConditionExpression("customerid", ConditionOperator.Equal, id));
            fe.AddCondition(new ConditionExpression("cap_externalcustomerid", ConditionOperator.Equal, id));
            qe.Criteria = fe;
            try
            {
                var collection = _service.RetrieveMultiple(qe);

                foreach (var ent in collection.Entities)
                {
                    var createdOn = (DateTime)ent["createdon"];
                    if (createdOn > DateTime.UtcNow.AddMonths(-monthsAgo)) return true;
                }
            }
            catch (Exception ex)
            {
                LogHelper.Error(Util.GetMethodName(), ex, Log);
            }
            return false;
        }


        public bool HasCaseLastGivenMonths(Guid id, int monthsAgo)
        {
            if (monthsAgo <= 0)
            {
                Log.Error($"{Util.GetMethodName()}. Bad input '{monthsAgo}'. Expecting int larger than 0.");
                return false;
            }
            var qe = new QueryExpression
            {
                EntityName = EntityName.incident.ToString(),
                ColumnSet = new ColumnSet("createdon")
            };

            var fe = new FilterExpression(LogicalOperator.Or);
            fe.AddCondition(new ConditionExpression("customerid", ConditionOperator.Equal, id));
            fe.AddCondition(new ConditionExpression("cap_externalcustomerid", ConditionOperator.Equal, id));
            qe.Criteria = fe;
            try
            {
                var collection = _service.RetrieveMultiple(qe);

                foreach (var ent in collection.Entities)
                {
                    var createdOn = (DateTime)ent["createdon"];
                    if (createdOn > DateTime.UtcNow.AddMonths(-monthsAgo)) return true;
                }
            }
            catch (Exception ex)
            {
                LogHelper.Error(Util.GetMethodName(), ex, Log);
            }
            return false;
        }


        public bool HasExternalCustomerOpportunityLastGivenMonths(Guid id, bool isContact, int monthsAgo)
        {
            if (monthsAgo <= 0)
            {
                Log.Error($"{Util.GetMethodName()}. Bad input '{monthsAgo}'. Expecting int larger than 0.");
                return false;
            }
            var qe = new QueryExpression
            {
                EntityName = isContact ? EntityName.cap_opportunity_contact.ToString() : EntityName.cap_opportunity_account.ToString(),
            };


            var link = qe.AddLink(EntityName.opportunity.ToString(), "opportunityid", "opportunityid", JoinOperator.LeftOuter);
            link.Columns = new ColumnSet("createdon");
            link.EntityAlias = "o";

            var fe = new FilterExpression();
            fe.AddCondition(new ConditionExpression(isContact ? "contactid" : "accountid", ConditionOperator.Equal, id));
            qe.Criteria = fe;
            try
            {
                var collection = _service.RetrieveMultiple(qe);

                foreach (var ent in collection.Entities)
                {
                    var createdOn = ent.Contains("o.createdon") ? (DateTime)(((AliasedValue)ent["o.createdon"]).Value) : DateTime.MinValue;

                    if (createdOn > DateTime.UtcNow.AddMonths(-monthsAgo)) return true;
                }
            }
            catch (Exception ex)
            {
                LogHelper.Error(Util.GetMethodName(), ex, Log);
            }
            return false;
        }


        public List<SalesOpportunity> GetSalesOpportunitiesByExtEventId(string extEventId, Guid bankId, ProcessedCustomerResponse response)
        {
            var result = new List<SalesOpportunity>();
            var qe = new QueryExpression
            {
                EntityName = EntityName.opportunity.ToString(),
                ColumnSet = new ColumnSet(true)
            };

            var fe = new FilterExpression(LogicalOperator.And);
            fe.AddCondition(new ConditionExpression("cap_externaleventid", ConditionOperator.Equal, extEventId));
            fe.AddCondition(new ConditionExpression("owningbusinessunit", ConditionOperator.Equal, bankId));
            qe.Criteria = fe;


            try
            {
                var coll = _service.RetrieveMultiple(qe);

                foreach (var ent in coll.Entities)
                {
                    result.Add(Mapper.SalesOpportunity(ent));
                }
            }
            catch (Exception ex)
            {
                LogHelper.Error(Util.GetMethodName(), ex, response, Log);
            }
            return result;
        }


        public List<Case> GetRelatedIncidents(Guid customerId, Guid subjectId, Guid vendorBankdId, bool isPersonOnProductCompanyCard, string bankNum, ProcessedCustomerResponse response)
        {
            var result = new List<Case>();
            var qe = new QueryExpression
            {
                EntityName = EntityName.incident.ToString(),
                ColumnSet = new ColumnSet(true)
            };

            var link = qe.AddLink(EntityName.product.ToString(), "productid", "productid", JoinOperator.LeftOuter);
            link.Columns = new ColumnSet("cap_vendorid");
            link.EntityAlias = "p";

            qe.Criteria.AddCondition(new ConditionExpression("customerid", ConditionOperator.Equal, customerId));
            qe.Criteria.AddCondition(new ConditionExpression("subjectid", ConditionOperator.Equal, subjectId));
            qe.Criteria.AddCondition(new ConditionExpression("p", "cap_vendorid", ConditionOperator.Equal, vendorBankdId));
            if (isPersonOnProductCompanyCard)
            {
                qe.Criteria.AddCondition(new ConditionExpression("cap_businessunitid", ConditionOperator.Equal, GetBankIdByBankNo(bankNum)));
            }

            try
            {
                var coll = _service.RetrieveMultiple(qe);

                foreach (var ent in coll.Entities)
                {
                    result.Add(Mapper.Case(ent, customerId));
                }
            }
            catch (Exception ex)
            {
                LogHelper.Error(Util.GetMethodName(), ex, response, Log);
            }
            return result;
        }

        public Guid GetSubjectIdByTitle(string title)
        {
            if (string.IsNullOrEmpty(title)) return Guid.Empty;

            try
            {
                var qe = new QueryExpression
                {
                    EntityName = EntityName.subject.ToString()
                };
                qe.Criteria.AddCondition(new ConditionExpression("title", ConditionOperator.Equal, title));

                var coll = _service.RetrieveMultiple(qe);
                if (coll.Entities.Count > 0)
                    return coll.Entities[0].Id;
            }
            catch (Exception ex)
            {
                LogHelper.Error(Util.GetMethodName(), ex, Log);
            }
            return Guid.Empty;
        }

        public ProductInfo GetProductByCode(string productNumber)
        {
            var result = new ProductInfo();
            if (string.IsNullOrEmpty(productNumber)) return result;

            try
            {
                var qe = new QueryExpression
                {
                    EntityName = EntityName.product.ToString(),
                    ColumnSet = new ColumnSet(true)
                };
                qe.Criteria.AddCondition(new ConditionExpression("productnumber", ConditionOperator.Equal, productNumber));

                var coll = _service.RetrieveMultiple(qe);

                foreach (var ent in coll.Entities)
                {
                    result = new ProductInfo
                    {
                        Id = ent.Id,
                        Name = ent.Contains("name") ? ent["name"].ToString() : string.Empty,
                        Number = productNumber,
                        Active = ((OptionSetValue)ent["statecode"]).Value == 0,
                        SubjectId = ent.Contains("subjectid") ? ((EntityReference)ent["subjectid"]).Id : Guid.Empty,
                        VendorId = ent.Contains("cap_vendorid") ? ((EntityReference)ent["cap_vendorid"]).Id : Guid.Empty,
                        PrimaryUomId = ent.Contains("defaultuomid") ? ((EntityReference)ent["defaultuomid"]).Id : Guid.Empty
                    };

                    if (coll.Entities.Count > 1) Console.WriteLine($"There is a duplicate for product with number: {productNumber}");

                    return result;
                }
            }
            catch (Exception ex)
            {
                LogHelper.Error(Util.GetMethodName(), ex, Log);
            }

            return result;
        }


        public List<Guid> GetSharedOpportunitiesOnCustomerForCollaborationTeam(Guid id, Guid subjectId, Guid collaborationTeamId)
        {
            var result = new List<Guid>();
            Console.WriteLine($"Getting shared SO for id: '{id}'");
            var qe = new QueryExpression
            {
                EntityName = EntityName.opportunity.ToString(),
                ColumnSet = new ColumnSet("createdon")
            };

            var link = qe.AddLink("principalobjectaccess", "opportunityid", "objectid", JoinOperator.LeftOuter);
            link.EntityAlias = "p";


            var fe = new FilterExpression();
            fe.AddCondition(new ConditionExpression("customerid", ConditionOperator.Equal, id));
            fe.AddCondition(new ConditionExpression("cap_subjectid", ConditionOperator.Equal, subjectId));
            fe.AddCondition(new ConditionExpression("p", "accessrightsmask", ConditionOperator.NotEqual, 0));
            fe.AddCondition(new ConditionExpression("p", "principalid", ConditionOperator.Equal, collaborationTeamId));
            qe.Criteria = fe;
            try
            {
                var collection = _service.RetrieveMultiple(qe);
                Console.WriteLine($"count: {collection.Entities.Count}");

                foreach (var ent in collection.Entities)
                {
                    result.Add(ent.Id);
                }
            }
            catch (Exception ex)
            {
                LogHelper.Error(Util.GetMethodName(), ex, Log);
            }
            return result;
        }


        public OrganizationResponse UnshareTeamAccessOnOpportunity(Guid opportunityId, Guid teamId, ProcessedCustomerResponse response)
        {
            Log.Debug($"Unsharing SO with id: '{opportunityId}'");

            var revokeeReference = new EntityReference(EntityName.team.ToString(), teamId);
            var targetReference = new EntityReference(EntityName.opportunity.ToString(), opportunityId);

            try
            {
                var revokeRequest = new RevokeAccessRequest
                {
                    Revokee = revokeeReference,
                    Target = targetReference
                };

                return _service.Execute(revokeRequest);
            }
            catch (Exception ex)
            {
                LogHelper.Error(Util.GetMethodName(), ex, response, Log);
            }

            return null;
        }


        public Guid GetCollaborationTeam(string bankName, Guid vendorBankId)
        {
            var qe = new QueryExpression("team");

            qe.Criteria = new FilterExpression(LogicalOperator.And);
            qe.Criteria.AddCondition("name", ConditionOperator.EndsWith, $"-{bankName}");
            qe.Criteria.AddCondition("businessunitid", ConditionOperator.Equal, vendorBankId);
            qe.Criteria.AddCondition("cap_iscollaboration", ConditionOperator.Equal, true);

            try
            {
                var coll = _service.RetrieveMultiple(qe);

                foreach (var ent in coll.Entities) return ent.Id;
            }
            catch (Exception ex)
            {
                LogHelper.Error(Util.GetMethodName(), ex, Log);
            }

            return Guid.Empty;
        }


        public bool MergeEntity(Guid masterId, Guid duplicateId, string entityName, Entity additionalFieldsToUpdate, ProcessedCustomerResponse response)
        {
            if (masterId == duplicateId) return true;

            var mreq = new MergeRequest();
            var targetAccountId = masterId;
            var toBeMergedAccountId = duplicateId;
            mreq.Target = new EntityReference(entityName, targetAccountId);  // Target Account ID , where other account will be mergered
            mreq.SubordinateId = toBeMergedAccountId;
            mreq.PerformParentingChecks = false;

            mreq.UpdateContent = additionalFieldsToUpdate;
            
            try
            {
                //_service.EnableProxyTypes();

                var mresp = (MergeResponse)_service.Execute(mreq);
                return true;
            }
            catch (Exception ex)
            {
                LogHelper.Error(Util.GetMethodName(), ex, response, Log);
                return false;
            }
        }


        public bool SetEntityStatus(Guid entityId, string entityName, int stateCode, int statusCode)
        {
            try
            {
                var setState = new SetStateRequest
                {
                    EntityMoniker = new EntityReference(entityName, entityId),
                    State = new OptionSetValue(stateCode),
                    Status = new OptionSetValue(statusCode)
                };

                _service.Execute(setState);
                Log.Debug($"Setstate succeeded for {entityName} with Guid {entityId} to state {stateCode} and status {statusCode}");
                return true;
            }
            catch (Exception ex)
            {
                LogHelper.Error(Util.GetMethodName(), ex, Log);
                return false;
            }
        }


        public bool AddToTeam(Guid teamId, List<Guid> userIds)
        {
            try
            {
                var addRequest = new AddMembersTeamRequest
                {
                    TeamId = teamId,
                    MemberIds = userIds.ToArray()
                };

                _service.Execute(addRequest);
                return true;
            }
            catch (Exception ex)
            {
                LogHelper.Error(Util.GetMethodName(), ex, Log);
                return false;
            }
        }


        public bool RemoveFromTeam(Guid teamId, List<Guid> userIds)
        {
            try
            {
                var removeRequest = new RemoveMembersTeamRequest
                {
                    TeamId = teamId,
                    MemberIds = userIds.ToArray()
                };

                _service.Execute(removeRequest);
                return true;
            }
            catch (Exception ex)
            {
                LogHelper.Error(Util.GetMethodName(), ex, Log);
                return false;
            }
        }


        public Guid CreateOrUpdateEntity(Entity entity, ProcessedCustomerResponse response)
        {
            if (entity.Id == Guid.Empty)
            {
                return CreateEntity(entity, response);
            }

            return UpdateEntity(entity, response) ? entity.Id : Guid.Empty;
        }


        public Guid CreateEntity(Entity ent, ProcessedCustomerResponse response)
        {
            try
            {
                return _service.Create(ent);
            }
            catch (Exception ex)
            {
                LogHelper.Error(Util.GetMethodName(), ex, response, Log);

                return Guid.Empty;
            }
        }


        public bool UpdateEntity(Entity ent, ProcessedCustomerResponse response)
        {
            if (ent.Id == Guid.Empty)
            {
                LogHelper.Msg($"{Util.GetMethodName()} failed due to receiving entity ({ent.LogicalName}) with Empty Guid.", response, LogLevel.Error, Log);
                return false;
            }

            try
            {
                _service.Update(ent);
                return true;
            }
            catch (Exception ex)
            {
                LogHelper.Error(Util.GetMethodName(), ex, response, Log);
                return false;
            }
        }


        public bool DeleteEntity(EntityName entityName, Guid id, ProcessedCustomerResponse response)
        {
            try
            {
                _service.Delete(entityName.ToString(), id);
                return true;
            }
            catch (Exception ex)
            {
                LogHelper.Error(Util.GetMethodName(), ex, response, Log);
                return false;
            }
        }


        #region Helper Classes

        public IOrganizationService GetCrmOrganizationServiceByXrmTooling()
        {
            try
            {
                Log.Debug($"Conn str (xrm tooling): {_connectionString}");

                var conn = new CrmServiceClient(_connectionString);
                if (_callerId != Guid.Empty)
                {
                    Log.Debug($"Setting callerId: '{_callerId}'");
                    conn.CallerId = _callerId;
                }

                return conn.OrganizationWebProxyClient ?? (IOrganizationService)conn.OrganizationServiceProxy;
            }
            catch (Exception ex)
            {
                LogHelper.Error(Util.GetMethodName(), ex, Log);
            }
            return null;
        }


        public string GetStr(Entity ent, string fieldName)
        {
            return ent.Contains(fieldName) ? ent[fieldName].ToString() : string.Empty;
        }
        public OptionSetValue GetOptionSetValue(Entity ent, string fieldName)
        {
            return ent.Contains(fieldName) ? (OptionSetValue)ent[fieldName] : new OptionSetValue();
        }


        #endregion

    }

}
