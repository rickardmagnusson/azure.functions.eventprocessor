﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Constants;
using Common.Entities;
using Common.Enums;
using Common.Helpers;
using Common.Logging;
using Common.RequestBuilders;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using Crm.Services.Extensions;
using Azure.Logging;

namespace Common.Adapters
{
    public class QueryExpressionHelper : IDisposable
    {
        private StringBuilder Logger;
        private IOrganizationService _service;
        private readonly MsmqLogger Log = MsmqLogger.CreateLogger("query-expression-helper");
        private static SingletonLogger log = SingletonLogger.Instance();

        public QueryExpressionHelper()
        {
            _service = OrganizationServiceHelper.GetIOrganizationService();
        }


        public QueryExpressionHelper(Dictionary<string, string> config, StringBuilder logger)
        {
            Logger = logger;
            _service = OrganizationServiceHelper.GetIOrganizationService();

        }


        public List<string> GetAllCommonEmailsFromBusinessUnits()
        {
            var result = new List<string>();
            const string field = "emailaddress";

            var qe = new QueryExpression(EntityName.businessunit.ToString())
            {
                ColumnSet = new ColumnSet(field)
            };

            var fe = new FilterExpression();
            fe.AddCondition(new ConditionExpression("emailaddress", ConditionOperator.NotNull));
            qe.Criteria = fe;

            try
            {
                var coll = _service.RetrieveMultiple(qe);

                var email = string.Empty;
                foreach (var ent in coll.Entities)
                {
                    if (ent.Contains(field))
                    {
                        email = ent[field].ToString().ToLower();
                        if (!result.Contains(email)) result.Add(email);
                    }
                }
            }
            catch (Exception ex)
            {
                var errMsg = $"Error while processing {System.Reflection.MethodBase.GetCurrentMethod().Name}. Error message: {ManagerHelper.GetExceptionMessage(ex)}";
                Log.Error(errMsg);
            }

            return result;
        }


        public Guid GetDefaultTeamIdByBankId(Guid bankId)
        {
            var entity = new Entity();
            var qe = new QueryExpression
            {
                EntityName = EntityName.team.ToString()
            };

            var fe = new FilterExpression(LogicalOperator.And);
            fe.AddCondition(new ConditionExpression("businessunitid", ConditionOperator.Equal, bankId));
            fe.AddCondition(new ConditionExpression("isdefault", ConditionOperator.Equal, "1"));
            qe.Criteria = fe;
            qe.PageInfo.ReturnTotalRecordCount = true;
            try
            {
                var collection = _service.RetrieveMultiple(qe);

                if (collection.Entities.Count > 0)
                    entity = collection.Entities[0];
                else
                    return Guid.Empty;
            }
            catch (Exception ex)
            {
                var errMsg = $"Error while processing {System.Reflection.MethodBase.GetCurrentMethod().Name}. Error message: {ManagerHelper.GetExceptionMessage(ex)}";
                Log.Error(errMsg);
            }

            return entity.Id;
        }


        public Guid GetBankIdByBankNo(string bankNo)
        {
            var bank = new Entity();
            var fe = new FilterExpression(LogicalOperator.And);
            var qe = new QueryExpression();
            ConditionExpression ce1;

            ce1 = new ConditionExpression("divisionname", ConditionOperator.Equal, bankNo);
            qe.EntityName = "businessunit";
            qe.ColumnSet = new ColumnSet("businessunitid");

            fe.AddCondition(ce1);
            qe.Criteria = fe;
            qe.PageInfo.ReturnTotalRecordCount = true;

            try
            {
                var bank_coll = _service.RetrieveMultiple(qe);

                if (bank_coll.Entities.Count > 0)
                    bank = bank_coll.Entities[0];
                else
                    return Guid.Empty;
            }
            catch (Exception ex)
            {
                var errMsg = $"Error while processing {System.Reflection.MethodBase.GetCurrentMethod().Name}. Error message: {ManagerHelper.GetExceptionMessage(ex)}";
                Log.Error(errMsg);
            }

            return bank.Id;
        }


        public Guid GetUserIdByUsernameAndBankId(string username, Guid bankId)
        {
            if (string.IsNullOrEmpty(username)) return Guid.Empty;

            var qe = new QueryExpression
            {
                EntityName = EntityName.systemuser.ToString()
            };

            var fe = new FilterExpression(LogicalOperator.And);
            fe.AddCondition(new ConditionExpression("businessunitid", ConditionOperator.Equal, bankId));
            fe.AddCondition(new ConditionExpression("domainname", ConditionOperator.EndsWith, $@"\{username}"));
            qe.Criteria = fe;
            qe.PageInfo.ReturnTotalRecordCount = true;
            try
            {
                var collection = _service.RetrieveMultiple(qe);

                if (collection.Entities.Count == 1)
                {
                    var entity = collection.Entities[0];
                    return entity.Id;
                }
                if (collection.Entities.Count > 1)
                {
                    Log.Warn($"Multiple hits on username: '{username}' with bankId: '{bankId}'. Forced to set the bank as owner");
                }
            }
            catch (Exception ex)
            {
                var errMsg = $"Error while processing {System.Reflection.MethodBase.GetCurrentMethod().Name}. Error message: {ManagerHelper.GetExceptionMessage(ex)}";
                Log.Error(errMsg);
            }

            return Guid.Empty;
        }
        public List<Guid> GetBankIdsByBankNos(List<string> bankNos)
        {
            var result = new List<Guid>();
            if (bankNos.Count == 0) return result;

            var qe = new QueryExpression
            {
                EntityName = EntityName.businessunit.ToString()
            };

            var fe = new FilterExpression();
            fe.AddCondition(new ConditionExpression("divisionname", ConditionOperator.In, bankNos));
            qe.Criteria = fe;
            qe.PageInfo.ReturnTotalRecordCount = true;
            try
            {
                var collection = _service.RetrieveMultiple(qe);
                if (collection.Entities.Count > 0)
                {
                    foreach (var entity in collection.Entities)
                    {
                        result.Add(entity.Id);
                    }
                }
            }
            catch (Exception ex)
            {
                var errMsg = $"Error while processing {System.Reflection.MethodBase.GetCurrentMethod().Name}. Error message: {ManagerHelper.GetExceptionMessage(ex)}";
                Log.Error(errMsg);
            }
            if (result.Count != bankNos.Count)
            {
                Log.Error("Error at GetBankIdsByBankNos. One or more of the bank numbers was not found in CRM.");
            }

            return result;
        }
        public List<ConnectionInfo> GetConnectionInfoFromBankConnections(Guid customerId, Guid productCompanyRole)
        {
            var result = new List<ConnectionInfo>();
            var qe = new QueryExpression
            {
                EntityName = EntityName.connection.ToString(),
                ColumnSet = new ColumnSet("record1id")
            };

            var fe = new FilterExpression(LogicalOperator.And);
            fe.AddCondition(new ConditionExpression("record2id", ConditionOperator.Equal, customerId));
            fe.AddCondition(new ConditionExpression("record2roleid", ConditionOperator.Equal, productCompanyRole));
            qe.Criteria = fe;
            qe.PageInfo.ReturnTotalRecordCount = true;
            try
            {
                var collection = _service.RetrieveMultiple(qe);
                if (collection.Entities.Count > 0)
                {
                    foreach (var entity in collection.Entities)
                    {
                        result.Add(new ConnectionInfo
                        {
                            ConnectionId = entity.Id,
                            BankId = ((EntityReference)entity["record1id"]).Id
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                var errMsg = $"Error while processing {System.Reflection.MethodBase.GetCurrentMethod().Name}. Error message: {ManagerHelper.GetExceptionMessage(ex)}";
                Log.Error(errMsg);
            }
            return result;
        }


        /// <summary>
        /// Need to be cached or impl as singleton... Slowwwww
        /// </summary>
        /// <param name="opportunities"></param>
        /// <param name="response"></param>
        public void GetOpportunityProducts(List<SalesOpportunity> opportunities, ProcessedCustomerResponse response)
        {
            
            log.Add("GetOpportunityProducts");

            foreach (var op in opportunities)
            {
                op.OpportunityProducts = GetOpportunityProducts(op.Id, response);
            }

            log.Add($"{response.ErrorMessage} \nHas 'SalesOpportunity': {opportunities.Any()}");
        }

        public List<OpportunityProduct> GetOpportunityProducts(Guid opportunityId, ProcessedCustomerResponse response)
        {
            var result = new List<OpportunityProduct>();
            var qe = new QueryExpression
            {
                EntityName = EntityName.opportunityproduct.ToString(),
                ColumnSet = new ColumnSet("productid", "uomid")
            };

            var fe = new FilterExpression();
            fe.AddCondition(new ConditionExpression("opportunityid", ConditionOperator.Equal, opportunityId));
            qe.Criteria = fe;
            qe.PageInfo.ReturnTotalRecordCount = true;
            try
            {
                var collection = _service.RetrieveMultiple(qe);
                if (collection.Entities.Count > 0)
                {
                    foreach (var entity in collection.Entities)
                    {
                        result.Add(new OpportunityProduct
                        {
                            Id = entity.Id,
                            OpportunityId = opportunityId,
                            ProductId = ((EntityReference)entity["productid"]).Id,
                            UnitId = ((EntityReference)entity["uomid"]).Id
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                var errMsg = $"GetOpportunityProducts: Error while processing {System.Reflection.MethodBase.GetCurrentMethod().Name}. Error message: {ManagerHelper.GetExceptionMessage(ex)}";
                Log.Error(errMsg);
                response.ErrorMessage = errMsg;
                response.FailedProcessing = true;
            }
            return result;
        }

   

        public Guid GetPriceListItemId(Guid pricelevelId, Guid productId)
        {
            var qe = new QueryExpression
            {
                EntityName = EntityName.productpricelevel.ToString(),
            };
            var fe = new FilterExpression(LogicalOperator.And);
            fe.AddCondition(new ConditionExpression("pricelevelid", ConditionOperator.Equal, pricelevelId));
            fe.AddCondition(new ConditionExpression("productid", ConditionOperator.Equal, productId));
            qe.Criteria = fe;

            try
            {
                var coll = _service.RetrieveMultiple(qe);
                if (coll.Entities.Count > 0)
                    return coll.Entities[0].Id;
            }
            catch (Exception ex)
            {
                var errMsg = $"Error while processing {System.Reflection.MethodBase.GetCurrentMethod().Name}. Error message: {ManagerHelper.GetExceptionMessage(ex)}";
                Log.Error(errMsg);
            }
            return Guid.Empty;
        }
        public string GetSourceIdByBankAndCustomerNumber(string customerNumber, Guid bankId, EntityName entityName, string sourceIdName)
        {
            var result = string.Empty;
            var isContact = entityName == EntityName.contact;

            var qe = new QueryExpression
            {
                EntityName = entityName.ToString(),
                ColumnSet = new ColumnSet(sourceIdName)
            };

            var fe = new FilterExpression(LogicalOperator.And);
            fe.AddCondition(new ConditionExpression("statecode", ConditionOperator.Equal, 0));
            fe.AddCondition(new ConditionExpression("owningbusinessunit", ConditionOperator.Equal, bankId));
            fe.AddCondition(new ConditionExpression(isContact ? "governmentid" : "accountnumber", ConditionOperator.Equal, customerNumber));
            qe.Criteria = fe;

            try
            {
                var collection = _service.RetrieveMultiple(qe);
                if (collection.Entities.Count > 0)
                {
                    foreach (var entity in collection.Entities)
                    {
                        var sourceId = entity.Contains(sourceIdName) ? entity[sourceIdName].ToString() : string.Empty;

                        if (!string.IsNullOrEmpty(sourceId))
                        {
                            return sourceId;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var errMsg = $"Error while processing {System.Reflection.MethodBase.GetCurrentMethod().Name}. Error message: {ManagerHelper.GetExceptionMessage(ex)}";
                Log.Error(errMsg);
            }
            return result;
        }
        public Dictionary<string, Guid> GetConnectionRoleIds(Dictionary<string, string> connectionRoleNames)
        {
            var result = new Dictionary<string, Guid>();
            var qe = new QueryExpression
            {
                EntityName = EntityName.connectionrole.ToString(),
                ColumnSet = new ColumnSet("connectionroleid", "name")
            };

            var fe = new FilterExpression();
            fe.AddCondition(new ConditionExpression("name", ConditionOperator.In, connectionRoleNames.Values.ToList()));
            qe.Criteria = fe;
            qe.PageInfo.ReturnTotalRecordCount = true;
            try
            {
                var collection = _service.RetrieveMultiple(qe);
                if (collection.Entities.Count > 0)
                {
                    foreach (var ent in collection.Entities)
                    {
                        var name = ent["name"].ToString();

                        foreach (var item in connectionRoleNames)
                        {
                            if (name.Equals(item.Value))
                            {
                                result.Add($"{item.Key}Id", ent.Id);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var errMsg = $"Error while processing {System.Reflection.MethodBase.GetCurrentMethod().Name}. Error message: {ManagerHelper.GetExceptionMessage(ex)}";
                Log.Error(errMsg);
            }
            return result;
        }

        public Guid GetCustomerIdByCustomerNumberAndBankIdAndNiceSource(string customerNo, Guid bankId, EntityName entityName)
        {
            var qe = new QueryExpression
            {
                EntityName = entityName.ToString()
            };

            var fe = new FilterExpression(LogicalOperator.And);
            fe.AddCondition(new ConditionExpression("owningbusinessunit", ConditionOperator.Equal, bankId));
            fe.AddCondition(new ConditionExpression(entityName == EntityName.contact ? "governmentid" : "accountnumber", ConditionOperator.Equal, customerNo));
            fe.AddCondition(new ConditionExpression("territorycode", ConditionOperator.Equal, (int)TerritoryCode.Nice));
            qe.Criteria = fe;
            qe.PageInfo.ReturnTotalRecordCount = true;
            try
            {
                var collection = _service.RetrieveMultiple(qe);
                if (collection.Entities.Count > 0)
                {
                    return collection.Entities[0].Id;
                }
            }
            catch (Exception ex)
            {
                var errMsg = $"Error while processing {System.Reflection.MethodBase.GetCurrentMethod().Name}. Error message: {ManagerHelper.GetExceptionMessage(ex)}";
                Log.Error(errMsg);
            }
            return Guid.Empty;
        }
        public string GetCustomerSourceId(Guid customerId, EntityName entityName, bool isSkadeInsurance)
        {
            var field = isSkadeInsurance ? "cap_nicesourceid" : "cap_nice2sourceid";

            try
            {
                var ent = _service.Retrieve(entityName.ToString(), customerId, new ColumnSet(field));
                return ent[field].ToString();
            }
            catch (Exception ex)
            {
                var errMsg = $"Error while processing {System.Reflection.MethodBase.GetCurrentMethod().Name}. Error message: {ManagerHelper.GetExceptionMessage(ex)}";
                Log.Error(errMsg);
            }
            return string.Empty;
        }
        public Guid GetCustomerIdBySourceIdAndBankIdAndNiceSource(string sourceId, Guid bankId, EntityName entityName, bool isSkadeInsurance)
        {
            if (string.IsNullOrEmpty(sourceId)) return Guid.Empty;

            var qe = new QueryExpression
            {
                EntityName = entityName.ToString(),
            };

            var fe = new FilterExpression(LogicalOperator.And);
            fe.AddCondition(new ConditionExpression("owningbusinessunit", ConditionOperator.Equal, bankId));
            fe.AddCondition(new ConditionExpression(isSkadeInsurance ? "cap_nicesourceid" : "cap_nice2sourceid", ConditionOperator.Equal, sourceId));
            fe.AddCondition(new ConditionExpression("territorycode", ConditionOperator.Equal, (int)TerritoryCode.Nice));
            qe.Criteria = fe;
            qe.PageInfo.ReturnTotalRecordCount = true;
            try
            {
                var collection = _service.RetrieveMultiple(qe);
                if (collection.Entities.Count > 0)
                {
                    return collection.Entities[0].Id;
                }
            }
            catch (Exception ex)
            {
                var errMsg = $"Error while processing {System.Reflection.MethodBase.GetCurrentMethod().Name}. Error message: {ManagerHelper.GetExceptionMessage(ex)}";
                Log.Error(errMsg);
            }
            return Guid.Empty;
        }


        public List<CrmCustomer> GetDetailedCrmCustomerBySourceId(string sourceId, CustomerExtra cust, CustomerRegisterSource source)
        {
            var result = new List<CrmCustomer>();
            if (string.IsNullOrEmpty(sourceId)) return result;

            #region Query

            var isContact = cust.EntityName == EntityName.contact;
            var qe = new QueryExpression
            {
                EntityName = cust.EntityName.ToString(),
                ColumnSet = new ColumnSet("statecode", "territorycode", "customertypecode", "cap_eventdate")
            };
            qe.ColumnSet.AddColumns("telephone1", "telephone2", "telephone3");
            qe.ColumnSet.AddColumns("emailaddress1", "emailaddress2");
            qe.ColumnSet.AddColumns("address1_line1", "address1_line2", "address1_line3", "address1_postalcode", "address1_stateorprovince", "address1_city", "address1_country");


            if (isContact)
            {
                qe.ColumnSet.AddColumns("governmentid", "firstname", "lastname", "mobilephone");
                //qe.ColumnSet.AddColumns("gendercode");
            }
            else
            {
                qe.ColumnSet.AddColumns("accountnumber", "name");
            }

            var fe = new FilterExpression(LogicalOperator.And);
            fe.AddCondition(new ConditionExpression("owningbusinessunit", ConditionOperator.Equal, cust.BankId));
            fe.AddCondition(new ConditionExpression(Helper.GetSourceIdName(source), ConditionOperator.Equal, sourceId));
            qe.Criteria = fe;
            qe.AddOrder("statecode", OrderType.Ascending);
            qe.PageInfo.ReturnTotalRecordCount = true;
            #endregion

            try
            {
                var collection = _service.RetrieveMultiple(qe);

                if (collection.Entities.Count > 0)
                {
                    foreach (var ent in collection.Entities)
                    {
                        #region Mapping Fields
                        result.Add(new CrmCustomer
                        {
                            Id = ent.Id,
                            EntityName = cust.EntityName,
                            SourceId = sourceId,
                            CustomerNumber = isContact
                                        ? GetStrIfValue(ent, "governmentid")
                                        : GetStrIfValue(ent, "accountnumber"),

                            FirstName = GetStrIfValue(ent, "firstname"),
                            LastName = GetStrIfValue(ent, "lastname"),
                            CompanyName = GetStrIfValue(ent, "name"),

                            Mobilephone = GetStrIfValue(ent, "mobilephone"),
                            Telephone1 = GetStrIfValue(ent, "telephone1"),
                            Telephone2 = GetStrIfValue(ent, "telephone2"),
                            Telephone3 = GetStrIfValue(ent, "telephone3"),

                            EmailAddress1 = GetStrIfValue(ent, "emailaddress1"),
                            EmailAddress2 = GetStrIfValue(ent, "emailaddress2"),

                            Address1_Line1 = GetStrIfValue(ent, "address1_line1"),
                            Address1_Line2 = GetStrIfValue(ent, "address1_line2"),
                            Address1_Line3 = GetStrIfValue(ent, "address1_line3"),
                            Address1_City = GetStrIfValue(ent, "address1_city"),
                            Address1_PostalCode = GetStrIfValue(ent, "address1_postalcode"),
                            Address1_StateOrProvince = GetStrIfValue(ent, "address1_stateorprovince"),
                            Address1_Country = GetStrIfValue(ent, "address1_country"),

                            CapEventdate = GetDateTimeIfValue(ent, "cap_eventdate"),
                            Type = (CustomerTypeCode)((OptionSetValue)ent["customertypecode"]).Value,
                            StateCode = ((OptionSetValue)ent["statecode"]).Value,
                            Source = (TerritoryCode)((OptionSetValue)ent["territorycode"]).Value
                        });
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                var errMsg = $"Error while processing {System.Reflection.MethodBase.GetCurrentMethod().Name}. Error message: {ManagerHelper.GetExceptionMessage(ex)}";
                Log.Error(errMsg);
            }

            return result;
        }
        public List<CrmCustomer> GetDetailedCrmCustomerByCustomerNumber(string customerNumber, CustomerExtra cust, CustomerRegisterSource source)
        {
            var result = new List<CrmCustomer>();

            #region Query
            var isContact = cust.EntityName == EntityName.contact;
            var qe = new QueryExpression
            {
                EntityName = cust.EntityName.ToString(),
                ColumnSet = new ColumnSet("statecode", "territorycode", "customertypecode", "cap_eventdate", "cap_nicesourceid", "cap_nice2sourceid")
            };
            qe.ColumnSet.AddColumns("telephone1", "telephone2", "telephone3");
            qe.ColumnSet.AddColumns("emailaddress1", "emailaddress2");
            qe.ColumnSet.AddColumns("address1_line1", "address1_line2", "address1_line3", "address1_postalcode", "address1_stateorprovince", "address1_city", "address1_country");


            if (isContact)
            {
                qe.ColumnSet.AddColumns("firstname", "lastname", "mobilephone");
                //qe.ColumnSet.AddColumns("gendercode");
            }
            else
            {
                qe.ColumnSet.AddColumns("name");
            }


            var fe = new FilterExpression(LogicalOperator.And);
            fe.AddCondition(new ConditionExpression("owningbusinessunit", ConditionOperator.Equal, cust.BankId));
            fe.AddCondition(new ConditionExpression(isContact ? "governmentid" : "accountnumber", ConditionOperator.Equal, customerNumber));
            qe.Criteria = fe;
            qe.AddOrder("statecode", OrderType.Ascending);
            qe.PageInfo.ReturnTotalRecordCount = true;
            #endregion


            try
            {
                var collection = _service.RetrieveMultiple(qe);

                if (collection.Entities.Count > 0)
                {
                    foreach (var ent in collection.Entities)
                    {
                        #region Mapping Fields
                        result.Add(new CrmCustomer
                        {
                            Id = ent.Id,
                            EntityName = cust.EntityName,
                            SourceId = GetStrIfValue(ent, Helper.GetSourceIdName(source)),
                            CustomerNumber = isContact
                                        ? GetStrIfValue(ent, "governmentid")
                                        : GetStrIfValue(ent, "accountnumber"),

                            FirstName = GetStrIfValue(ent, "firstname"),
                            LastName = GetStrIfValue(ent, "lastname"),
                            CompanyName = GetStrIfValue(ent, "name"),

                            Mobilephone = GetStrIfValue(ent, "mobilephone"),
                            Telephone1 = GetStrIfValue(ent, "telephone1"),
                            Telephone2 = GetStrIfValue(ent, "telephone2"),
                            Telephone3 = GetStrIfValue(ent, "telephone3"),

                            EmailAddress1 = GetStrIfValue(ent, "emailaddress1"),
                            EmailAddress2 = GetStrIfValue(ent, "emailaddress2"),

                            Address1_Line1 = GetStrIfValue(ent, "address1_line1"),
                            Address1_Line2 = GetStrIfValue(ent, "address1_line2"),
                            Address1_Line3 = GetStrIfValue(ent, "address1_line3"),
                            Address1_City = GetStrIfValue(ent, "address1_city"),
                            Address1_PostalCode = GetStrIfValue(ent, "address1_postalcode"),
                            Address1_StateOrProvince = GetStrIfValue(ent, "address1_stateorprovince"),
                            Address1_Country = GetStrIfValue(ent, "address1_country"),

                            CapEventdate = GetDateTimeIfValue(ent, "cap_eventdate"),
                            Type = (CustomerTypeCode)((OptionSetValue)ent["customertypecode"]).Value,
                            StateCode = ((OptionSetValue)ent["statecode"]).Value,
                            Source = (TerritoryCode)((OptionSetValue)ent["territorycode"]).Value
                        });
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                var errMsg = $"Error while processing {System.Reflection.MethodBase.GetCurrentMethod().Name}. Error message: {ManagerHelper.GetExceptionMessage(ex)}";
                Log.Error(errMsg);
            }

            return result;
        }


        public List<CrmCustomer> GetDetailedCrmCustomerBySourceId(string sourceId, CustomerExtra cust, string source, ProcessedCustomerResponse response)
        {
            var result = new List<CrmCustomer>();
            if (string.IsNullOrEmpty(sourceId)) return result;

            #region Query

            var isContact = cust.EntityName == EntityName.contact;
            var qe = new QueryExpression
            {
                EntityName = cust.EntityName.ToString(),
                ColumnSet = new ColumnSet(true)
            };

            var fe = new FilterExpression(LogicalOperator.And);
            fe.AddCondition(new ConditionExpression("owningbusinessunit", ConditionOperator.Equal, cust.BankId));
            fe.AddCondition(new ConditionExpression(Helper.GetSourceIdName(source), ConditionOperator.Equal, sourceId));
            qe.Criteria = fe;
            qe.AddOrder("statecode", OrderType.Ascending);
            qe.PageInfo.ReturnTotalRecordCount = true;
            #endregion

            try
            {
                var collection = _service.RetrieveMultiple(qe);

                if (collection.Entities.Count > 0)
                {
                    foreach (var ent in collection.Entities)
                    {
                        #region Mapping Fields
                        result.Add(new CrmCustomer
                        {
                            Id = ent.Id,
                            EntityName = cust.EntityName,
                            SourceId = sourceId,
                            CustomerNumber = isContact
                                        ? GetStrIfValue(ent, "governmentid")
                                        : GetStrIfValue(ent, "accountnumber"),

                            FirstName = GetStrIfValue(ent, "firstname"),
                            LastName = GetStrIfValue(ent, "lastname"),
                            CompanyName = GetStrIfValue(ent, "name"),

                            Mobilephone = GetStrIfValue(ent, "mobilephone"),
                            Telephone1 = GetStrIfValue(ent, "telephone1"),
                            Telephone2 = GetStrIfValue(ent, "telephone2"),
                            Telephone3 = GetStrIfValue(ent, "telephone3"),

                            EmailAddress1 = GetStrIfValue(ent, "emailaddress1"),
                            EmailAddress2 = GetStrIfValue(ent, "emailaddress2"),

                            Address1_Line1 = GetStrIfValue(ent, "address1_line1"),
                            Address1_Line2 = GetStrIfValue(ent, "address1_line2"),
                            Address1_Line3 = GetStrIfValue(ent, "address1_line3"),
                            Address1_City = GetStrIfValue(ent, "address1_city"),
                            Address1_PostalCode = GetStrIfValue(ent, "address1_postalcode"),
                            Address1_StateOrProvince = GetStrIfValue(ent, "address1_stateorprovince"),
                            Address1_Country = GetStrIfValue(ent, "address1_country"),

                            CapEventdate = GetDateTimeIfValue(ent, "cap_eventdate"),
                            Type = (CustomerTypeCode)((OptionSetValue)ent["customertypecode"]).Value,
                            StateCode = ((OptionSetValue)ent["statecode"]).Value,
                            Source = (TerritoryCode)((OptionSetValue)ent["territorycode"]).Value,
                            OwnerReference = (EntityReference)ent["ownerid"]
                        });
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                var errMsg = $"Error while processing {System.Reflection.MethodBase.GetCurrentMethod().Name}. Error message: {ManagerHelper.GetExceptionMessage(ex)}";
                Log.Error(errMsg);
                response.ErrorMessage = errMsg;
                response.FailedProcessing = true;
            }

            return result;
        }
        public List<CrmCustomer> GetDetailedCrmCustomerByCustomerNumber(string customerNumber, CustomerExtra cust, string source, ProcessedCustomerResponse response)
        {
            var result = new List<CrmCustomer>();

            #region Query
            var isContact = cust.EntityName == EntityName.contact;
            var qe = new QueryExpression
            {
                EntityName = cust.EntityName.ToString(),
                ColumnSet = new ColumnSet(true)
            };


            var fe = new FilterExpression(LogicalOperator.And);
            fe.AddCondition(new ConditionExpression("owningbusinessunit", ConditionOperator.Equal, cust.BankId));
            fe.AddCondition(new ConditionExpression(isContact ? "governmentid" : "accountnumber", ConditionOperator.Equal, customerNumber));
            qe.Criteria = fe;
            qe.AddOrder("statecode", OrderType.Ascending);
            qe.PageInfo.ReturnTotalRecordCount = true;
            #endregion


            try
            {
                var collection = _service.RetrieveMultiple(qe);

                if (collection.Entities.Count > 0)
                {
                    foreach (var ent in collection.Entities)
                    {
                        #region Mapping Fields
                        result.Add(new CrmCustomer
                        {
                            Id = ent.Id,
                            EntityName = cust.EntityName,
                            SourceId = GetStrIfValue(ent, Helper.GetSourceIdName(source)),
                            CustomerNumber = isContact
                                        ? GetStrIfValue(ent, "governmentid")
                                        : GetStrIfValue(ent, "accountnumber"),

                            FirstName = GetStrIfValue(ent, "firstname"),
                            LastName = GetStrIfValue(ent, "lastname"),
                            CompanyName = GetStrIfValue(ent, "name"),

                            Mobilephone = GetStrIfValue(ent, "mobilephone"),
                            Telephone1 = GetStrIfValue(ent, "telephone1"),
                            Telephone2 = GetStrIfValue(ent, "telephone2"),
                            Telephone3 = GetStrIfValue(ent, "telephone3"),

                            EmailAddress1 = GetStrIfValue(ent, "emailaddress1"),
                            EmailAddress2 = GetStrIfValue(ent, "emailaddress2"),

                            Address1_Line1 = GetStrIfValue(ent, "address1_line1"),
                            Address1_Line2 = GetStrIfValue(ent, "address1_line2"),
                            Address1_Line3 = GetStrIfValue(ent, "address1_line3"),
                            Address1_City = GetStrIfValue(ent, "address1_city"),
                            Address1_PostalCode = GetStrIfValue(ent, "address1_postalcode"),
                            Address1_StateOrProvince = GetStrIfValue(ent, "address1_stateorprovince"),
                            Address1_Country = GetStrIfValue(ent, "address1_country"),

                            CapEventdate = GetDateTimeIfValue(ent, "cap_eventdate"),
                            Type = (CustomerTypeCode)((OptionSetValue)ent["customertypecode"]).Value,
                            StateCode = ((OptionSetValue)ent["statecode"]).Value,
                            Source = (TerritoryCode)((OptionSetValue)ent["territorycode"]).Value,
                            OwnerReference = (EntityReference)ent["ownerid"]
                        });
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                var errMsg = $"Error while processing {System.Reflection.MethodBase.GetCurrentMethod().Name}. Error message: {ManagerHelper.GetExceptionMessage(ex)}";
                Log.Error(errMsg);
                response.ErrorMessage = errMsg;
                response.FailedProcessing = true;
            }

            return result;
        }


        public void GetCustomerInfo(string customerNo, CustomerExtra cust)
        {
            var qe = new QueryExpression
            {
                EntityName = cust.EntityName.ToString(),
                ColumnSet = new ColumnSet("statecode", "territorycode")
            };

            var fe = new FilterExpression(LogicalOperator.And);
            fe.AddCondition(new ConditionExpression("owningbusinessunit", ConditionOperator.Equal, cust.BankId));
            fe.AddCondition(new ConditionExpression(cust.EntityName == EntityName.contact ? "governmentid" : "accountnumber", ConditionOperator.Equal, customerNo));
            fe.AddCondition(new ConditionExpression("territorycode", ConditionOperator.Equal, (int)TerritoryCode.Nice));
            qe.Criteria = fe;
            qe.PageInfo.ReturnTotalRecordCount = true;
            try
            {
                var collection = _service.RetrieveMultiple(qe);
                if (collection.Entities.Count > 0)
                {
                    var ent = collection.Entities[0];

                    cust.CustomerId = ent.Id;
                    cust.IsActive = ((OptionSetValue)ent["statecode"]).Value == 0;
                    //cust.Source = (TerritoryCode)((OptionSetValue)ent["territorycode"]).Value;
                }
            }
            catch (Exception ex)
            {
                var errMsg = $"Error while processing {System.Reflection.MethodBase.GetCurrentMethod().Name}. Error message: {ManagerHelper.GetExceptionMessage(ex)}";
                Log.Error(errMsg);
            }
        }
        public DepartmentInfo GetDepartmentInfoByNumberAndBank(string departmentNumber, Guid bankId)
        {
            if (string.IsNullOrEmpty(departmentNumber))
                return new DepartmentInfo();

            var result = new DepartmentInfo();

            var qe = new QueryExpression
            {
                EntityName = EntityName.cap_department.ToString(),
                ColumnSet = new ColumnSet("statecode", "cap_name")
            };

            var fe = new FilterExpression(LogicalOperator.And);
            fe.AddCondition(new ConditionExpression("owningbusinessunit", ConditionOperator.Equal, bankId));
            fe.AddCondition(new ConditionExpression("cap_departmentnumber", ConditionOperator.Equal, departmentNumber));
            qe.Criteria = fe;
            qe.PageInfo.ReturnTotalRecordCount = true;
            try
            {
                var collection = _service.RetrieveMultiple(qe);
                if (collection.Entities.Count > 0)
                {
                    var ent = collection.Entities[0];

                    result.DepartmentId = ent.Id;
                    result.IsActive = ((OptionSetValue)ent["statecode"]).Value == 0;
                    result.DepartmentName = ent["cap_name"].ToString();
                }
            }
            catch (Exception ex)
            {
                var errMsg = $"Error while processing {System.Reflection.MethodBase.GetCurrentMethod().Name}. Error message: {ManagerHelper.GetExceptionMessage(ex)}";
                Log.Error(errMsg);
            }

            return result;
        }






        public Guid GetSubjectIdByTitle(string title)
        {
            if (string.IsNullOrEmpty(title)) return Guid.Empty;

            try
            {
                var qe = new QueryExpression
                {
                    EntityName = EntityName.subject.ToString()
                };
                qe.Criteria.AddCondition(new ConditionExpression("title", ConditionOperator.Equal, title));

                var coll = _service.RetrieveMultiple(qe);
                if (coll.Entities.Count > 0)
                    return coll.Entities[0].Id;
            }
            catch (Exception ex)
            {
                var errMsg = $"Error while processing {System.Reflection.MethodBase.GetCurrentMethod().Name}. Error message: {ManagerHelper.GetExceptionMessage(ex)}";
                Log.Error(errMsg);
            }
            return Guid.Empty;
        }

        public Guid GetPriceListIdByTitle(string title)
        {
            if (string.IsNullOrEmpty(title)) return Guid.Empty;

            try
            {
                var qe = new QueryExpression
                {
                    EntityName = EntityName.pricelevel.ToString()
                };
                qe.Criteria.AddCondition(new ConditionExpression("name", ConditionOperator.Equal, title));

                var coll = _service.RetrieveMultiple(qe);
                if (coll.Entities.Count > 0)
                    return coll.Entities[0].Id;
            }
            catch (Exception ex)
            {
                var errMsg = $"Error while processing {System.Reflection.MethodBase.GetCurrentMethod().Name}. Error message: {ManagerHelper.GetExceptionMessage(ex)}";
                Log.Error(errMsg);
            }
            return Guid.Empty;
        }

        public ProductInfo GetProductByCode(string productNumber)
        {
            var result = new ProductInfo();
            if (string.IsNullOrEmpty(productNumber)) return result;

            try
            {
                var qe = new QueryExpression
                {
                    EntityName = EntityName.product.ToString(),
                    ColumnSet = new ColumnSet(true)
                };
                qe.Criteria.AddCondition(new ConditionExpression("productnumber", ConditionOperator.Equal, productNumber));

                var coll = _service.RetrieveMultiple(qe);

                foreach (var ent in coll.Entities)
                {
                    result = new ProductInfo
                    {
                        Id = ent.Id,
                        Name = ent.Contains("name") ? ent["name"].ToString() : string.Empty,
                        Number = productNumber,
                        Active = ((OptionSetValue)ent["statecode"]).Value == 0,
                        SubjectId = ent.Contains("subjectid") ? ((EntityReference)ent["subjectid"]).Id : Guid.Empty,
                        VendorId = ent.Contains("cap_vendorid") ? ((EntityReference)ent["cap_vendorid"]).Id : Guid.Empty,
                        PrimaryUomId = ent.Contains("defaultuomid") ? ((EntityReference)ent["defaultuomid"]).Id : Guid.Empty
                    };

                    if (coll.Entities.Count > 1) Console.WriteLine($"There is a duplicate for product with number: {productNumber}");

                    return result;
                }
            }
            catch (Exception ex)
            {
                var errMsg = $"Error while processing {System.Reflection.MethodBase.GetCurrentMethod().Name}. Error message: {ManagerHelper.GetExceptionMessage(ex)}";
                Log.Error(errMsg);
            }

            return result;
        }

        public Guid GetDefaultUnitGroupId()
        {
            try
            {
                var qe = new QueryExpression
                {
                    EntityName = EntityName.uomschedule.ToString()
                };
                qe.Criteria.AddCondition(new ConditionExpression("name", ConditionOperator.Equal, "Default Unit"));

                var coll = _service.RetrieveMultiple(qe);
                if (coll.Entities.Count > 0)
                    return coll.Entities[0].Id;
            }
            catch (Exception ex)
            {
                var errMsg = $"Error while processing {System.Reflection.MethodBase.GetCurrentMethod().Name}. Error message: {ManagerHelper.GetExceptionMessage(ex)}";
                Log.Error(errMsg);
            }
            return Guid.Empty;
        }

        public Guid GetPrimaryUnitId()
        {
            try
            {
                var qe = new QueryExpression
                {
                    EntityName = EntityName.uom.ToString()
                };

                qe.Criteria.AddCondition(new ConditionExpression("name", ConditionOperator.Equal, "Primary Unit"));

                var coll = _service.RetrieveMultiple(qe);
                if (coll.Entities.Count > 0)
                    return coll.Entities[0].Id;
            }
            catch (Exception ex)
            {
                var errMsg = $"Error while processing {System.Reflection.MethodBase.GetCurrentMethod().Name}. Error message: {ManagerHelper.GetExceptionMessage(ex)}";
                Log.Error(errMsg);
            }

            return Guid.Empty;
        }


        public void CloseIncidentResolved(Entity incident, ProcessedCustomerResponse response)
        {
            if (incident.Id == Guid.Empty) return;

            try
            {
                var incidentResolution = new Entity(EntityName.incidentresolution.ToString())
                {
                    ["incidentid"] = new EntityReference(EntityName.incident.ToString(), incident.Id)
                };

                if (incident.Contains("title")) incidentResolution["subject"] = incident["title"].ToString();
                if (incident.Contains("description")) incidentResolution["description"] = incident["description"].ToString();

                var closeIncidentRequest = new CloseIncidentRequest
                {
                    IncidentResolution = incidentResolution,
                    Status = new OptionSetValue(5)
                };

                _service.Execute(closeIncidentRequest);
            }
            catch (Exception ex)
            {
                var errMsg = $"Error while processing {System.Reflection.MethodBase.GetCurrentMethod().Name}. Error message: {ManagerHelper.GetExceptionMessage(ex)}";
                Log.Error(errMsg);
                response.ErrorMessage = errMsg;
                response.FailedProcessing = true;
            }
        }

        public OrganizationResponse ReopenSalesOpportunity(Guid opportunityId, ProcessedCustomerResponse response)
        {
            if (opportunityId == Guid.Empty) return null;

            try
            {
                var openOpp = new SetStateRequest
                {
                    EntityMoniker = new EntityReference(EntityName.opportunity.ToString(), opportunityId),
                    State = new OptionSetValue(0)
                };

                return _service.Execute(openOpp);
            }
            catch (Exception ex)
            {
                log.Add($"{Util.GetMethodName()}, {ex}, {response}");
                return null;
            }
        }


        public OrganizationResponse CloseSalesOpportunityWon(Guid opportunityId, long value, DateTime timestamp, ProcessedCustomerResponse response, string description = "")
        {
            if (opportunityId == Guid.Empty) return null;

            if (string.IsNullOrEmpty(description))
            {
                description = SalesOpportunityConstant.WonDescription;
            }

            try
            {
                var opportunityClose = RequestBuilder.BuildCreateOpportunityClose(opportunityId, timestamp, value, description);
                var winOppRequest = new WinOpportunityRequest
                {
                    OpportunityClose = opportunityClose,
                    Status = new OptionSetValue(3)
                };

                return _service.Execute(winOppRequest);
            }
            catch (Exception ex)
            {
                log.Add($"{Util.GetMethodName()}, {ex}, {response}");
                return null;
            }
        }



        public OrganizationResponse CloseSalesOpportunityLost(Guid opportunityId, DateTime timestamp, ProcessedCustomerResponse response)
        {
            if (opportunityId == Guid.Empty) return null;

            try
            {
                var opportunityClose = RequestBuilder.BuildCreateOpportunityClose(opportunityId, timestamp, 0, SalesOpportunityConstant.LostDescription);
                var lostOppRequest = new LoseOpportunityRequest
                {
                    OpportunityClose = opportunityClose,
                    Status = new OptionSetValue(4)
                };

                return _service.Execute(lostOppRequest);
            }
            catch (Exception ex)
            {
                log.Add($"{Util.GetMethodName()}, {ex}, {response}");
                return null;
            }
        }


     


        public List<SalesOpportunity> GetSalesOpportunitiesByCustomerId(Guid customerId, ProcessedCustomerResponse response)
        {
            log.Add($"GetSalesOpportunitiesByCustomerId: {customerId}, Response:{response.Success}");

            var result = new List<SalesOpportunity>();
            var qe = new QueryExpression
            {
                EntityName = EntityName.opportunity.ToString(),
                ColumnSet = new ColumnSet(true)
            };

            qe.Criteria.AddCondition(new ConditionExpression("customerid", ConditionOperator.Equal, customerId));

            StringBuilder sb = new StringBuilder();
            log.Add("Createing SalesOpportunity...");
            try
            {
                var coll = _service.RetrieveMultiple(qe);

                foreach (var ent in coll.Entities)
                {
                    sb.AppendLine($"{Mapper.SalesOpportunity(ent).Description}");
                    result.Add(Mapper.SalesOpportunity(ent));
                }
                log.Add(sb.ToString());
            }
            catch (Exception ex)
            {
                log.Add($"GetSalesOpportunitiesByCustomerId failed: {Util.GetMethodName()} {ex}, {response.ErrorMessage}");
            }
            return result;
        }

        public List<SalesOpportunity> GetSalesOpportunitiesBySakId(string sakId, Guid customerId, ProcessedCustomerResponse response)
        {
            var result = new List<SalesOpportunity>();
            var qe = new QueryExpression
            {
                EntityName = EntityName.opportunity.ToString(),
                ColumnSet = new ColumnSet(true)
            };


            var fe = new FilterExpression(LogicalOperator.Or);
            fe.AddCondition(new ConditionExpression("cap_externaleventid", ConditionOperator.Equal, sakId));
            if (customerId != null) fe.AddCondition(new ConditionExpression("customerid", ConditionOperator.Equal, customerId));

            qe.Criteria.Filters.AddRange(fe);

            try
            {
                var coll = _service.RetrieveMultiple(qe);

                foreach (var ent in coll.Entities)
                {
                    result.Add(Mapper.SalesOpportunity(ent));
                }
            }
            catch (Exception ex)
            {
                log.Add($"{Util.GetMethodName()}, {ex}, {response}");
            }

            result = result.OrderByDescending(e => e.ExternalEventId.Equals(sakId)).ToList();
            return result;
        }




        public List<SalesOpportunity> GetSalesOpportunitiesKreditt(Guid bankId, ProcessedCustomerResponse response)
        {
            var result = new List<SalesOpportunity>();
            var qe = new QueryExpression
            {
                EntityName = EntityName.opportunity.ToString(),
                ColumnSet = new ColumnSet(true)
            };

            var fe = new FilterExpression(LogicalOperator.And);
            fe.AddCondition(new ConditionExpression("owningbusinessunit", ConditionOperator.Equal, bankId));
            //fe.AddCondition(new ConditionExpression("subject", ConditionOperator.BeginsWith, "Kreditt"));

            var fe2 = new FilterExpression(LogicalOperator.Or);
            fe2.AddCondition(new ConditionExpression("statecode", ConditionOperator.Equal, 0));
            //fe2.AddCondition(new ConditionExpression("actualclosedate", ConditionOperator.GreaterThan, DateTime.Now.AddMonths(-3)));

            qe.Criteria.Filters.AddRange(fe);
            qe.Criteria.Filters.AddRange(fe2);

            try
            {
                var coll = _service.RetrieveMultiple(qe);

                foreach (var ent in coll.Entities)
                {
                    #region Mapping Fields
                    result.Add(new SalesOpportunity
                    {
                        Id = ent.Id,
                        CustomerId = ent.Contains("customerid") ? ((EntityReference)ent["customerid"]).Id : Guid.Empty,
                        StateCode = ((OptionSetValue)ent["statecode"]).Value,
                        StatusCode = ((OptionSetValue)ent["statuscode"]).Value,

                        Topic = GetStrIfValue(ent, "name"),
                        Description = GetStrIfValue(ent, "description"),

                        Source = ent.Contains("cap_source") ? (TerritoryCode)((OptionSetValue)ent["cap_source"]).Value : TerritoryCode.Crm,
                        Channel = ent.Contains("cap_channel") ? (CapChannel)((OptionSetValue)ent["cap_channel"]).Value : CapChannel.Bankkontor,
                        App = GetStrIfValue(ent, "cap_app"),
                        InitialApp = GetStrIfValue(ent, "cap_initialapp"),
                        ExternalEventId = GetStrIfValue(ent, "cap_externaleventid"),

                        SubjectId = ent.Contains("cap_subjectid") ? ((EntityReference)ent["cap_subjectid"]).Id : Guid.Empty,
                        ProductId = ent.Contains("cap_product") ? ((EntityReference)ent["cap_product"]).Id : Guid.Empty,

                        EstimatedRevenue = ent.Contains("estimatedvalue") ? ((Money)ent["estimatedvalue"]).Value : 0,
                        ActualRevenue = ent.Contains("actualvalue") ? ((Money)ent["actualvalue"]).Value : 0,

                        CreatedOn = (DateTime)ent["createdon"]
                    });
                    #endregion
                }
            }
            catch (Exception ex)
            {
                var errMsg = $"Error while processing {System.Reflection.MethodBase.GetCurrentMethod().Name}. Error message: {ManagerHelper.GetExceptionMessage(ex)}";
                Log.Error(errMsg);
                response.ErrorMessage = errMsg;
                response.FailedProcessing = true;
            }
            return result;
        }
        public List<SalesOpportunity> GetSalesOpportunitiesByExtEventId(string extEventId, Guid bankId, ProcessedCustomerResponse response)
        {
            var result = new List<SalesOpportunity>();
            var qe = new QueryExpression
            {
                EntityName = EntityName.opportunity.ToString(),
                ColumnSet = new ColumnSet(true)
            };

            //qe.Criteria.AddCondition(new ConditionExpression("cap_externaleventid", ConditionOperator.Equal, extEventId));


            var fe = new FilterExpression(LogicalOperator.And);
            fe.AddCondition(new ConditionExpression("cap_externaleventid", ConditionOperator.Equal, extEventId));
            fe.AddCondition(new ConditionExpression("owningbusinessunit", ConditionOperator.Equal, bankId));
            qe.Criteria = fe;


            try
            {
                var coll = _service.RetrieveMultiple(qe);

                foreach (var ent in coll.Entities)
                {
                    result.Add(Mapper.SalesOpportunity(ent));
                }
            }
            catch (Exception ex)
            {
                log.Add($"{Util.GetMethodName()}, {ex}, {response}");
            }
            return result;
        }

        public void SetInfoActivityAsCompleted(Guid infoActivityId, ProcessedCustomerResponse response)
        {
            if (infoActivityId == Guid.Empty) return;

            try
            {
                var setState = new SetStateRequest
                {
                    EntityMoniker = new EntityReference(EntityName.cap_info_activity.ToString(), infoActivityId),
                    State = new OptionSetValue(1),
                    Status = new OptionSetValue(2)
                };
                _service.Execute(setState);
            }
            catch (Exception ex)
            {
                log.Add($"{Util.GetMethodName()}, {ex}, {response}");
            }
        }


        public bool SalesOpportunityHasChild(Guid opportunityId, ProcessedCustomerResponse response)
        {
            try
            {
                var qe = new QueryExpression
                {
                    EntityName = EntityName.opportunity.ToString()
                };

                qe.Criteria.AddCondition(new ConditionExpression("cap_opportunityid", ConditionOperator.Equal, opportunityId));
                //qe.Criteria.AddCondition(new ConditionExpression("statecode", ConditionOperator.Equal, 0));

                var coll = _service.RetrieveMultiple(qe);
                if (coll.Entities.Count > 0)
                    return true;
            }
            catch (Exception ex)
            {
                log.Add($"{Util.GetMethodName()}, {ex}, {response}");
            }

            return false;
        }



        public List<SalesOpportunity> GetOpportunitiesWithClosedLast20Days(List<SalesOpportunity> opportunityList, ProcessedCustomerResponse response)
        {
            return opportunityList.Where(op => ClosedOpportunityWithinLast20Days(op.Id, response)).ToList();
        }

        public SalesOpportunity GetFirstOpportunityWithClosedLast20Days(List<SalesOpportunity> opportunityList, ProcessedCustomerResponse response)
        {
            return opportunityList.FirstOrDefault(op => ClosedOpportunityWithinLast20Days(op.Id, response));
        }
        public bool ClosedOpportunityWithinLast20Days(Guid opportunityId, ProcessedCustomerResponse response)
        {
            try
            {
                var qe = new QueryExpression
                {
                    EntityName = EntityName.opportunityclose.ToString(),
                    ColumnSet = new ColumnSet("actualend")
                };

                qe.Criteria.AddCondition(new ConditionExpression("opportunityid", ConditionOperator.Equal, opportunityId));

                var coll = _service.RetrieveMultiple(qe);
                foreach (var ent in coll.Entities)
                {
                    var date = ent.Contains("actualend") ? (DateTime)ent["actualend"] : DateTime.MinValue;
                    return (DateTime.UtcNow - date).TotalDays < 20;
                }
            }
            catch (Exception ex)
            {
                log.Add($"Error while processing {Util.GetMethodName()}. Error message: {ManagerHelper.GetExceptionMessage(ex)}");
            }

            return false;
        }



        public int GetLastSixMonthsActivityCount(Guid id)
        {
            var result = 0;
            var qe = new QueryExpression
            {
                EntityName = EntityName.activitypointer.ToString(),
                ColumnSet = new ColumnSet("createdon")
            };

            var fe = new FilterExpression();
            fe.AddCondition(new ConditionExpression("regardingobjectid", ConditionOperator.Equal, id));
            qe.Criteria = fe;
            try
            {
                var collection = _service.RetrieveMultiple(qe);

                if (collection.Entities.Count > 0)
                {
                    foreach (var ent in collection.Entities)
                    {
                        var createdOn = (DateTime)ent["createdon"];
                        if (createdOn < DateTime.UtcNow && createdOn > DateTime.UtcNow.AddMonths(-6))
                        {
                            result++;
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                log.Add($"Error while processing {Util.GetMethodName()}. Error message: {ManagerHelper.GetExceptionMessage(ex)}");
            }
            return result;
        }
        public int GetLastSixMonthsOpportunityCount(Guid id)
        {
            var result = 0;
            var qe = new QueryExpression
            {
                EntityName = EntityName.opportunity.ToString(),
                ColumnSet = new ColumnSet("createdon")
            };

            var fe = new FilterExpression();
            fe.AddCondition(new ConditionExpression("customerid", ConditionOperator.Equal, id));
            qe.Criteria = fe;
            try
            {
                var collection = _service.RetrieveMultiple(qe);

                if (collection.Entities.Count > 0)
                {
                    foreach (var ent in collection.Entities)
                    {
                        var createdOn = (DateTime)ent["createdon"];
                        if (createdOn < DateTime.UtcNow && createdOn > DateTime.UtcNow.AddMonths(-6))
                        {
                            result++;
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                log.Add($"Error while processing {Util.GetMethodName()}. Error message: {ManagerHelper.GetExceptionMessage(ex)}");
            }
            return result;
        }
        public int GetLastSixMonthsCaseCount(Guid id)
        {
            var result = 0;
            var qe = new QueryExpression
            {
                EntityName = EntityName.incident.ToString(),
                ColumnSet = new ColumnSet("createdon")
            };

            var fe = new FilterExpression(LogicalOperator.Or);
            fe.AddCondition(new ConditionExpression("customerid", ConditionOperator.Equal, id));
            fe.AddCondition(new ConditionExpression("cap_externalcustomerid", ConditionOperator.Equal, id));
            qe.Criteria = fe;
            try
            {
                var collection = _service.RetrieveMultiple(qe);

                if (collection.Entities.Count > 0)
                {
                    foreach (var ent in collection.Entities)
                    {
                        var createdOn = (DateTime)ent["createdon"];
                        if (createdOn < DateTime.UtcNow && createdOn > DateTime.UtcNow.AddMonths(-6))
                        {
                            result++;
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                log.Add($"Error while processing {Util.GetMethodName()}. Error message: {ManagerHelper.GetExceptionMessage(ex)}");
            }
            return result;
        }


        public bool MergeEntity(Guid masterId, Guid duplicateId, string entityName)
        {
            if (masterId == duplicateId) return true;

            var mreq = new MergeRequest();
            var targetAccountId = masterId;
            var toBeMergedAccountId = duplicateId;
            mreq.Target = new EntityReference(entityName, targetAccountId);  // Target Account ID , where other account will be mergered
            mreq.SubordinateId = toBeMergedAccountId;
            mreq.PerformParentingChecks = false;

            var ent = new Entity(entityName);
            mreq.UpdateContent = ent;

            try
            {
                //_service.EnableProxyTypes();

                var mresp = (MergeResponse)_service.Execute(mreq);
                return true;
            }
            catch (Exception ex)
            {
                //var errMsg = $"Error while processing {System.Reflection.MethodBase.GetCurrentMethod().Name}. Error message: {ManagerHelper.GetExceptionMessage(ex)}";
                //Log.Warn(errMsg);
                Log.Warn($"Exception while merging entity in CRM: {ex.Message}");
                return false;
            }
        }


        public Guid CreateOrUpdateEntity(Entity entity, ProcessedCustomerResponse response)
        {
            if (entity.Id == Guid.Empty)
            {
                return CreateEntity(entity, response);
            }

            return UpdateEntity(entity, response) ? entity.Id : Guid.Empty;
        }



        public Guid CreateEntity(Entity ent, ProcessedCustomerResponse response)
        {
            //CreateEntity with key/values: Entity: 
            //opportunityid : Microsoft.Xrm.Sdk.EntityReferenceEntity: 
            //productid : Microsoft.Xrm.Sdk.EntityReferenceEntity: 
            //uomid : Microsoft.Xrm.Sdk.EntityReferenceEntity: 
            //ispriceoverridden : TrueEntity: 
            //priceperunit : Microsoft.Xrm.Sdk.MoneyEntity: 
            //baseamount : Microsoft.Xrm.Sdk.MoneyEntity: 
            //extendedamount : Microsoft.Xrm.Sdk.MoneyEntity: 
            //quantity : 1

            //var opportunityid = ent.Attributes["opportunityid"].ToString();
            //var uomid = ent.Attributes["uomid"].ToString();
            //var priceperunit = ent.Attributes["priceperunit"].ToString();
            //log.Add($"opportunityid: {opportunityid}, uomid: {uomid}, priceperunit: {priceperunit}");

            //var productid = ent.GetAttributeValue<Guid>("productid");
            //var uomid = ent.GetAttributeValue<Guid>("uomid");
            //var priceperunit = ent.GetAttributeValue<Money>("uomid");
            //var baseamount = ent.GetAttributeValue<Money>("baseamount");
            //var extendedamount = ent.GetAttributeValue<Money>("extendedamount");
            //var quantity = 1;
            //var ispriceoverridden = ent.GetAttributeValue<bool>("ispriceoverridden");


            //log.Add($"{opportunityid}," +
            //    $"productid: {productid}," +
            //    $"uomid: {uomid}," +
            //    $"priceperunit: {priceperunit}," +
            //    $"baseamount: {baseamount}," +
            //    $"extendedamount: {extendedamount}," +
            //    $"ispriceoverridden: {ispriceoverridden}");


            foreach(var a in ent.KeyAttributes)
            {
                log.Add($"{a.GetType()}, {a.Key}, {a.Value}");
            }

            try
            {
                return _service.Create(ent);
            }
            catch (Exception ex)
            {
                var errorMsg = $"Exception while creating entity ({ent.LogicalName}) in CRM: {ManagerHelper.GetExceptionMessage(ex)}";
                response.ErrorMessage = errorMsg;
                response.FailedProcessing = true;
                log.Add(errorMsg);
                return Guid.Empty;
            }
        }


        public bool UpdateEntity(Entity ent, ProcessedCustomerResponse response)
        {
            if (ent.Id == Guid.Empty)
            {
                var errorMsg = $"UpdateEntity failed due to receiving entity ({ent.LogicalName}) with Empty Guid.";
                response.ErrorMessage = errorMsg;
                response.FailedProcessing = true;
                Log.Error(errorMsg);
                return false;
            }

            try
            {
                _service.Update(ent);
                return true;
            }
            catch (Exception ex)
            {
                var errorMsg = $"Exception while updating entity ({ent.LogicalName}) in CRM: {ManagerHelper.GetExceptionMessage(ex)}";
                response.ErrorMessage = errorMsg;
                response.FailedProcessing = true;
                Log.Error(errorMsg);
                return false;
            }
        }
        public bool DeleteEntity(EntityName entityName, Guid id, ProcessedCustomerResponse response)
        {
            try
            {
                _service.Delete(entityName.ToString(), id);
                return true;
            }
            catch (Exception ex)
            {
                var errorMsg = $"Exception while deleting entity in CRM with id: {id}: {ManagerHelper.GetExceptionMessage(ex)}";
                response.ErrorMessage = errorMsg;
                response.FailedProcessing = true;
                Log.Error(errorMsg);
                return false;
            }
        }


        public bool ExecuteMultipleRequestToCrm(ExecuteMultipleRequest multipleRequest)
        {
            try
            {
                bool success = true;
                var responseWithResults = (ExecuteMultipleResponse)_service.Execute(multipleRequest);
                foreach (var responseItem in responseWithResults.Responses)
                {
                    if (responseItem.Response == null && responseItem.Fault != null)
                    {
                        success = false;
                        Log.Error($"Response failed: {responseItem.Fault?.Message}");
                    }
                }

                return success;
            }
            catch (Exception ex)
            {
                Log.Error($"Exception while processing multiple requests in CRM: {ex.Message}");
                throw;
            }
        }


        #region Private Methods
        private string GetStrIfValue(Entity ent, string field)
        {
            return ent.Contains(field) ? ent[field].ToString() : string.Empty;
        }

        private DateTime GetDateTimeIfValue(Entity ent, string field)
        {
            return ent.Contains(field) ? (DateTime)ent[field] : DateTime.MinValue;
            //return ent.Contains(field) ? (DateTime)ent[field] + (DateTime.Now - DateTime.UtcNow) : DateTime.MinValue;
        }
        #endregion

        #region Dispose Methods
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                //DisposeHelper.LoggedDispose(_service, Log);
            }
        }

        #endregion

    }
}
