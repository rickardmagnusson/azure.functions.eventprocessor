﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Common
{
    public class EventSerializerCollection
    {
        private Dictionary<Type, XmlSerializer> _serializers;

        private EventSerializerCollection() { }


        public EventSerializerCollection(params Type[] supportedTypes)
        {
            _serializers = new Dictionary<Type, XmlSerializer>();

            foreach(var t in supportedTypes)
            {
                _serializers.Add(t, new XmlSerializer(t));
            }
        }

        public XmlSerializer this[Type type]
        {
            get
            {
                if (!_serializers.ContainsKey(type))
                {
                    throw new NotSupportedException($"Serialization of type '{type}' is not supported.  Make sure the specified type is registered in the InitSerialzers method.");
                }

                return _serializers[type];
            }
        }
    }
}
