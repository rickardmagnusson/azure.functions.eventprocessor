﻿using System;
using System.IO;
using System.Net.Mail;
using System.Text;
using Common.Adapters;
using Common.Entities;
using log4net;

namespace Common
{
    public class Notify
    {
        public static string smtpServer, logFileName;
        public static string[] toAddress;
        public static bool AllowSendEmail = false;
        public string subject = "CRM Success Job notification from " + Environment.MachineName.ToUpperInvariant();

        //private static readonly ILog Log = LogManager.GetLogger("msmq-receive-logger");
        private string fromDisplayName = "CRM Notifications",
            fromAddress = Environment.MachineName.ToUpperInvariant() + "@eika.no",
            fileName = string.Empty;

        public bool SendEmail(string message)
        {
            return SendEmail(message, false);
        }

        public bool SendEmail(string message, bool sendLogFile)
        {
            MailMessage mail = new MailMessage();
            SmtpClient client = new SmtpClient();
            try
            {
                if (string.IsNullOrEmpty(message) || toAddress.Length == 0)
                    return false;

                if (AllowSendEmail)
                {
                    mail.From = new MailAddress(fromAddress, fromDisplayName);
                    mail.ReplyToList.Add("noreply@eika.no");
                    foreach (string addr in toAddress)
                    {
                        mail.To.Add(new MailAddress(addr));
                    }
                    mail.Subject = subject;
                    mail.Body = message;
                    if (File.Exists(logFileName) && sendLogFile)
                        mail.Attachments.Add(new Attachment(logFileName));
                    mail.IsBodyHtml = true;
                    client.Host = smtpServer;
                    client.Port = 25;
                    //sc.EnableSsl = true;
                    client.Send(mail);
                }
                else
                {
                    StringBuilder sb = new StringBuilder();
                    sb.AppendFormat("<html><h1>Date/Time Stamp: {0}</h1>", DateTime.Now.ToLocalTime());
                    sb.AppendFormat("<br/>From: " + fromAddress);
                    sb.AppendFormat("<br/>To: " + string.Join("; ", toAddress));
                    sb.AppendFormat("<br/>Subject: " + subject);
                    if (File.Exists(logFileName) && sendLogFile)
                        sb.AppendFormat("<br/>Attachments: " + logFileName);
                    sb.AppendFormat("<br/>Body: " + message);
                    sb.AppendFormat("</html>");
                    fileName = Path.Combine(@"c:\temp\msmqListerner_", string.Format("Email_{0:yyyyMMdd}_{1}.html", DateTime.Today, Guid.NewGuid())); ;
                    File.AppendAllText(fileName, sb.ToString());
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }

            return true;
        }

        public void SendNotification(EtlMaster crmEtl, TimeSpan ts, DateTime start)
        {
            string breakStr = "<br />";
            string status = Enum.GetName(typeof(CrmStatuscode), crmEtl.StatusCode);
            StringBuilder sb = new StringBuilder("<h1>Execution Summary for Data loader</h1>");
            sb.AppendFormat("===============================================================");
            sb.AppendFormat("{0}<a href=\"{1}\">LOAD ID: {2}</a>", breakStr, string.Format(CrmWsHelper.CrmEditRecordUrl, "cap_etlmaster", crmEtl.CRMGuid), crmEtl.LoadId);
            sb.AppendFormat("{0}MACHINE NAME: {1}", breakStr, Environment.MachineName.ToUpperInvariant());
            sb.AppendFormat("{0}JOB START: {1:dd-MM-yyyy hh:mm:ss}", breakStr, start);
            sb.AppendFormat("{0}Run Duration: {1} days, {2} hours, {3} minutes, {4} seconds", breakStr, ts.Days, ts.Hours, ts.Minutes, ts.Seconds);
            sb.AppendFormat("{0}<p>===========================ETL DETAILS=============================</p>", breakStr);
            sb.AppendFormat("{0}BANK: {1}", breakStr, crmEtl.BankNo);
            sb.AppendFormat("{0}ENTITY: {1}", breakStr, crmEtl.Entity);
            sb.AppendFormat("{0}RETRY CNT: {1}", breakStr, crmEtl.RetryCount);
            sb.AppendFormat("{0}ETL START: {1:dd-MM-yyyy hh:mm:ss}", breakStr, crmEtl.StartTime);
            if (crmEtl.EndTime.HasValue)
            {
                sb.AppendFormat("{0}ETL END: {1:dd-MM-yyyy hh:mm:ss}", breakStr, crmEtl.EndTime.Value);
                TimeSpan tEtl = crmEtl.EndTime.Value - crmEtl.StartTime;
                sb.AppendFormat("{0}ETL DURATION: {1} days, {2} hours, {3} minutes, {4} seconds", breakStr, tEtl.Days, tEtl.Hours, tEtl.Minutes, tEtl.Seconds);
            }
            sb.AppendFormat("{0}STATUS: {1}", breakStr, status);
            sb.AppendFormat("{0}RUN ONDEMAND: {1}", breakStr, crmEtl.RunOnDemand.ToString().ToUpperInvariant());
            sb.AppendFormat("{0}<p>===========================ETL STATS=============================</p>", breakStr);
            sb.AppendFormat("{0}FROM EVENT: {1}", breakStr, crmEtl.FromEventId);
            sb.AppendFormat("{0}TO EVENT: {1}", breakStr, crmEtl.ToEventId);
            sb.AppendFormat("{0}NUM FETCHED: {1}", breakStr, crmEtl.NumFetched);
            sb.AppendFormat("{0}NUM CREATED: {1}", breakStr, crmEtl.NumCreated);
            sb.AppendFormat("{0}NUM UPDATED: {1}", breakStr, crmEtl.NumUpdated);
            sb.AppendFormat("{0}NUM DELETED: {1}", breakStr, crmEtl.NumDeleted);
            sb.AppendFormat("{0}NUM ERROR: {1}", breakStr, crmEtl.NumError);
            sb.AppendFormat("{0}NUM IGNORED: {1}", breakStr, crmEtl.NumIgnored);
            sb.AppendFormat("{0}ERROR MESSAGE: {1}", breakStr, crmEtl.ErrorMessage);
            fromDisplayName = Environment.MachineName.ToUpperInvariant();
            subject = string.Format("Job completed with {3} for {0} with LoadId {1} and Bank No {2}", crmEtl.Entity, crmEtl.LoadId, crmEtl.BankNo, status);
            fileName = Path.Combine(@"c:\temp\msmqListerner_",
                $"Email_{DateTime.Today:yyyyMMdd}_{crmEtl.BankNo}_{crmEtl.Entity}_{crmEtl.LoadId}_{Guid.NewGuid()}.html"); ;
            SendEmail(sb.ToString());
        }
    }

}
