﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Common.Entities;

namespace Common.Helpers
{
    [Serializable]
    [XmlType]
    [DataContract]
    public class CustomerInfo
    {
        [Convert(In = "firstname", Out = "FirstName")]
        [XmlElement(ElementName = "FirstName")]
        [DataMember]
        public string FirstName { get; set; }

        [Convert(In = "lastname", Out = "LastName")]
        [XmlElement(ElementName = "LastName")]
        [DataMember]
        public string LastName { get; set; }

        [Convert(In = "address1", Out = "Address1")]
        [XmlElement(ElementName = "Address1")]
        [DataMember]
        public Address Address1 { get; set; }

        [Convert(In = "address2", Out = "Address2")]
        [XmlElement(ElementName = "Address2")]
        [DataMember]
        public Address Address2 { get; set; }

        [Convert(In = "address3", Out = "Address3")]
        [XmlElement(ElementName = "Address3")]
        [DataMember]
        public Address Address3 { get; set; }

        [Convert(In = "phone1", Out = "Phone1")]
        [XmlElement(ElementName = "Phone1")]
        [DataMember]
        public string Phone1 { get; set; }

        [Convert(In = "phone2", Out = "Phone2")]
        [XmlElement(ElementName = "Phone2")]
        [DataMember]
        public string Phone2 { get; set; }

        [Convert(In = "email1", Out = "Email1")]
        [XmlElement(ElementName = "Email1")]
        [DataMember]
        public string Email1 { get; set; }

        [Convert(In = "email2", Out = "Email2")]
        [XmlElement(ElementName = "Email2")]
        [DataMember]
        public string Email2 { get; set; }

        [Convert(In = "status", Out = "Status")]
        [XmlElement(ElementName = "Status")]
        [DataMember]
        public string Status { get; set; }

        [Convert(In = "timeofdeath", Out = "TimeOfDeath")]
        [XmlElement(ElementName = "TimeOfDeath")]
        [DataMember]
        public DateTime TimeOfDeath { get; set; }

        [Convert(In = "segment", Out = "Segment")]
        [XmlElement(ElementName = "Segment")]
        [DataMember]
        public string Segment { get; set; }

        [Convert(In = "industrialcode", Out = "IndustrialCode")]
        [XmlElement(ElementName = "IndustrialCode")]
        [DataMember]
        public string IndustrialCode { get; set; }

        [Convert(In = "industrialname", Out = "IndustrialName")]
        [XmlElement(ElementName = "IndustrialName")]
        [DataMember]
        public string IndustrialName { get; set; }

        [Convert(In = "sectorCode", Out = "SectorCode")]
        [XmlElement(ElementName = "SectorCode")]
        [DataMember]
        public string SectorCode { get; set; }

        [Convert(In = "sectorname", Out = "SectorName")]
        [XmlElement(ElementName = "SectorName")]
        [DataMember]
        public string SectorName { get; set; }

        [Convert(In = "shieldedcustomer", Out = "ShieldedCustomer")]
        [XmlElement(ElementName = "ShieldedCustomer")]
        [DataMember]
        public bool ShieldedCustomer { get; set; }
    }
}
