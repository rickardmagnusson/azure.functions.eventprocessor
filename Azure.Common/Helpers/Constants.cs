﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public partial class Team
    {
        public const int EntityTypeCode = 9;
        public const string EntityLogicalName = "team";
    }

    public partial class SystemUser
    { 
        public const int EntityTypeCode = 8;
        public const string EntityLogicalName = "systemuser";
    }

    public partial class Account
    {
        //public const int EntityTypeCode = 8;
        public const string EntityLogicalName = "account";
    }

    public partial class Contact
    {
        //public const int EntityTypeCode = 8;
        public const string EntityLogicalName = "contact";
    }
}
