﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Common.Adapters;

namespace Common.Helpers
{
    [Obsolete("Will be removed")]
    public static class ConfigLoader
    {

        private static void IntializeETLVariables(Dictionary<string, string> etlConfig)
        {
            //MSMQListenerService.pageSize = etlConfig.ContainsKey("pageSize") ? int.Parse(etlConfig["pageSize"]) : 50;
            //MSMQListenerService.batchSize = etlConfig.ContainsKey("numPages") ? int.Parse(etlConfig["numPages"]) * ETL.pageSize : ETL.pageSize;
            //MSMQListenerService.eventStoreUrl = etlConfig.ContainsKey("eventStoreUrl") ? etlConfig["eventStoreUrl"] : string.Empty;
            Notify.smtpServer = etlConfig.ContainsKey("smtpServer") ? etlConfig["smtpServer"] : "smtp.eika.no";
            Notify.toAddress = etlConfig.ContainsKey("toAddress") ? etlConfig["toAddress"].Split(';') : new string[] { "drift_crm@eika.no" };
            Notify.AllowSendEmail = etlConfig.ContainsKey("AllowSendEmail") && bool.Parse(etlConfig["AllowSendEmail"]);
        }

        internal static void InitializeEnvironment(Dictionary<string, string> etl_CrmSetting)
        {
            var serverNames = etl_CrmSetting["serverName"].Split(';');
            var crmOrg = etl_CrmSetting["crmOrg"];
            var useSSL = bool.Parse(etl_CrmSetting["useSSL"]);

            foreach (string serverName in serverNames)
            {
                string url;
                if (useSSL)
                    url = "https://" + serverName;
                else
                    url = "http://" + serverName;
                CrmWsHelper.CrmServers.Enqueue(url + "/" + crmOrg + "/XRMServices/2011/Organization.svc");
            }
            CrmWsHelper.CrmEditRecordUrl = (useSSL ? "https://" : "http://") + serverNames[0] + "/" + crmOrg + "/main.aspx?etn={0}&amp;id=%7b{1}%7d&amp;pagetype=entityrecord";
            CrmWsHelper.useThreading = bool.Parse(etl_CrmSetting["useThreading"]);
            CrmWsHelper.CrmDbConnString = etl_CrmSetting["CrmSqlConnectionString"];
            //CrmWsHelper._service = CrmWsHelper.GetCRMThreadService(Thread.CurrentThread.ManagedThreadId, (useSSL ? "https://" : "http://") + serverNames[0] + "/" + crmOrg + "/XRMServices/2011/Organization.svc");
        }

        private static Dictionary<string, string> LoadFromDb(string category, string EtlConnStr)
        {
            Dictionary<string, string> returnVal;
            string cmdText = @"Select [Property]
                                    ,[Value]
                                FROM [CRM_ETL].[dbo].[ETL_Config]
                                WHERE [Category] = 'CATEGORY'
                                AND [IsActive] = 1
                                ORDER BY [value]";
            cmdText = cmdText.Replace("CATEGORY", category);
            DataTable dt = new DataTable("result");
            // Create an SqlConnection from the provided connection string.
            using (SqlConnection cnn = new SqlConnection(EtlConnStr))
            {

                SqlCommand command = new SqlCommand(cmdText, cnn);
                cnn.Open();
                SqlDataAdapter da = new SqlDataAdapter(command);
                // this will query your database and return the result to your datatable
                da.Fill(dt);
                returnVal = dt.AsEnumerable().ToDictionary(row => row[0].ToString(),
                                       row => row[1].ToString());
                cnn.Close();
            }
            return returnVal;
        }
    }
}
