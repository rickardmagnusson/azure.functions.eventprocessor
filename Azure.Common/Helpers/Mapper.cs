﻿using System;
using System.Linq;
using Azure.Logging;
using Common.Entities;
using Common.Enums;
using Microsoft.Xrm.Sdk;

namespace Common.Helpers
{
    public class Mapper
    {
        private static SingletonLogger log = SingletonLogger.Instance();

        public static CrmCustomer CrmCustomer(Entity ent, CustomerExtra cust, bool isContact)
        {
            return new CrmCustomer
            {
                Id = ent.Id,
                EntityName = cust.EntityName,
                BankId = cust.BankId,
                CustomerNumber = isContact
                    ? Helper.GetStrIfValue(ent, "governmentid")
                    : Helper.GetStrIfValue(ent, "accountnumber"),

                FirstName = Helper.GetStrIfValue(ent, "firstname"),
                LastName = Helper.GetStrIfValue(ent, "lastname"),
                CompanyName = Helper.GetStrIfValue(ent, "name"),

                Mobilephone = Helper.GetStrIfValue(ent, "mobilephone"),
                Telephone1 = Helper.GetStrIfValue(ent, "telephone1"),
                Telephone2 = Helper.GetStrIfValue(ent, "telephone2"),
                Telephone3 = Helper.GetStrIfValue(ent, "telephone3"),

                EmailAddress1 = Helper.GetStrIfValue(ent, "emailaddress1"),
                EmailAddress2 = Helper.GetStrIfValue(ent, "emailaddress2"),

                Address1_Line1 = Helper.GetStrIfValue(ent, "address1_line1"),
                Address1_Line2 = Helper.GetStrIfValue(ent, "address1_line2"),
                Address1_Line3 = Helper.GetStrIfValue(ent, "address1_line3"),
                Address1_City = Helper.GetStrIfValue(ent, "address1_city"),
                Address1_PostalCode = Helper.GetStrIfValue(ent, "address1_postalcode"),
                Address1_StateOrProvince = Helper.GetStrIfValue(ent, "address1_stateorprovince"),
                Address1_Country = Helper.GetStrIfValue(ent, "address1_country"),
                
                KerneSourceId = Helper.GetStrIfValue(ent, "cap_sourceid"),
                NiceSkadeSourceId = Helper.GetStrIfValue(ent, "cap_nicesourceid"),
                NicePersonSourceId = Helper.GetStrIfValue(ent, "cap_nice2sourceid"),
                BanQsoftSourceId = Helper.GetStrIfValue(ent, "cap_banqsoftsourceid"),
                TradexSourceId = Helper.GetStrIfValue(ent, "cap_tradexsourceid"),

                CapEventdate = Helper.GetDateTimeIfValue(ent, "cap_eventdate"),
                Type = (CustomerTypeCode) ((OptionSetValue) ent["customertypecode"]).Value,
                StateCode = ((OptionSetValue) ent["statecode"]).Value,
                Source = (TerritoryCode) ((OptionSetValue) ent["territorycode"]).Value,
                OwnerReference = (EntityReference) ent["ownerid"]
            };
        }


        public static SalesOpportunity SalesOpportunity(Entity ent)
        {
            var opportunity = SalesOpportunityAll(ent);
            //opportunity.CustomerId = GetEntityReferenceIdIfValue(ent, "customerid"); 

            return opportunity;
        }

        //public static SalesOpportunity SalesOpportunity(Entity ent, Guid customerId)
        //{
        //    var opportunity = SalesOpportunityAll(ent);
        //    //opportunity.CustomerId = customerId;

        //    return opportunity;
        //}

        public static Case Case(Entity ent, Guid customerId)
        {
            var result = CaseAll(ent);
            result.CustomerId = customerId;

            return result;
        }

        private static SalesOpportunity SalesOpportunityAll(Entity ent)
        {
            var opportunity = new SalesOpportunity
            {
                Id = ent.Id,
                
                CustomerId = GetEntityReferenceIdIfValue(ent, "customerid"),
                StateCode = ((OptionSetValue)ent["statecode"]).Value,
                StatusCode = ((OptionSetValue)ent["statuscode"]).Value,

                Topic = GetStrIfValue(ent, "name"),
                Description = GetStrIfValue(ent, "description"),

                Source = (TerritoryCode) GetOptionSetValue(ent, "cap_source"), // ent.Contains("cap_source") ? (TerritoryCode)((OptionSetValue)ent["cap_source"]).Value : TerritoryCode.Default,
                Channel = (CapChannel)GetOptionSetValue(ent, "cap_channel"), // ent.Contains("cap_channel") ? (CapChannel)((OptionSetValue)ent["cap_channel"]).Value : CapChannel.Bankkontor,
                App = GetStrIfValue(ent, "cap_app"),
                InitialApp = GetStrIfValue(ent, "cap_initialapp"),
                ExternalEventId = GetStrIfValue(ent, "cap_externaleventid"),
                EventDate = GetDateTimeIfValue(ent, "cap_eventdate"),

                SubjectId = GetEntityReferenceIdIfValue(ent, "cap_subjectid"),  // ent.Contains("cap_subjectid") ? ((EntityReference)ent["cap_subjectid"]).Id : Guid.Empty,
                ProductId = GetEntityReferenceIdIfValue(ent, "cap_product"),  // ent.Contains("cap_product") ? ((EntityReference)ent["cap_product"]).Id : Guid.Empty,

                EstimatedRevenue = GetMoneyIfValue(ent, "estimatedvalue"), //ent.Contains("estimatedvalue") ? ((Money)ent["estimatedvalue"]).Value : 0,
                ActualRevenue = GetMoneyIfValue(ent, "actualvalue"), //ent.Contains("actualvalue") ? ((Money)ent["actualvalue"]).Value : 0,

                CreatedOn = (DateTime)ent["createdon"]
            };

            //log.Add($"SalesOpportunity:\n" +
            //    $"{opportunity.Id}," +
            //    $"{opportunity.StateCode}," +
            //    $"{opportunity.StatusCode}," +
            //    $"{opportunity.SubjectId}," +
            //    $"{opportunity.ProductId}");

            if (opportunity.Source == TerritoryCode.Crm)
            {
                opportunity.EventDate = opportunity.CreatedOn;
            }

            return opportunity;
        }

        private static Case CaseAll(Entity ent)
        {
            var opportunity = new Case
            {
                Id = ent.Id,
                StateCode = ((OptionSetValue)ent["statecode"]).Value,
                StatusCode = ((OptionSetValue)ent["statuscode"]).Value,

                Title = GetStrIfValue(ent, "title"),
                Description = GetStrIfValue(ent, "description"),

                Source = (TerritoryCode)GetOptionSetValue(ent, "cap_source"), 
                Channel = (CapChannel)GetOptionSetValue(ent, "cap_channel"), 
                App = GetStrIfValue(ent, "cap_app"),
                EventDate = GetDateTimeIfValue(ent, "cap_eventdate"),

                SubjectId = GetEntityReferenceIdIfValue(ent, "subjectid"),  
                ProductId = GetEntityReferenceIdIfValue(ent, "productid"),  

                // Missing fields to consider mapping:
                // casetypecode
                // ownerid
                // cap_colloborateflag
                // caseorigincode


                CreatedOn = (DateTime)ent["createdon"]
            };

            if (opportunity.Source == TerritoryCode.Crm)
            {
                opportunity.EventDate = opportunity.CreatedOn;
            }

            return opportunity;
        }



        public static KundeRegisterEvent MapOldToNewKundeRegisterEvent(EventMaster evt)
        {
            return new KundeRegisterEvent
            {
                CustomerEventSent = evt.updatedOn,
                LastUpdated = evt.updatedOn,
                CreatedOn = DateTime.MinValue,

                Source = Source.Kerne,
                EventType = EventType.Insert,
                CustomerNumber = evt.customerId,
                DistributorBankNumber = evt.bankId,
                OwnerBankNumber = evt.bankId,
                CustomerSourceId = "empty",

                CustomerType = 0,
                DepartmentName = "",
                DepartmentNumber = "",
                InsuranceType = InsuranceType.Undefined,
                EventId = $"{evt.eventId}",
                OwnerUsername = evt.createUpdate?.owner,

                CustomerInformation = new CustomerInfo
                {
                    FirstName = evt.createUpdate?.firstName,
                    LastName = !string.IsNullOrEmpty(evt.createUpdate?.lastName) ? evt.createUpdate?.lastName : evt.createUpdate?.companyName,
                    Address1 = new Address
                    {
                        AddressLine1 = evt.createUpdate?.address1,
                        AddressLine2 = evt.createUpdate?.address2,
                        AddressLine3 = evt.createUpdate?.address3,
                        City = evt.createUpdate?.city,
                        Country = evt.createUpdate?.country,
                        CountryCode = evt.createUpdate?.countryCode,
                        PostalCode = evt.createUpdate?.postCode,
                        State = "",
                    },
                    Address2 = null,
                    Address3 = null,
                    Email1 = evt.createUpdate?.email,
                    Email2 = null,
                    Phone1 = evt.createUpdate?.mobilePhone,
                    Phone2 = evt.createUpdate?.personType == 1 ? evt.createUpdate?.homePhone : evt.createUpdate?.workPhone,
                }
            };


        }
        public static KundeRegisterEvent MapOldToNewKundeRegisterEvent(DataWarehouseTempEvent evt)
        {
            var res = new KundeRegisterEvent
            {
                CustomerEventSent = evt.TimeStamp,
                LastUpdated = evt.TimeStamp,
                CreatedOn = DateTime.MinValue,

                Source = Source.Kerne,
                EventType = EventType.Insert,
                CustomerNumber = evt.CustomerId,
                DistributorBankNumber = evt.DivisionName,
                OwnerBankNumber = evt.DivisionName,
                CustomerSourceId = $"{evt.BankCustomerSourceId}",

                CustomerType = 0,
                DepartmentName = evt.CustomerDepartmentName,
                DepartmentNumber = evt.CustomerDepartment,
                InsuranceType = InsuranceType.Undefined,
                EventId = $"{evt.EventId}",
                OwnerUsername = evt.CustomerAccountManager,

                CustomerInformation = new CustomerInfo
                {
                    FirstName = evt.FirstName,
                    LastName = !string.IsNullOrEmpty(evt.LastName) ? evt.LastName : evt.CompanyName,
                    Address1 = new Address
                    {
                        AddressLine1 = evt.DwhAddress?.StreetName,
                        AddressLine2 = evt.DwhAddress?.HouseNumber,
                        AddressLine3 = "",
                        City = evt.DwhAddress?.PostalName,
                        Country = "",
                        CountryCode = evt.DwhAddress?.AddressCountryCode,
                        PostalCode = evt.DwhAddress?.PostalCode,
                        State = evt.DwhAddress?.StateOrProvince,
                    },
                    Address2 = null,
                    Address3 = null,
                    Email1 = evt.DwhEmail.EmailPrivate,
                    Email2 = "",
                    Phone1 = evt.DwhPhone.TelephoneMobile,
                    Phone2 = "",
                }
            };

            var entityName = Helper.GetEntityName(res);
            var isContact = entityName == EntityName.contact;

            res.CustomerInformation.Email1 = isContact ? evt.DwhEmail.EmailPrivate : evt.DwhEmail.EmailWork;
            res.CustomerInformation.Phone1 = isContact ? evt.DwhPhone.TelephoneMobile : evt.DwhPhone.TelephoneWork;


            return res;
        }
        public static KundeRegisterEvent MapOldToNewKundeRegisterEvent(CustomerEvent evt)
        {
            var source = Source.Undefined;
            var insuranceType = InsuranceType.Undefined;

            if (evt.Source == CustomerRegisterSource.NiceSkade)
            {
                source = Source.NiceSkade;
                insuranceType = InsuranceType.Skade;
            }
            else if (evt.Source == CustomerRegisterSource.NicePerson)
            {
                source = Source.NicePerson;
                insuranceType = InsuranceType.Person;
            }


            var addressList = evt.CustomerInformation?.Addresses;
            var address1 = new Address();
            var address2 = new Address();
            var address3 = new Address();

            if (addressList != null)
            {
                if(addressList.Count > 0) address1 = addressList[0];
                if(addressList.Count > 1) address2 = addressList[1];
                if(addressList.Count > 2) address3 = addressList[2];
            }

            return new KundeRegisterEvent
            {
                CustomerEventSent = evt.Timestamp,
                LastUpdated = evt.Timestamp,
                CreatedOn = DateTime.MinValue,

                Source = source,
                EventType = evt.EventType,
                CustomerNumber = evt.CustomerNumber,
                DistributorBankNumber = evt.BankNumber,
                OwnerBankNumber = "",
                CustomerSourceId = evt.CustomerSourceId,

                CustomerType = 0,
                DepartmentName = evt.DepartmentName,
                DepartmentNumber = evt.DepartmentNumber,
                InsuranceType = insuranceType,
                EventId = $"{evt.EventId}",
                OwnerUsername = evt.OwnerUsername,

                CustomerInformation = new CustomerInfo
                {
                    FirstName = evt.CustomerInformation?.FirstName,
                    LastName = !string.IsNullOrEmpty(evt.CustomerInformation?.LastName) ? evt.CustomerInformation?.LastName : evt.CustomerInformation?.CompanyName,
                    Address1 = address1,
                    Address2 = address2,
                    Address3 = address3,
                    Email1 = evt.CustomerInformation?.Email1,
                    Email2 = evt.CustomerInformation?.Email2,
                    Phone1 = evt.CustomerInformation?.Phone1,
                    Phone2 = evt.CustomerInformation?.Phone2,
                }
            };
        }



        #region Private Methods
        private static string GetStrIfValue(Entity ent, string field)
        {
            return ent.Contains(field) ? ent[field].ToString() : string.Empty;
        }
        private static DateTime GetDateTimeIfValue(Entity ent, string field)
        {
            return ent.Contains(field) ? (DateTime)ent[field] : DateTime.MinValue;
            //return ent.Contains(field) ? (DateTime)ent[field] + (DateTime.Now - DateTime.UtcNow) : DateTime.MinValue;
        }

        private static Guid GetEntityReferenceIdIfValue(Entity ent, string field)
        {
            return ent.Contains(field) ? ((EntityReference) ent[field]).Id : Guid.Empty;
        }
        private static decimal GetMoneyIfValue(Entity ent, string field)
        {
            return ent.Contains(field) ? ((Money)ent[field]).Value : 0;
        }

        private static int GetOptionSetValue(Entity ent, string field)
        {
            return ent.Contains(field) ? ((OptionSetValue)ent[field]).Value : 0;
        }
        #endregion

    }
}
