﻿using Common.Entities;
using System.Collections.Generic;

namespace Common.Models
{
    /// <summary>
    /// Needs to check if these are to be filled with values or not. If not remove this class
    /// </summary>
    public class InboxMessages
    {
        public List<MsmqMessageInbox> ToProcess { get; set; }
        public List<MsmqMessageInbox> Duplicates { get; set; }
    }


    public class LogItem : ITable, IEntity
    {
        public virtual int Id { get; set; }

        public virtual string Category { get; set; }

        public virtual string Message { get; set; }

        public virtual LevelType LevelType { get; set; }
    }

    public enum LevelType
    {
        Info,
        Error
    }


}
