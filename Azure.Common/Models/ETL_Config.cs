﻿
using Common.Entities;
using System.ComponentModel.DataAnnotations.Schema;

namespace Common.Models
{
    [Table("ETL_Config")]
    public class ETL_Config : ITable, IEntity
    {
        public virtual int Id { get; set; }
        public virtual string Category { get; set; }
        public virtual string Property { get; set; }
        public virtual string Value { get; set; }
        public virtual string Type { get; set; }
        public virtual bool IsActive { get; set; }
    }
}
