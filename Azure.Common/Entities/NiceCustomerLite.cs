﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Constants;
using Common.Enums;
using Common.Interfaces;
using Microsoft.Xrm.Sdk;

namespace Common.Entities
{
    // Not in use - Might not use
    /*
    public class NiceCustomerLite : IEntityMappable
    {
        public EntityName Entity;

        public Guid Id { get; set; }
        public string CustomerNumber { get; set; }
        public Guid OwningBusinessUnit { get; set; }
        public Guid OwnerId { get; set; }
        public Guid DefaultTeamId { get; set; }
        public Guid CapDepartmentId { get; set; }
        public bool OwnerBankDiffers { get; set; }
        public bool Reactivate { get; set; }
        public string NiceAgentNumber { get; set; }
        public TerritoryCode Source { get; set; }


        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CompanyName { get; set; }
        public string Address { get; set; }
        public string PostalCode { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }


        public Entity MapToEntity(Entity ent)
        {
            var isContact = EntityName.ToLower().Equals("contact");


            if (Id != Guid.Empty) ent.Id = Id;
            //ent.SetValueIfPresent("cap_customernumbernice", NiceAgentNumber);
            ent.SetValueIfPresent("cap_nicesourceid", NiceAgentNumber);

            if (isContact)
            {
                ent.SetValueIfPresent("governmentid", CustomerNumber);
                ent.SetValueIfPresent("firstname", FirstName);
                ent.SetValueIfPresent("lastname", LastName);
                ent.SetValueIfPresent("mobilephone", Phone);

                if (CustomerNumber.Length == 11)
                {
                    ent["gendercode"] = CustomerNumber.ElementAt(8) % 2 == 0
                                            ? new OptionSetValue(2)  // female
                                            : new OptionSetValue(1); // male
                }
            }
            else
            {
                ent.SetValueIfPresent("accountnumber", CustomerNumber);
                ent.SetValueIfPresent("name", CompanyName);
                ent.SetValueIfPresent("telephone1", Phone);
            }

            ent.SetValueIfPresent("emailaddress1", Email);

            ent.SetValueIfPresent("address1_line1", Address);
            ent.SetValueIfPresent("address1_city", City);
            ent.SetValueIfPresent("address1_country", Country);
            ent.SetValueIfPresent("address1_postalcode", PostalCode);
            ent.SetValueIfPresent("address1_stateorprovince", State);

            // TODO: check if its a valid bank change if we want NICE to be able to change bank as well
            // TODO: Set owner on update as well?
            if (Id == Guid.Empty)
            {
                if (OwnerId != Guid.Empty && !OwnerBankDiffers)
                {
                    ent["ownerid"] = new EntityReference(CrmConstants.SystemUserEntityName, OwnerId);
                }
                else
                {
                    ent["ownerid"] = new EntityReference(CrmConstants.TeamEntityName, DefaultTeamId);
                }
            }


            if (CapDepartmentId != Guid.Empty)
            {
                ent["cap_departmentid"] = new EntityReference(CrmConstants.DepartmentEntityName, CapDepartmentId);
            }


            if (Id == Guid.Empty || Reactivate || Source != TerritoryCode.Kjerne)
            {
                ent["territorycode"] = new OptionSetValue((int)TerritoryCode.Nice); // NICE Code
            }


            if (Reactivate)
            {
                ent["statecode"] = new OptionSetValue(0);
                ent["statuscode"] = new OptionSetValue(1);
            }

            ent["customertypecode"] = new OptionSetValue(1);

            return ent;
        }

        public string EntityName => Entity.ToString().ToLower();
    }
    */
}
    