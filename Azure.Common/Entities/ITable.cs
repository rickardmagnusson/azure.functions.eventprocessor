﻿
namespace Common.Entities
{

    /// <summary>
    /// Dummy interface to let NHibernate create a table without ClassMap
    /// Can be overridden with your own ClassMap to set specific settings for a table.
    /// </summary>
    public interface ITable : IEntity
    {
    }

}
