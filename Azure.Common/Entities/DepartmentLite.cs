﻿using System;
using Microsoft.Xrm.Sdk;

namespace Common.Entities
{
    public class DepartmentLite
    {
        public Guid CrmId { get; set; }
        public string CapName { get; set; }
        public string CapDepartmentNumber { get; set; }
        public Guid OwnerId { get; set; }
        public bool Reactivate { get; set; }

        public Entity MapToEntity(Entity targetEntity, DataWarehouseTempEvent evt)
        {
            targetEntity.SetValueReflect("cap_departmentnumber", evt.CustomerDepartment);
            targetEntity.SetValueReflect("cap_name", evt.CustomerDepartmentName);
            if(OwnerId != Guid.Empty) targetEntity["ownerid"] = new EntityReference("team", OwnerId);

            if (Reactivate)
            {
                targetEntity["statecode"] = new OptionSetValue(0);
                targetEntity["statuscode"] = new OptionSetValue(1);
            }

            return targetEntity;
        }
    }
}
