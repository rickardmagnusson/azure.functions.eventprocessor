﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Entities
{
    public class ConvertAttribute : Attribute
    {
        private string _prop;

        public bool IsValid(string inp)
        {
            if (inp.Contains(",")) { 
                string[] keys = inp.Split(',');
                foreach (var key in keys)
                {
                    if (key.ToLower().Equals(Out.ToLower()))
                    {
                        return true;
                    }
                }
            }

            return inp.ToLower().Equals(Out.ToLower());
        }

        public string In {
            get; set;
            //set
            //{
            //    if (!IsValid(value))
            //        throw new ArgumentException($"The value of '{value}' is not correct.");
            //    this._prop = value;
            //}
            //get { return this._prop; }
        }
        public string Out { get; set; }
    }
}
