﻿using System;

namespace Common.Entities
{
    public class EtlMaster : IEquatable<EtlMaster>
    {
        public string LoadId = string.Empty;
        public Guid CRMGuid = Guid.Empty;
        public string FromEventId = string.Empty;
        public string ToEventId = string.Empty;
        public DateTime StartTime = DateTime.Now;
        public DateTime? EndTime = null;
        public int StatusCode = 1;
        public string Entity = string.Empty;
        public string BankNo = string.Empty;
        public int RetryCount = 0;
        public int NumFetched = 0;
        public int NumCreated = 0;
        public int NumUpdated = 0;
        public int NumDeleted = 0;
        public int NumIgnored = 0;
        public int NumError = 0;
        public string ErrorMessage = string.Empty;
        public bool RunOnDemand = false;

        public bool Equals(EtlMaster other)
        {
            if (LoadId != other?.LoadId) return false;
            if (CRMGuid != other?.CRMGuid) return false;
            if (FromEventId != other.FromEventId) return false;
            if (ToEventId != other.ToEventId) return false;
            if ($"{StartTime:ddMMyyyyHHmmss}" != $"{other.StartTime:ddMMyyyyHHmmss}") return false;
            if (EndTime != other.EndTime) return false;
            if (StatusCode != other.StatusCode) return false;
            if (Entity != other.Entity) return false;
            if (BankNo != other.BankNo) return false;
            if (RetryCount != other.RetryCount) return false;
            if (NumFetched != other.NumFetched) return false;
            if (NumCreated != other.NumCreated) return false;
            if (NumUpdated != other.NumUpdated) return false;
            if (NumDeleted != other.NumDeleted) return false;
            if (NumIgnored != other.NumIgnored) return false;
            if (ErrorMessage != other.ErrorMessage) return false;

            // TODO: Compare Members and return false if not the same

            return true;
        }
    }
    public enum CrmStatuscode
    {
        Open = 1,
        InProgress = 809020000,
        Success = 809020001,
        Error = 809020002
    }
    public enum CrmOperation
    {
        Undefined = 0,
        Insert = 1,
        Update = 2,
        Delete = 3,
        Ignore = 4,
        Key = 5
    }

    public enum EtlStatus
    {
        Success = 1,
        Failed = 2,
        NotStarted = 3,
        Ignored = 4
    }
}
