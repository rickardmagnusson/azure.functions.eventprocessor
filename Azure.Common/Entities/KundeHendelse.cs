﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;


namespace Common.Entities
{
    [Serializable]
    [XmlType]
    public class KundeHendelse
    {
        [XmlElement]
        [JsonConvert(In = "id")]
        public string Uuid { get; set; }

        [XmlElement]
        [JsonConvert(In = "app.navn")]
        public string Applikasjon { get; set; }

        [XmlElement]
        [JsonConvert(In = "applikasjonVersjon.versjon")]
        public string ApplikasjonVersjon { get; set; }

        [XmlElement]
        [JsonConvert(In = "app.prosessomraade")]
        public ProcessArea ProsessOmraade { get; set; }

        [XmlElement]
        [JsonConvert(In = "fagsystem.name")]
        public string FagSystem { get; set; }

        [XmlElement]
        [JsonConvert(In = "fase.name")]
        public string Fase { get; set; }

        [XmlElement]
        [JsonConvert(In = "transaksjonsType.name")]
        public string TransaksjonsType { get; set; }

        [XmlElement]
        [JsonConvert(In = "produkt.name")]
        public string ProduktNavn { get; set; }

        [XmlElement]
        [JsonConvert(In = "produkt.produktKode")]
        public string ProduktKode { get; set; }

        [XmlElement]
        [JsonConvert(In = "handling.name")]
        public string Handling { get; set; }

        [XmlElement]
        [JsonConvert(In = "avtaleId")]
        public string AvtaleId { get; set; }

        [XmlElement]
        [JsonConvert(In = "kundeid.value")]
        public string KundeId { get; set; } // offentligid

        [XmlElement]
        [JsonConvert(In = "fagsystemKundenummer")]
        public string FagsystemKundenummer { get; set; } // kildeid

        [XmlElement]
        [JsonConvert(In = "brukersted.value")]
        public string Brukersted { get; set; } // bankregnr

        [XmlElement]
        [JsonConvert(In = "channel.name")]
        public string Channel { get; set; }

        [XmlElement]
        [JsonConvert(In = "innloggetRaadgiver.value")]
        public string InnloggetRaadgiver { get; set; }

        [XmlElement]
        [JsonConvert(In = "tidspunkt")]
        public DateTime Tidspunkt { get; set; }

        [XmlElement]
        [JsonConvert(In = "verdi")]
        public long Verdi { get; set; }

        [XmlElement]
        [JsonConvert(In = "tidligereVerdi")]
        public long TidligereVerdi { get; set; }

        [XmlElement]
        [JsonConvert(In = "annet")]
        public string Annet { get; set; }

    }
}
