﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Common.Entities
{
    [Serializable]
    [XmlType]
    public class SakHendelse
    {
        [XmlElement]
        [JsonConvert(In = "uuid")]
        public string Uuid { get; set; }

        [XmlElement]
        [JsonConvert(In = "applikasjon")]
        public string Applikasjon { get; set; }

        [XmlElement]
        [JsonConvert(In = "applikasjonVersjon.versjon")]
        public string ApplikasjonVersjon { get; set; }

        [XmlElement]
        [JsonConvert(In = "fagsystem.name")]
        public string FagSystem { get; set; }

        [XmlElement]
        [JsonConvert(In = "channel.name")]
        public string Channel { get; set; }

        [XmlElement]
        [JsonConvert(In = "app.prosessomraade")]
        public ProcessArea Prosessomraade { get; set; }

        [XmlElement]
        [JsonConvert(In = "brukersted.value")]
        public string Bankregnr { get; set; }

        [XmlElement]
        [JsonConvert(In = "raadgiverId.value")]
        public string RaadgiverId { get; set; }

        [XmlElement]
        [JsonConvert(In = "raadgiverNavn")]
        public string RaadgiverName { get; set; }

        [XmlElement]
        [JsonConvert(In = "sakUuid")]
        public string SakUuid { get; set; }

        [XmlElement]
        [JsonConvert(In = "sakId")]
        public string SakId { get; set; }

        [XmlElement]
        [JsonConvert(In = "sakHendelseType.name")]
        public string SakHendelsesType { get; set; }

        [XmlElement]
        [JsonConvert(In = "sakStatus")]
        public string SakStatus { get; set; }

        [XmlElement]
        [JsonConvert(In = "sakType")]
        public string SakType { get; set; }

        [XmlElement]
        [JsonConvert(In = "sakOppdatering")]
        public string SakOppdatering { get; set; }

        [XmlElement]
        [JsonConvert(In = "interessent")] //Not set in json
        public Interessent Interessent { get; set; }

        [XmlElement]
        [JsonConvert(In = "tidspunkt")]
        public DateTime Tidspunkt { get; set; }

        [XmlElement]
        [JsonConvert(In = "attributter")]
        public List<Attributt> Attributter { get; set; }

        [XmlElement]
        [JsonConvert(In = "produkt.navn")]
        public string ProduktNavn { get; set; }

        [XmlElement]
        [JsonConvert(In = "produkt.kode")]
        public string ProduktKode { get; set; }

        [XmlElement]
        [JsonConvert(In = "fagsystemKundenummer")]
        public string FagsystemKundenummer { get; set; }

        [XmlElement]
        [JsonConvert(In = "verdi")]
        public string Verdi { get; set; }

        [XmlElement]
        [JsonConvert(In = "tidligereverdi")]
        public string TidligereVerdi { get; set; }
    }



    [Serializable]
    [XmlType]
    public class Interessent
    {
        [XmlElement]
        [JsonConvert(In = "offentligId")]
        public string OffentligId { get; set; }
        [XmlElement]
        [JsonConvert(In = "rolle")]
        public string Rolle { get; set; }
        [XmlElement]
        [JsonConvert(In = "fornavn")]
        public string Fornavn { get; set; }
        [XmlElement]
        [JsonConvert(In = "etternavn")]
        public string Etternavn { get; set; }

        [JsonConvert(In = "kundeId")]
        public KundeId KundeId { get; set; }
    }


    public class KundeId
    {
        [XmlElement]
        [JsonConvert(In = "value")]
        public string Value { get; set; }
    }

    [Serializable]
    [XmlType]
    public class Attributt
    {
        [XmlElement]
        [JsonConvert(In = "attributter.key")]
        public string Key { get; set; }
        [XmlElement]
        [JsonConvert(In = "attributter.value")]
        public string Value { get; set; }
    }

}
