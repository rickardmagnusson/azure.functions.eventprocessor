﻿using System;
using Microsoft.Xrm.Sdk;
using Common.Helpers;
using Common.Enums;
using Common.Interfaces;

namespace Common.Entities
{
    public class CustomerLite : IEntityMappable, ITrackingGuid
    {
        #region Fields
        public EntityName entity;
        public Guid customerGuid = Guid.Empty;
        public string cap_eventid; //<eventId>33091</eventId>
        public string governmentid; //<customerId>24115340405</customerId>
        public DateTime cap_eventdate; //<updatedOn>2016-01-21T06:00:00.000+01:00</updatedOn>
        public string firstname; //<firstName>Kari Aarnes</firstName>
        public string lastname;//<lastName>Hovden</lastName> //companyname
        public string workphone;//workphone
        public string homephone; //homephone
        public string mobilephone; //<mobilePhone>40400968</mobilePhone>
        public string fax;
        public string emailaddress1; //<email>aarnes.hovden @tinnbank.no</email>
        public string address1_line1; //<address1>Skriugata 7</address1>
        public string address1_line2;
        public string address1_line3;
        public string address1_postalcode; //<postCode>3660</postCode>
        public string address1_city;//<city>RJUKAN</city>
        public string address1_country; //<country>NO</country>
        public string bankNo;
        public int statecode = 0;
        public bool reactivateCustomer;
        public bool deactivateCustomer;
        public EntityReference ownerid = null;
        public CrmOperation delta_flag;
        public EtlStatus status_flag = EtlStatus.NotStarted;
        public string errorMessage;
        #endregion 
        public string EntityName => entity.ToString().ToLower();

        public Guid Id => customerGuid;

        public Guid TrackingGuid { get; set; }

        public CustomerLite ReturnCopy()
        {
            CustomerLite newItem = new CustomerLite
            {
                entity = entity,
                address1_city = address1_city,
                address1_country = address1_country,
                address1_line1 = address1_line1,
                address1_line2 = address1_line2,
                address1_line3 = address1_line3,
                address1_postalcode = address1_postalcode,
                bankNo = bankNo,
                cap_eventdate = cap_eventdate,
                cap_eventid = cap_eventid,
                customerGuid = customerGuid,
                TrackingGuid = TrackingGuid,
                delta_flag = delta_flag,
                emailaddress1 = emailaddress1,
                errorMessage = errorMessage,
                fax = fax,
                firstname = firstname,
                governmentid = governmentid,
                lastname = lastname,
                mobilephone = mobilephone,
                ownerid = ownerid,
                statecode = statecode,
                status_flag = status_flag,
                workphone = workphone,
                homephone = homephone
            };
            return newItem;
        }

        public Entity MapToEntity(Entity targetEntity)
        {
            targetEntity["cap_eventdate"] = cap_eventdate != DateTime.MinValue ? cap_eventdate : DateTime.Now;
            
            #region Deactivate
            if (deactivateCustomer)
            {
                targetEntity["statecode"] = new OptionSetValue(1);
                targetEntity["statuscode"] = new OptionSetValue(2);
                return targetEntity;
            }
            #endregion

            #region Reactivate
            if (reactivateCustomer)
            {
                targetEntity["statecode"] = new OptionSetValue(0);
                targetEntity["statuscode"] = new OptionSetValue(1);
            }
            #endregion

            #region Source & Type
            targetEntity["customertypecode"] = new OptionSetValue((int)CustomerTypeCode.Customer);
            targetEntity["territorycode"] = new OptionSetValue(809020000);
            #endregion

            #region Owner & CustomerNumber
            if (delta_flag == CrmOperation.Key || delta_flag == CrmOperation.Insert)
            {
                if (ownerid != null)
                {
                    targetEntity["ownerid"] = ownerid;
                }

                if (entity == Enums.EntityName.contact)
                {
                    targetEntity.SetValueIfPresent("governmentid", governmentid);
                    if (!string.IsNullOrEmpty(governmentid) && governmentid.Length == 11)
                    {
                        targetEntity["gendercode"] = new OptionSetValue(Util.GetGenderCodeFromGovernmentId(governmentid));

                        var dateOfBirth = Helper.CalculateDoB(governmentid);
                        if (dateOfBirth != null) targetEntity["birthdate"] = dateOfBirth;
                    }
                }
                else if (entity == Enums.EntityName.account)
                {
                    targetEntity.SetValueIfPresent("accountnumber", governmentid);
                }

                if(delta_flag == CrmOperation.Key) return targetEntity;
            }

            #endregion

            #region Address & Email
            targetEntity.SetValueReflect("address1_fax", fax);
            targetEntity.SetValueReflect("emailaddress1", emailaddress1);
            targetEntity.SetValueReflect("emailaddress2", string.Empty);
            targetEntity.SetValueReflect("address1_line1", address1_line1);
            targetEntity.SetValueReflect("address1_line2", address1_line2);
            targetEntity.SetValueReflect("address1_line3", address1_line3);
            targetEntity.SetValueReflect("address1_postalcode", address1_postalcode);
            targetEntity.SetValueReflect("address1_city", address1_city);
            targetEntity.SetValueReflect("address1_country", address1_country);
            targetEntity.SetValueReflect("address1_county", string.Empty);
            #endregion

            #region Contact Specific
            if (entity == Enums.EntityName.contact)
            {
                targetEntity.SetValueReflect("firstname", Helper.GiveContactNameCrmStandard(firstname));
                targetEntity.SetValueReflect("lastname", Helper.GiveContactNameCrmStandard(lastname));
                //targetEntity.SetValueReflect("firstname", firstname);
                //targetEntity.SetValueReflect("lastname", lastname);
                targetEntity.SetValueReflect("mobilephone", mobilephone);
                //targetEntity.SetValueSafely("telephone1", homephone);
                //targetEntity.SetValueSafely("telephone2", workphone);
            }
            #endregion
            #region Account Specific
            if (entity == Enums.EntityName.account)
            {
                targetEntity.SetValueReflect("name", Helper.GiveAccountNameCrmStandard(string.Empty, lastname));
                //targetEntity.SetValueReflect("name", lastname);
                targetEntity.SetValueReflect("telephone1", workphone);
                if (string.IsNullOrEmpty(workphone)) targetEntity.SetValueReflect("telephone1", mobilephone);
                //targetEntity.SetValueSafely("telephone2", homephone);
                //targetEntity.SetValueSafely("telephone3", mobilephone);
            }
            #endregion

            return targetEntity;
        }
    }
}
