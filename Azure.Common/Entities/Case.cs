﻿using System;
using Common.Enums;

namespace Common.Entities
{
    public class Case
    {
        public Guid Id { get; set; }
        public int StateCode { get; set; }
        public int StatusCode { get; set; }
        public TerritoryCode Source { get; set; }
        public Guid CustomerId { get; set; }
        //public string BankNumber { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public ProcessArea ProductArea { get; set; }
        public decimal EstimatedRevenue { get; set; }
        public decimal ActualRevenue { get; set; }
        public CapChannel Channel { get; set; }
        public string App { get; set; }
        public string InitialApp { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime EventDate { get; set; }
        public Guid SubjectId { get; set; }
        public Guid ProductId { get; set; }
        public string ExternalEventId { get; set; }
    }
}
