﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Common.Enums;

namespace Common.Entities
{
    [Serializable]
    [XmlType]
    public class CustomerEvent
    {

        [XmlElement]
        public CustomerRegisterSource Source { get; set; }

        [XmlElement]
        public EventType EventType { get; set; }

        [XmlElement]
        public string EventId { get; set; }

        [XmlElement]
        public DateTime Timestamp { get; set; }

        [XmlElement]
        public bool IsDeltaChange { get; set; }

        [XmlElement]
        public InsuranceType InsuranceType { get; set; }

        [XmlElement]
        public NiceCustomerType NiceCustomerType { get; set; }

        [XmlElement]
        public string CustomerNumber { get; set; }

        [XmlElement]
        public string BankNumber { get; set; }

        [XmlElement]
        public string CustomerSourceId { get; set; }

        [XmlElement]
        public string OwnerUsername { get; set; }

        [XmlElement]
        public string DepartmentNumber { get; set; }


        [XmlElement]
        public string DepartmentName { get; set; }

        [XmlElement]
        public CustomerInformation CustomerInformation { get; set; }
    }


    [Serializable]
    [XmlType]
    public class CustomerInformation
    {
        [Convert(In = "firstname", Out = "FirstName")]
        [XmlElement(ElementName = "FirstName")]
        public string FirstName { get; set; }

        [Convert(In = "lastname", Out = "LastName")]
        [XmlElement(ElementName = "LastName")]
        public string LastName { get; set; }

        [Convert(In = "companyname", Out = "CompanyName")]
        [XmlElement(ElementName = "CompanyName")]
        public string CompanyName { get; set; }

        [Convert(In = "addresses", Out = "Addresses")]
        [XmlElement(ElementName = "Addresses")]
        public List<Address> Addresses { get; set; }

        [Convert(In = "phone1", Out = "Phone1")]
        [XmlElement(ElementName = "Phone1")]
        public string Phone1 { get; set; }

        [Convert(In = "phone2", Out = "Phone2")]
        [XmlElement(ElementName = "Phone2")]
        public string Phone2 { get; set; }

        [Convert(In = "phone3", Out = "Phone3")]
        [XmlElement(ElementName = "Phone3")]
        public string Phone3 { get; set; }

        [Convert(In = "email1", Out = "Email1")]
        [XmlElement(ElementName = "Email1")]
        public string Email1 { get; set; }

        [Convert(In = "email2", Out = "Email2")]
        [XmlElement(ElementName = "Email2")]
        public string Email2 { get; set; }

        [Convert(In = "email3", Out = "Email3")]
        [XmlElement(ElementName = "Email3")]
        public string Email3 { get; set; }

    }


    [Serializable]
    [XmlType]
    public class Address
    {
        [Convert(In = "type", Out = "Type")]
        [XmlElement(ElementName = "Type")]
        public string Type { get; set; }

        [Convert(In = "addressline1", Out = "AddressLine1")]
        [XmlElement(ElementName = "AddressLine1")]
        public string AddressLine1 { get; set; }

        [Convert(In = "addressline2", Out = "AddressLine2")]
        [XmlElement(ElementName = "AddressLine2")]
        public string AddressLine2 { get; set; }

        [Convert(In = "addressline3", Out = "AddressLine3")]
        [XmlElement(ElementName = "AddressLine3")]
        public string AddressLine3 { get; set; }

        [Convert(In = "addressline4", Out = "AddressLine4")]
        [XmlElement(ElementName = "FirstName")]
        public string AddressLine4 { get; set; }

        [Convert(In = "addressLine5", Out = "AddressLine5")]
        [XmlElement(ElementName = "AddressLine5")]
        public string AddressLine5 { get; set; }

        [Convert(In = "postalcode", Out = "PostalCode")]
        [XmlElement(ElementName = "PostalCode")]
        public string PostalCode { get; set; }

        [Convert(In = "state", Out = "State")]
        [XmlElement(ElementName = "State")]
        public string State { get; set; }

        [Convert(In = "county", Out = "County")]
        [XmlElement(ElementName = "County")]
        public string County { get; set; }

        [Convert(In = "city", Out = "City")]
        [XmlElement(ElementName = "City")]
        public string City { get; set; }

        [Convert(In = "country", Out = "Country")]
        [XmlElement(ElementName = "Country")]
        public string Country { get; set; }

        [Convert(In = "countryCode", Out = "CountryCode")]
        [XmlElement(ElementName = "CountryCode")]
        public string CountryCode { get; set; }
    }



    public class ProcessedCustomerResponse
    {
        public long MsmqInboxId { get; set; }
        public bool Success { get; set; }
        public string ErrorMessage { get; set; }
        public int RetryCount { get; set; }
        public bool IsIgnored { get; set; }
        public bool FailedProcessing { get; set; }
    }

    public class NiceRequestMetadata
    {
        public InsuranceType InsuranceType { get; set; }
        public EntityName EntityName { get; set; }
    }

}
