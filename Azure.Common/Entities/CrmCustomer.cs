﻿using System;
using Common.Enums;
using Microsoft.Xrm.Sdk;

namespace Common.Entities
{
    public class CrmCustomer
    {
        public EntityName EntityName { get; set; }

        public Guid Id { get; set; }
        public int StateCode { get; set; }
        public DateTime CapEventdate { get; set; }
        public string SourceId { get; set; }
        public string MainSourceId { get; set; }
        public string CustomerNumber { get; set; }
        public CustomerTypeCode Type { get; set; }
        public TerritoryCode Source { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CompanyName { get; set; }
        public string Address1_Line1 { get; set; }
        public string Address1_Line2 { get; set; }
        public string Address1_Line3 { get; set; }
        public string Address1_City { get; set; }
        public string Address1_PostalCode { get; set; }
        public string Address1_StateOrProvince { get; set; }
        public string Address1_Country { get; set; }
        public string Mobilephone { get; set; }
        public string Telephone1 { get; set; }
        public string Telephone2 { get; set; }
        public string Telephone3 { get; set; }
        public string EmailAddress1 { get; set; }
        public string EmailAddress2 { get; set; }
        public string Role { get; set; }
        public EntityReference OwnerReference { get; set; }
        public Guid BankId { get; set; }
        public bool IsActive { get; set; }

        
        public string KerneSourceId { get; set; }
        public string NiceSkadeSourceId { get; set; }
        public string NicePersonSourceId { get; set; }
        public string BanQsoftSourceId { get; set; }
        public string TradexSourceId { get; set; }


        //Helpers for getting data from customers
        public string contactid { get; set; }
        public string territorycode { get; set; }
        public string statuscode { get; set; }
        public string statecode { get; set; }
        public string owningbusinessunit { get; set; }
        public string governmentid { get; set; }
        public string cap_lastactivitycreatedon_date { get; set; }
        public string cap_lastcasecreatedon_date { get; set; }
        public string cap_lastoptycreatedon_date { get; set; }
        public string cap_sourceid { get; set; }
        public string cap_nicesourceid { get; set; }
        public string cap_nice2sourceid { get; set; }
        public string cap_banqsoftsourceid { get; set; }
        public string cap_tradexsourceid { get; set; }

        public string fullname { get; set; }

}
}
