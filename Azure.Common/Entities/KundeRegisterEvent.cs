﻿using System;
using System.Xml.Serialization;
using Common.Enums;
using Common.Helpers;

namespace Common.Entities
{
    [Serializable]
    [XmlType]
    public class KundeRegisterEvent
    {
        [XmlElement]
        public EventType EventType { get; set; }

        [XmlElement]
        public string EventId { get; set; }
        [XmlElement]
        public DateTime CustomerEventSent { get; set; }
        [XmlElement]
        public DateTime LastUpdated { get; set; }
        [XmlElement]
        public DateTime CreatedOn { get; set; }

        [XmlElement]
        public CustomerType CustomerType { get; set; }
        [XmlElement]
        public Source Source { get; set; }

        [XmlElement]
        public string CustomerNumber { get; set; }
        [XmlElement]
        public string DistributorBankNumber { get; set; }
        [XmlElement]
        public string OwnerBankNumber { get; set; }
        [XmlElement]
        public string CustomerSourceId { get; set; }
        
        [XmlElement]
        public string OwnerName { get; set; }
        [XmlElement]
        public string OwnerUsername { get; set; }
        [XmlElement]
        public string DepartmentNumber { get; set; }
        [XmlElement]
        public string DepartmentName { get; set; }

        [XmlElement]
        public CustomerInfo CustomerInformation { get; set; }


        [XmlElement]
        public InsuranceType InsuranceType { get; set; }
    }

}
