﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Azure.Logging;
using Common.Entities;
using Common.Enums;
using Common.Helpers;
using Common.Logging;
using Microsoft.Xrm.Sdk;

namespace Common
{
    #region Div classes
    public class CustomerExtra
    {
        public bool PreCheckIsValidated { get; set; }
        public Guid CustomerId { get; set; }
        public Guid ContributerCustomerId { get; set; }
        public Guid BankId { get; set; }
        public Guid OppositeBankId { get; set; }
        public Guid DefaultTeamId { get; set; }
        public Guid CustomerCardSystemuserOwnerId { get; set; }
        public Guid LoggedInUserOwnerId { get; set; }
        public Guid DepartmentId { get; set; }
        public bool IsActive { get; set; }
        public EntityName EntityName { get; set; }
        public string VendorBankNumber { get; set; }
        public string EventBankNumber { get; set; }
        public bool IsDirectCustomer { get; set; }
        public Guid SubjectId { get; set; }
        public Guid PriceListId { get; set; }
        //public Guid ProductId { get; set; }
        public ProductInfo Product { get; set; }
        public Guid VendorBankId { get; set; }
        public bool IsNewCustomer { get; set; }

        public string VendorRoleName { get; set; }
        public string HasCustomerRoleName { get; set; }
        public SakHendelseAttributes SakHendelseAttributes { get; set; }

    }

    public class SakHendelseAttributes
    {
        public DateTime Startdate { get; set; }
        public DateTime FollowupDate { get; set; }
        public string GoalOfInvestment { get; set; }
        public string ChosenPortefolio { get; set; }
        public string Result { get; set; }
        public string ResultComment { get; set; }
        public string TerraCustomer { get; set; }
        public string Telephone { get; set; }
        public string MobilePhone { get; set; }
        public string Email { get; set; }
        public string MeetingChannel { get; set; }
    }

    public class BankConnection
    {
        public Guid CustomerId { get; set; }
        public Guid BankConnectionId { get; set; }
        public Guid VendorRoleId { get; set; }
        public Guid HasCustomerRoleId { get; set; }
        public Guid OwnerId { get; set; }
        public InsuranceType InsuranceType { get; set; }
        public EntityName EntityName { get; set; }
        public bool Validated { get; set; }
    }

    public class ConnectionInfo
    {
        public Guid ConnectionId { get; set; }
        public Guid BankId { get; set; }
    }

    public class DepartmentInfo
    {
        public Guid DepartmentId { get; set; }
        public bool IsActive { get; set; }
        public string DepartmentName { get; set; }
    }

    public class CustomerActiveInfo
    {
        public Guid Id { get; set; }
        public bool IsActive { get; set; }
    }

    public class CustomerSourceIdInfo
    {
        public Guid Id { get; set; }
        public string SourceId { get; set; }
    }

    public class ProductInfo
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Number { get; set; }
        public bool Active { get; set; }


        public Guid VendorId { get; set; }
        public Guid SubjectId { get; set; }
        public Guid PrimaryUomId { get; set; }
        public decimal Value { get; set; }
    }
    public class OpportunityProduct
    {
        public Guid Id { get; set; }
        public Guid OpportunityId { get; set; }
        public Guid ProductId { get; set; }
        public Guid UnitId { get; set; }
    }

    #endregion


    public static class Helper
    {
        private static readonly MsmqLogger Log = MsmqLogger.CreateLogger("helper");
        private static SingletonLogger log = SingletonLogger.Instance();


        #region Source Specific Methods

        public static string GetCardMainSourceId(CrmCustomer crmCustomer)
        {
            switch (crmCustomer.Source)
            {
               case TerritoryCode.Kjerne:
                   return crmCustomer.KerneSourceId;
                case TerritoryCode.Nice:
                    return !string.IsNullOrEmpty(crmCustomer.NiceSkadeSourceId) ? crmCustomer.NiceSkadeSourceId : crmCustomer.NicePersonSourceId;
                case TerritoryCode.BanQsoft:
                    return crmCustomer.BanQsoftSourceId;
                case TerritoryCode.Tradex:
                    return crmCustomer.TradexSourceId;
               default:
                   return "";
            }
        }

        public static string GetSourceIdName(string source)
        {
            source = source.ToLower();

            if (source.Contains("kerne") || source.Contains("sdc")) return "cap_sourceid";
            if (source.Contains("nice"))
            {
                if (source.Contains("skade")) return "cap_nicesourceid";
                if (source.Contains("person")) return "cap_nice2sourceid";
            }
            if (source.Contains("tradex")) return "cap_tradexsourceid";
            if (source.Contains("banqsoft")) return "cap_banqsoftsourceid";

            return string.Empty;
        }
        public static string GetSourceIdName(CustomerRegisterSource source)
        {

            return SourceSpecific.NiceSourceIdMapper.ContainsKey(source)
                ? SourceSpecific.NiceSourceIdMapper[source]
                : string.Empty;

            //switch (source)
            //{
            //    case CustomerRegisterSource.Sdc:
            //        return "cap_sourceid";
            //    case CustomerRegisterSource.NiceSkade:
            //        return "cap_nicesourceid";
            //    case CustomerRegisterSource.NicePerson:
            //        return "cap_nice2sourceid";
            //}
            //return string.Empty;
        }
        public static string GetSourceIdName(Source source)
        {
            return SourceSpecific.KunderegisterSourceIdMapper.ContainsKey(source) 
                ? SourceSpecific.KunderegisterSourceIdMapper[source] 
                : string.Empty;

            //switch (source)
            //{
            //    case Source.Kerne:
            //        return "cap_sourceid";
            //    case Source.NiceSkade:
            //        return "cap_nicesourceid";
            //    case Source.NicePerson:
            //        return "cap_nice2sourceid";
            //    case Source.Tradex:
            //        return "cap_tradexsourceid";
            //    case Source.BanQsoft:
            //        return "cap_banqsoftsourceid";
            //}
        }
        
        public static EntityName GetEntityName(KundeRegisterEvent evt)
        {
            switch (evt.Source)
            {
                case Source.Kerne:
                    return GetEntityNameKerne(evt.CustomerNumber, evt.CustomerInformation?.FirstName, evt.CustomerInformation?.LastName);
                case Source.NiceSkade:
                    return GetEntityNameNice(evt.CustomerNumber, evt.CustomerType);
                case Source.NicePerson:
                    return GetEntityNameNice(evt.CustomerNumber, evt.CustomerType);
                case Source.Tradex:
                    return GetEntityNameRealCustomers(evt.CustomerNumber);
                case Source.BanQsoft:
                    return GetEntityNameRealCustomers(evt.CustomerNumber);
            }
            return EntityName.Undefined;
        }
        public static EntityName GetEntityName(KundeHendelse evt)
        {
            return GetEntityNameRealCustomers(evt.KundeId);
        }
        public static EntityName GetEntityName(SakHendelse evt)
        {
            if (evt.Interessent == null) return EntityName.Undefined;

            return GetEntityNameRealCustomers(evt.Interessent.OffentligId);
        }
        
        public static EntityName GetEntityName(EventMaster evt)
        {
            return GetEntityNameKerne(evt.customerId, evt.createUpdate?.firstName, evt.createUpdate?.lastName);
        }
        public static EntityName GetEntityName(DataWarehouseTempEvent evt)
        {
            return GetEntityNameKerne(evt.CustomerId, evt.FirstName, evt.LastName);
        }



        public static OptionSetValue GetTerritoryCode(string source)
        {
            // For kundehendelse (?)
            source = source.ToLower();

            if (source.Contains("kerne")) return new OptionSetValue((int)TerritoryCode.Kjerne);
            if (source.Contains("nice")) return new OptionSetValue((int)TerritoryCode.Nice);
            if (source.Contains("tradex")) return new OptionSetValue((int)TerritoryCode.Tradex);
            if (source.Contains("banqsoft")) return new OptionSetValue((int)TerritoryCode.BanQsoft);
            if (source.Contains("raadgiver_pluss")) return new OptionSetValue((int)TerritoryCode.RaagiverPluss);

            return new OptionSetValue((int)TerritoryCode.Crm);
        }
        public static OptionSetValue GetTerritoryCode(CustomerRegisterSource source)
        {
            var code = SourceSpecific.NiceTerritoryCodeMapper.ContainsKey(source)
                ? SourceSpecific.NiceTerritoryCodeMapper[source]
                : (int)TerritoryCode.Default;

            return new OptionSetValue(code);

            //switch (source)
            //{
            //    case CustomerRegisterSource.Sdc:
            //        return new OptionSetValue((int)TerritoryCode.Kjerne);
            //    case CustomerRegisterSource.NiceSkade:
            //        return new OptionSetValue((int)TerritoryCode.Nice);
            //    case CustomerRegisterSource.NicePerson:
            //        return new OptionSetValue((int)TerritoryCode.Nice);
            //}

            //return new OptionSetValue((int)TerritoryCode.Default);
        }
        public static OptionSetValue GetTerritoryCode(Source source)
        {
            var code = SourceSpecific.KunderegisterTerritoryCodeMapper.ContainsKey(source)
                ? SourceSpecific.KunderegisterTerritoryCodeMapper[source]
                : (int)TerritoryCode.Default;

            return new OptionSetValue(code);


            //switch (source)
            //{
            //    case Source.Kerne:
            //        return new OptionSetValue((int)TerritoryCode.Kjerne);
            //    case Source.NiceSkade:
            //        return new OptionSetValue((int)TerritoryCode.Nice);
            //    case Source.NicePerson:
            //        return new OptionSetValue((int)TerritoryCode.Nice);
            //    case Source.Tradex:
            //        return new OptionSetValue((int)TerritoryCode.Tradex);
            //    case Source.BanQsoft:
            //        return new OptionSetValue((int)TerritoryCode.BanQsoft);
            //}

            //return new OptionSetValue((int)TerritoryCode.Default);
        }


        //public static TerritoryCode GetTerritoryCode2(Source source)
        //{
        //    var code = SourceSpecific.KunderegisterTerritoryCodeMapper.ContainsKey(source)
        //        ? (TerritoryCode)SourceSpecific.KunderegisterTerritoryCodeMapper[source]
        //        : TerritoryCode.Default;

        //    return code;
        //}


        private static EntityName GetEntityNameKerne(string customerNumber, string firstname, string lastname)
        {
            if (customerNumber.Length == 9) return EntityName.account;
            if (customerNumber.Length == 11) return EntityName.contact;

            if (!string.IsNullOrEmpty(firstname) && !string.IsNullOrEmpty(lastname))
            {
                return EntityName.contact;
            }

            return EntityName.account;
        }

        private static EntityName GetEntityNameRealCustomers(string customerNumber)
        {
            if (customerNumber.Length == 11) return EntityName.contact;
            if (customerNumber.Length == 9) return EntityName.account;

            return EntityName.Undefined;
        }


        public static EntityName GetEntityNameNice(string customerNumber, NiceCustomerType niceCustomerType)
        {

            //var entityName = evt.CustomerNumber.Length == 11
            //    ? EntityName.contact
            //    : EntityName.account;
            if (niceCustomerType == NiceCustomerType.Privat)
            {
                return EntityName.contact;
            }

            if (niceCustomerType == NiceCustomerType.Naering)
            {
                return EntityName.account;
            }
            if (niceCustomerType == NiceCustomerType.Landbruk)
            {
                if (customerNumber.Length == 11)
                {
                    return EntityName.contact;
                }
                return EntityName.account;
            }


            if (customerNumber.Length == 11)
            {
                return EntityName.contact;
            }
            return EntityName.account;
        }
        private static EntityName GetEntityNameNice(string customerNumber, CustomerType customerType)
        {
            if (customerType == CustomerType.Privat)
            {
                return EntityName.contact;
            }

            if (customerType == CustomerType.Naering)
            {
                return EntityName.account;
            }
            if (customerType == CustomerType.Landbruk)
            {
                if (customerNumber.Length == 11)
                {
                    return EntityName.contact;
                }
                return EntityName.account;
            }


            if (customerNumber.Length == 11)
            {
                return EntityName.contact;
            }
            return EntityName.account;
        }

        #endregion

        #region Unsorted

        public static bool ApplicationIsFromRaadgiverPluss(string appName, Dictionary<string, string> appNames)
        {
            appName = appName.ToUpper();

            foreach (var app in appNames)
            {
                if (app.Value.ToUpper().Equals(appName))
                {
                    return app.Key.Contains("_raadgiver");
                }
            }

            return false;
        }
        public static bool ApplicationIsFromWeb(string appName, Dictionary<string, string> appNames)
        {
            appName = appName.ToUpper();

            foreach (var app in appNames)
            {
                if (app.Value.ToUpper().Equals(appName))
                {
                    return app.Key.Contains("_web");
                }
            }

            return false;
        }
        public static bool ApplicationIsFromSource(string appName, Dictionary<string, string> appNames)
        {
            appName = appName.ToUpper();

            foreach (var app in appNames)
            {
                if (app.Value.ToUpper().Equals(appName))
                {
                    return app.Key.Contains("_source");
                }
            }

            return false;
        }



        public static CrmCustomer UpdateToKjerneCustomerCardIfExist(CrmCustomer crmCustomer, List<CrmCustomer> crmCustomersLookup)
        {
            if (crmCustomer?.Source == TerritoryCode.Kjerne && crmCustomer.StateCode == 0) return crmCustomer;
            foreach (var current in crmCustomersLookup)
            {
                if (current.StateCode == 0)
                {
                    if (current.Source != TerritoryCode.Kjerne) continue;
                    return current;
                }
                break;
            }
            return crmCustomer;
        }

        public static void MapMissingFields(CustomerEvent to, CrmCustomer from)
        {
            if (@from == null) return;
            if (to.CustomerInformation == null) return;

            to.CustomerInformation.CompanyName = GetNewestField(to.CustomerInformation.CompanyName, @from.CompanyName);
            to.CustomerInformation.FirstName = GetNewestField(to.CustomerInformation.FirstName, @from.FirstName);
            to.CustomerInformation.LastName = GetNewestField(to.CustomerInformation.LastName, @from.LastName);

            if (to.CustomerInformation.Addresses.Count > 0)
            {
                var adr = to.CustomerInformation.Addresses.First();
                to.CustomerInformation.Addresses[0].AddressLine1 = GetNewestField(adr.AddressLine1, @from.Address1_Line1);
                to.CustomerInformation.Addresses[0].AddressLine2 = GetNewestField(adr.AddressLine2, @from.Address1_Line2);
                to.CustomerInformation.Addresses[0].AddressLine3 = GetNewestField(adr.AddressLine3, @from.Address1_Line3);
                to.CustomerInformation.Addresses[0].City = GetNewestField(adr.City, @from.Address1_City);
                to.CustomerInformation.Addresses[0].State = GetNewestField(adr.State, @from.Address1_StateOrProvince);
                to.CustomerInformation.Addresses[0].PostalCode = GetNewestField(adr.PostalCode, @from.Address1_PostalCode);
                to.CustomerInformation.Addresses[0].Country = GetNewestField(adr.Country, @from.Address1_Country);
            }

            to.CustomerInformation.Email1 = GetNewestField(to.CustomerInformation.Email1, @from.EmailAddress1);
            to.CustomerInformation.Email2 = GetNewestField(to.CustomerInformation.Email2, @from.EmailAddress2);

            to.CustomerInformation.Phone1 = GetNewestField(to.CustomerInformation.Phone1, @from.EntityName == EntityName.contact ? @from.Mobilephone : @from.Telephone1);
            to.CustomerInformation.Phone2 = GetNewestField(to.CustomerInformation.Phone2, @from.EntityName == EntityName.contact ? @from.Telephone1 : @from.Telephone2);
            to.CustomerInformation.Phone3 = GetNewestField(to.CustomerInformation.Phone3, @from.EntityName == EntityName.contact ? @from.Telephone2 : @from.Telephone3);
        }


        //public static OptionSetValue GetChannel(string channel)
        //{
        //    channel = channel.ToLower();

        //    if (channel.Contains("direkte")) return new OptionSetValue(0);
        //    if (channel.Contains("mobilbank")) return new OptionSetValue(2);
        //    if (channel.Contains("web")) return new OptionSetValue(4);

        //    return new OptionSetValue(0);
        //}
        
        public static OptionSetValue GetChannel(string channel)
        {
            channel = channel.ToLower();
            if (string.IsNullOrEmpty(channel)) channel = "ukjent";
            else if(channel.Contains("direkte")) channel = "bankkontor";
            else if (channel.Contains("kontor")) channel = "bankkontor";
            
            return new OptionSetValue((int)EnumUtils.Parse<CapChannel>(channel));
        }

        public static string GetTitleForIncident(string action, string transactionType, string productName)
        {
            var actionCheck = action.ToLower();

            return actionCheck.Equals("enkelthendelse")
                ? $"{transactionType} {productName}"
                : $"{action} {transactionType} {productName}";
        }


        /// <summary>
        /// Adds the title to a "transactionType"
        /// </summary>
        /// <param name="action"></param>
        /// <param name="transactionType"></param>
        /// <param name="productName"></param>
        /// <param name="source"></param>
        /// <returns></returns>
        public static string GetTitleForKundehendelse(string action, string transactionType, string productName, string source)
        {
            var actionCheck = action.ToLower();

            log.Add($"GetTitleForKundehendelse: {action}, {transactionType}, {productName}, {source}");

            return actionCheck.Equals("enkelthendelse") ? $"{CapitalizeFirstLetter(transactionType)} {GetProductNameForTitle(productName, source)}".Trim() 
                : $"{CapitalizeFirstLetter(action)} {transactionType.ToLower()} {GetProductNameForTitle(productName, source)}".Trim();
        }


        private static string GetProductNameForTitle(string productName, string source)
        {
            log.Add($"Trying to set Title: {productName} :: {source}");
            if (string.IsNullOrEmpty(productName)) return productName;

            return source.ToLower().Contains("nice") ? productName.ToLower() : productName;
        }

        public static string GetVendorBankNumber(string source)
        {
            source = source.ToLower();

            if (source.Contains("nice"))
            {
                if (source.Contains("skade")) return "0770";
                if (source.Contains("person")) return "0771";
            }
            if (source.Contains("tradex")) return "0501";
            if (source.Contains("raadgiver_pluss")) return "0501";

            return string.Empty;
        }

        

        public static EntityReference SetOwner(CustomerExtra cust)
        {
            log.Add("SetOwner...");
            if (cust.CustomerCardSystemuserOwnerId != Guid.Empty)
            {
                log.Add("Has Owner is true");
                return new EntityReference(EntityName.systemuser.ToString(), cust.CustomerCardSystemuserOwnerId);
            }

            log.Add($"Has Owner is false, enter with {EntityName.team.ToString()} : {cust.DefaultTeamId}");
            return new EntityReference(EntityName.team.ToString(), cust.DefaultTeamId);
        }

        public static EntityReference SetOwnerWithLoggedInUser(CustomerExtra cust)
        {
            if (cust.LoggedInUserOwnerId != Guid.Empty)
            {
                return new EntityReference(EntityName.systemuser.ToString(), cust.LoggedInUserOwnerId);
            }

            return new EntityReference(EntityName.team.ToString(), cust.DefaultTeamId);
        }

        public static bool ValidateKundehendelseAppNames(Dictionary<string, string> kundehendelseAppNames)
        {
            foreach (var hastoBeThere in Enum.GetValues(typeof(KundehendelseAppName)))
            {
                if (!kundehendelseAppNames.Keys.Contains(hastoBeThere.ToString())) {
                    log.Add($"ValidateKundehendelseAppNames is false");
                    return false;
                }
            }
            log.Add($"ValidateKundehendelseAppNames is true");
            return true;
        }

        public static string GetApplicationNameForWeb(Dictionary<string, string> kundehendelseAppNames, string appName)
        {
            if (!kundehendelseAppNames.ContainsValue(appName.ToUpper())) return string.Empty;

            var appNameSourceKey = kundehendelseAppNames.Where(e => e.Value.Equals(appName.ToUpper())).Select(f => f.Key).ToList().FirstOrDefault();
            if (string.IsNullOrEmpty(appNameSourceKey) || !appNameSourceKey.Contains("_")) return string.Empty;

            var split = appNameSourceKey.Split('_');
            var appNameWebKey = string.Empty;
            for (var i = 0; i < split.Length - 1; i++) appNameWebKey += split[i] + "_";
            appNameWebKey += "web";

            return kundehendelseAppNames.ContainsKey(appNameWebKey) ? kundehendelseAppNames[appNameWebKey] : string.Empty;
        }
        public static string GetApplicationNameForSource(Dictionary<string, string> kundehendelseAppNames, string appName)
        {
            if (!kundehendelseAppNames.ContainsValue(appName.ToUpper())) return string.Empty;

            var appNameWebKey = kundehendelseAppNames.Where(e => e.Value.Equals(appName.ToUpper())).Select(f => f.Key).ToList().FirstOrDefault();
            if (string.IsNullOrEmpty(appNameWebKey) || !appNameWebKey.Contains("_")) return string.Empty;

            var split = appNameWebKey.Split('_');
            var appNameSourceKey = string.Empty;
            for (var i = 0; i < split.Length - 1; i++) appNameSourceKey += split[i] + "_";
            appNameSourceKey += "source";

            return kundehendelseAppNames.ContainsKey(appNameSourceKey) ? kundehendelseAppNames[appNameSourceKey] : string.Empty;
        }

        /// <summary>
        /// THIS IS REALLY BAD!!!!
        /// </summary>
        /// <param name="prosessOmraade"></param>
        /// <param name="fagSystem"></param>
        /// <returns></returns>
        public static string MapSubject(ProcessArea prosessOmraade, string fagSystem)
        {
            var kilde = fagSystem.ToLower();

            if (kilde.Contains("nice"))
            {
                if (kilde.Contains("skade")) return "skadeforsikring";
                if (kilde.Contains("person")) return "personforsikring";
            }

            return prosessOmraade.ToString();
        }
        
        public static string MapSubject(string fagSystem)
        {
            var kilde = fagSystem.ToLower();

            if (kilde.Contains("nice"))
            {
                if (kilde.Contains("skade")) return "skadeforsikring";
                if (kilde.Contains("person")) return "personforsikring";
            }

            if (kilde.Contains("tradex")) return ProcessArea.Sparing.ToString();

            return string.Empty;
        }

        
        public static SakHendelseAttributes MapSakhendelseAttributes(SakHendelse evt)
        {
            return new SakHendelseAttributes
            {
                Startdate = SetDateTimeSafely(evt.Attributter, "Startdato"),
                FollowupDate = SetDateTimeSafely(evt.Attributter, "Oppfølgingsdato"),
                GoalOfInvestment = SetStrSafely(evt.Attributter, "Målsetting med invistering"),
                ChosenPortefolio = SetStrSafely(evt.Attributter, "Valgt portefølje"),
                Result = SetStrSafely(evt.Attributter, "Resultat"),
                ResultComment = SetStrSafely(evt.Attributter, "Kommentar resultat"),
                TerraCustomer = SetStrSafely(evt.Attributter, "Terrakunde"),
                Telephone = SetStrSafely(evt.Attributter, "Telefon"),
                MobilePhone = SetStrSafely(evt.Attributter, "Mobil"),
                Email = SetStrSafely(evt.Attributter, "E-post"),
                MeetingChannel = SetStrSafely(evt.Attributter, "Møtekanal"),
            };
        }

        private static string SetStrSafely(List<Attributt> attributes, string field)
        {
            field = field.ToLower();
            foreach (var item in attributes)
            {
                if (item.Key.ToLower().Equals(field)) return item.Value;
            }
            return "";
        }
        private static DateTime SetDateTimeSafely(List<Attributt> attributes, string field)
        {
            field = field.ToLower();
            var date = DateTime.MinValue;
            foreach (var item in attributes)
            {
                if (item.Key.ToLower().Equals(field))
                {
                    DateTime.TryParse(item.Value, out date);
                    break;
                }
            }
            return date;
        }



        public static void SetLogAndResponse(string msg, ProcessedCustomerResponse response, LogLevel level, MsmqLogger log, bool setIgnore = false)
        {
            switch (level)
            {
                case LogLevel.Debug:
                    log.Debug(msg);
                    break;
                case LogLevel.Info:
                    log.Info(msg);
                    break;
                case LogLevel.Warn:
                    log.Warn(msg);
                    break;
                case LogLevel.Error:
                    log.Error(msg);
                    break;
                case LogLevel.Fatal:
                    log.Fatal(msg);
                    break;
            }

            response.ErrorMessage = msg;
            if(setIgnore) response.IsIgnored = true;
        }


        //??? Why not make this a separate class.
        //There are lot of examples of validating SSN
        public static DateTime? CalculateDoB(string govId)
        {
            //born 1854 - 1899: allocated from series 749 - 500
            //born 2000 - 2039: allocated from series 999 - 500
            //born 1900 - 1999: allocated from series 499 - 000
            //born 1940 - 1999: also allocated from series 999 - 900

            try
            {
                #region Validate day and month
                if (string.IsNullOrWhiteSpace(govId)) return null;
                if (govId.Length != 11)
                {
                    //Logger.Info($"Birthdate not calculated for {govId}. Reason - govId has more than 11 deigits.");
                    return null;
                }

                var firstDigit = Convert.ToInt32(govId.Substring(0, 1));
                if (firstDigit >= 8)
                {
                    //Logger.Info($"Birthdate not calculated for {govId}. Reason - govId has invalid first digit.");
                    return null;
                }

                var dayDigit = govId.Substring(0, 2);

                if (firstDigit >= 4)
                {
                    //D number
                    var firstDayDigit = Convert.ToInt32(govId.Substring(0, 1)) - 4;
                    var secondDayDigit = Convert.ToInt32(govId.Substring(1, 1));
                    dayDigit = $"{firstDayDigit}{secondDayDigit}";
                }

                var monthDigit = Convert.ToInt32(govId.Substring(2, 2));
                if (monthDigit > 12)
                {
                    //Logger.Info($"Birthdate not calculated for {govId}. Reason - govId has invalid month digit.");
                    return null;
                }
                #endregion

                var yearDigitPart2 = govId.Substring(4, 2);
                var individualNum = Convert.ToInt32(govId.Substring(6, 3));
                string year = $"19{yearDigitPart2}";

                if (54 <= Convert.ToInt32(yearDigitPart2) && Convert.ToInt32(yearDigitPart2) <= 99)
                {
                    if (500 <= individualNum && individualNum <= 749)
                    {
                        year = $"18{yearDigitPart2}";
                    }
                }

                if (0 <= Convert.ToInt32(yearDigitPart2) && Convert.ToInt32(yearDigitPart2) <= 39)
                {
                    if (500 <= individualNum && individualNum <= 999)
                    {
                        year = $"20{yearDigitPart2}";
                    }
                }

                return (new DateTime(Convert.ToInt32(year), monthDigit, Convert.ToInt32(dayDigit)));
            }
            catch (Exception ex)
            {
                Log.Warn($"Birthdate not calculated for '{govId}'. Reason - invalid date. Error: '{ex.Message}'");
                return null;
            }
        }

        public static string ConvertToTitleCasing(string input)
        {
            return System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(input.ToLower()).Trim();
        }
        public static string CapitalizeFirstLetter(string input)
        {
            if (string.IsNullOrEmpty(input)) return input;
            return $"{input.Substring(0, 1).ToUpper()}{input.Substring(1).ToLower()}";
        }

        public static string GiveContactNameCrmStandard(string name)
        {
            if (string.IsNullOrEmpty(name)) return name;

            if (!IsAllCaps(name)) return name.Trim();
            return System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(name.ToLower()).Trim();
        }
        public static string GiveAccountNameCrmStandard(string firstname, string lastname)
        {
            var potentialFixCompanyName = $"{firstname} {lastname}".Trim();
            if (string.IsNullOrEmpty(potentialFixCompanyName)) return "Navn ikke spesifisert";

            if (!IsAllCaps(potentialFixCompanyName)) return potentialFixCompanyName.Trim();

            potentialFixCompanyName = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(potentialFixCompanyName.ToLower());
            return FixCompanyNamesEndingWithAs(potentialFixCompanyName).Trim();
        }


        public static string GetDescription(KundeHendelse evt)
        {
            var phase = EnumUtils.Parse<HendelseStatusEnum>(evt.Fase);
            var source = EnumUtils.Parse<HendelseSource>(evt.FagSystem);
            log.Add($"GetDescription: {phase}, {source}");

            //var phase = evt.Fase.ToLower();
            //var source = evt.FagSystem.ToLower();

            if (phase == HendelseStatusEnum.Avsluttet)
            {
                return GetDescriptionEarlierValue(evt.TidligereVerdi, source);
            }
            return GetDescriptionWithValues(evt.Verdi, evt.TidligereVerdi, source);

            //if(phase.Equals("endret") || phase.Equals("annet"))
            //{
            //    return GetDescriptionWithValues(evt.Verdi, evt.TidligereVerdi, source);
            //}
            //if (phase.Equals("salg"))
            //{
            //    return string.Empty;
            //}
            //if (source.Contains("nice"))
            //{
            //    return GetDescriptionWithValues(evt.Verdi, evt.TidligereVerdi, source);
            //}

            //return string.Empty;
        }
        private static string GetDescriptionWithValues(long verdi, long tidVerdi, HendelseSource source)
        {
            if (source == HendelseSource.TRADEX)
            {
                return $"Nytt sparebeløp: {verdi}\n" +
                       $"Tidligere sparebeløp: {tidVerdi}";
            }

            if (source == HendelseSource.NICE_SKADE || source == HendelseSource.NICE_PERSON)
            {
                return $"Ny premie: {verdi}\n" +
                       $"Tidligere premie: {tidVerdi}";
            }

            return string.Empty;
        }
        private static string GetDescriptionEarlierValue(long tidVerdi, HendelseSource source)
        {
            if (source == HendelseSource.TRADEX)
            {
                return $"Tidligere sparebeløp: {tidVerdi}";
            }
            if (source == HendelseSource.NICE_SKADE || source == HendelseSource.NICE_PERSON)
            {
                return $"Tidligere premie: {tidVerdi}";
            }

            return string.Empty;
        }



        public static string GetStrIfValue(Entity ent, string field)
        {
            return ent.Contains(field) ? ent[field].ToString() : string.Empty;
        }
        public static DateTime GetDateTimeIfValue(Entity ent, string field)
        {
            return ent.Contains(field) ? (DateTime)ent[field] : DateTime.MinValue;
            //return ent.Contains(field) ? (DateTime)ent[field] + (DateTime.Now - DateTime.UtcNow) : DateTime.MinValue;
        }

        #endregion

        #region Private Methods

        private static string FixCompanyNamesEndingWithAs(string companyName)
        {
            if (companyName.EndsWith(" As")) companyName = companyName.Substring(0, companyName.Length - 3) + " AS";

            return companyName;
        }
        private static bool IsAllCaps(string msg)
        {
            return msg.ToUpper().Equals(msg);
        }

        private static string GetNewestField(string possiblyChangedField, string oldField)
        {
            if (possiblyChangedField != null && possiblyChangedField == string.Empty) return string.Empty;

            if (!string.IsNullOrEmpty(possiblyChangedField))
            {
                return possiblyChangedField;
            }

            if (!string.IsNullOrEmpty(oldField))
            {
                return oldField;
            }
            return string.Empty;

            //return !string.IsNullOrEmpty(possiblyChangedField) ? possiblyChangedField : oldField;
        }

        #endregion
    }
}
