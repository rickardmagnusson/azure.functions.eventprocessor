﻿using Azure.Functions.Extensions.Converters;
using Azure.Functions.Extensions.Converters.SakHendelseEvent;
using Common.Entities;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using SakHendelse = Common.Entities.SakHendelse;

namespace Converter
{
    /// <summary>
    /// Test to see if a json object can be casted or flatteded into a class of type T
    /// </summary>
    class Program
    {
        static Action<object> P = s => { Console.ForegroundColor = ConsoleColor.Green; Console.WriteLine(s); Console.ResetColor(); };
        static Action<object> I = s => { Console.ForegroundColor = ConsoleColor.Yellow; Console.WriteLine(s); Console.ResetColor(); };

        //Test json, also test if a key exists, if it not exist in class, it should be ignored.
        static string TestJson = @"
        {
            'uuid' : '1',
            'amount' : '10',
            'customer' : {
                'address' : 'My street one',
                'city' : 'My city'
             },
        }";


        static string sakhendelse = @"
          {
          'uuid': 'ff260d80-a278-494b-859e-8b0cc0e931d0',
          'app': {
            'prosessomraade': 'KREDITT',
            'internId': 2,
            'navn': 'KREDITT_RAADGIVER_DOMENE'
          },
          'fagsystem': {
            'name': 'KERNE'
          },
          'channel': {
            'name': 'INTRAWEB'
          },
          'brukersted': {
            'value': '2230'
          },
          'raadgiverId': {
            'value': 'I983010'
          },
          'raadgiverNavn': 'Eika user 2',
          'sakId': '24934',
          'sakUuid': '428f058e-4452-4a88-91af-8d9a1cfcec0f',
          'sakHendelseType': {
            'name': 'OPPRETTET'
          },
          'sakOppdatering': null,
          'sakStatus': 'NY',
          'sakFase': 'AVTALE',
          'sakType': 'FORBRUKER',
          'interessent': {
            'kundeId': {
              'value': '21105535616'
            },
            'fornavn': 'Michael',
            'etternavn': 'Essien',
            'rolle': 'HOVEDLANTAKER'
          },
          'tidspunkt': '2019-03-04T13:46:41.665Z',
          'resultat': null,
          'applikasjonVersjon': {
            'versjon': 'DEV.1579'
          },
          'attributter': {}
        }

        ";


    static void Main(string[] args)
        {
            I("Start...");
            var t = JsonConvert.DeserializeObject<SakHendelse>(sakhendelse);

            Type type = t.GetType();
            PropertyInfo[] properties = type.GetProperties();
            foreach (PropertyInfo property in properties)
            {
                Console.WriteLine("{0} = {1}", property.Name, property.GetValue(t, null));
            }

            I("End");
            Console.ReadLine();
        }
    }


    public static class Convert
    {
        static Action<object> P = s => { Console.ForegroundColor = ConsoleColor.Green; Console.WriteLine(s); Console.ResetColor(); };
       

        public static T Cast<T>(this object myobj)
        {
            return JsonConvert.DeserializeObject<T>(myobj.ToString());
        }


        public static T ConvertTo<T>(string json)
        {
            P("Convert...");

            var dict = GetProperties<T>();
            foreach (var i in dict.Keys)
                P($"{i} : {dict[i]}");

            var kh = JObject.Parse(json);
            var newClass = new JObject();

            foreach (var k in dict.Keys)
            {
                if (dict[k].Contains(','))
                {
                    foreach (string a in dict[k].Split(','))
                    {
                        JToken token = null;
                        try
                        {
                            token = kh.SelectToken(a);
                            
                            if (token != null) {
                                newClass.Add(k, token);
                            }
                        }
                        catch
                        {
                            P($"{a}");
                        }
                    }
                }
                else
                {
                   
                    newClass.Add(k, kh.SelectToken(dict[k]));
                }
            }

            return Cast<T>(newClass);
        }
       

        public static Dictionary<string, string> GetProperties<T>()
        {
            var _dict = new Dictionary<string, string>();
            var props = typeof(T).GetProperties();

            foreach (PropertyInfo prop in props)
            {
               
                object[] attrs = prop.GetCustomAttributes(true);
               
                foreach (object attrib in attrs)
                {
                    var attr = attrib as Common.JsonConvertAttribute;
                    if (attr != null)
                    {
                        string propName = prop.Name;
                        string inVal = attr.In;
                        P(propName + " : " + inVal);
                        _dict.Add(propName, inVal);
                    }
                   
                }
            }

            return _dict;
        }
    }


    //Class that is flatten from the json
    public class ConvertTest
    {
        [JsonConvert(In = "uuid,intid")] //Test with multiple attributes from json
        public string Id { get; set; }

        [JsonConvert(In = "amount")]
        public int Value { get; set; }

        [JsonConvert(In = "customer.address")] //Test with deep level item
        public string CustomerAdress { get; set; }
    }
}
