﻿using System;
using System.Collections.Generic;
using Azure.Queues.Process;
using Common.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rules = Azure.Queues.Process.RuleProcessor;
using Common.Enums;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Linq;
using System.Threading.Tasks;
using System.Diagnostics;

namespace Businessrules.Tests
{
    /// <summary>
    /// NEED REAL TESTCASES!!!
    /// All assertions should be tested by another developer then the current one "ME" !!!
    /// </summary>
    [TestClass]
    public class BusinessRulesTest
    {
        #region Tests


        [TestMethod()]
        public void ShouldOnlyPassIfSourceIsKerne()
        {
            EventCache();
            Rules.ValidateSource(TestEvents).PassedItems.ForEach(t => {
                Assert.AreEqual(t.Source, Source.Kerne);
            });
        }


        [TestMethod()]
        public void ShouldOnlyPassIfHasBANKREGNR_IKKE_EIKABANKorFIKTIVID_IKKE_I_KERNE()
        {
            EventCache();
            Rules.ItemsToIgnore(TestEvents).PassedItems.ForEach(t => {
                bool evt = t.EventType == EventType.BANKREGNR_IKKE_EIKABANK || t.EventType == EventType.FIKTIVID_IKKE_I_KERNE;
                Assert.IsTrue(evt);
            });
        }


        [TestMethod()]
        public void ShouldPassIfCustomerActiveOrInactive()
        {
            EventCache();
            var events = Rules.ValidateStatus(TestEvents);
            events.PassedItems.ForEach(t => {
                bool pass = events.NotPassedItems.Contains(t) == false; 
                Assert.IsTrue(pass);
            });
        }


        [TestMethod()]
        public void ShouldPassIfCustomerHasRecentActivity()
        {
            EventCache();
            var contacts = CRMCache.Instance().Contacts;
            var events = Rules.ValidateRecentActivities(TestEvents);
            events.PassedItems.ForEach(t => {
                bool pass = events.NotPassedItems.Contains(t) == false;
                Assert.IsTrue(pass);
            });
        }


        [TestMethod()]
        public void ShouldOnlyPassIfSourceIdIsKerne()
        {
            EventCache();
            var events = Rules.ValidateSourceId(TestEvents);
            Rules.ValidateSourceId(TestEvents).PassedItems.ForEach(t =>
            {
                bool pass = events.NotPassedItems.Contains(t) == false;
                Assert.IsTrue(pass);
            });
        }

        [TestMethod()]
        public void ShouldOnlyPassIfBothRulesApplied()
        {
            EventCache();
            Rules.ValidateSource(TestEvents)
                .And(Rules.ValidateBanks(TestEvents)).PassedItems.ForEach(t => {
                Assert.AreEqual(t.Source, Source.Kerne);
            });
        }


        #endregion


        #region Mocks


        static List<KundeRegisterEvent> TestEvents = GenerateEvents().Take(10).ToList();


        public static List<CrmCustomer> GetCustomerPassed(List<KundeRegisterEvent> items)
        {
            var contacts = CRMCache.Instance().Contacts;
            var events = Rules.ValidateRecentActivities(TestEvents);

            var valid = new List<CrmCustomer>();
            items.ForEach(p => {
                var cust = contacts.Where(c => c.CustomerNumber == p.CustomerNumber).First();
                if (cust != null)
                    valid.Add(cust);
            });

            return valid;
        }


        /// <summary>
        /// Needed to create BUs and customers, accounts etc.
        /// </summary>
        public void EventCache()
        {
            CRMCache.Instance(TestEvents);
        }


        public static List<KundeRegisterEvent> GenerateEvents()
        {
            var list = new List<KundeRegisterEvent>();
            var doc = new XmlDocument();

            var loc = System.IO.Path.GetDirectoryName("./");
            Debug.WriteLine(loc);
            doc.Load(@"D:\Rickard\Azure\Azure.Functions.Eventprocessor\Tests\BusinessRules.Test\KundeRegisterEvents.xml");

            foreach (XmlNode node in doc.DocumentElement.ChildNodes)
            {
                var o = ConvertNode<KundeRegisterEvent>(node);
                list.Add(o);
            }

            return list;
        }


        private static T ConvertNode<T>(XmlNode node) where T : class
        {
            var stm = new MemoryStream();
            var stw = new StreamWriter(stm);

            stw.Write(node.OuterXml);
            stw.Flush();
            stm.Position = 0;

            var ser = new XmlSerializer(typeof(T));
            T result = (ser.Deserialize(stm) as T);

            return result;
        }

        #endregion

    }
}
