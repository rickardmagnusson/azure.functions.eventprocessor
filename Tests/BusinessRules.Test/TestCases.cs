﻿using Common.Entities;
using Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Businessrules.Tests
{
    /// <summary>
    /// A list of fake customers.
    /// These customer must be connected to the ones in the xml file.
    /// </summary>
    public class TestCases
    {
        /// <summary>
        /// Get a list of fake customer that could be run thru the tests. 
        /// </summary>
        /// <param name="ssn"></param>
        /// <returns></returns>
        public IEnumerable<CrmCustomer> GetCustomerBySSN(string ssn)
        {
            //Need fake, but connected users... 
            var customerlist = new List<CrmCustomer> {

                new CrmCustomer{
                    contactid = "",
                    CustomerNumber = "09107334707",
                    FirstName = "",
                    LastName ="",
                    BankId = Guid.NewGuid(),
                    territorycode = "",
                    Source = TerritoryCode.Kjerne,
                    owningbusinessunit = "4730",
                    SourceId = "5011"
                },
                new CrmCustomer
                {
                    contactid = "",
                    CustomerNumber = "05089742643",
                    FirstName = "Jonas",
                    LastName = "Figenschau",
                    BankId = Guid.NewGuid(),
                    territorycode = "",
                    Source = TerritoryCode.Kjerne,
                     owningbusinessunit = "4730"
                }
            };

            return customerlist.Where(c=> c.CustomerNumber.Equals(ssn));
        }
    }
}
